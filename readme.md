# Shooting Activity #

A webapp for keeping track of one's activity at the shooting range.

It was created as an alternative to paper logs in Swedish pistol clubs.

## Requirements ##

## Installation ##

- git clone
- run composer install
- make your http root the /public/ directory.
- copy .env.dist to .env
- edit the new .env file if you wish
- visit your site, and you should be redirected to the install steps
