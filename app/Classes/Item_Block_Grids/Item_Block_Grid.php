<?php

namespace App\Classes\Item_Block_Grids;

/**
	@brief		Generic class for handling things that are to be displayed in a block grid.
	@since		2018-11-27 16:37:15
**/
abstract class Item_Block_Grid
	extends \App\Classes\Collections\Collection
{
	/**
		@brief		Add a new action.
		@since		2018-11-27 16:37:15
	**/
	public function add( $item )
	{
		$item = array_merge( [
			'class' => '',
			'generic_model' => null,
			'html' => '',
			'icon' => '',
			'route' => '',
			'sort_order' => 50,
			'text' => '',
			'url' => '',
		], $item );

		$item = (object) $item;

		if ( $item->generic_model )
		{
			$item->icon = $item->generic_model::icon();
			$item->route = $item->generic_model::routes()->index;
			$item->text = $item->generic_model::labels()->Plural;
		}

		if ( $item->route != '' )
			$item->url = route( $item->route );

		$this->append( $item );
		return $this;
	}

	/**
		@brief		Return the css class of this container.
		@since		2018-11-27 18:13:34
	**/
	public function get_css_class()
	{
		return 'item_block_grid';
	}

	/**
		@brief		Return the view we use for items.
		@since		2018-11-27 17:06:08
	**/
	public abstract function get_item_view();

	/**
		@brief		Return the output string containing the items.
		@since		2018-11-22 22:28:53
	**/
	public function get_output()
	{
		if ( $this->sort_items() )
		{
			$this->items = $this->sortBy( function( $item )
			{
				return $item->sort_order . '_' . $item->text;
			} );
		}

		$items = '';
		foreach( $this->items as $item )
			$items .= view( $this->get_item_view(), (array)$item );

		return view( $this->get_view(),
		[
			'class' => $this->get_css_class(),
			'items' => $items,
		] );
	}

	/**
		@brief		Return the name of the view this component is using.
		@since		2018-11-27 16:56:48
	**/
	public function get_view()
	{
		return 'components.item_block_grids.container';
	}

	/**
		@brief		Sort the items?
		@since		2018-11-27 17:09:41
	**/
	public function sort_items()
	{
		return true;
	}

	/**
		@brief		Output the whole div.
		@since		2018-11-22 21:55:45
	**/
	public function output()
	{
		echo $this->get_output();
	}
}
