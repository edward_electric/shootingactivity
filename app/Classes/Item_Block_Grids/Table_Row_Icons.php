<?php

namespace App\Classes\Item_Block_Grids;

/**
	@brief		Table row icons.
	@since		2018-11-27 17:29:30
**/
class Table_Row_Icons
	extends Item_Block_Grid
{
	/**
		@brief		Insert default values for various icons.
		@since		2018-11-27 18:41:44
	**/
	public function add( $data )
	{
		switch( $data[ 'action' ] )
		{
			case 'delete':
				$data[ 'icon' ] = 'fa-solid fa-times';
				$data[ 'sort_order' ] = 75;
			break;
			case 'edit':
				$data[ 'icon' ] = 'fa-solid fa-edit';
				$data[ 'sort_order' ] = 25;
			break;
			case 'login':
				$data[ 'icon' ] = 'fa-solid fa-sign-in-alt';
			break;
			case 'restore':
				$data[ 'icon' ] = 'fa-solid fa-redo-alt';
			break;
		}
		return parent::add( $data );
	}
	/**
		@brief		Return the view we use for items.
		@since		2018-11-27 17:06:08
	**/
	public function get_item_view()
	{
		return 'components.item_block_grids.table_row_icons.action';
	}
}
