<?php

namespace App\Classes\Item_Block_Grids;

/**
	@brief		Actions in a grid.
	@since		2018-11-27 16:55:20
**/
class Action_Icons
	extends Item_Block_Grid
{
	/**
		@brief		Return the css class of this container.
		@since		2018-11-27 18:13:34
	**/
	public function get_css_class()
	{
		return 'action_icons';
	}

	/**
		@brief		Return the view we use for items.
		@since		2018-11-27 17:06:08
	**/
	public function get_item_view()
	{
		return 'components.item_block_grids.action_icons.action';
	}
}
