<?php

namespace App\Classes\Collections;

/**
	@brief		Convenience class so we don't have to call the SDK class directly.
	@since		2018-04-12 09:53:20
**/
class Collection
	extends \Illuminate\Database\Eloquent\Collection
{
	public function append( $item )
	{
		$this->items[] = $item;
		return $this;
	}

	/**
		@brief		Return a collection using this key.
		@details	Think of this method as recursive collections. If the key is not set, a new collection is created and returned.
		@since		2015-02-09 13:17:44
	**/
	public function collection( $key )
	{
		if ( ! $this->has( $key ) )
			$this->set( $key, new static() );
		return $this->get( $key );
	}

	/**
		@brief		Import this array.
		@since		2018-07-08 09:49:39
	**/
	public function import_array( $array )
	{
		$this->items = $array;
	}

	public function set( $key, $value )
	{
		return $this->put( $key, $value );
	}
}