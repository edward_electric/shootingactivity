<?php

namespace App\Classes\Collections;

/**
	@brief		A collection for models that have names.
	@since		2018-11-22 15:23:48
**/
class Named_Collection
	extends Collection
{
	/**
		@brief		Constructor. Ensure the items are based on model ID.
		@since		2018-11-22 15:35:17
	**/
	public function __construct( $items = [] )
	{
		foreach( $items as $item )
		{
			if ( isset( $item->id ) )
				$this->items[ $item->id ] = $item;
			else
				$this->items []= $item;
		}
	}

	/**
		@brief		Return the collection as an array for for a Plainview select options.
		@since		2018-11-22 15:25:10
	**/
	public function as_options()
	{
		$r = [];
		foreach( $this as $item )
			$r[ $item->id ] = $item->name;
		return $r;
	}

	/**
		@brief		Return this collection as ordered by this key.
		@since		2020-02-23 18:18:19
	**/
	public function order_by( $key )
	{
		return $this->sortBy( function( $item ) use ( $key )
		{
			return $item->$key;
		} );
	}
}
