<?php

namespace App\Classes\Statistics;

use App\Models\Activities\Activity;
use App\Models\Sites\Site;
use App\Models\Users\User;

/**
	@brief		The main statistics helper.
	@details	We use a collection in order to more easily save properties.
	@since		2018-11-28 11:08:57
**/
class Statistics
	extends \App\Classes\Collections\Collection
{
	/**
		@brief		The model we are fetching stats from.
		@detail		The common stats functions fetch the data from here, instead of $user or $site.
		@since		2018-11-30 00:09:28
	**/
	public $model;

	/**
		@brief		Generate new users stats.
		@since		2018-11-30 00:09:54
	**/
	public $new_users = false;

	/**
		@brief		Has the site enabled scoring?
		@since		2018-11-30 00:09:40
	**/
	public $scoring = false;

	/**
		@brief		The site we are working on.
		@since		2018-12-05 09:12:10
	**/
	public $site = null;

	/**
		@brief		The user we are working on.
		@since		2018-12-05 09:12:10
	**/
	public $user = null;

	/**
		@brief		The year for which to collect stats.
		@since		2018-11-30 00:09:16
	**/
	public $year;

	/**
		@brief		Add this activity to the statistics for the site and the user.
		@since		2018-11-28 11:13:48
	**/
	public function add_activity( Activity $activity )
	{
		$time = $activity->created_at->timestamp;

		app()->log()->info( 'Adding activity to statistics: %s', $activity );

		$score_count = 0;
		$score_total = 0;
		if ( $activity->user->site->has_enabled( 'scoring' ) )
		{
			// Sum the scores.
			$strings = $activity->strings();
			$score_count = count( $strings );
			$score_total = $strings->get_sum();
		}

		// We can't add an activity without a gun.
		// This is for mass imported activities that are incomplete.
		if ( ! $activity->gun )
			return;

		$owners = [
			$activity->category,
			$activity->gun,
			$activity->gun->caliber,
			$activity->gun->group,
			$activity->user,
			$activity->user->site,
		];

		// General site stats.
		foreach( $owners as $owner )
		{
			if ( $owner === null )
				continue;
			$owner->increase_meta( 'stats_ammo',							$activity->ammo );		// Ammo count.
			$owner->increase_meta( 'stats_ammo_' . date( 'Y', $time ),		$activity->ammo );		// Activity count for year
			$owner->increase_meta( 'stats_ammo_' . date( 'Y_m', $time ),	$activity->ammo );		// Activity count for month
			$owner->increase_meta( 'stats_activities' );										// Activity count
			$owner->increase_meta( 'stats_activities_' . date( 'Y', $time ) );					// Activity count for year
			$owner->increase_meta( 'stats_activities_' . date( 'Y_m', $time ) );				// Activity count for month

			if ( $score_count > 0 )
			{
				$owner->increase_meta( 'stats_scores_count',							$score_count );
				$owner->increase_meta( 'stats_scores_count_' . date( 'Y', $time ),		$score_count );
				$owner->increase_meta( 'stats_scores_count_' . date( 'Y_m', $time ),	$score_count );
				$owner->increase_meta( 'stats_scores_total',							$score_total );
				$owner->increase_meta( 'stats_scores_total_' . date( 'Y', $time ),		$score_total );
				$owner->increase_meta( 'stats_scores_total_' . date( 'Y_m', $time ),	$score_total );
			}
		}

		// Stats that belong in the user's meta.
		$user_stats = [
			$activity->category,
			$activity->gun->caliber,
			$activity->gun->group,
		];
		$user = $activity->user;
		foreach( $user_stats as $item )
		{
			if ( ! $item )
				continue;
			$item_key = sprintf( '%s_%s', $item->model_key(), $item->id );

			$year = date( 'Y', $time );
			$years = $user->get_meta( 'stats_years', [] );
			// Storing arrays in meta turns them into objects.
			$years = (array) $years;
			$years[ $year ] = $year;
			ksort( $years );
			$user->set_meta( 'stats_years', $years );

			$user->increase_meta( 'stats_ammo_' . $item_key,								$activity->ammo );		// Ammo count.
			$user->increase_meta( 'stats_ammo_' . $item_key . '_' . $year,		$activity->ammo );		// Activity count for year
			$user->increase_meta( 'stats_ammo_' . $item_key . '_' . date( 'Y_m', $time ),	$activity->ammo );		// Activity count for month
			$user->increase_meta( 'stats_activities_' . $item_key );										// Activity count
			$user->increase_meta( 'stats_activities_' . $item_key . '_' . $year );					// Activity count for year
			$user->increase_meta( 'stats_activities_' . $item_key . '_' . date( 'Y_m', $time ) );				// Activity count for month

			if ( $score_count > 0 )
			{
				$user->increase_meta( 'stats_scores_count_' . $item_key,								$score_count );
				$user->increase_meta( 'stats_scores_count_' . $item_key . '_' . $year,		$score_count );
				$user->increase_meta( 'stats_scores_count_' . $item_key . '_' . date( 'Y_m', $time ),	$score_count );
				$user->increase_meta( 'stats_scores_total_' . $item_key,								$score_total );
				$user->increase_meta( 'stats_scores_total_' . $item_key . '_' . $year,		$score_total );
				$user->increase_meta( 'stats_scores_total_' . $item_key . '_' . date( 'Y_m', $time ),	$score_total );
			}
		}
	}

	/**
		@brief		Queue the adding of this activity.
		@since		2018-12-05 11:56:39
	**/
	public function add_activity_later( Activity $activity )
	{
		\App\Jobs\Statistics\Add::dispatch( $activity );
	}

	/**
		@brief		Compile the yearly sums.
		@since		2018-12-04 17:03:19
	**/
	public function compile_year( $collection, $year )
	{
		$year_key = ( $year == '' ? '' : '_' . $year );
		// Totals.
		$collection->set( 'activities', $this->model->get_meta( 'stats_activities' . $year_key ) );
		$collection->set( 'ammo', $this->model->get_meta( 'stats_ammo' . $year_key ) );
		if ( $this->new_users )
		{
			if ( $year != '' )
			{
				$collection->set( 'new_users',
					$this->model->users()
					->whereYear( 'created_at', '=', $year )
					->count()
				);
				$collection->set( 'total_users',
					$this->model
					->users()
					->active()
					->whereYear( 'created_at', '=', $year )
					->count()
				);
			}
			else
			{
				$collection->set( 'new_users', 'n/a' );
				$collection->set( 'total_users',
					$this->model
					->users()
					->active()
					->count()
				);
			}
		}
		if ( $this->scoring )
		{
			$count = $this->model->get_meta( 'stats_scores_count' . $year_key );
			$total = $this->model->get_meta( 'stats_scores_total' . $year_key );
			$average = $count > 0 ? round( ( $total / $count ), 2 ) : '';
			$collection->set( 'scores_count', $count );
			$collection->set( 'scores_average', $average );
		}
	}

	/**
		@brief		Compile the stats for the year.
		@since		2018-11-30 00:06:39
	**/
	public function compile_stats()
	{
		// Related items
		foreach( $this->related_items as $type => $label )
		{
			// Only bother with items if they have had any activity this year.
			$related_items = $this->model->$type()->orderBy( 'name' )->get();
			foreach( $related_items as $item_index => $item )
				if ( $item->get_meta( 'stats_activities' ) < 1 )
					$related_items->forget( $item_index );

			$this->collection( 'related_items' )
				->set( $type, $related_items  );
			$this->collection( 'related_item_labels' )->set( $type, $label );
		}

		// If this is a user, look for the user related items.
		foreach( $this->collection( 'user_related_items' ) as $type => $related_items )
		{
			// Only bother with items if they have had any activity this year.
			foreach( $related_items as $item_index => $item )
			{
				$item_key = sprintf( '%s_%s', $item->model_key(), $item->id );
				if ( $this->model->get_meta( 'stats_activities_' . $item_key ) < 1 )
					$related_items->forget( $item_index );
			}

			// Save this in both related_items and related_user_items so we don't have two loops.
			$this->collection( 'related_items' )
				->set( $type, $related_items );
			$this->collection( 'related_user_items' )
				->set( $type, $related_items );
			$this->collection( 'related_item_labels' )->set( $type, $item->labels()->Plural );
		}

		$this->compile_year( $this, '' );

		$years = $this->model->get_meta( 'stats_years', [] );
		$years = (array) $years;
		foreach( $years as $year )
		{
			$activities = $this->model->get_meta( 'stats_activities_' . $year );
			$year_c = $this->collection( 'years' )->collection( $year );
			$this->compile_year( $year_c, $year );
		}

		// Collect the stats for the requested year.
		$year_c = $this->collection( 'years' )
			->collection( $this->year );

		$year_c->set( 'ammo',			$this->model->get_meta( 'stats_ammo_' . $this->year ) );
		$year_c->set( 'activities',		$this->model->get_meta( 'stats_activities_' . $this->year ) );

		if ( $this->new_users )
			$year_c->set( 'new_users',	$this->model->users()->whereYear('created_at', '=', $this->year )->count() );

		if ( $this->scoring )
		{
			$count = $this->model->get_meta( 'stats_scores_count_' . $this->year );
			$total = $this->model->get_meta( 'stats_scores_total_' . $this->year );
			$average = $count > 0 ? round( ( $total / $count ), 2 ) : '';
			$year_c->set( 'scores_count',		$count );
			$year_c->set( 'scores_average',	$average );
		}

		// Put the year summary in for the related items.
		foreach( $this->collection( 'related_items' ) as $item_type => $items )
			foreach( $items as $item_index => $item )
			{
				$item_key = '';
				$meta_owner = $item;		// User related items store their meta in the user.
				// Where do we fetch the meta from?
				if ( $this->collection( 'related_user_items' )->has( $item_type ) )
					if ( $this->collection( 'related_user_items' )->get( $item_type )->has( $item->id ) )
					{
						$item_key = sprintf( '%s_%s_', $item->model_key(), $item->id );
						$meta_owner = $this->model;
					}

				// If this item type has no activities this year, there is no point in displaying it.
				$activities = $meta_owner->get_meta( 'stats_activities_' . $item_key . $this->year );
				if ( ! $activities )
				{
					$items->forget( $item_index );
					continue;
				}

				// This is the yearly amount.
				$item_collection = $year_c->collection( 'related_items' )
					->collection( $item_type )
					->collection( $item->id )
					->set( 'activities', $activities )
					->set( 'ammo', $meta_owner->get_meta( 'stats_ammo_' . $item_key . $this->year ) )
					->set( 'name', $item->name )
					;
				if ( $this->scoring )
				{
					$count = $meta_owner->get_meta( 'stats_scores_count_' . $item_key . $this->year );
					$total = $meta_owner->get_meta( 'stats_scores_total_' . $item_key . $this->year );
					$average = $count > 0 ? round( ( $total / $count ), 2 ) : '';
					$item_collection->set( 'scores_count' . $this->year,		$count );
					$item_collection->set( 'scores_average_' . $this->year,	$average );
				}
			}

		// Collect the stats for each month.
		$months = $year_c->collection( 'months' );
		foreach( range( 1, 12 ) as $month )
		{
			if ( $month < 10 )
				$month = '0' . $month;
			$year_month = sprintf( '_%s_%s', $this->year, $month );
			$month_c = $months->collection( $month );
			$month_c->set( 'ammo',			$this->model->get_meta( 'stats_ammo' . $year_month ) );
			$month_c->set( 'activities',	$this->model->get_meta( 'stats_activities' . $year_month ) );

			if( $this->new_users )
			{
				$month_c->set( 'new_users',			$this->model->users()->whereYear('created_at', '=', $this->year )
													->whereMonth('created_at', '=', $month )
													->count() ?: '' );
			}

			if ( $this->scoring )
			{
				$count = $this->model->get_meta( 'stats_scores_count' . $year_month );
				$total = $this->model->get_meta( 'stats_scores_total' . $year_month );
				$average = $count > 0 ? round( ( $total / $count ), 2 ) : '';
				$month_c->set( 'scores_count',		$count );
				$month_c->set( 'scores_average',		$average );
			}

			foreach( $this->collection( 'related_items' ) as $item_type => $items )
				foreach( $items as $item )
				{

					$item_key = '';
					$meta_owner = $item;		// User related items store their meta in the user.

					// Where do we fetch the meta from?
					if ( $this->collection( 'related_user_items' )->has( $item_type ) )
						if ( $this->collection( 'related_user_items' )->get( $item_type )->has( $item->id ) )
						{
							$item_key = sprintf( '_%s_%s', $item->model_key(), $item->id );
							$meta_owner = $this->model;
						}

					$item_collection = $month_c->collection( 'related_items' )
						->collection( $item_type )
						->collection( $item->id )
						->set( 'activities',	$meta_owner->get_meta( 'stats_activities' . $item_key . $year_month ) )
						->set( 'ammo',			$meta_owner->get_meta( 'stats_ammo' . $item_key . $year_month ) )
						;
					if ( $this->scoring )
					{
						$count = $meta_owner->get_meta( 'stats_scores_count' . $item_key . $year_month );
						$total = $meta_owner->get_meta( 'stats_scores_total' . $item_key . $year_month );
						$average = $count > 0 ? round( ( $total / $count ), 2 ) : '';
						$item_collection->set( 'scores_count',		$count );
						$item_collection->set( 'scores_average',	$average );
					}
				}
		}
	}

	/**
		@brief		Create a CSV file.
		@since		2020-12-03 21:05:31
	**/
	public function create_csv()
	{
		$activities_query = Activity::where( 'user_id', $this->user->id );
		$activities_query->where( 'created_at', '>=', $this->year . '-01-01' );
		$activities_query->where( 'created_at', '<', ( $this->year + 1 ) . '-01-01' );
		$activities_query->orderBy( 'id' );
		$activities_query->with( 'category' );
		$activities_query->with( 'gun' );
		$activities = $activities_query->get();

		$this->title = __( 'ShootingActivity.com statistics for :modelname (:year)', [ 'modelname' => $this->model->name, 'year' => $this->year ] );

		$csv = [];
		$csv []= [
			'date',
			'time',
			'category',
			'gun',
			'shots',
			'series',
			'shot_average',
		];

		$max_series = 0;

		foreach( $activities as $activity )
		{
			$row = [];
			$row []= $activity->created_at->format( 'Y-m-d' );
			$row []= $activity->created_at->format( 'H:i' );
			$row []= $activity->category->name;
			$row []= $activity->gun->name;

			$strings = $activity->strings();
			$row []= $strings->count_shots();
			$row []= $strings->count();
			$row []= $strings->get_average();
			foreach( $strings as $index => $ignore )
			{
				$max_series = max( $max_series, $index );
				$shots = $strings->get_shots( $index );
				$row []= implode( ',', $shots );
			}
			$csv []= $row;
		}

		for( $counter = 0; $counter <= $max_series; $counter++ )
			$csv[ 0 ] []= 'series_' . ( $counter + 1 );

		return $csv;
	}

	/**
		@brief		Create the statistics PDF out of the stats.
		@since		2018-12-04 00:05:07
	**/
	public function create_pdf()
	{
		$this->pdf = new \Mpdf\Mpdf( [
			//'defaultCssFile' => public_path() . '/css/app.css',
			'defaultCssFile' => app_path() . '/../resources/scss/mpdf.css',
			'tempDir' => storage_path() . '/app/pdf',
		] );
		$this->pdf->SetCreator( __(  'ShootingActivity.com' ) );
		$this->title = __( 'ShootingActivity.com statistics for :modelname (:year)', [ 'modelname' => $this->model->name, 'year' => $this->year ] );
		$this->pdf->SetTitle( $this->title );

		$html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';	// UTF8
		$html .= view( 'statistics.pdf',
			[
				'statistics' => $this,
			]
		);
		$html = $this->remove_anchors( $html );
		$this->pdf->WriteHTML( $html );
		return $this;
	}

	/**
		@brief		Flush the statistics of a site.
		@since		2018-12-05 16:53:07
	**/
	public function flush( Site $site )
	{
		foreach( [
			[ $site ],
			$site->categories,
			$site->gun_calibers,
			$site->gun_groups,
			$site->users,
		] as $type )
			foreach( $type as $t )
				$t->meta()
					->where( 'meta_key', 'LIKE', 'stats_%' )
					->delete();
		foreach( $site->users as $user )
			foreach( $user->guns as $gun )
				$gun->meta()
					->where( 'meta_key', 'LIKE', 'stats_%' )
					->delete();

	}

	/**
		@brief		An array of submodels that have meta we can look through.
		@since		2018-11-28 19:19:39
	**/
	public function get_site_related_items()
	{
		return [
			'categories' => \App\Models\Categories\Category::labels()->Plural,
			'gun_calibers' => \App\Models\Guns\Caliber::labels()->Plural,
			'gun_groups' => \App\Models\Guns\Group::labels()->Plural,
		];
	}

	/**
		@brief		Array of submodels for users.
		@since		2018-11-29 23:18:00
	**/
	public function get_user_related_items()
	{
		return [
			'guns' => \App\Models\Guns\Gun::labels()->Plural,
		];
	}

	/**
		@brief		Return a list of months.
		@since		2018-11-28 22:21:32
	**/
	public static function months()
	{
		// The 01 part is important because date() generates zeroes.
		return [
			'01' => __( 'January' ),
			'02' => __( 'February' ),
			'03' => __( 'March' ),
			'04' => __( 'April' ),
			'05' => __( 'May' ),
			'06' => __( 'June' ),
			'07' => __( 'July' ),
			'08' => __( 'August' ),
			'09' => __( 'September' ),
			'10' => __( 'October' ),
			'11' => __( 'November' ),
			'12' => __( 'December' ),
		];
	}

	/**
		@brief		Output the CSV to the browser.
		@since		2020-12-03 21:04:48
	**/
	public function output_csv()
	{
		$csv = $this->create_csv();

        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=\"" . $this->title . '.csv"',
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $callback = function() use( $csv )
       	{
            $file = fopen('php://output', 'w');
            $header = array_shift( $csv );
          		fputcsv( $file, $header );
            foreach( $csv as $row )
            {
            	$output = [];
            	foreach( $header as $index => $text )
            	{
            		$row_data = ( isset( $row[ $index ] ) ? $row[ $index ] : '' );
            		$output[ $text ] = $row_data;
            	}
           		fputcsv( $file, $output );
            }
            fclose($file);
        };
		return response()->stream( $callback, 200, $headers );
	}

	/**
		@brief		Output the PDF to the browser.
		@since		2018-12-04 16:40:28
	**/
	public function output_pdf()
	{
		if ( ! isset( $this->pdf ) )
			$this->create_pdf();
		$this->pdf->Output( $this->title . '.pdf', 'I' );
	}

	/**
		@brief		Regenerate the stats for a site.
		@details	Automatically flushes the stats, else we get double-added stats.
		@see		flush()
		@since		2018-12-05 17:02:52
	**/
	public function regenerate( Site $site )
	{
		$this->flush( $site );
		foreach( $site->users as $user )
			foreach( $user->activities as $activity )
			{
				// Only add moderated activities to the stats.
				if ( $activity->is_unmoderated() )
					continue;
				$this->add_activity_later( $activity );
			}
	}

	/**
		@brief		Convenience method to regenerate the stats using a job.
		@since		2019-01-23 14:03:20
	**/
	public function regenerate_later( Site $site = null )
	{
		if ( ! $site )
			$site = app()->site();
		\App\Jobs\Statistics\Regenerate::dispatch( $site );
	}

	/**
		@brief		Remove the anchors out of the html.
		@details	https://stackoverflow.com/questions/5870201/remove-anchors-from-text
		@since		2018-12-04 00:23:00
	**/
	public function remove_anchors( $html )
	{
		$xml = new \DOMDocument();
		$xml->loadHTML( $html );

		$links = $xml->getElementsByTagName('a');

		//Loop through each <a> tags and replace them by their text content
		for ($i = $links->length - 1; $i >= 0; $i--)
		{
			$linkNode = $links->item($i);
			$lnkText = $linkNode->textContent;
			$newTxtNode = $xml->createTextNode($lnkText);
			$linkNode->parentNode->replaceChild($newTxtNode, $linkNode);
		}
		return $xml->saveHTML();
	}

	/**
		@brief		Load the statistics for the site.
		@since		2018-11-28 17:08:36
	**/
	public function site( Site $site, $year )
	{
		$this->model = $site;
		$this->new_users = true;
		$this->related_items = [
			'categories' => \App\Models\Categories\Category::labels()->Plural,
			'gun_calibers' => \App\Models\Guns\Caliber::labels()->Plural,
			'gun_groups' => \App\Models\Guns\Group::labels()->Plural,
		];
		$this->scoring = $site->has_enabled( 'scoring' );
		$this->site = $site;
		$this->year = $year;

		$this->compile_stats();

		return $this;
	}

	/**
		@brief		Generate user stats.
		@since		2018-11-29 22:28:38
	**/
	public function user( User $user, $year )
	{
		$this->model = $user;
		$this->related_items = [
			'guns' => \App\Models\Guns\Gun::labels()->Plural,
		];
		$this->scoring = $user->site->has_enabled( 'scoring' );
		$this->user = $user;

		$this->collection( 'user_related_items' )
			->set( 'categories', \App\Models\Categories\Category::orderBy( 'name' )->get() )
			->set( 'gun_calibers', \App\Models\Guns\Caliber::orderBy( 'name' )->get() )
			->set( 'gun_groups', \App\Models\Guns\Group::orderBy( 'name' )->get() );
			;
		$this->year = $year;

		$this->compile_stats();

		return $this;
	}
}
