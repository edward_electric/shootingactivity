<?php

namespace App\Classes;

/**
	@brief		Mail class using the SDK and the complete PHPmailer namespace.
	@since		2018-04-13 08:16:40
**/
class Mail
	extends \PHPMailer\PHPMailer\PHPMailer
{
	use \plainview\sdk\mail\mail_trait;

	/**
		@brief		Create a mail object.
		@since		2018-04-13 08:25:48
	**/
	public static function create()
	{
		$mail = new static();
		$mail->CharSet = 'UTF-8';
		return $mail;
	}

	/**
		@brief		Wrapper function to return the mime type of a file.
		@since		2018-04-13 08:51:50
	**/
	public static function mime_content_type( $filename )
	{
		if ( strpos( $filename, '.ods' ) !== false )
			return 'application/vnd.oasis.opendocument.spreadsheet';
		return mime_content_type( $filename );
	}

}
