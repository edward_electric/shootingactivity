<?php

namespace App\Classes;

class App
	extends \Illuminate\Foundation\Application
{
	/**
		@brief		Return the instance of action icons.
		@since		2018-11-22 21:53:03
	**/
	public function action_icons( $id = 'default' )
	{
		// Store the action icons in the view object, since app() gets its variables cleared for some reason.
		$key = '__action_icons_' . $id;
		if ( ! isset( view()->$key ) )
		{
			view()->$key = new Item_Block_Grids\Action_Icons();
		}
		return view()->$key;
	}

	/**
		@brief		Return the alerts instance.
		@since		2018-11-28 14:40:20
	**/
	public function alerts()
	{
		$r = new \App\Classes\Alerts();
		$r->load_from_session();
		return $r;
	}

    /**
    	@brief		Return a Plainview Form object.
    	@since		2018-11-13 13:09:48
    **/
    public static function form()
    {
    	$form = new \plainview\sdk\form2\bootstrap\bootstrap5\form();
		$form->enctype( 'multipart/form-data' );
		return $form;
    }

	/**
		@brief		Return the logger.
		@since		2018-11-16 16:57:59
	**/
	public function log()
	{
		return new Logger();
	}

	/**
		@brief		Reload a config file from disk.
		@since		2018-11-14 12:33:07
	**/
	public function reload_config( $key )
	{
		$file = sprintf( '%s/%s.php',
			config_path(),
			$key
		);
		app( 'config' )->set( $key, require $file );
	}

	/**
		@brief		Modify the .env file with new key-value pairs.
		@param		array	$values		New key-values to set.
		@since		2018-11-16 15:34:09
	**/
	public function set_env( $values )
	{
		$filename = app_path( '../.env' );

		$contents = file_get_contents( $filename );

		foreach( $values as $key => $value )
		{
			$assembled_key = sprintf( '%s=', $key );
			$assembled_key_value = sprintf( '%s="%s"', $key, $value );

			$pattern = preg_quote( $assembled_key, '/' );

			if ( preg_match( '/' . $pattern . '/', $contents, $matches ) )
			{
				// Replace in place.
				$escaped = sprintf( '/%s="?%s"?/',
					preg_quote( $key ),
					str_replace( '/', '\/', preg_quote( env( $key ) ) )		// Replace the slashes in URLs. Strange that preg_quote doesn't handle it.
				);
				$contents = preg_replace( $escaped, $assembled_key_value, $contents );
			}
			else
			{
				// Append it to the end.
				$contents .= $assembled_key_value . "\n";
			}
		}

		// Save the new file.
		$contents = file_put_contents( $filename, $contents );
	}

	/**
		@brief		Return the currently logged in user's site.
		@since		2018-11-17 21:03:24
	**/
	public function site()
	{
		if ( $this->user() )
			return $this->user()->site;
		else
			return new \App\Models\Sites\Site;
	}

	/**
		@brief		Return an instance of the statistics helper.
		@since		2018-11-28 11:11:47
	**/
	public function statistics()
	{
		return new \App\Classes\Statistics\Statistics();
	}

	/**
		@brief		Return the current user.
		@since		2018-11-17 21:04:24
	**/
	public function user()
	{
		return \Auth::user();
	}

	/**
		@brief		Return an instance of the table row actions.
		@since		2018-11-27 17:28:40
	**/
	public function table_row_icons( $id = 'default' )
	{
		// Store the icons in the view object, since app() gets its variables cleared for some reason.
		$key = '__table_row_icons_' . $id;
		if ( ! isset( view()->$key ) )
		{
			view()->$key = new Item_Block_Grids\Table_Row_Icons();
		}
		return view()->$key;
	}
}
