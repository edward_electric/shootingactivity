<?php

namespace App\Jobs\Activities;

use \App\Models\Activities\Activity;
use \App\Models\Users\User;

/**
	@brief		Reorder the activities for this user.
	@since		2023-03-08 19:03:54
**/
class Reorder_For_User
	extends \App\Jobs\Job
{
	/**
		@brief		The user for which to reorder the activities.
		@since		2023-03-08 19:04:05
	**/
	public $user;

    /**
     * Create a new job instance.
     */
    public function __construct( User $user )
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
		Activity::reorder_for_user( $this->user->id );
    }
}
