<?php

namespace App\Jobs\Statistics;

use \App\Models\Activities\Activity;

/**
	@brief		Add an activity to the statistics.
	@since		2018-12-05 11:16:56
**/
class Add
	extends \App\Jobs\Job
{
	/**
		@brief		The activity to add.
		@since		2018-12-05 11:23:23
	**/
	public $activity;

	/**
		@brief		Construct.
		@since		2018-12-05 11:18:12
	**/
	public function __construct( Activity $activity )
	{
		$this->activity = $activity;
	}

	/**
		@brief		Handle the job.
		@since		2018-12-05 11:53:19
	**/
	public function handle()
	{
		\Artisan::call( 'app:statistics:add', [ 'activity_id' => $this->activity->id ] );
	}
}
