<?php

namespace App\Jobs\Statistics;

use \App\Models\Sites\Site;

/**
	@brief		Regenerate the stats of a site.
	@since		2019-01-23 14:00:52
**/
class Regenerate extends \App\Jobs\Job
{
	/**
		@brief		The site for which to regenerate the stats.
		@since		2019-01-23 14:00:52
	**/
	public $site;

	/**
		@brief		Construct.
		@since		2019-01-23 14:00:52
	**/
	public function __construct( Site $site )
	{
		$this->site = $site;
	}

	/**
		@brief		Handle the job.
		@since		2019-01-23 14:00:52
	**/
	public function handle()
	{
		\Artisan::call( 'app:statistics:regenerate', [ 'site_id' => $this->site->id ] );
	}
}
