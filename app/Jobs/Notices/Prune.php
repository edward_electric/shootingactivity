<?php

namespace App\Jobs\Notices;

use App\Models\Sites\Site;

/**
	@brief		Prune old notices from all sites.
	@since		2018-12-23 21:37:08
**/
class Prune
	extends \App\Jobs\Job
{
	/**
		@brief		Handle the job.
		@since		2018-12-05 11:53:19
	**/
	public function handle()
	{
		\Artisan::call( 'app:notices:prune' );
	}
}
