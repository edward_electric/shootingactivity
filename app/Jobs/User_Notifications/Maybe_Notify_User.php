<?php

namespace App\Jobs\User_Notifications;

use App\Models\Model;
use App\Models\Users\User;

/**
	@brief		Maybe notify the user of this new model.
	@since		2018-12-23 23:05:05
**/
class Maybe_Notify_User
	extends \App\Jobs\Job
{
	/**
		@brief		The model that caused the notification.
		@since		2018-12-09 19:44:44
	**/
	public $model;

	/**
		@brief		The moderator to notify.
		@since		2018-12-09 19:44:44
	**/
	public $user;

	/**
		@brief		Construct.
		@since		2018-12-05 11:18:12
	**/
	public function __construct( Model $model, User $user )
	{
		$this->model = $model;
		$this->user = $user;
	}

	/**
		@brief		Handle the job.
		@since		2018-12-05 11:53:19
	**/
	public function handle()
	{
		$this->model->maybe_notify_user( $this->user );
	}
}
