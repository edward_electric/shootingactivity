<?php

namespace App\Models\Traits;

use App\Models\Users\User;
use Illuminate\Support\Facades\Mail;

/**
	@brief		This model sends notifications to users.
	@since		2018-12-23 22:45:59
**/
trait User_Notification
{
	/**
		@brief		Return the notification data unique to this model.
		@since		2018-12-23 22:46:26
	**/
	public static function get_user_notification_data()
	{
		return (object)[
			'mail_subject' => 'mail.XXX.notification.subject',
			'mail_text' => 'mail.XXX.notification.text',
			'user_notification_enabled_key' => 'email_new_XXX',
			'user_notification_key' => 'email_new_XXX_notified',
		];
	}

	/**
		@brief		Clear the notification key.
		@since		2018-12-09 20:58:02
	**/
	public static function clear_user_notification_key( User $user )
	{
		$data = static::get_user_notification_data();
		$user->delete_meta( $data->user_notification_key );
	}

	/**
		@brief		Return all the users that can be notified.
		@details	Override this.
		@since		2018-12-23 23:01:19
	**/
	public function get_notifiable_users()
	{
		// Get only the active users.
		return $this->user->site->users()->active()->get();
	}

	/**
		@brief		Get an array of notification options used in the user's e-mail settings.
		@since		2018-12-25 16:08:41
	**/
	public static function get_notification_options()
	{
		$opts = [
			'always' => __( 'Always' ),
			'never' => __( 'Never' ),
			'once_per_login' => __( 'Once between logins' ),
		];
		asort( $opts );
		return $opts;
	}

	/**
		@brief		Should this user be notified?
		@see		notify_user()
		@since		2018-12-23 22:49:15
	**/
	public function should_notify_user()
	{
		return true;
	}

	/**
		@brief		Internal function to decide whether to notify the user.
		@see		should_notify_user()
		@since		2018-12-23 22:56:04
	**/
	public function _should_notify_user( $user )
	{
		$data = static::get_user_notification_data();

		switch( $user->get_meta( $data->user_notification_enabled_key ) )
		{
			case 'once_per_login':
				// If the user already has been notified, leave him alone.
				if ( static::user_notified( $user ) )
					return false;
			break;
			case 'never':
				return false;
			break;
		}

		if ( ! $this->should_notify_user( $user ) )
			return false;

		return true;
	}

	/**
		@brief		Notify the user of the new item.
		@since		2018-12-23 22:57:12
	**/
	public function maybe_notify_user( User $user )
	{
		if ( ! $this->_should_notify_user( $user ) )
			return app()->log()->debug( '%s no need to send user notification to %s %s', get_class( $this ), $user->name, $user->id );

		app()->log()->debug( '%s: notifying user %s %s', get_class( $this ), $user->name, $user->id );

		$this->notify_user( $user );
	}

	/**
		@brief		Send the notification to this user.
		@since		2018-12-25 00:01:06
	**/
	public function notify_user( User $user )
	{
		$data = static::get_user_notification_data();

		$mail = new \App\Mail\User_Notifications\Notification( $this, $user );

		Mail::to( $user->email )->queue( $mail );

		app()->log()->info( '%s sent user notification to %s %s', get_class( $this ), $user->name, $user->id );

		$user->set_meta( $data->user_notification_key, time() );
	}

	/**
		@brief		Notify all users of this item.
		@since		2018-12-23 22:57:48
	**/
	public function notify_users()
	{
		$data = static::get_user_notification_data();

		$users = static::get_notifiable_users();

		app()->log()->debug( '%s %s users found during notify_users().', get_class( $this ), count( $users ) );
		foreach( $users as $user )
			\App\Jobs\User_Notifications\Maybe_Notify_User::dispatch( $this, $user );
	}

	/**
		@brief		Was this user notified already?
		@since		2018-12-09 22:27:18
	**/
	public static function user_notified( User $user )
	{
		$data = static::get_user_notification_data();
		return ( $user->get_meta( $data->user_notification_key ) > 0 );
	}
}
