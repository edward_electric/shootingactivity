<?php

namespace App\Models\Traits;

use App\Classes\Collections\Named_Collection;

/**
	@brief		Methods that help integration with Plainview Forms class.
	@since		2018-11-17 20:47:24
**/
trait Forms_Helper
{
    /**
    	@brief		Return results as a named collection, in order to assist in retrieving things by ID.
    	@since		2018-12-02 19:37:50
    **/
    public function newCollection(array $models = [])
    {
        return new Named_Collection($models);
    }
}
