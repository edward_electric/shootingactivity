<?php

namespace App\Models\Traits;

/**
	@brief		Functions for handling models that can be archived.
	@since		2018-11-18 21:20:09
**/
trait Archivable
{
    /**
    	@brief		Mark the model as archived.
    	@since		2018-11-17 23:58:59
    **/
    public function archive()
    {
    	$this->archived = true;
    }

    /**
    	@brief		Is this model archived?
    	@since		2020-02-23 18:28:36
    **/
    public function is_archived()
    {
    	return $this->archived;
    }

    /**
    	@brief		Mark this model as active again.
    	@since		2018-11-18 00:29:21
    **/
    public function restore()
    {
    	$this->archived = null;
    }

    /**
    	@brief		Return the active, non-archived models.
    	@since		2018-11-17 22:10:45
    **/
    public function scopeActive( $query )
    {
    	if ( $this->is_archivable() )
    		return $query->whereNull( 'archived' );
    	else
    		return $query;
    }

    /**
    	@brief		The archived models.
    	@since		2018-11-18 00:02:00
    **/
    public function scopeArchived( $query )
    {
    	return $query->whereNotNull( 'archived' );
    }
}
