<?php

namespace App\Models\Traits;

/**
	@brief		Helper for interacting with the object's meta table.
	@detail		Add the meta_owner function.
	@since		2018-11-14 22:26:14
**/
trait Meta
{
	/**
		@brief		Delete the meta keypair.
		@since		2018-12-05 00:18:45
	**/
	public function delete_meta( $key )
	{
    	foreach( $this->meta as $meta )
    		if ( $meta->meta_key == $key )
    			$meta->delete();
    	return $this;
	}

    /**
    	@brief		Retrieve a meta key, if available.
    	@since		2018-11-14 22:10:01
    **/
    public function get_meta( $key, $default = null )
    {
    	foreach( $this->meta as $meta )
    		if ( $meta->meta_key == $key )
    			return json_decode( $meta->meta_value );

    	// Not found. Try a default value.
    	return static::get_meta_default( $key, $default );
    }

    /**
    	@brief		Get the default value of a meta key.
    	@details	Override this if your model should return default values other than null.
    	@since		2018-11-16 23:30:00
    **/
    public function get_meta_default( $key, $default = null )
    {
    	return $default;
    }

    /**
    	@brief		Convenience method to increase the value of a meta key.
    	@since		2018-11-27 15:05:31
    **/
    public function increase_meta( $key, $increment = 1 )
    {
    	$value = $this->get_meta( $key, 0 );
    	$value += $increment;
    	$this->set_meta( $key, $value );
    }

    /**
    	@brief		All of the meta of this site.
    	@since		2018-11-14 21:50:34
    **/
    public function meta()
    {
        return $this->morphToMany( 'App\Models\Meta', 'object', 'metas', 'object_id' );
    }

    /**
    	@brief		Set a meta key.
    	@since		2018-11-14 22:10:51
    **/
    public function set_meta( $key, $value )
    {
   		$value = json_encode( $value );

    	foreach( $this->meta as $meta )
    		if ( $meta->meta_key == $key )
    		{
    			$meta->meta_value = $value;
    			$meta->save();
    			return $this;
    		}

    	// The below saves us forcing all of the classes to supply us with a new_meta() function.
    	$class = get_class( $this->meta()->getRelated() );
    	// Create a new meta instance.
    	$meta = new $class();

    	$owner = $this->meta_owner();

    	$meta->meta_key = $key;
    	$meta->meta_value = $value;
    	$meta->save();
    	$meta->$owner()->attach( $this );

    	// We need to refresh the meta collection.
    	$this->load( 'meta' );

    	return $this;
    }
}
