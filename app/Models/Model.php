<?php

namespace App\Models;

//use GeneaLabs\LaravelModelCaching\Traits\Cachable;

/**
	@brief		Base model class.
	@since		2018-11-13 15:53:40
**/
class Model
	extends \Illuminate\Database\Eloquent\Model
{
    //use Cachable;

    /**
    	@brief		Return the name of the table this model uses.
    	@details	Why is this not standard??
    	@since		2018-12-27 12:12:36
    **/
	public static function getTableName()
	{
		return with( new static() )->getTable();
	}
}
