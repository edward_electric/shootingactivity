<?php

namespace App\Models\Alerts;

/**
	@brief		Info.
	@since		2018-07-12 22:52:55
**/
class Info
	extends Alert
{
	/**
		@brief		Return the type of alert this is.
		@since		2018-07-12 22:54:25
	**/
	public function get_type()
	{
		return 'info';
	}
}
