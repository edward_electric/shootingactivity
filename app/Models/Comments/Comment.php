<?php

namespace App\Models\Comments;

use App\Models\Notices\Notice;

/**
	@brief		A comment that a user can write somewhere.
	@since		2018-12-23 16:21:30
**/
class Comment
	extends	\App\Models\Model
{
	protected $table = 'comments';

	/**
		@brief		Delete this comment.
		@since		2018-12-23 20:17:15
	**/
	public function delete()
	{
		foreach( $this->replies as $reply )
			$reply->delete();
		// Delete notices associated to this comment.
		Notice::where( 'comment_id', $this->id )->delete();
		// Delete the parent.
		parent::delete();
	}

	/**
		@brief		Extract the first few words of the comment.
		@since		2018-12-23 19:57:25
	**/
	public function first_words()
	{
		$count = 5;
		preg_match("/(?:\w+(?:\W+|$)){0,$count}/", $this->text, $matches );
		return trim( $matches[ 0 ] );
	}

	/**
		@brief		Does this comment have a parent?
		@since		2018-12-23 16:39:45
	**/
	public function has_parent()
	{
		return $this->parent_id > 0;
	}

	/**
		@brief		Return the text in html format.
		@since		2018-12-23 17:49:36
	**/
	public function html_text()
	{
		return \plainview\sdk\base::wpautop( htmlspecialchars( $this->text ) );
	}

	/**
		@brief		Return the last comment in this thread.
		@details	If there are no replies, return this comment.
		@since		2018-12-25 00:09:35
	**/
	public function last_comment()
	{
		$last = $this->replies->last();
		if ( ! $last )
			return $this;
		return $last;
	}

    /**
    	@brief		The parent comment, if any.
		@since		2018-12-23 14:58:18
    **/
    public function parent()
    {
    	return $this->belongsTo( 'App\Models\Comments\Comment', 'parent_id' );
    }

    /**
    	@brief		Reply comments.
		@since		2018-12-23 14:58:18
    **/
    public function replies()
    {
		return $this->hasMany( 'App\Models\Comments\Comment', 'parent_id' );
    }

	/**
		@brief		Site this model belongs to.
		@since		2018-11-21 00:04:11
	**/
	public function user()
	{
		return $this->belongsTo( 'App\Models\Users\User', 'user_id' );
	}
}
