<?php

namespace App\Models;

/**
	@brief		Meta key/value pairs for models.
	@since		2018-11-23 16:25:17
**/
class Meta
	extends Model
{
	protected $table = 'meta';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'meta_key',
        'meta_value',
    ];

    /**
    	@brief		Common function for all owners.
    	@details	Convenience method so I don't keep repeating myself.
    	@since		2018-11-28 09:31:15
    **/
    public function morphed_by_many( $model )
    {
        return $this->morphedByMany( $model, 'object', 'metas', 'meta_id' );
    }

    // --------------
    //	Owner methods
    // --------------

	/**
		@brief		Activity we belong to.
		@since		2018-11-28 09:29:04
	**/
	public function activity()
	{
		return $this->morphed_by_many( 'App\Models\Activities\Activity' );
	}

	/**
		@brief		Category we belong to.
		@since		2018-11-28 09:29:04
	**/
	public function category()
	{
		return $this->morphed_by_many( 'App\Models\Categories\Category' );
	}

	/**
		@brief		Caliber we belong to.
		@since		2018-11-28 09:29:04
	**/
	public function gun_caliber()
	{
		return $this->morphed_by_many( 'App\Models\Guns\Caliber' );
	}

	/**
		@brief		File we belong to.
		@since		2018-11-28 09:29:04
	**/
	public function file()
	{
		return $this->morphed_by_many( 'App\Models\Files\File' );
	}

	/**
		@brief		Group we belong to.
		@since		2018-11-28 09:29:04
	**/
	public function gun_group()
	{
		return $this->morphed_by_many( 'App\Models\Guns\Group' );
	}

	/**
		@brief		Gun we belong to.
		@since		2018-11-28 09:29:04
	**/
	public function gun()
	{
		return $this->morphed_by_many( 'App\Models\Guns\Gun' );
	}

	/**
		@brief		Site we belong to.
		@since		2018-11-28 09:29:04
	**/
	public function site()
	{
		return $this->morphed_by_many( 'App\Models\Sites\Site' );
	}

	/**
		@brief		User we belong to.
		@since		2018-11-28 09:29:04
	**/
	public function user()
	{
		return $this->morphed_by_many( 'App\Models\Users\User' );
	}
}
