<?php

namespace App\Models\Activities;

/**
	@brief		Index of which moderators have done which activities.
	@details	Used to access the timestamps since the relationship doesn't do it.
	@since		2018-11-24 22:21:58
**/
class Moderated_Index
	extends \App\Models\Model
{
	protected $table = 'activities_moderators';

    protected $fillable = [
        'activity_id',
        'user_id',
    ];

    /**
    	@brief		The Activity that this draft marker is from.
    	@since		2018-11-23 16:41:58
    **/
    public function activity()
    {
        return $this->BelongsTo( 'App\Models\Activities\Activity' );
    }

    /**
    	@brief		The site this activity belongs to.
    	@since		2018-11-24 22:21:58
    **/
    public function user()
    {
        return $this->BelongsTo( 'App\Models\Users\User' );
    }
}
