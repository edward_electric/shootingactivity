<?php

namespace App\Models\Activities;

use App\Models\Activities\Draft;
use App\Models\Users\User;
use Exception;
use Illuminate\Support\Facades\Mail;

/**
	@brief		A shooting activity.
	@since		2018-11-22 14:27:23
**/
class Activity
	extends \App\Models\Model
{
	use \App\Models\Files\Has_File;
	use \App\Models\Files\File_Is_Model_Property;

    use \App\Models\Traits\Meta;
    use \App\Models\Traits\User_Notification;

    /**
    	@brief		The meta use used to store an array of activity IDs that are incomplete.
    	@since		2019-01-20 15:37:37
    **/
    public static $incomplete_meta_key = 'incomplete_activities';

	protected $table = 'activities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ammo',
        'created_at',
        'file_id',
        'gun_id',
        'category_id',
        'user_id',
        'year',
    ];

    /**
    	@brief		A nice string.
    	@since		2020-03-22 17:01:21
    **/
    public function __toString()
    {
    	return sprintf( "%s: %s %s %s, %s",
    		$this->user->site->name,
    		$this->created_at,
    		$this->user->name,
    		$this->category->name,
    		$this->gun->name
    	);
    }

    /**
    	@brief		Add a moderator to this activity.
    	@details	If the moderator count matches the site moderator setting, the activity is taken out of the moderation queue.
    	@since		2018-11-23 20:59:00
    **/
    public function add_moderator()
    {
    	$this->moderators()->syncWithoutDetaching( app()->user() );

    	// Does the activity need more moderation?
    	$moderators = count( $this->moderators );
    	if ( $moderators >= app()->site()->get_meta( 'moderators' ) )
    		$this->fully_accepted();

    	return $this;
    }

	/**
		@brief		Where this activity was created.
		@since		2018-11-22 14:27:23
	**/
	public function category()
	{
		return $this->belongsTo( 'App\Models\Categories\Category', 'category_id' );
	}

    /**
    	@brief		Also delete the image.
    	@since		2018-11-22 17:27:04
    **/
    public function delete()
    {
    	if ( $this->file )
    		$this->file->delete();
		$this->unmark_as_draft();
   		$this->unmark_as_unmoderated();
		parent::delete();
	}

    /**
    	@brief		Delete all models associated to this user.
    	@since		2018-12-27 10:54:02
    **/
    public static function delete_by_user( User $user )
    {
    	// Remove everything moderated by this user.
    	Moderated_Index::where( 'user_id', $user->id )->delete();
    	$activities = static::where( 'user_id', $user->id )->get();
    	foreach( $activities as $activity )
    		$activity->delete();
    }

	/**
		@brief		Return the base query for accessing drafts.
		@since		2018-11-23 16:34:12
	**/
	public static function draft_query()
	{
		return Draft_Index::where( 'user_id', app()->user()->id );
	}

    /**
    	@brief		The optional image file of this activity.
    	@since		2018-11-18 13:33:06
    **/
    public function file()
    {
    	return $this->hasOne('App\Models\Files\File', 'id', 'file_id' );
    }

    /**
    	@brief		The user is finished editing this draft activity.
    	@since		2018-11-23 17:54:49
    **/
    public function finish_editing()
    {
    	$this->unmark_as_draft();
    	if ( app()->site()->uses_moderation() )
    	{
    		$this->mark_as_unmoderated();
    		$this->notify_users();
    	}
    	else
    		$this->fully_accepted();
    }

    /**
    	@brief		Do all the handling required when this activity is fully accepted.
    	@since		2018-11-27 15:11:19
    **/
    public function fully_accepted()
    {
		$this->unmark_as_unmoderated();

    	if ( app()->site()->uses_moderation() )
    	{
			// Inform the user of this acceptance.
			$mail = new \App\Mail\Activities\Moderation\Accept( $this );
			Mail::to( $this->user->email )->queue( $mail );
		}

		app()->statistics()->add_activity_later( $this );
    }

    /**
    	@brief		Return the optional comment of this activity.
    	@since		2019-01-20 19:29:28
    **/
    public function get_comment()
    {
    	return $this->get_meta( 'comment' );
    }

    /**
    	@brief		Convenience method to return the description in unsafe html.
    	@since		2023-03-07 20:01:01
    **/
    public function get_description()
    {
    	return htmlspecialchars_decode( $this->description );
    }

    /**
    	@brief		Return the draft, of null if no draft is available for this user.
    	@since		2018-11-23 16:34:45
    **/
    public static function get_draft()
    {
    	$activity = static::draft_query()->first();
    	if ( ! $activity )
    	{
    		// Create a new draft.
    		$activity = new Activity();

			$value = app()->user()->get_meta( 'last_activity_category' );
			if ( ! $value )
				$value = app()->site()->categories()->active()->get()->first()->id;
    		$activity->category_id = $value;

			$value = app()->user()->get_meta( 'last_activity_gun' );
			if ( ! $value )
				$value = app()->user()->guns()->active()->get()->first()->id;
    		$activity->gun_id = $value;

    		$activity->year = date( 'Y' );

    		$activity->user()->associate( app()->user() );
    		$activity->save();
    		$activity->mark_as_draft();
    	}
    	else
    		$activity = $activity->activity;

    	return $activity;
    }

	/**
		@brief		Return an array with customized file options.
		@details	Add to this method to customize the options.
		@since		2018-11-22 17:16:42
	**/
	public function get_file_options()
	{
		$options = $this->get_default_file_options();
		$options->delete_file_input_description		= __( 'Delete the current photo so you can upload a new one.' );
		$options->delete_file_input_label				= __( 'Delete this photo' );
		$options->file_is_image = true;
		$options->upload_file_input_description		= __( 'Take a photo of your visit to the range.' );
		$options->upload_file_input_label				= __( 'Photo' );
		return $options;
	}

    /**
    	@brief		Return all of the user's incomplete activities.
    	@since		2019-01-20 15:51:32
    **/
    public static function get_incomplete_activities( User $user )
    {
    	$ids = $user->get_meta( static::$incomplete_meta_key, [] );
    	$ids = (array)$ids;
    	return static::whereIn( 'id', $ids )->get();
    }

	/**
		@brief		Return all the users that can be notified.
		@since		2018-12-23 23:01:19
	**/
	public function get_notifiable_users()
	{
		return $this->user->site->get_users_with_capability( 'moderate_activities' );
	}

    /**
    	@brief		Return the ID of the site.
    	@since		2018-12-08 19:43:16
    **/
    public function get_site_id()
    {
    	return $this->user->site->id;
    }

    /**
    	@brief		Return the query showing activities that this user must moderate.
    	@since		2018-11-23 19:16:40
    **/
    public static function get_unmoderated_by_me_query()
    {
    	$unmoderated_ids = Unmoderated_Index::select( 'activity_id' )
    		->where( 'site_id', app()->site()->id )
    		->pluck( 'activity_id' );
    	$moderated_by_me = Moderated_Index::select( 'activity_id' )
    		->where( 'user_id', app()->user()->id )
    		->pluck( 'activity_id' );
    	// Find all activities that do not belong to me and that I have not moderated.
    	$activities = static::whereIn( 'id', $unmoderated_ids )
    		->whereNotIn( 'id', $moderated_by_me )
    		->where( 'user_id', '<>', app()->user()->id )	// Can't moderate your own activities.
    		->with( 'user' )
    		->orderBy( 'id', 'DESC' );
		return $activities;
    }

	/**
		@brief		Return the activities that this user must moderate.
		@since		2018-11-23 19:15:36
	**/
	public static function get_unmoderated_by_me()
	{
		return static::get_unmoderated_by_me_query()->get();
	}

	/**
		@brief		Return the notification data unique to this model.
		@since		2018-12-23 22:46:26
	**/
	public static function get_user_notification_data()
	{
		return (object)[
			'mail_subject' => 'mail.activities.notification.subject',
			'mail_view' => 'mail.activities.notification.text',
			'user_notification_enabled_key' => 'email_new_activities',
			'user_notification_key' => 'email_new_activities_notified',
		];
	}

	/**
		@brief		Which gun was used on this activity.
		@since		2018-11-22 14:27:23
	**/
	public function gun()
	{
		return $this->belongsTo( 'App\Models\Guns\Gun', 'gun_id' );
	}

	/**
		@brief		Does the user have a draft activity?
		@since		2018-11-23 16:28:45
	**/
	public static function has_draft()
	{
		return static::draft_query()->count() > 0;
	}

	/**
		@brief		Is the activity unmoderated?
		@since		2018-11-24 11:04:38
	**/
	public function is_unmoderated()
	{
    	return Unmoderated_Index::where( 'activity_id', $this->id )
    		->count() > 0;
	}

	/**
		@brief		Return whether this activity is unmoderated by the user.
		@since		2018-11-23 19:53:35
	**/
	public function is_unmoderated_by_me()
	{
		$unmoderated_by_me = static::get_unmoderated_by_me_query();
		$unmoderated_by_me = $unmoderated_by_me->select( 'id' )->get()->pluck( 'id' )->toArray();
		return in_array( $this->id, $unmoderated_by_me );
	}

	/**
		@brief		Mark this activity as a draft.
		@since		2018-11-23 16:43:51
	**/
	public function mark_as_draft()
	{
		Draft_Index::firstOrCreate( [
			'activity_id' => $this->id,
			'user_id' => $this->user->id,
		] );
	}

	/**
		@brief		Mark this activty as incomplete.
		@since		2019-01-20 15:36:51
	**/
	public function mark_as_incomplete()
	{
		$ids = static::get_incomplete_activities( $this->user )->pluck( 'id' );
		$ids[] = $this->id;
		$this->user->set_meta( static::$incomplete_meta_key, $ids );
	}

	/**
		@brief		Mark this activity as requiring moderation.
		@since		2018-11-23 17:55:55
	**/
	public function mark_as_unmoderated()
	{
		Unmoderated_Index::firstOrCreate( [
			'activity_id' => $this->id,
			'site_id' => $this->user->site->id,
		] );
	}

    /**
    	@brief		Return the meta owner.
    	@since		2018-11-16 01:32:51
    **/
    public function meta_owner()
    {
    	return 'activity';
    }

	/**
		@brief		Who has moderated this activity?
		@since		2018-11-23 10:30:27
	**/
	public function moderators()
	{
		return $this->belongsToMany( 'App\Models\Users\User', 'activities_moderators', 'activity_id', 'user_id' )
			->withTimestamps();
	}

	/**
		@brief		Fetch the next Activity for the user, if any.
		@since		2018-12-01 18:46:16
	**/
	public function next()
	{
		return Activity::where( 'user_id', $this->user->id )
			->where( 'id', '>', $this->id )
			->orderBy( 'id', 'asc' )
			->limit( 1 )
			->first();
	}

	/**
		@brief		Fetch the previous Activity for the user, if any.
		@since		2018-12-01 18:46:16
	**/
	public function previous()
	{
		return Activity::where( 'user_id', $this->user->id )
			->where( 'id', '<', $this->id )
			->orderBy( 'id', 'desc' )
			->limit( 1 )
			->first();
	}

	/**
		@brief		Cause a mass reorder of all activities.
		@since		2023-03-08 18:59:21
	**/
	public static function reorder_for_all_users()
	{
		$users = User::orderBy( 'id' )
			->get();

        foreach( $users as $user )
        	static::reorder_for_user( $user->id );
	}

	/**
		@brief		Reorder this user's activities.
		@since		2023-03-08 00:05:33
	**/
	public static function reorder_for_user( int $user_id )
	{
		$activities = Activity::where( 'user_id', $user_id )
			->get();

		app()->log()->debug( 'Reordering %s activities for user %s', count( $activities ), $user_id );
		$year_counters = [];
		foreach( $activities as $user_activity )
		{
			$activity_time = \Carbon\Carbon::parse( $user_activity->created_at );

			$year = $activity_time->year;

			if ( ! isset( $year_counters[ $year ] ) )
				$year_counters[ $year ] = [];
			$year_counters[ $year ][ $activity_time->timestamp ] = $user_activity;
		}
		ksort( $year_counters );
		app()->log()->debug( '- %s years count', count( $year_counters ) );
		foreach( $year_counters as $year => $activities )
		{
			ksort( $year_counters[ $year ] );
			$counter = 0;
			app()->log()->debug( '- Handling year %s', $year );
			foreach( $year_counters[ $year ] as $timestamp => $activity )
			{
				$activity->order = $counter;
				$activity->year = $year;
				app()->log()->debug( '--Reordering activity %s to %s', $activity->id, $activity->order );
				$activity->save();
				$counter++;
			}
		}
	}

	/**
		@brief		Reject this activity by a moderator.
		@since		2018-11-24 12:25:50
	**/
	public function reject()
	{
		// Inform the user of this acceptance.
		$mail = new \App\Mail\Activities\Moderation\Reject( $this );
		// Send it immediately because the model is going to disappear afterwards.
		Mail::to( $this->user->email )->send( $mail );
		$this->delete();
	}

	/**
		@brief		Return the strings helper.
		@since		2018-11-25 16:19:11
	**/
	public function strings()
	{
		return new Strings( $this->get_meta( 'strings', [] ) );
	}

	/**
		@brief		Set the comment for this activity.
		@details	Deletes the comment if nothing is to be set.
		@since		2019-01-20 19:28:48
	**/
	public function set_comment( $comment )
	{
		if ( $comment != '' )
			$this->set_meta( 'comment', $comment );
		else
			$this->delete_meta( 'comment' );
		return $this;
	}

	/**
		@brief		Should this user be notified?
		@since		2018-12-23 22:49:15
	**/
	public function should_notify_user( User $user )
	{
		// Don't notify the moderator of his own activity;
		if ( $this->user_id != $user->id )
		{
			app()->log()->debug( 'Yes! Notify user %s %s of this activity.', $user->name, $user->id );
			return true;
		}
		// The bad English is for the log file to be searchable.
		app()->log()->debug( 'Not notify user %s %s of his own activity.', $user->name, $user->id );
		return false;
	}

	/**
		@brief		Remove the draft marking for this activity.
		@since		2018-11-23 16:43:51
	**/
	public function unmark_as_draft()
	{
		Draft_Index::where( 'activity_id', $this->id )
			->where(  'user_id', $this->user->id )
			->delete();
	}

	/**
		@brief		Unmark this activity as no longer needing moderation.
		@since		2018-11-23 17:56:47
	**/
	public function unmark_as_unmoderated()
	{
		Unmoderated_Index::where( 'activity_id', $this->id )
			->where(  'site_id', $this->user->site->id )
			->delete();
	}

	/**
		@brief		Unmark this activty as incomplete.
		@since		2019-01-20 15:36:51
	**/
	public function unmark_as_incomplete()
	{
		$ids = static::get_incomplete_activities( $this->user )->pluck( 'id' );
		foreach( $ids as $index => $id )
			if ( $id == $this->id )
				unset( $ids[ $index ] );
		if ( count( $ids ) < 1 )
			$this->user->delete_meta( static::$incomplete_meta_key );
		else
			$this->user->set_meta( static::$incomplete_meta_key, $ids );
	}

	/**
		@brief		User this activity belongs to.
		@since		2018-11-22 14:27:23
	**/
	public function user()
	{
		return $this->belongsTo( 'App\Models\Users\User', 'user_id' );
	}

	/**
		@brief		Which years are available for viewing?
		@since		2023-03-08 21:32:06
	**/
	public static function years( User $user )
	{
		// We could to this:
		/**
		return static::select( 'year' )
			->where( 'user_id', $user->id )
			->distinct()
			->get()
			->pluck( 'year' );
		**/
		// But it's faster to query the stats meta.
		$years = $user->get_meta( 'stats_years' );
		return (array) $years;
	}
}
