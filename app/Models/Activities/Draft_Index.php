<?php

namespace App\Models\Activities;

/**
	@brief		A draft of an activity.
	@details	Is used only for an index.
	@since		2018-11-23 16:32:43
**/
class Draft_Index
	extends \App\Models\Model
{
	protected $table = 'activities_drafts';

    protected $fillable = [
        'activity_id',
        'user_id',
    ];

    /**
    	@brief		The Activity that this draft marker is from.
    	@since		2018-11-23 16:41:58
    **/
    public function activity()
    {
        return $this->BelongsTo( 'App\Models\Activities\Activity' );
    }

    /**
    	@brief		The user this draft belongs to.
    	@since		2018-10-06 19:41:50
    **/
    public function user()
    {
        return $this->BelongsTo( 'App\Models\Users\User' );
    }
}
