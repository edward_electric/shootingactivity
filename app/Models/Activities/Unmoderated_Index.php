<?php

namespace App\Models\Activities;

/**
	@brief		Index of unmoderated activities.
	@since		2018-11-23 17:57:46
**/
class Unmoderated_Index
	extends \App\Models\Model
{
	protected $table = 'activities_unmoderated';

    protected $fillable = [
        'activity_id',
        'site_id',
    ];

    /**
    	@brief		The Activity that this draft marker is from.
    	@since		2018-11-23 16:41:58
    **/
    public function activity()
    {
        return $this->BelongsTo( 'App\Models\Activities\Activity' );
    }

    /**
    	@brief		The site this activity belongs to.
    	@since		2018-10-06 19:41:50
    **/
    public function site()
    {
        return $this->BelongsTo( 'App\Models\Sites\Site' );
    }
}
