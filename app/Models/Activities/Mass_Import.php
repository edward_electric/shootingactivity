<?php

namespace App\Models\Activities;

use Carbon\Carbon;

/**
	@brief		A mass import model storing the import data.
	@since		2019-01-20 10:16:40
**/
class Mass_Import
	extends \App\Classes\Collections\Collection
{
	/**
		@brief		The session key where the mass import data is stored.
		@since		2019-01-20 10:16:53
	**/
	public static $session_key = 'activities_mass_import';

	/**
		@brief		Convenience method to prepare a new model.
		@since		2019-01-20 11:44:04
	**/
	public static function create()
	{
		$r = new static();

		$r->set( 'datetime', Carbon::createFromTimestamp( time() ) );
		$r->set( 'ready', false );

		return $r;
	}

	/**
		@brief		Does a mass import model exist in the session?
		@since		2019-01-20 10:33:34
	**/
	public static function exists()
	{
		return session()->has( static::$session_key );
	}

	/**
		@brief		Remove the model from memory.
		@since		2019-01-20 10:41:44
	**/
	public static function flush()
	{
		session()->forget( static::$session_key );
	}

	/**
		@brief		Is this model ready for import?
		@since		2019-01-20 12:14:44
	**/
	public function is_ready()
	{
		return $this->get( 'ready' );
	}

	/**
		@brief		Load the model from the session.
		@since		2019-01-20 12:12:58
	**/
	public static function load_from_session()
	{
		if ( static::exists() )
			$r = unserialize( session()->get( static::$session_key ) );
		else
			$r = static::create();
		return $r;
	}

	/**
		@brief		Mark as ready.
		@since		2019-01-20 12:55:41
	**/
	public function ready()
	{
		$this->set( 'ready', true );
		return $this;
	}

	/**
		@brief		Save the model to the session.
		@since		2019-01-20 10:38:45
	**/
	public function save()
	{
		session()->put( static::$session_key, serialize( $this ) );
	}

	/**
		@brief		Mark as unready.
		@since		2019-01-20 12:55:41
	**/
	public function unready()
	{
		$this->set( 'ready', false );
		return $this;
	}
}
