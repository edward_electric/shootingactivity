<?php

// UPDATE `meta` SET `meta_key` = 'strings' WHERE `meta_key` = 'scores';

namespace App\Models\Activities;

use App\Models\Sites\Site;
use Exception;

/**
	@brief		A helper class for handling the scores in an activity.
	@since		2018-11-25 16:19:44
**/
class Strings
	extends \App\Classes\Collections\Collection
{
	/**
		@brief		Append a string.
		@since		2018-12-06 22:53:08
	**/
	public function add_string( $score, $shots = [] )
	{
		return $this->append( (object)[ 'score' => $score, 'shots' => $shots ] );
	}

	/**
		@brief		A more flexible way to add strings.
		@details	The data is an object.

					It has a ->score key, int.
					An array of single hits.
					X int, optional, the number of inner 10 hits, which can only be the same amount of 10 hits.

		@param		$data		Object of -> score, -> shots[], -> x
		@since		2021-01-11 16:40:37
	**/
	public function add_string_data( $data )
	{
		$data = (object) $data;

		// Check that there are an equal amount of 10 scores as there are x.
		if ( isset( $data->x ) )
		{
			$tens = 0;
			foreach( $data->shots as $shot )
				if ( $shot == 10 )
				$tens++;
			if ( $data->x > $tens )
				throw new Exception( __( 'You cannot add more inner 10s than you have 10s.' ) );
		}

		if ( isset( $data->index ) )
		{
			$index = $data->index;
			unset( $data->index );
			$this->set( $index, $data );
		}
		else
			$this->append( $data );

		return $this;
	}

	/**
		@brief		After forgetting, reorder.
		@since		2018-12-06 23:12:17
	**/
	public function forget( $index )
	{
		parent::forget( $index );
		$this->reorder();
	}

	/**
		@brief		Return a count of all of the shots.
		@since		2018-12-06 23:16:27
	**/
	public function count_shots()
	{
		$shots = 0;
		foreach( $this as $index => $ignore )
			$shots += count( $this->get_shots( $index ) );
		return $shots;
	}

	/**
		@brief		Convenience method to return whether the user has scoring enabled.
		@since		2021-01-11 16:13:39
	**/
	public static function scoring_enabled( Site $site )
	{
		return $site->has_enabled( 'scoring' );
	}

	/**
		@brief		Convenience method to return whether the user's site has X scoring enabled.
		@since		2021-01-11 16:13:39
	**/
	public static function scoring_x_enabled( Site $site = null )
	{
		if ( $site === null )
			$site = app()->user()->site;

		if ( ! static::scoring_enabled( $site ) )
			return false;

		return $site->has_enabled( 'scoring_x' );
	}

	/**
		@brief		Return an average of all of the scores.
		@since		2018-12-06 23:23:07
	**/
	public function get_average()
	{
		if ( count( $this ) < 1 )
			return 0;
		return round( $this->get_sum() / count( $this ), 2 );
	}

	/**
		@brief		Return an array containing how often points occur.
		@since		2019-09-04 21:16:58
	**/
	public function get_distribution_array()
	{
		$occurrences = [];
		$total = 0;
		foreach( $this as $index => $ignore )
		{
			$shots = $this->get_shots( $index );
			foreach( $shots as $points )
			{
				if ( ! isset( $occurrences[ $points ] ) )
					$occurrences[ $points ] = [
						'count' => 0,
						'percent' => 0,
					];
				$total++;
				$occurrences[ $points ][ 'count' ]++;
			}
			if ( $this->scoring_x_enabled() )
			{
				$data = $this->get( $index );
				if ( isset( $data->x ) )
				{
					if ( isset( $occurrences[ 10 ] ) )
					{
						$x = $data->x;
						$occurrences[ 10 ][ 'count' ] -= $x;
						if ( ! isset( $occurrences[ 'X' ] ) )
							$occurrences[ 'X' ] = [
								'count' => 0,
								'percent' => 0,
							];
						$occurrences[ 'X' ][ 'count' ] += $x;
					}
				}
			}
		}

		ksort( $occurrences );

		// We want the Xs last, if any.
		if ( isset( $occurrences[ 'X' ] ) )
		{
			$data = $occurrences[ 'X' ];
			unset( $occurrences[ 'X' ] );
			$occurrences[ 'X' ] = $data;
		}

		// And now add a percent value.
		foreach( $occurrences as $point => $occurrence )
		{
			$occurrences[ $point ][ 'percent' ] = round( $occurrence[ 'count' ] / $total * 100 );
		}

		return $occurrences;
	}

	/**
		@brief		Return the percentage occurrence of a specific point.
		@since		2021-01-16 16:47:24
	**/
	public function get_point_occurrence( $point )
	{
		$array = $this->get_distribution_array();
		if ( ! isset( $array[ $point ] ) )
			return 0;
		return $array[ $point ][ 'percent' ];
	}

	/**
		@brief		Return the score of a string.
		@since		2018-12-06 23:05:32
	**/
	public function get_score( $index )
	{
		$item = $this->get( $index );
		if ( ! $item )
			return 0;
		return $this->get( $index )->score;
	}

	/**
		@brief		Get the sum of all series with individual shots.
		@since		2021-01-11 21:00:22
	**/
	public function get_shot_sum()
	{
		$sum = 0;
		foreach( $this as $score )
		{
			if ( count( $score->shots ) < 1 )
				continue;
			$sum += $score->score;
		}
		return $sum;
	}

	/**
		@brief		Return the shots array of a string.
		@since		2018-12-06 23:08:04
	**/
	public function get_shot( $index, $number )
	{
		$r = $this->get( $index );
		if ( ! $r )
			return '';
		$shots = $r->shots;
		if ( ! isset( $shots[ $number ] ) )
			return '';
		return $shots[ $number ];
	}

	/**
		@brief		Return the shots array of a string.
		@since		2018-12-06 23:08:04
	**/
	public function get_shots( $index )
	{
		return $this->get( $index )->shots;
	}

	/**
		@brief		Return the shots including Xs
		@since		2021-01-18 22:10:29
	**/
	public function get_shots_and_x( $index )
	{
		$get = $this->get( $index );
		if ( ! isset( $get->x ) )
			return $get->shots;

		$r = [];
		foreach( $get->shots as $hit )
		{
			if ( $hit == 10 )
				if ( $get->x > 0 )
				{
					$get->x--;
					$hit = 'X';
				}
			$r []= $hit;
		}
		return $r;
	}

	/**
		@brief		Return the shots, but sorted as the user prefers.
		@since		2019-09-04 20:42:50
	**/
	public function get_sorted_shots( $index, $user )
	{
		$key = 'string_order';
		$string_order = $user->get_meta( $key );
		$shots = $this->get_shots( $index );

		if ( $this->scoring_x_enabled() )
		{
			$data = $this->get( $index );
			if ( isset( $data->x ) )
			{
				for( $counter = 0; $counter < $data->x; $counter ++ )
					foreach( $shots as $shot_index => $score )
						if ( $score == 10 )
						{
							$shots[ $shot_index ] = 'X';
							continue 2;
						}
			}
		}
		switch( $string_order )
		{
			case 'ascending':
				asort( $shots );
			break;
			case 'descending':
				rsort( $shots );
			break;
		}
		return $shots;
	}

	/**
		@brief		Return the sum of all of the scores.
		@since		2018-12-06 23:06:16
	**/
	public function get_sum()
	{
		$sum = 0;
		foreach( $this as $string )
			$sum += $string->score;
		return $sum;
	}

	/**
		@brief		Reorder the items so that we have a 1 based index.
		@since		2018-11-25 16:43:19
	**/
	public function reorder()
	{
		$new_items = [];
		foreach( $this as $item )
			$new_items[ count( $new_items ) ] = $item;
		$this->items = $new_items;
		return $this;
	}

	/**
		@brief		Save the scores to the model.
		@since		2018-11-25 16:52:44
	**/
	public function save( $model )
	{
		$model->set_meta( 'strings', $this->toArray() );
	}

	/**
		@brief		Set the score data.
		@since		2023-02-28 08:45:39
	**/
	public function set_string_data( $index, $data )
	{
		if ( $index < 0 )
			$this->add_score_data( $data );
		else
			$this->set( $index, $data );
		return $this;
	}
}
