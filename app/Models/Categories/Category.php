<?php

namespace App\Models\Categories;

/**
	@brief		A category of an activity.
	@since		2018-11-18 21:18:44
**/
class Category
	extends	\App\Models\Generic\Site
{
    use \App\Models\Traits\Meta;

	protected $table = 'categories';

	/**
		@brief		Can this category be deleted?
		@since		2018-11-25 12:36:26
	**/
	public function can_be_deleted()
	{
		return \App\Models\Activities\Activity::where( 'category_id', $this->id )->count() < 1;
	}

	/**
		@brief		Delete ourself.
		@since		2018-11-27 15:17:45
	**/
	public function delete()
	{
		$this->meta()->delete();
		parent::delete();
	}

	/**
		@brief		The model's preferred Fontawesome icon.
		@details	GenericModel method.
		@since		2018-11-21 11:30:55
	**/
	public static function icon()
	{
		return 'fa-solid fa-share-alt-square';
	}

	/**
		@brief		Return an object containing all of the model's labels.
		@details	GenericModel method.
		@since		2018-11-21 11:19:34
	**/
	public static function labels()
	{
		return (object)[
			'name_input_description' => __( 'The type of activity category.' ),
			'name_input_placeholder' => __( 'Practice at 25m range' ),
			'plural' => __( 'activity categories' ),
			'Plural' => __( 'Activity categories' ),
			'singular' => __( 'activity category' ),
			'Singular' => __( 'Activity category' ),
		];
	}

    /**
    	@brief		Return the meta owner.
    	@since		2018-11-16 01:32:51
    **/
    public function meta_owner()
    {
    	return 'category';
    }

	/**
		@brief		Return the non-conflicting model name in the owner model.
		@details	GenericModel method.
		@since		2018-11-21 11:24:53
	**/
	public static function model_key()
	{
		return 'categories';
	}

	/**
		@brief		Return the name of the model as used in routes.
		@since		2020-01-15 15:41:03
	**/
	public static function route_model_key()
	{
		return 'category';
	}
}
