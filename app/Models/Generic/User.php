<?php

namespace App\Models\Generic;

/**
	@brief		A generic model for a model that belongs to a user.
	@since		2018-11-21 16:46:39
**/
abstract class User
	extends Model
{
    protected $fillable = [
        'name',
        'user_id',
    ];

    /**
    	@brief		Delete all models associated to this user.
    	@since		2018-12-27 10:54:02
    **/
    public static function delete_by_user( \App\Models\Users\User $user )
    {
    	static::where( 'user_id', $user->id )->delete();
    }

    /**
    	@brief		Return the ID of the site the user is a member of.
    	@since		2018-12-08 19:43:16
    **/
    public function get_site_id()
    {
    	return $this->user->site->id;
    }

	/**
		@brief		Create a new model.
		@since		2018-11-21 11:54:17
	**/
	public static function new_model()
	{
		$model = new static();
		$model->user()->associate( app()->user() );
		return $model;
	}

	/**
		@brief		Return which model owns this model.
		@since		2018-11-21 15:42:32
	**/
	public function owner()
	{
		return app()->user();
	}

	/**
		@brief		Site this model belongs to.
		@since		2018-11-21 00:04:11
	**/
	public function user()
	{
		return $this->belongsTo( 'App\Models\Users\User', 'user_id' );
	}
}
