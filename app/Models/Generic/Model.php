<?php

namespace App\Models\Generic;

/**
	@brief		The base generic model that is used in the generic editor.
	@since		2018-11-21 00:41:02
**/
abstract class Model
	extends \App\Models\Model
{
	use \App\Models\Traits\Archivable;
	use \App\Models\Traits\Forms_Helper;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
    	@brief		Can this model be archived?
		@since		2018-11-21 11:30:55
    **/
    public function can_be_archived()
    {
    	return true;
    }

    /**
    	@brief		Can this model be edited?
    	@since		2018-11-25 13:51:32
    **/
    public function can_be_edited()
    {
    	return true;
    }

    /**
    	@brief		Can this model be deleted?
		@since		2018-11-21 11:30:55
    **/
    public function can_be_deleted()
    {
    	return true;
    }

    /**
    	@brief		Can this model be restored?
		@since		2018-11-21 11:30:55
    **/
    public function can_be_restored()
    {
    	return true;
    }

    /**
    	@brief		Create the generic controller, feed it us, and return the command.
    	@since		2018-11-21 11:15:56
    **/
    public static function generic_controller( $type, $method, $model = null )
    {
    	if ( ! $model )
    		$model = new Static();

    	$class = 'App\\Http\\Controllers\\GenericModel\\' . $type;
    	$class = new $class();
    	return $class->$method( $model );
    }

    /**
    	@brief		Modify the edit form.
    	@since		2018-11-21 16:53:46
    **/
    public function get_edit_form( $form, $model = null )
    {
    }

	/**
		@brief		The model's preferred Fontawesome icon.
		@details	GenericModel method.
		@since		2018-11-21 11:30:55
	**/
	public static function icon()
	{
		return 'spider';
	}

	/**
		@brief		Is this model archivable?
		@since		2018-12-02 18:44:54
	**/
	public function is_archivable()
	{
		return true;
	}

	/**
		@brief		Return an object containing all of the model's labels.
		@details	GenericModel method.
		@since		2018-11-21 11:19:34
	**/
	public static function labels()
	{
		return (object)[
			'name_input_description' => __( 'Model name description here' ),
			'name_input_placeholder' => __( 'Model name placeholder here' ),
			'plural' => 'models',
			'Plural' => 'Models',
			'singular' => 'model',
			'Singular' => 'Model',
		];
	}

	/**
		@brief		The key for this model.
		@details	GenericModel method.
		@since		2018-11-21 11:24:53
	**/
	public abstract static function model_key();

	/**
		@brief		Create a new model.
		@details	Do the footwork necessary to associate the model to the owner.
		@since		2018-11-21 11:54:17
	**/
	public abstract static function new_model();

	/**
		@brief		Return which model owns this model.
		@since		2018-11-21 15:42:32
	**/
	public abstract function owner();

	/**
		@brief		Return the models that this owner has.
		@since		2018-11-21 16:01:39
	**/
	public function owner_models()
	{
		$key = static::model_key();
		$models = $this->owner()->$key();
		return $models;
	}

    /**
    	@brief		Handle the processing of the editor form.
    	@since		2018-11-21 16:56:53
    **/
    public function post_edit_form( $form, $model = null )
    {
    }

    /**
    	@brief		This function is run after the model has been saved after editing.
    	@since		2020-04-09 21:03:13
    **/
    public function post_after_edit_form( $form )
    {
    }

    /**
    	@brief		Generate a route for this model.
    	@since		2020-01-15 15:38:21
    **/
    public function route( $type )
    {
		$key = static::model_key();
    	$routes = static::routes();
    	$route_model_key = static::route_model_key();
    	return route( $routes->$type, [ $route_model_key => $this->id ] );
    }

	/**
		@brief		Return an object containing all of our planned routes
		@details	GenericModel method.
		@since		2018-11-21 11:19:34
	**/
	public static function routes( $routes = [] )
	{
		$key = static::model_key();
		$routes = (object) $routes;
		$routes->add = $key . '_add';
		$routes->archive = $key . '_archive';
		$routes->archived = $key . '_archived';
		$routes->delete = $key . '_delete';
		$routes->edit = $key . '_edit';
		$routes->index = $key . '_index';
		$routes->restore = $key . '_restore';
		return $routes;
	}

	/**
		@brief		Return the site key with this suffix.
		@details	GenericModel method.
		@since		2018-11-21 13:00:24
	**/
	public static function owner_key_suffix( $suffix )
	{
		return static::model_key() . $suffix;
	}

	/**
		@brief		Convenience method to return a specific view, if available. Else null.
		@since		2018-11-21 17:46:57
	**/
	public static function view( $key )
	{
		$views = static::views();
		if ( isset( $views->$key ) )
			return $views->$key;
		return null;
	}

	/**
		@brief		Return an object containing all of our planned views.
		@since		2018-11-21 11:19:34
	**/
	public static function views( $views = [] )
	{
		$views = (object) $views;
		$views->add = 'genericmodel.add';
		$views->add_a_model = 'genericmodel.add_a_model';
		$views->archive = 'genericmodel.archive';
		$views->archived = 'genericmodel.archived';
		$views->delete = 'genericmodel.delete';
		$views->edit = 'genericmodel.edit';
		$views->index = 'genericmodel.index';
		$views->restore = 'genericmodel.restore';
		return $views;
		// This is what subclasses override with.
		// $views = parent::views();
		// return $views;
	}
}
