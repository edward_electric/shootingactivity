<?php

namespace App\Models\Generic;

/**
	@brief		A model with no owner.
	@since		2018-11-21 20:16:40
**/
abstract class No_Owner
	extends Model
{
	/**
		@brief		Create a new model.
		@since		2018-11-21 11:54:17
	**/
	public static function new_model()
	{
		$model = new static();
		return $model;
	}

	/**
		@brief		Return which model owns this model.
		@since		2018-11-21 15:42:32
	**/
	public function owner()
	{
		return $this;
	}

	/**
		@brief		Return the models that this owner has.
		@since		2018-11-21 16:01:39
	**/
	public function owner_models()
	{
		// We don't have an owner, so return ourselves.
		return static::query();
	}

}
