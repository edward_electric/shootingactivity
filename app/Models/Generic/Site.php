<?php

namespace App\Models\Generic;

/**
	@brief		A generic model for a model that belongs to a site.
	@since		2018-11-21 15:23:53
**/
abstract class Site
	extends Model
{
    protected $fillable = [
        'name',
        'site_id',
    ];

    /**
    	@brief		Return the ID of the site the user is a member of.
    	@since		2018-12-08 19:43:16
    **/
    public function get_site_id()
    {
    	return $this->site->id;
    }

	/**
		@brief		Create a new model.
		@since		2018-11-21 11:54:17
	**/
	public static function new_model()
	{
		$model = new static();
		$model->site()->associate( app()->user()->site );
		return $model;
	}

	/**
		@brief		Return the models on this site.
		@since		2018-11-24 22:53:23
	**/
	public function scopeon_this_site( $query )
	{
		return $query->where( 'site_id', app()->site()->id );
	}

	/**
		@brief		Return which model owns this model.
		@since		2018-11-21 15:42:32
	**/
	public function owner()
	{
		return app()->user()->site;
	}

	/**
		@brief		Site this model belongs to.
		@since		2018-11-21 00:04:11
	**/
	public function site()
	{
		return $this->belongsTo( 'App\Models\Sites\Site', 'site_id' );
	}
}
