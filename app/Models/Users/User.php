<?php

namespace App\Models\Users;

use Illuminate\Auth\Authenticatable;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

use App\Models\Activities\Activity;
use App\Models\Permissions\Role;
use App\Models\Sites\Site;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class User
	extends \App\Models\Generic\Site
	implements AuthenticatableContract, AuthorizableContract
{
	use \App\Models\Traits\Archivable;
    use \App\Models\Traits\Meta;
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'name',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
    	@brief		A nice string.
    	@since		2020-03-24 21:35:43
    **/
    public function __toString()
    {
    	return sprintf( "%s / %s",
    		$this->name,
    		$this->site->name
    	);
    }

    /**
    	@brief		Log activities.
    	@since		2018-11-23 11:27:05
    **/
    public function activities()
    {
		return $this->hasMany( 'App\Models\Activities\Activity', 'user_id' );
    }

    /**
    	@brief		Assign this role to the user.
    	@since		2018-11-15 23:36:28
    **/
    public function assign_role( $name )
    {
    	foreach( $this->site->roles as $role )
    		if ( $role->name == $name )
    		{
				$this->roles()->detach( $role );		// This is to keep things clean.
				$this->roles()->attach( $role );
    		}
    }

    /**
    	@brief		Assign several roles to this user.
    	@since		2018-11-15 23:40:25
    **/
    public function assign_roles( $roles )
    {
    	foreach( $roles as $role )
    		$this->assign_role( $role );
    }

    /**
    	@brief		Can this user be archived?
    	@since		2018-11-19 17:30:48
    **/
    public function can_be_archived()
    {
    	return $this->id != app()->user()->id;
    }

    /**
    	@brief		Can this user be deleted?
    	@since		2018-11-19 17:30:48
    **/
    public function can_be_deleted()
    {
    	return $this->id != app()->user()->id;
    }

    /**
    	@brief		Check that this password is valid.
    	@since		2018-11-13 20:45:00
    **/
    public function check_password( $password )
    {
    	return Hash::check( $password, $this->password );
    }

    /**
    	@brief		Comments this user has written.
    	@since		2018-12-27 10:39:17
    **/
    public function comments()
    {
		return $this->hasMany( 'App\Models\Comments\Comment', 'user_id' );
    }

    /**
    	@brief		Delete the user and everything belonging to him.
    	@since		2018-12-27 10:37:15
    **/
    public function delete()
    {
    	\App\Models\Activities\Activity::delete_by_user( $this );
    	\App\Models\Guns\Gun::delete_by_user( $this );
    	// We have to load each comment and delete it as a whole model.
    	foreach( $this->comments()->get() as $comment )
    		$comment->delete();
    	$this->roles()->sync( [] );
    	parent::delete();
    }

    /**
    	@brief		Create or return the e-mail fieldset.
    	@since		2018-12-09 18:12:51
    **/
    public static function email_fieldset( $form )
    {
    	$fs = $form->fieldset( 'fs_email' );
    	$fs->legend->label( __( 'E-mail settings' ) );
    	return $fs;
    }

    /**
    	@brief		Return when the user was last seen online.
    	@since		2018-12-25 15:16:12
    **/
    public function get_last_seen_for_humans()
    {
    	$last_seen = $this->get_meta( 'last_seen' );
    	if ( ! $last_seen )
    		return __( 'never' );
    	return \Carbon\Carbon::createFromTimestamp( $last_seen )->diffForHumans();
    }

    /**
    	@brief		Get the roles the user is allowed to assign.
    	@since		2018-11-19 17:53:56
    **/
    public function get_manageable_roles()
    {
		$r = [];
		$manage_network = $this->may( 'manage_network' );
   		$roles = app()->user()->site->roles()->get();
   		foreach( $roles as $role )
   		{
   			if ( $role->can( 'manage_network' ) )
   				if ( ! $manage_network )
   					continue;
   			$r[ $role->id ] = $role->name;
   		}
    	return $r;
    }

    /**
    	@brief		Return the names of all of our roles.
    	@since		2018-11-19 18:40:25
    **/
    public function get_role_names()
    {
    	$r = [];
    	foreach( $this->roles as $role )
    		$r []= $role->name;
    	return $r;
    }

	/**
		@brief		The model's preferred Fontawesome icon.
		@details	GenericModel method.
		@since		2018-11-21 11:30:55
	**/
	public static function icon()
	{
		return 'fa-solid fa-users';
	}

    /**
    	@brief		Generate a new password.
    	@since		2018-11-13 20:24:52
    **/
    public static function generate_password()
    {
    	return \Illuminate\Support\Str::random( 8 );
    }

    /**
    	@brief		Modify the edit form.
    	@since		2018-11-21 16:53:46
    **/
    public function get_edit_form( $form, $model = null )
    {
		$form->email = $form->email( 'email' )
			->label( __( 'E-mail' ) )
			->placeholder( 'edward@shootingactivity.com' )
			->required()
			->trim();
		if ( $model )
			$form->email->value( $model->email );

		if ( $model )
			$form->last_seen = $form->text( 'last_seen' )
				->label( __( 'messages.user.last_seen' ) )
				->disabled()
				->value( $model->get_last_seen_for_humans() );

		$fs = $form->fieldset( 'fs_password' );
		$fs->legend()->label( __( 'Password' ) );

		$form->password = $fs->text( 'password' )
			->description( __( "Leave it empty to not change the user's password." ) )
			->label( __( 'Password' ) )
			->trim();

		$form->send_password = $fs->checkbox( 'send_password' )
			->description( __( "Send a password reset link to the user." ) )
			->label( __( 'Send password reset link' ) );


		static::get_email_inputs( $form, $model );

		// Network admin may change the sites of users.
		if ( app()->user()->may( 'manage_network' ) )
		{
			$sites = Site::orderBy( 'name' )->get();
			if( count( $sites ) > 0 )
			{
				$options = $sites->pluck( 'id', 'name' );
				$options = $options->toArray();
				$options = array_flip( $options );

				$fs = $form->fieldset( 'fs_site' );
				$fs->legend()->label( __( 'Site' ) );

				$form->site = $fs->select( 'site' )
					->label( __( 'Site' ) )
					->opts( $options )
					->required()
					->value( app()->user()->site->id );
			}
		}
		$show_roles = ( $model === null );

		if ( $model )
		{
			if ( app()->user()->may( 'manage_network' ) )
				$show_roles = true;
			if ( $model->id != app()->user()->id )
				$show_roles = true;
		}

		if ( $show_roles )
		{
			$fs = $form->fieldset( 'fs_roles' );
			$fs->legend()->label( __( 'Roles' ) );

			$form->roles = $fs->select( 'roles' )
				->description( __( 'Select which roles this user has.' ) )
				->label( __( 'Roles' ) )
				->multiple()
				->opts( app()->user()->get_manageable_roles() )
				->required()
				->autosize();

			if ( $model->id )
			{
				$ids = [];
				foreach( $model->roles as $role )
					$ids []= $role->id;
				$form->roles->value( $ids );
			}
		}
    }

    /**
    	@brief		Add e-mail form inputs.
    	@since		2018-12-20 15:43:17
    **/
    public static function get_email_inputs( $form, $model )
    {
    	// Anything that uses the user_notification trait is sufficient, but lets use Activity.
    	$notification_options = \App\Models\Activities\Activity::get_notification_options();

    	if ( $model->may( 'moderate_activities' ) )
    	{
    		$fs = static::email_fieldset( $form );
    		$form->email_new_activities = $fs->select( 'email_new_activities' )
				->description( __( "Receive an e-mail when there are new activities to moderate." ) )
				->label( __( 'New activities to moderate' ) )
				->opts( $notification_options )
				->value( $model->get_meta( 'email_new_activities' ) );
    	}
    	if ( app()->site()->has_enabled( 'notice_board' ) )
    	{
    		$fs = static::email_fieldset( $form );
    		$form->email_new_notices = $fs->select( 'email_new_notices' )
				->description( __( "Receive an e-mail when there are new notices on the notice board." ) )
				->label( __( 'New notices' ) )
				->opts( $notification_options )
				->value( $model->get_meta( 'email_new_notices' ) );
    	}

    }

    /**
    	@brief		Get the default value of a meta key.
    	@details	Override this if your model should return default values other than null.
    	@since		2018-11-16 23:30:00
    **/
    public function get_meta_default( $key, $default = null )
    {
    	$defaults = [
    		/**
    			@brief		Moderators receive an e-mail when there are new activities to moderate.
    			@since		2018-12-09 18:19:08
    		**/
    		'email_new_activities' => 'once_per_login',
    		/**
    			@brief		Receive an e-mail when new notices have been written.
    			@since		2018-12-23 23:10:37
    		**/
    		'email_new_notices' => 'once_per_login',
    	];
    	if ( isset( $defaults[ $key ] ) )
    		return $defaults[ $key ];
    	return $default;
    }

    /**
    	@brief		Get the password reminder token.
    	@since		2018-11-13 20:01:57
    **/
    public function get_reminder_token()
    {
    	$key = $this->id . $this->email . $this->password . $this->updated_at;
    	$key = md5( $key );
    	return $key;
    }

    /**
    	@brief		Return the URL for the password reminder.
    	@since		2018-11-13 20:03:22
    **/
    public function get_password_reset_url()
    {
    	return route( 'user_password_reset', [
    		'token' => $this->get_reminder_token(),
    		'user_id' => $this->id,
    	] );
    }

    /**
    	@brief		The guns this user owns.
    	@since		2018-11-17 20:31:03
    **/
    public function guns()
    {
		return $this->hasMany( 'App\Models\Guns\Gun', 'user_id' );
    }

    /**
    	@brief		Return whether the user has more than 1 gun.
    	@since		2018-11-23 21:42:08
    **/
    public function has_guns()
    {
    	return $this->guns()->active()->count() > 0;
    }

    /**
    	@brief		Hash the password.
    	@since		2018-11-27 15:25:09
    **/
    public static function hash_password( $password )
    {
    	return Hash::make( $password );
    }

	/**
		@brief		Return an object containing all of the model's labels.
		@details	GenericModel method.
		@since		2018-11-21 11:19:34
	**/
	public static function labels()
	{
		return (object)[
			'name_input_description' => __( 'The name, real or fictive, of the user.' ),
			'name_input_placeholder' => __( 'Edward P' ),
			'plural' => __( 'users' ),
			'Plural' => __( 'Users' ),
			'singular' => __( 'user' ),
			'Singular' => __( 'User' ),
		];
	}

    /**
    	@brief		Convenience method to check whether the user can do this thing.
    	@since		2018-11-15 23:30:26
    **/
    public function may( $thing )
    {
    	foreach( $this->roles as $role )
    		foreach( $role->capabilities as $capability )
    		{
    			// Network admins can do whatever they want.
    			if ( $capability->name == 'manage_network' )
    				return true;
    			if ( $capability->name == $thing )
    				return true;
    		}
    	return false;
    }

    /**
    	@brief		Return the meta owner.
    	@since		2018-11-16 01:32:51
    **/
    public function meta_owner()
    {
    	return 'user';
    }

	/**
		@brief		Return the non-conflicting model name in the owner model.
		@details	GenericModel method.
		@since		2018-11-21 11:24:53
	**/
	public static function model_key()
	{
		return 'users';
	}

	/**
		@brief		Return a string of the user's name and site.
		@since		2020-03-24 21:33:06
	**/
	public function name_and_site()
	{
		return $this->name . ' / ' . $this->site->name;
	}

    /**
    	@brief		Handle the processing of the editor form.
    	@since		2018-11-21 16:56:53
    **/
    public function post_edit_form( $form, $model = null )
    {
    	if ( ! $model->id )
    	{
			$model->set_password( microtime() );
    	}

		$model->name = $form->name->get_filtered_post_value();
		$model->email = $form->email->get_filtered_post_value();

		// Maybe set the user's password.
		$password = $form->password->get_filtered_post_value();
		if ( $password != '' )
			$model->set_password( $password );

		$model->save();

		if ( $form->send_password->get_post_value() )
		{
			$model->send_password();
			$alert = app()->alerts()->success();
			$alert->set_message( __( 'An e-mail with a password reset link is on its way!' ) );
		}

		// These are saved in the meta, so the user has to exist.
		static::post_email_inputs( $form, $model );

		if ( isset( $form->roles ) )
		{
			// Sync the roles.
			// If this user is not able to edit ALL roles, then prevent the inaccessible roles from disappearing.
			// Using the arrays via key (array_flip) is far easier than values.
			$form_roles = $form->roles->get_post_value();
			$form_roles = array_flip( $form_roles );
			$current_roles = $model->roles()->get()->pluck( 'id' )->toArray();
			$current_roles = array_flip( $current_roles );
			$manageable_roles = app()->user()->get_manageable_roles();
			foreach( $manageable_roles as $role_id => $ignore )
				unset( $current_roles[ $role_id ] );
			foreach( $form_roles as $role_id => $ignore )
				$current_roles[ $role_id ] = $role_id;
			$current_roles = array_keys( $current_roles );
			$model->roles()->sync( $current_roles );
		}

		if ( isset( $form->site ) )
		{
			$new_site_id = $form->site->get_filtered_post_value();
			if ( $new_site_id != $model->site->id )
			{
				$new_site = Site::findOrFail( $new_site_id );
				$new_site->migrate_user( $model );
			}
		}
    }

    /**
    	@brief		Handle the POST of the email inputs.
    	@since		2018-12-20 15:52:45
    **/
    public static function post_email_inputs( $form, $user )
    {
    	foreach ( [
    		'email_new_activities',
    		'email_new_notices',
    	] as $key )
			if ( isset( $form->$key ) )
				$user->set_meta( $key, $form->$key->get_post_value() );
    }

	/**
		@brief		The roles this user has.
		@since		2018-11-15 21:02:00
	**/
	public function roles()
	{
		return $this->belongsToMany( 'App\Models\Permissions\Role', 'user_roles', 'user_id', 'role_id' );
	}

	/**
		@brief		Return the name of the model as used in routes.
		@since		2020-01-15 15:41:03
	**/
	public static function route_model_key()
	{
		return 'user';
	}

    /**
    	@brief		Return the sites belonging to this site ID.
    	@since		2018-11-19 17:23:23
    **/
    public function scopeSite( $query, $site_id )
    {
    	return $query->where( 'site_id', $site_id );
    }

    /**
    	@brief		Send the user a password reset link.
    	@since		2020-04-09 21:39:07
    **/
    public function send_password()
    {
		$mail = new \App\Mail\User\Password\Send( $this );
		Mail::to( $this->email )->send( $mail );
		app()->log()->info( 'Password reset sent to %s / %s', $this->name, $this->email );
    }

    /**
    	@brief		Convenience method to set a new password.
    	@since		2018-11-13 15:37:14
    **/
    public function set_password( $new_password )
    {
    	$this->password = static::hash_password( $new_password );
    	return $this;
    }

    /**
    	@brief		The site this user belongs to.
    	@since		2018-10-06 19:41:50
    **/
    public function site()
    {
        return $this->BelongsTo( 'App\Models\Sites\Site' );
    }

	/**
		@brief		Return an object containing all of our planned views.
		@since		2018-11-21 11:19:34
	**/
	public static function views( $views = [] )
	{
		$views = parent::views();
		$views->index_actions = 'user.index_actions';
		$views->index_table_th = 'user.index_table_th';
		$views->index_table_td = 'user.index_table_td';
		return $views;
	}
}
