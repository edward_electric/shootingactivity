<?php

namespace App\Models\Permissions;

/**
	@brief		A role a user can take.
	@since		2018-11-15 20:16:54
**/
class Role
	extends	\App\Models\Generic\Site
{
	protected $table = 'roles';

	/**
		@brief		Assign a capability to this role.
		@since		2018-11-15 20:37:12
	**/
	public function add_capability( $capability )
	{
		$cap = Capability::firstOrCreate( [ 'name' => $capability ] );
		$this->capabilities()->attach( $cap );
		return $this;
	}

	/**
		@brief		Assign several capabilities to this role.
		@since		2018-11-19 08:31:29
	**/
	public function add_capabilities( $capabilities )
	{
		foreach( $capabilities as $capability )
			$this->add_capability( $capability );
		return $this;
	}

	/**
		@brief		Can this role do this?
		@since		2019-01-03 21:05:54
	**/
	public function can( $capability )
	{
		foreach( $this->capabilities as $cap )
			if ( $cap->name == $capability )
				return true;
		return false;
	}

    /**
    	@brief		Can this model be archived?
		@since		2018-11-21 11:30:55
    **/
    public function can_be_archived()
    {
    	return false;
    }

	/**
		@brief		The capabilities for this role.
		@since		2018-11-15 20:21:15
	**/
	public function capabilities()
	{
		return $this->belongsToMany( 'App\Models\Permissions\Capability', 'role_capabilities', 'role_id', 'capability_id' );
	}

    /**
    	@brief		The edit form has capabilities.
    	@since		2018-12-02 19:33:34
    **/
    public function get_edit_form( $form, $model = null )
    {
    	$opts = new \App\Classes\Collections\Named_Collection( Capability::get_assignable()->get() );
    	$opts = $opts->as_options();

		$form->capabilities = $form->select( 'capabilities' )
			->description( __( 'What users who have this role may do on the site.' ) )
			->label( __( 'Capabilities' ) )
			->multiple()
			->opts( $opts )
			->required()
			->autosize();
		if ( $model )
			$form->capabilities->value( $model->capabilities->pluck( 'id' )->toArray() );
    }

	/**
		@brief		The model's preferred Fontawesome icon.
		@details	GenericModel method.
		@since		2018-11-21 11:30:55
	**/
	public static function icon()
	{
		return 'fa-solid fa-paint-roller';
	}

	/**
		@brief		Is this model archivable?
		@since		2018-12-02 18:44:54
	**/
	public function is_archivable()
	{
		return false;
	}

	/**
		@brief		Return an object containing all of the model's labels.
		@details	GenericModel method.
		@since		2018-11-21 11:19:34
	**/
	public static function labels()
	{
		return (object)[
			'name_input_description' => __( 'The name of the role as visible to admins.' ),
			'name_input_placeholder' => 'moderator',
			'plural' => __( 'roles' ),
			'Plural' => __( 'Roles' ),
			'singular' => __( 'role' ),
			'Singular' => __( 'role' ),
		];
	}

	/**
		@brief		Return the non-conflicting model name in the sites model.
		@details	GenericModel method.
		@since		2018-11-21 11:24:53
	**/
	public static function model_key()
	{
		return 'roles';
	}

    /**
    	@brief		Handle the processing of the editor form.
    	@since		2018-11-21 16:56:53
    **/
    public function post_edit_form( $form, $model = null )
    {
    	$caps = $form->capabilities->get_post_value();

    	// If the user may not assign manage_network, prevent manage_network from being set or removed.
    	if ( ! app()->user()->may( 'manage_network' ) )
    	{
    		$model_caps = $model->capabilities->pluck( 'id' )->toArray();
    		// If the old caps have manage_network...
    		$manage_network = Capability::where( 'name', 'manage_network' )->first()->id;
    		if ( in_array( $manage_network, $model_caps ) )
    			if ( ! in_array( $manage_network, $caps ) )
    				$caps []= $manage_network;
    	}

    	$model->capabilities()->sync( $caps );
    }

	/**
		@brief		Return the name of the model as used in routes.
		@since		2020-01-15 15:41:03
	**/
	public static function route_model_key()
	{
		return 'role';
	}

    /**
    	@brief		Override this to return all roles that the user may edit.
    	@since		2018-12-02 20:25:23
    **/
    public function scopeActive( $query )
    {
    	// Active = all except network admin, which can never be changed.
    	return $query->whereHas( 'capabilities', function( $q )
    	{
    		return $q->whereNotIn( 'name', [ 'manage_network' ] );
    	} );
   		return $query;
    }

	/**
		@brief		Site this role is assigned to.
		@since		2018-11-15 21:09:04
	**/
	public function site()
	{
		return $this->belongsTo( 'App\Models\Sites\Site', 'site_id' );
	}

	/**
		@brief		Users assigned to this role.
		@since		2018-11-15 21:09:04
	**/
	public function users()
	{
		return $this->belongsToMany( 'App\Models\Users\User', 'user_roles', 'role_id', 'user_id' );
	}
}
