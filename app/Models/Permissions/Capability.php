<?php

namespace App\Models\Permissions;

/**
	@brief		A specific capability that is available to users.
	@details	Is contained in a role.
	@since		2018-11-15 20:20:00
**/
class Capability
	extends \App\Models\Model
{
	protected $table = 'capabilities';

	/**
		@brief		Properties that can be mass assigned.
		@since		2018-11-15 23:24:29
	**/
	public $fillable = [ 'name' ];

	/**
		@brief		Return a query of the assignable caps.
		@details	If you're not network admin, you can't assign that role, either.
		@since		2018-12-02 20:05:56
	**/
	public static function get_assignable()
	{
		$query = static::query();
		if ( ! app()->user()->may( 'manage_network' ) )
			$query->whereNotIn( 'name', [ 'manage_network' ] );
		$query->orderBy( 'name' );
		return $query;
	}

	/**
		@brief		The roles this cap is in.
		@since		2018-11-15 21:02:00
	**/
	public function roles()
	{
		return $this->belongsToMany( 'App\Models\Permissions\Role', 'role_capabilities', 'capability_id', 'role_id' );
	}
}
