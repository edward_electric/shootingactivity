<?php

namespace App\Models\Notices;

use App\Models\Sites\Site;
use App\Models\Users\User;

/**
	@brief		A notice on the site's notice board.
	@since		2018-12-23 14:58:18
**/
class Notice
	extends	\App\Models\Model
{
    use \App\Models\Traits\User_Notification;

	protected $table = 'notices';

    /**
    	@brief		The comment this notice is comprised of.
		@since		2018-12-23 14:58:18
    **/
    public function comment()
    {
    	return $this->belongsTo('App\Models\Comments\Comment', 'comment_id' );
    }

	/**
		@brief		Deleting this notice will delete the comment also.
		@since		2018-12-23 20:17:15
	**/
	public function delete()
	{
		$this->comment->delete();
		parent::delete();
	}

	/**
		@brief		Return all the users that can be notified.
		@details	Override this.
		@since		2018-12-23 23:01:19
	**/
	public function get_notifiable_users()
	{
		// Get only the active users.
		return $this->site->users()->active()->get();
	}

	/**
		@brief		Return the notification data unique to this model.
		@since		2018-12-23 22:46:26
	**/
	public static function get_user_notification_data()
	{
		return (object)[
			'mail_subject' => 'mail.notices.notification.subject',
			'mail_view' => 'mail.notices.notification.text',
			'user_notification_enabled_key' => 'email_new_notices',
			'user_notification_key' => 'email_new_notices_notified',
		];
	}

    /**
    	@brief		Return the notices on this site.
    	@since		2018-12-23 16:44:25
    **/
    public function scopeon_this_site( $query )
    {
    	return $query->where( 'site_id', app()->site()->id )
    		->orderBy( 'updated_at', 'desc' );
    }

    /**
    	@brief		Prune all old notices from this site.
    	@since		2018-12-23 21:34:13
    **/
    public static function prune( Site $site )
    {
    	$old_age = $site->get_meta( 'notice_board_days' );
    	if ( $old_age < 1 )
    		return;
    	$days = '-' . $old_age . ' days';
    	$notices = static::where( 'site_id', $site->id )
    		->whereDate( 'created_at', '<=', \Carbon\Carbon::parse( $days ) )
    		->get();
		app()->log()->debug( 'Pruning %s notices from %s %s', count( $notices ), $site->name, $site->id );
    	foreach( $notices as $notice )
    		$notice->delete();
    }

	/**
		@brief		Should this user be notified?
		@since		2018-12-23 22:49:15
	**/
	public function should_notify_user( User $user )
	{
		$comment_to_check = $this->comment;
		if ( $this->comment->replies->last() )
			$comment_to_check = $this->comment->replies->last();

		// Don't notify the moderator of his own activity;
		if ( $comment_to_check->user_id != $user->id )
			return true;
		app()->log()->debug( 'Not notify user %s %s of his own comment.', $user->name, $user->id );
		return false;
	}


	/**
		@brief		Site this model belongs to.
		@since		2018-12-23 14:58:18
	**/
	public function site()
	{
		return $this->belongsTo( 'App\Models\Sites\Site', 'site_id' );
	}
}
