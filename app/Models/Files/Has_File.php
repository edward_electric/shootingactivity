<?php

namespace App\Models\Files;

/**
	@brief		Convenience methods for handling file uploads to models.
	@details	This requires one of the file source traits.
	@since		2018-11-22 17:11:55
**/
trait Has_File
{
	/**
		@brief		Return an array with the default file options.
		@since		2018-11-22 17:16:30
	**/
	public function get_default_file_options()
	{
		$options = (object)[];

		$options->accept								= 'image/jpeg;capture=camera';			// What kind of images this file accepts.
		$options->delete_file_input_description		= '';
		$options->delete_file_input_label				= '';
		$options->delete_file_input_name				= 'delete_file';
		$options->file_is_image = false;

		// This is for files stored in the model's meta.
		$options->file_meta_key = 'file_id';					// The meta key which points to the file.
		$options->file_meta_source = null;						// Where should we address our meta get / set queries?

		// This is for files stored in the model itself.
		$options->file_property = 'file';						// Which property of the model points to the live file model.
		$options->file_property_column = 'file_id';			// The reference in the table that points to the file.

		$options->upload_file_input_description		= '';
		$options->upload_file_input_label				= '';
		$options->upload_file_input_name				= 'file';
		return $options;
	}

	/**
		@brief		Modify the form with file inputs.
		@since		2018-11-22 16:18:43
	**/
	public function get_file_form( $form )
	{
		if ( app()->site()->uses_images() )
		{
			$options = $this->get_file_options();
			$form->has_file_form = true;

			$file = $this->get_file_model();

			if ( $file )
			{
				if( $options->file_is_image )
				{
					$view = view( 'image.full', [ 'file' => $file ] );
					$form->markup( 'm_file' )
						->p( $view );
				}
				$form->checkbox( $options->delete_file_input_name )
					->description( $options->delete_file_input_description )
					->label( $options->delete_file_input_label );
			}
			else
				$form->file( $options->upload_file_input_name )
					->description( $options->upload_file_input_description )
					->accept( $options->accept )
					->label( $options->upload_file_input_label )
					->css_class( 'sa_ajax_upload' )
					->set_attribute( 'data-image_size', '1600' );
		}
	}

	/**
		@brief		Return a single option from the options.
		@since		2023-02-27 21:31:59
	**/
	public function get_file_option( $option_key )
	{
		$options = $this->get_file_options();
		return $options->$option_key;
	}

	/**
		@brief		post_file_form
		@since		2018-11-22 16:31:55
	**/
	public function post_file_form( $form )
	{
		$options = $this->get_file_options();
		if ( app()->site()->uses_images() )
		{
			$file = $this->get_file_model();
			if ( $file )
			{
				if ( $form->input( $options->delete_file_input_name )->is_checked() )
				{
					$this->delete_file( $options );
				}
			}
			else
			{
				// Is there a nicely resized input available?
				$file_name_input = $form->hidden_input( 'file_name' );
				$file_name = $file_name_input->get_post_value();
				$handle_image = true;

				if ( $file_name != '' )
				{
					$file_data_input = $form->hidden_input( 'file_data' );
					$file_data = $file_data_input->get_post_value();
					// Save the resized file to disk.
					$file_data = base64_decode( $file_data );
					$tmp_name = tempnam( sys_get_temp_dir(), 'sa_file_resized' );
					file_put_contents( $tmp_name, $file_data );
					$this->store_file( $tmp_name, $file_name );
				}
				else
				{
					$file = $form->input( $options->upload_file_input_name )->get_post_value();
					if ( $file )
						if ( $file->tmp_name != '' )
							$this->store_file( $file->tmp_name, $file->name );
				}
			}
		}
	}
}
