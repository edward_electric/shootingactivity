<?php

namespace App\Models\Files;

use App\Models\Files\File;

/**
	@brief		The file is stored as a property in the model.
	@details	$modelinstance->file;
	@see		get_default_file_options()->file_property
	@see		get_default_file_options()->file_property_column
	@since		2023-02-27 19:55:42
**/
trait File_Is_Model_Property
{
	/**
		@brief		Convenience method to delete the model's file.
		@since		2018-12-27 01:16:58
	**/
	public function delete_file( $options )
	{
		$options = $this->get_file_options();
		$fp = $options->file_property;
		$fpc = $options->file_property_column;

		$this->$fp->delete();
		$this->$fpc = null;
		$this->save();
		$this->refresh();	// To catch that the file is now missing.
	}

	/**
		@brief		Retrieve the file model from this model.
		@since		2023-02-25 22:24:00
	**/
	public function get_file_model()
	{
		$options = $this->get_file_options();
		$fp = $options->file_property;
		return $this->$fp;
	}

    /**
    	@brief		Put the uploaded file into storage.
    	@since		2018-11-22 16:43:00
    **/
    public function store_file( $path_on_disk, $filename )
    {
		$options = $this->get_file_options();
		$fpc = $options->file_property_column;

		$storage_path =
			'files'
			. DIRECTORY_SEPARATOR
			. $this->get_site_id()
			. DIRECTORY_SEPARATOR
			. $this->table
			. DIRECTORY_SEPARATOR
			. $this->id
			. DIRECTORY_SEPARATOR
			. $filename;
    	$file = File::store( $path_on_disk, $storage_path );
    	$this->$fpc = $file->id;
    	$this->save();
    	return $this;
    }
}
