<?php

namespace App\Models\Files;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

/**
	@brief		A file in storage.
	@since		2018-11-18 12:20:25
**/
class File
	extends \App\Models\Model
{
    use \App\Models\Traits\Meta;

	protected $table = 'files';

	/**
		@brief		Delete this file.
		@since		2018-11-18 14:30:15
	**/
	public function delete()
	{
		// First delete it from storage.
		Storage::delete( $this->get_path() );

		// Maybe delete the thumbnail.
		$thumbnail_filename = $this->get_meta( 'thumbnail_filename' );
		if ( $thumbnail_filename )
		{
			$path = sprintf( '%s/%s', $this->path, $thumbnail_filename );
			Storage::delete( $path );
		}

		// Delete all meta.
		$this->meta()->delete();
		// And now from the database.
		parent::delete();
	}

	/**
		@brief		Display this file to the browser.
		@since		2018-11-18 14:52:55
	**/
	public function display( $size = 'full' )
	{
		$filename = false;
		switch( $size )
		{
			case 'thumbnail':
				$this->maybe_generate_thumbnail();
				$filename = $this->get_meta( 'thumbnail_filename' );
				break;
		}
		if ( ! $filename )
			$filename = $this->filename;
		$this->filename = $filename;
		$headers = [];
		$headers[ 'Content-Disposition' ] = sprintf( 'inline; filename="%s"', $filename );
		$headers[ 'Cache-Control' ] = 'private, max-age=86400';
		$last_modified = Storage::lastModified( $this->path . '/' . $this->filename );
		$headers[ 'Last-Modified' ] = gmdate( 'D, d M Y H:i:s T', $last_modified );
		return $this->do_response( $headers );
	}

	/**
		@brief		do_response
		@since		2018-11-18 15:21:12
	**/
	public function do_response( $headers = [] )
	{
		$data = Storage::get( $this->path . '/' . $this->filename );
		$response = response()->make( $data, 200 );

		$headers[ 'Content-Length' ] = strlen( $data );
		$headers[ 'Content-Type' ] = $this->get_meta( 'mimetype' );

		foreach( $headers as $key => $value )
			$response->header( $key, $value );

		return $response;
	}

	/**
		@brief		Download this file to the browser.
		@since		2018-11-18 14:52:55
	**/
	public function download()
	{
		$headers = [];
		$headers[ 'Content-Disposition' ] = sprintf( 'attachment; filename="%s"', $this->filename );
		return $this->do_response( $headers );
	}

	/**
		@brief		Generate a secret key.
		@since		2018-11-18 12:21:41
	**/
	public function get_key()
	{
		$key = config( 'app.key' );
		$key .= $this->id;
		$key .= session()->getId();
		$key = hash( 'sha512', $key );
		return $key;
	}

	/**
		@brief		Get the inline url for this file that is valid only for this current session.
		@since		2018-11-18 12:20:59
	**/
	public function get_inline_url( $size = 'full' )
	{
		return route( 'file_inline', [ 'id' => $this->id, 'key' => $this->get_key(), 'size' => $size ] );
	}

	/**
		@brief		Return the complete path to the file in the storage dir.
		@since		2018-11-18 13:57:24
	**/
	public function get_path()
	{
		return $this->path . '/' . $this->filename;
	}

	/**
		@brief		Maybe generate a thumbnail for this image.
		@since		2018-11-22 18:40:14
	**/
	public function maybe_generate_thumbnail()
	{
		// Is this an image?
		if ( strpos( $this->get_meta( 'mimetype' ), 'image/' ) === false )
			return false;
		// We can't thumbnail svgs.
		if ( strpos( $this->get_meta( 'mimetype' ), 'image/svg' ) !== false )
			return false;
		$meta_key = 'thumbnail_filename';
		$thumbnail_filename = $this->get_meta( $meta_key );
		if ( $thumbnail_filename != '' )
			return;

		$ext = pathinfo ( $this->filename, \PATHINFO_EXTENSION );
		$thumbnail_filename = sprintf( '%s.tn.%s', $this->filename, $ext );

		// Get the image
		$data = Storage::get( $this->path . '/' . $this->filename );
		$image = Image::make( $data );
		// Restore.
		$image->resize( 300, null, function ($constraint) {
			$constraint->aspectRatio();
		});

		// Store back to disk.
		$output_filename = tempnam( sys_get_temp_dir(), 'sa_thumbnail' );
		$output_filename .= '.' . $ext;
		$image->save( $output_filename, 75 );

		// And put the thumbnail into storage.
		$path_in_storage = sprintf( '%s/%s', $this->path, $thumbnail_filename );
		Storage::put( $path_in_storage, file_get_contents( $output_filename ) );

		$this->set_meta( $meta_key, $thumbnail_filename );
	}

    /**
    	@brief		Return the meta owner.
    	@since		2018-11-16 01:32:51
    **/
    public function meta_owner()
    {
    	return 'file';
    }

	/**
		@brief		Store this file into this directory.
		@since		2018-11-18 12:45:11
	**/
	public static function store( $path_on_disk, $path_in_storage )
	{
		Storage::put( $path_in_storage, file_get_contents( $path_on_disk ) );
		$file = new static();
		$file->filename = basename( $path_in_storage );
		$file->path = dirname( $path_in_storage );
		$file->save();

		$mimetype = false;
		if ( strpos( $path_in_storage, '.svg' ) !== false )
			$mimetype = 'image/svg+xml';
		if ( ! $mimetype )
			$mimetype = mime_content_type( $path_on_disk );
		$file->set_meta( 'mimetype', $mimetype );
		return $file;
	}
}
