<?php

namespace App\Models\Files;

use App\Models\Files\File;

/**
	@brief		The file is stored in the model's meta.
	@since		2023-02-27 19:55:27
**/
trait File_Is_Model_Meta
{
	/**
		@brief		Convenience method to delete the model's file.
		@since		2018-12-27 01:16:58
	**/
	public function delete_file( $options )
	{
		$file = $this->get_file_model();
		$file->delete();

		$file_meta_key = $this->get_file_option( 'file_meta_key' );
		$source = $this->get_file_meta_source();
		$source->delete_meta( $file_meta_key );
	}

	/**
		@brief		Return the source we query for the file meta key.
		@details	Return either the set option or this model itself.
		@since		2023-02-27 21:30:22
	**/
	public function get_file_meta_source()
	{
		$file_meta_source = $this->get_file_option( 'file_meta_source' );
		if ( ! $file_meta_source )
			$file_meta_source = $this;
		return $file_meta_source;
	}

	/**
		@brief		Retrieve the file model from this model.
		@since		2023-02-25 22:24:00
	**/
	public function get_file_model()
	{
		$file_meta_key = $this->get_file_option( 'file_meta_key' );
		$source = $this->get_file_meta_source();
		$file_id = $source->get_meta( $file_meta_key );
		return File::find( $file_id );
	}

    /**
    	@brief		Put the uploaded file into storage.
    	@since		2018-11-22 16:43:00
    **/
    public function store_file( $path_on_disk, $filename )
    {
		$storage_path =
			'files'
			. DIRECTORY_SEPARATOR
			. app()->site()->id
			. DIRECTORY_SEPARATOR
			. $this->table
			. DIRECTORY_SEPARATOR
			. $this->id
			. DIRECTORY_SEPARATOR
			. $filename;
    	$file = File::store( $path_on_disk, $storage_path );

		$file_meta_key = $this->get_file_option( 'file_meta_key' );
		$source = $this->get_file_meta_source();
		$source->set_meta( $file_meta_key, $file->id );

    	return $this;
    }
}
