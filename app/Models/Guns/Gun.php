<?php

namespace App\Models\Guns;

use App\Models\Files\File;
use App\Models\Guns\Caliber;
use App\Models\Guns\Group;

/**
	@brief		A gun that one uses with which to shoot.
	@since		2018-11-17 19:22:20
**/
class Gun
	extends	\App\Models\Generic\User
{
	use \App\Models\Files\Has_File;
	use \App\Models\Files\File_Is_Model_Property;
	use \App\Models\Traits\Archivable;
    use \App\Models\Traits\Meta;

	protected $table = 'guns';

    /**
    	@brief		Gun caliber: 22lr, 9mm, etc.
    	@since		2018-11-17 19:23:24
    **/
    public function caliber()
    {
    	return $this->belongsTo('App\Models\Guns\Caliber', 'caliber_id' );
    }

    /**
    	@brief		Can this model be deleted?
    	@since		2018-11-23 21:45:59
    **/
    public function can_be_deleted()
    {
    	return \App\Models\Activities\Activity::where( 'user_id', $this->user->id )
    		->where( 'gun_id', $this->id )->count() < 1;
    }

    /**
    	@brief		Also delete the image.
    	@since		2018-11-18 15:46:09
    **/
    public function delete()
    {
    	if ( $this->file )
    		$this->file->delete();
		parent::delete();
	}

    /**
    	@brief		The optional image file of this gun.
    	@since		2018-11-18 13:33:06
    **/
    public function file()
    {
    	return $this->hasOne('App\Models\Files\File', 'id', 'file_id' );
    }

    /**
    	@brief		Modify the edit form.
    	@since		2018-11-21 16:53:46
    **/
    public function get_edit_form( $form, $model = null )
    {
		$form->caliber = $form->select( 'caliber' )
			->label( __( 'Caliber' ) )
			->opts( Caliber::on_this_site()->active()->get()->as_options() )
			->required();
		if ( $model )
			$form->caliber->value( $model->caliber_id );

		$form->group = $form->select( 'group' )
			->label( __( 'Group' ) )
			->opt( '', 'n/a' )
			->opts( Group::on_this_site()->active()->get()->as_options() );
		if ( $model )
			$form->group->value( $model->group_id );

			$this->get_file_form( $form );
    }

	/**
		@brief		Return an array with customized file options.
		@details	Add to this method to customize the options.
		@since		2018-11-22 17:16:42
	**/
	public function get_file_options()
	{
		$options = $this->get_default_file_options();
		$options->delete_file_input_description		= __( 'Delete the current photo so you can upload a new one.' );
		$options->delete_file_input_label				= __( 'Delete this photo' );
		$options->file_is_image = true;
		$options->upload_file_input_description		= __( 'Take a photo of your gun. A side view is ideal.' );
		$options->upload_file_input_label				= __( 'Photo' );
		return $options;
	}

    /**
    	@brief		Convenience method to return the group name, if any.
    	@since		2018-11-17 22:18:02
    **/
    public function get_group_name()
    {
    	if( $this->group_id < 1 )
    		return '';
    	return $this->group->name;
    }

    /**
    	@brief		Gun group: A, B, C, etc.
    	@since		2018-11-17 19:23:24
    **/
    public function group()
    {
    	return $this->belongsTo('App\Models\Guns\Group', 'group_id' );
    }

	/**
		@brief		The model's preferred Fontawesome icon.
		@details	GenericModel method.
		@since		2018-11-21 11:30:55
	**/
	public static function icon()
	{
		return 'fa-solid fa-gun';
	}

	/**
		@brief		Return an object containing all of the model's labels.
		@details	GenericModel method.
		@since		2018-11-21 11:19:34
	**/
	public static function labels()
	{
		return (object)[
			'name_input_description' => __( 'The make and model of the gun.' ),
			'name_input_placeholder' => __( 'Ruger Mark IV' ),
			'plural' => __( 'guns' ),
			'Plural' => __( 'Guns' ),
			'singular' => __( 'gun' ),
			'Singular' => __( 'Gun' ),
		];
	}

    /**
    	@brief		Return the meta owner.
    	@since		2018-11-16 01:32:51
    **/
    public function meta_owner()
    {
    	return 'gun';
    }

	/**
		@brief		Return the non-conflicting model name in the owner model.
		@details	GenericModel method.
		@since		2018-11-21 11:24:53
	**/
	public static function model_key()
	{
		return 'guns';
	}

    /**
    	@brief		Handle the processing of the editor form.
    	@since		2018-11-21 16:56:53
    **/
    public function post_edit_form( $form, $model = null )
    {
		$model->set_caliber( $form->caliber->get_filtered_post_value() );
		$model->set_group( $form->group->get_filtered_post_value() );
		$model->post_file_form( $form );
    }

	/**
		@brief		Return the name of the model as used in routes.
		@since		2020-01-15 15:41:03
	**/
	public static function route_model_key()
	{
		return 'gun';
	}

    /**
    	@brief		Set the caliber.
    	@since		2018-11-17 21:53:22
    **/
    public function set_caliber( $caliber_id )
    {
    	if ( $caliber_id < 1 )
    		$caliber_id = null;
    	$this->caliber_id = $caliber_id;
    	return $this;
    }

    /**
    	@brief		Set the group.
    	@since		2018-11-17 21:53:22
    **/
    public function set_group( $group_id )
    {
    	if ( $group_id < 1 )
    		$group_id = null;
    	$this->group_id = $group_id;
    	return $this;
    }

	/**
		@brief		Return an object containing all of our planned views.
		@since		2018-11-21 11:19:34
	**/
	public static function views( $views = [] )
	{
		$views = parent::views( $views );
		$views->index = 'guns.index';		// To display a helpful link back to the dashboard.
		$views->index_table_after_name = 'guns.index_table_after_name';
		$views->index_table_th = 'guns.index_table_th';
		$views->index_table_td = 'guns.index_table_td';
		return $views;
	}
}
