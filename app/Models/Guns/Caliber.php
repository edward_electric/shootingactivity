<?php

namespace App\Models\Guns;

use App\Models\Guns\Gun;

/**
	@brief		A caliber of a gun.
	@since		2018-11-17 18:29:11
**/
class Caliber
	extends	\App\Models\Generic\Site
{
    use \App\Models\Traits\Meta;

	protected $table = 'gun_calibers';

	/**
		@brief		Can be deleted?
		@since		2018-11-22 23:05:19
	**/
	public function can_be_deleted()
	{
		return ( $this->gun()->count() == 0 );
	}

    /**
    	@brief		Which guns use this caliber.
    	@since		2018-11-22 23:07:37
    **/
    public function gun()
    {
        return $this->hasOne( 'App\Models\Guns\Gun', 'caliber_id' );
    }

	/**
		@brief		The model's preferred Fontawesome icon.
		@details	GenericModel method.
		@since		2018-11-21 11:30:55
	**/
	public static function icon()
	{
		return 'fa-solid fa-bullseye';
	}

	/**
		@brief		Return an object containing all of the model's labels.
		@details	GenericModel method.
		@since		2018-11-21 11:19:34
	**/
	public static function labels()
	{
		return (object)[
			'name_input_description' => __( 'The name of the caliber of a gun.' ),
			'name_input_placeholder' => '.22 LR',
			'plural' => __( 'gun calibers' ),
			'Plural' => __( 'Gun calibers' ),
			'singular' => __( 'gun caliber' ),
			'Singular' => __( 'Gun caliber' ),
		];
	}

    /**
    	@brief		Return the meta owner.
    	@since		2018-11-16 01:32:51
    **/
    public function meta_owner()
    {
    	return 'gun_caliber';
    }

	/**
		@brief		Return the non-conflicting model name in the sites model.
		@details	GenericModel method.
		@since		2018-11-21 11:24:53
	**/
	public static function model_key()
	{
		return 'gun_calibers';
	}

	/**
		@brief		Return the name of the model as used in routes.
		@since		2020-01-15 15:41:03
	**/
	public static function route_model_key()
	{
		return 'gun_caliber';
	}
}
