<?php

namespace App\Models\Guns;

/**
	@brief		A gun group, which should be "class" but we are restricted by PHP.
	@since		2018-11-17 19:15:43
**/
class Group
	extends	\App\Models\Generic\Site
{
    use \App\Models\Traits\Meta;

	protected $table = 'gun_groups';

	/**
		@brief		Can be deleted?
		@since		2018-11-22 23:05:19
	**/
	public function can_be_deleted()
	{
		return ( $this->gun()->count() == 0 );
	}

	/**
		@brief		The model's preferred Fontawesome icon.
		@details	GenericModel method.
		@since		2018-11-21 11:30:55
	**/
	public static function icon()
	{
		return 'fa-solid fa-tags';
	}

    /**
    	@brief		Modify the edit form.
    	@since		2018-11-21 16:53:46
    **/
    public function get_edit_form( $form, $model = null )
    {
		if ( $model )
			$value =$model->get_meta( 'string_size', 5 );
		$form->string_size = $form->number( 'string_size' )
			->description( __( 'messages.gun.group.string_size.description' ) )
			->label( __( 'messages.gun.group.string_size.label' ) )
			->min( 1, 100 )
			->required()
			->value( $value );

		if ( $model )
			$value =$model->get_meta( 'string_score_count', 5 );
		$form->string_score_count = $form->number( 'string_score_count' )
			->description( __( 'messages.gun.group.string_score_count.description' ) )
			->label( __( 'messages.gun.group.string_score_count.label' ) )
			->min( 1, 100 )
			->required()
			->value( $value );
    }


	/**
		@brief		How many rounds is a string, for the scoring?
		@details	The default is 5. Black powder want 13.
		@since		2022-07-22 23:47:12
	**/
	public function get_string_size()
	{
		return $this->get_meta( 'string_size', 5 );
	}

    /**
    	@brief		Which guns use this caliber.
    	@since		2018-11-22 23:07:37
    **/
    public function gun()
    {
        return $this->hasOne( 'App\Models\Guns\Gun', 'caliber_id' );
    }

	/**
		@brief		Return an object containing all of the model's labels.
		@details	GenericModel method.
		@since		2018-11-21 11:19:34
	**/
	public static function labels()
	{
		return (object)[
			'name_input_description' => __( 'The group classification of a gun.' ),
			'name_input_placeholder' => __( 'A / B / C / R / Black powder' ),
			'plural' => __( 'gun groups' ),
			'Plural' => __( 'Gun groups' ),
			'singular' => __( 'gun group' ),
			'Singular' => __( 'Gun group' ),
		];
	}

    /**
    	@brief		Return the meta owner.
    	@since		2018-11-16 01:32:51
    **/
    public function meta_owner()
    {
    	return 'gun_group';
    }

	/**
		@brief		Return the non-conflicting model name in the owner model.
		@details	GenericModel method.
		@since		2018-11-21 11:24:53
	**/
	public static function model_key()
	{
		return 'gun_groups';
	}

    /**
    	@brief		Handle the processing of the editor form.
    	@since		2018-11-21 16:56:53
    **/
    public function post_after_edit_form( $form, $model = null )
    {
    	$string_size = $form->string_size->get_filtered_post_value();
		$model->set_meta( 'string_size', $string_size );

		$string_score_count = $form->string_score_count->get_filtered_post_value();
		// We can't count more scores than the string size.
		$string_score_count = min( $string_score_count, $string_size );
		$model->set_meta( 'string_score_count', $string_score_count );
    }

	/**
		@brief		Return the name of the model as used in routes.
		@since		2020-01-15 15:41:03
	**/
	public static function route_model_key()
	{
		return 'gun_group';
	}
}
