<?php

namespace App\Models\Sites;

use App\Models\Permissions\Capability;
use App\Models\Permissions\Role;
use App\Models\Categories\Category;
use App\Models\Users\User;
use App\Models\Guns\Caliber;
use App\Models\Guns\Group;

/**
	@brief		A "site" is a collection of users and their settings and activities.
	@since		2018-11-15 18:54:47
**/
class Site
	extends \App\Models\Generic\No_Owner
{
    use \App\Models\Traits\Meta;

	protected $table = 'sites';

	/**
		@brief		Can this site be archived?
		@since		2018-11-19 12:46:30
	**/
	public function can_be_archived()
	{
		return app()->user()->site->id != $this->id;
	}

	/**
		@brief		Can this site be deleted?
		@since		2018-11-19 12:46:30
	**/
	public function can_be_deleted()
	{
		return app()->user()->site->id != $this->id;
	}

    /**
    	@brief		Categories this site offers.
    	@since		2018-11-18 21:42:12
    **/
    public function categories()
    {
		return $this->hasMany( 'App\Models\Categories\Category', 'site_id' );
    }

	/**
		@brief		Create a role and assign it to this site.
		@since		2018-11-15 20:34:46
	**/
	public function create_role( $name )
	{
		$role = new Role();
		$role->name = $name;
		$role->site()->associate( $this );
		$role->save();
		return $role;
	}

	/**
		@brief		Create a category and assign it to this site.
		@since		2018-11-15 20:34:46
	**/
	public function create_category( $name )
	{
		$category = new Category();
		$category->name = $name;
		$category->site()->associate( $this );
		$category->save();
		return $category;
	}

	/**
		@brief		Delete everything related to this site.
		@since		2018-11-24 22:36:20
	**/
	public function delete()
	{
		foreach( [
			'categories',
			'gun_calibers',
			'gun_groups',
			'meta',
			'notices',
			'roles',
			'users',
		] as $type )
			foreach( $this->$type as $model )
					$model->delete();
		parent::delete();
	}

    /**
    	@brief		Modify the edit form.
    	@since		2018-12-09 10:31:19
    **/
    public function get_edit_form( $form, $model = null )
    {
    	if ( ! app()->user()->may( 'manage_network' ) )
    		return;
    	if ( $model->id > 0 )
    	{
			// Can't switch to the same site.
			if ( app()->user()->site->id == $model->id )
				return;
    	}
		$form->switch_to = $form->checkbox( 'switch_to' )
			->description( __( 'Switch the membership of your user to this site.' ) )
			->label( __( 'Switch to' ) );
    }

    /**
    	@brief		Get the default value of a meta key.
    	@details	Override this if your model should return default values other than null.
    	@since		2018-11-16 23:30:00
    **/
    public function get_meta_default( $key, $default = null )
    {
    	$defaults = [
    		/**
    			@brief		Allow mass import by moderators.
    			@since		2023-03-05 17:08:33
    		**/
    		'allow_mass_import' => false,
    		/**
    			@brief		Enable upload of images to guns and activities?
    			@since		2018-11-16 23:32:01
    		**/
    		'images' => true,
    		/**
    			@brief		How many moderators are required to accept an image.
    			@since		2018-11-16 23:31:47
    		**/
    		'moderators' => 0,
    		/**
    			@brief		Are the notices enabled so that people can send read and write notices?
    			@since		2018-12-22 20:28:09
    		**/
    		'notice_board' => true,
    		/**
    			@brief		How many days to keep messages on the notice board before deleting them.
    			@since		2018-12-22 20:27:03
    		**/
    		'notice_board_days' => 14,
    		/**
    			@brief		Is the social part of SA enabled?
    			@since		2018-12-22 20:16:33
    		**/
    		'socialization' => true,
    		/**
    			@brief		Enable scoring in activities.
    			@since		2018-11-25 22:55:10
    		**/
    		'scoring' => true,
    		/**
    			@brief		Enable tracking of X (inner 10s) in the scoring?
    			@since		2021-01-11 16:13:03
    		**/
    		'scoring_x' => true,
    	];
    	if ( isset( $defaults[ $key ] ) )
    		return $defaults[ $key ];
    	return $default;
    }

    /**
    	@brief		Return all users with this capability.
    	@since		2018-12-09 19:14:14
    **/
    public function get_users_with_capability( $capability )
    {
    	// I wish we could use a site->capabilities()->roles() type query, but that doesn't seem possible.
    	$role_ids = Role::whereHas( 'capabilities', function( $query ) use ( $capability )
    	{
    		return $query->where( 'name', $capability );
    	} )->get()->pluck( 'id' );
    	$users = $this->users()->whereHas( 'roles', function( $query ) use ( $role_ids )
    	{
    		return $query->whereIn( 'roles.id', $role_ids );
    	} )->get();
    	return $users;
    }

    /**
    	@brief		Gun calibers on this site.
    	@since		2018-11-20 23:39:23
    **/
    public function gun_calibers()
    {
		return $this->hasMany( 'App\Models\Guns\Caliber', 'site_id' );
    }

    /**
    	@brief		Gun groups on this site.
    	@since		2018-11-20 23:39:23
    **/
    public function gun_groups()
    {
		return $this->hasMany( 'App\Models\Guns\Group', 'site_id' );
    }

    /**
    	@brief		Convenience method to return whether the site has more than one category.
    	@since		2018-11-23 21:40:28
    **/
    public function has_categories()
    {
    	return $this->categories()->active()->count() > 1;
    }

    /**
    	@brief		Convenience method to check whether an option is set.
    	@since		2018-11-25 16:14:08
    **/
    public function has_enabled( $setting )
    {
    	return $this->get_meta( $setting, false );
    }


	/**
		@brief		The model's preferred Fontawesome icon.
		@details	GenericModel method.
		@since		2018-11-21 11:30:55
	**/
	public static function icon()
	{
		return 'fa-solid fa-network-wired';
	}

	/**
		@brief		Run the initial setup for this site.
		@since		2018-11-16 09:13:55
	**/
	public function install( $options = [] )
	{
		$options = (object) $options;

		// -----------
		// PERMISSIONS
		// -----------

    	// USER

    	$user_caps = [
			'activities',
			'change_password',
			'notice_board',
			'socialize',
    	];
    	$role = $this->create_role( __( 'User' ) );
    	$role->add_capabilities( $user_caps );

    	// MODERATOR

    	$moderator_caps = [
			'moderate_activities',
    	];
    	$role = $this->create_role( __( 'Moderator' ) );
    	$role->add_capabilities( $user_caps );
    	$role->add_capabilities( $moderator_caps );

    	// ADMIN

    	$admin_caps = [
			'access_admin',
			'access_site_reports',
			'manage_site',
			'manage_users',
    	];
    	$role = $this->create_role( __( 'Administrator' ) );
    	// Admins can do moderator + more.
    	$role->add_capabilities( $user_caps );
    	$role->add_capabilities( $moderator_caps );
    	$role->add_capabilities( $admin_caps );

    	// NETWORK ADMIN

    	$role = $this->create_role( 'Network Admin' );
    	// If you can manage the network, you can do anything you want.
    	$role->add_capability( 'manage_network' );

    	$this->create_category( __( 'Practice' ) );
    	$this->create_category( __( 'Club competition' ) );
    	$this->create_category( __( 'External competition' ) );

		// ---------
		// GUN STUFF
		// ---------
		foreach ( [
			[ 'name' => '.22 LR' ],
			[ 'name' => '.32 ACP' ],
			[ 'name' => '9mm Luger' ],
			[ 'name' => __( 'messages.gun.group.Black_powder' ) ],
		] as $model )
		{
			$model[ 'site_id' ] = $this->id;
			Caliber::firstOrCreate( $model );
		}

		foreach ( [
			[ 'name' => 'A' ],
			[ 'name' => 'B' ],
			[ 'name' => 'C' ],
			[ 'name' => 'R' ],
			[ 'name' => __( 'messages.gun.group.Black_powder' ),
				'meta' => [
					'string_size' => '13',
					'string_score_count' => '10',
				],
			],
		] as $model )
		{
			$model_to_create = [
				'name' => $model[ 'name' ],
				'site_id' => $this->id,
			];
			$created_model = Group::firstOrCreate( $model_to_create );

			if ( isset( $model[ 'meta' ] ) )
			{
				foreach( $model[ 'meta' ] as $key => $value )
					$created_model->set_meta( $key, $value );
			}
		}
	}

	/**
		@brief		Return an object containing all of the model's labels.
		@details	GenericModel method.
		@since		2018-11-21 11:19:34
	**/
	public static function labels()
	{
		return (object)[
			'name_input_description' => __( 'The name of the club or organisation using Shooting Activity.' ),
			'name_input_placeholder' => __( 'Limhamn Shooting Club' ),
			'plural' => __( 'sites' ),
			'Plural' => __( 'Sites' ),
			'singular' => __( 'site' ),
			'Singular' => __( 'Site' ),
		];
	}

    /**
    	@brief		Return the meta owner.
    	@since		2018-11-16 01:32:51
    **/
    public function meta_owner()
    {
    	return 'site';
    }

    /**
    	@brief		Migrate a user to this site.
    	@since		2018-11-19 22:41:53
    **/
    public function migrate_user( User $user )
    {
    	// Load the user's current roles.
    	$old_roles = $user->roles;
    	// Remove current roles.
    	$user->roles()->sync( [] );
    	// Switch the user's site.
    	$user->site()->associate( $this );
    	$user->save();
    	$user->refresh();
    	// Assign new roles.
    	foreach( $old_roles as $role )
    		$user->assign_role( $role->name );
    }

	/**
		@brief		The key for this model.
		@details	GenericModel method.
		@since		2018-11-21 11:24:53
	**/
	public static function model_key()
	{
		return 'sites';
	}

    /**
    	@brief		Notices on the notice board.
    	@since		2018-12-24 12:00:01
    **/
    public function notices()
    {
		return $this->hasMany( 'App\Models\Notices\Notice', 'site_id' );
    }

    /**
    	@brief		Handle the processing of the editor form.
    	@since		2018-12-09 10:33:12
    **/
    public function post_after_edit_form( $form )
    {
    	if ( ! isset( $form->switch_to ) )
    		return;

		if ( $form->switch_to->get_post_value() )
		{
			$this->migrate_user( app()->user() );
			app()->alerts()->success()->set_message( __( 'Welcome to :sitename!', [ 'sitename' => $this->name ] ) );
		}
    }

    /**
    	@brief		Which roles are available on this site.
    	@since		2018-11-15 23:33:36
    **/
    public function roles()
    {
		return $this->hasMany( 'App\Models\Permissions\Role', 'site_id' );
    }

	/**
		@brief		Return the name of the model as used in routes.
		@since		2020-01-15 15:41:03
	**/
	public static function route_model_key()
	{
		return 'site';
	}

    /**
    	@brief		Automatically install after creating a new site.
    	@details	Else permissions will be broken.
    	@since		2018-11-24 22:01:42
    **/
    public function save( array $options = [] )
    {
    	parent::save( $options );
    	if ( $this->wasRecentlyCreated )
    	{
    		$this->install();
    	}
    }

    /**
    	@brief		Return the statistics helper for this site.
    	@since		2018-11-27 14:48:48
    **/
    public function statistics()
    {
    	return new Statistics( $this );
    }

    /**
    	@brief		Return the theme class.
    	@since		2023-02-25 17:27:04
    **/
    public function theme()
    {
    	return new Theme\Theme( $this );
    }

    /**
    	@brief		Users on this site.
    	@since		2018-11-19 17:08:19
    **/
    public function users()
    {
		return $this->hasMany( 'App\Models\Users\User', 'site_id' );
    }

    /**
    	@brief		Convenience method to query whether the site uses images.
    	@since		2018-11-17 21:06:34
    **/
    public function uses_images()
    {
    	return $this->get_meta( 'images' ) == true;
    }

    /**
    	@brief	Convenience method to return whether the site uses moderation.
    	@since		2018-11-18 18:55:58
    **/
    public function uses_moderation()
    {
    	return $this->get_meta( 'moderators' ) > 0;
    }

	/**
		@brief		Return an object containing all of our planned views.
		@since		2018-11-21 11:19:34
	**/
	public static function views( $views = [] )
	{
		$views = parent::views();
		$views->index_actions = 'sites.index_actions';
		return $views;
	}
}
