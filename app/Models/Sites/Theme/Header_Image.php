<?php

namespace App\Models\Sites\Theme;

use App\Models\Sites\Site;

/**
	@brief		The header image.
	@since		2023-02-25 20:32:19
**/
class Header_Image
	extends	Site
{
	use \App\Models\Files\Has_File;
	use \App\Models\Files\File_Is_Model_Meta;

	/**
		@brief		Load the Header_Image model using this site model.
		@since		2023-02-27 21:26:37
	**/
	public static function from_site( Site $site )
	{
		$r = static::findOrFail( $site->id );
		$r->site = $site;
		return $r;
	}

	/**
		@brief		Return an array with customized file options.
		@details	Add to this method to customize the options.
		@since		2018-11-22 17:16:42
	**/
	public function get_file_options()
	{
		$options = $this->get_default_file_options();

		$options->accept								= 'image';				// What kind of images this file accepts.

		$options->file_is_image = true;
		$options->delete_file_input_description	= __( 'The logo is visible on the dashboard, instead of the Shooting Activity text. A wide, short image is recommended.' );
		$options->delete_file_input_label			= __( 'Remove the logo' );
		$options->upload_file_input_description	= __( 'The logo is visible on the dashboard, instead of the Shooting Activity text. A wide, short image is recommended.' );
		$options->upload_file_input_label			= __( 'Front page logo' );

		$options->file_meta_key = 'file_id';					// The meta key which points to the file.
		$options->file_meta_source = $this->site;						// Where should we address our meta get / set queries?

		return $options;
	}
}
