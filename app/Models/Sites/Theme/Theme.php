<?php

namespace App\Models\Sites\Theme;

use App\Models\Sites\Site;

/**
	@brief		The site's theme.
	@since		2023-02-25 17:25:39
**/
class Theme
{
	/**
		@brief		The default values.
		@since		2023-02-25 17:33:00
	**/
	public $defaults = [
		'theme_color_background' => '#FFFFFF',
		'theme_color_primary' => '#148516',
		'theme_color_text' => '#212529',
	];

	/**
		@brief		The site we belong to.
		@since		2023-02-25 17:30:08
	**/
	public Site $site;

	/**
		@brief		Constructor.
		@since		2023-02-25 17:29:50
	**/
	public function __construct( Site $site )
	{
		$this->site = $site;
	}

	/**
		@brief		Delete this key.
		@since		2023-02-25 17:42:55
	**/
	public function delete( $key )
	{
    	$this->site->delete_meta( $key );
    	return $this;
	}

    /**
    	@brief		Return the theme defaults.
    	@since		2023-02-25 11:13:43
    **/
    public function get_defaults()
    {
    	return (object) $this->defaults;
    }

    /**
    	@brief		Return a theme setting, or a collection of all of them.
    	@since		2023-02-25 17:26:10
    **/
    public function get( $key = false )
    {
    	if ( ! $key )
    	{
    		$r = new \plainview\sdk\collections\collection( $this->defaults );
    		foreach ( $r as $key => $value )
    			$r[ $key ] = $this->get( $key );
    		return $r;
    	}

    	$r = $this->site->get_meta( $key );
    	if ( trim( $r ) == '' )
    		if ( isset( $this->defaults[ $key ] ) )
    			$r = $this->defaults[ $key ];
    	return $r;
    }

    /**
    	@brief		Load the header image model.
    	@details	Which isn't really a separate model.
    	@since		2023-02-27 20:16:34
    **/
    public function header_image()
    {
    	$header_image = Header_Image::from_site( $this->site );
    	return $header_image;
    }

    /**
    	@brief		Set a value.
    	@since		2023-02-25 17:37:47
    **/
    public function set( $key, $value )
    {
    	$this->site->set_meta( $key, $value );
    	return $this;
    }
}
