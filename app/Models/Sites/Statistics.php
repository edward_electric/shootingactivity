<?php

namespace App\Models\Sites;

use App\Models\Activities\Activity;

/**
	@brief		A statistics helper / generator for the site.
	@since		2018-11-27 14:49:16
**/
class Statistics
{
	/**
		@brief		The Site we are helping and working on.
		@since		2018-11-27 15:01:53
	**/
	public $site;

	/**
		@brief		The constructor.
		@since		2018-11-27 15:01:39
	**/
	public function __construct( Site $site )
	{
		$this->site = $site;
	}

	/**
		@brief		Add this activity into the site statistics.
		@since		2018-11-27 15:00:16
	**/
	public function accept_activity( Activity $activity )
	{
		// Increase counters for site and category.
		foreach( [ $this->site, $activity->category ] as $owner )
		{
			$owner->increase_meta( 'stats_ammo',						$activity->ammo );		// Ammo count.
			$owner->increase_meta( 'stats_ammo_' . date( 'Y' ),			$activity->ammo );		// Activity count for year
			$owner->increase_meta( 'stats_ammo_' . date( 'Y_m' ),		$activity->ammo );		// Activity count for month
			$owner->increase_meta( 'stats_activities' );										// Activity count
			$owner->increase_meta( 'stats_activities_' . date( 'Y' ) );							// Activity count for year
			$owner->increase_meta( 'stats_activities_' . date( 'Y_m' ) );						// Activity count for month
		}
	}
}
