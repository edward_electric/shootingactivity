<?php

namespace App\Mail\Activities\Moderation;

use App\Models\Activities\Activity;

/**
	@brief		An activity was accepted.
	@since		2019-01-06 16:02:30
**/
class Accept
	extends \App\Mail\Mailable
{
	/**
		@brief		The activity to inform about.
		@since		2019-01-06 16:23:34
	**/
	public $activity;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( Activity $activity )
    {
    	$this->activity = $activity;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
    	$subject = \View::make( 'mail.activities.moderation.accept.subject', [ 'activity' => $this->activity ] );
    	$subject = $subject->render();
    	$this->subject( $subject );
        return $this->view( 'mail.activities.moderation.accept.text', [
        	'activity' => $this->activity,
        ] );
    }
}
