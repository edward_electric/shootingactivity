<?php

namespace App\Mail\Activities\Moderation;

use App\Models\Activities\Activity;

/**
	@brief		Activity rejection e-mail.
	@since		2019-01-06 16:24:31
**/
class Reject
	extends \App\Mail\Mailable
{
	/**
		@brief		The activity to inform about.
		@since		2019-01-06 16:23:34
	**/
	public $activity;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( Activity $activity )
    {
    	$this->activity = $activity;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
    	$subject = \View::make( 'mail.activities.moderation.reject.subject', [ 'activity' => $this->activity ] );
    	$subject = $subject->render();
    	$this->subject( $subject );
        return $this->view( 'mail.activities.moderation.reject.text', [
        	'activity' => $this->activity,
        ] );
    }
}
