<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Mailable
	extends \Illuminate\Mail\Mailable
{
    use Queueable, SerializesModels;
}
