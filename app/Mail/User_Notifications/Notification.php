<?php

namespace App\Mail\User_Notifications;

use App\Models\Model;
use App\Models\Users\User;

/**
	@brief		User notification.
	@since		2018-12-23 23:31:35
**/
class Notification
	extends \App\Mail\Mailable
{
	/**
		@brief		The model that caused the notification.
		@since		2018-12-09 22:22:33
	**/
	public $model;

	/**
		@brief		The user to notify.
		@since		2018-12-09 22:22:46
	**/
	public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( Model $model, User $user )
    {
    	$this->model = $model;
    	$this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
    	$data = $this->model->get_user_notification_data();
    	$args = [
        	'model' => $this->model,
        	'user' => $this->user,
        ];
    	$subject = \View::make( $data->mail_subject, $args );
    	$subject = $subject->render();
    	$this->subject( $subject );
        return $this->view( $data->mail_view, $args );
    }
}
