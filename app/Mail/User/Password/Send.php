<?php

namespace App\Mail\User\Password;

use App\Models\Users\User;

/**
	@brief		User's password reminder.
	@since		2018-11-13 19:05:21
**/
class Send
	extends \App\Mail\Mailable
{
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( User $user )
    {
    	$this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
    	$subject = \View::make( 'mail.user.password.send.subject', [ 'user' => $this->user ] );
    	$subject = $subject->render();
    	$this->subject( $subject );
        return $this->view( 'mail.user.password.send.text', [
        	'user' => $this->user,
        ] );
    }
}
