<?php

namespace App\Console\Commands\Users;

use App\Models\Users\User;

/**
	@brief		Set the password of a user.
	@since		2020-08-20 13:57:18
**/
class Set_Password
	extends \App\Console\Commands\Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "app:users:set_password { email : The e-mail of the user. } { password : The user's new password. }";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sets the password of a user.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$email = $this->argument( 'email' );
    	$user = User::where( 'email', $email )->firstOrFail();
    	$password = $this->argument( 'password' );
    	$user->set_password( $password );
    	$user->save();
    	$this->line( sprintf( 'Password for %s set to %s', $email, $password ) );
		return 0;
    }
}
