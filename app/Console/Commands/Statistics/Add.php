<?php

namespace App\Console\Commands\Statistics;

use App\Models\Activities\Activity;

/**
	@brief		Add an activity to the statistics.
	@since		2018-12-05 11:16:56
**/
class Add
	extends \App\Console\Commands\Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:statistics:add { activity_id : The ID of the activity to add to the stats. }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add the stats of an activity to the site and user stats.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$activity_id = $this->argument( 'activity_id' );
    	$activity = Activity::findOrFail( $activity_id );
    	$this->info( sprintf( 'Adding activity %s to statistics for user %s at %s',
    		$activity->id,
    		$activity->user->name,
    		$activity->user->site->name
    	) );
    	app()->statistics()->add_activity( $activity );
		return 0;
    }
}
