<?php

namespace App\Console\Commands\Statistics;

use App\Models\Sites\Site;

/**
	@brief		Delete all stats for this site.
	@since		2018-12-05 16:55:16
**/
class Flush
	extends \App\Console\Commands\Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:statistics:flush { site_id : The ID of the site. }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Completely remove all statistics for a site.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$site_id = $this->argument( 'site_id' );
    	$site = Site::findOrFail( $site_id );
    	$this->info( sprintf( 'Deleting statistics for %s', $site->name ) );
    	app()->statistics()->flush( $site );
		return 0;
    }
}
