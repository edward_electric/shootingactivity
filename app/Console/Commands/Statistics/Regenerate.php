<?php

namespace App\Console\Commands\Statistics;

use App\Models\Sites\Site;

/**
	@brief		Regenerate the site stats.
	@since		2019-01-23 14:05:31
**/
class Regenerate
	extends \App\Console\Commands\Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:statistics:regenerate { site_id? : The ID of the site. }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Regenerate the stats for a site.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$site_id = $this->argument( 'site_id' );
    	if ( $site_id )
    		$sites = [ Site::findOrFail( $site_id ) ];
    	else
    		$sites = Site::get();

		foreach( $sites as $site )
			$this->regenerate_for( $site );

		return 0;
    }

    /**
    	@brief		Regenerate the stats for this site.
    	@since		2018-12-16 14:21:29
    **/
    public function regenerate_for( Site $site )
    {
    	$this->info( sprintf( 'Regenerating statistics for %s', $site->name ) );
    	app()->statistics()->regenerate( $site );
    }
}
