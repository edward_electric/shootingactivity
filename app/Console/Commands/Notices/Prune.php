<?php

namespace App\Console\Commands\Notices;

use App\Models\Notices\Notice;
use App\Models\Sites\Site;

/**
	@brief		Prune old notices
	@since		2018-12-23 21:38:38
**/
class Prune
	extends \App\Console\Commands\Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:notices:prune { site_id? : The ID of the site. }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Prune old notices from a site.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$site_id = $this->argument( 'site_id' );
    	if ( $site_id )
    		$sites = [ Site::findOrFail( $site_id ) ];
    	else
    		$sites = Site::get();

		foreach( $sites as $site )
			$this->prune( $site );

		return 0;
    }

    /**
    	@brief		Prune notices from this site.
    	@since		2018-12-23 21:40:23
    **/
    public function prune( Site $site )
    {
    	Notice::prune( $site );
    }
}
