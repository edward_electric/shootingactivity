<?php

namespace App\Listeners;

/**
	@brief		Handle everything for users.
	@since		2018-12-25 15:29:27
**/
class Users
	extends Listener
{
	/**
		@brief		Login a user.
		@since		2018-12-25 15:30:39
	**/
	public function login( $event )
	{
		$user = $event->user;
		$user->set_meta( 'last_seen', time() );
		\App\Models\Activities\Activity::clear_user_notification_key( $user );
		\App\Models\Notices\Notice::clear_user_notification_key( $user );
	}

	/**
	 * Register the listeners for the subscriber.
	 *
	 * @param  \Illuminate\Events\Dispatcher  $events
	 */
	public function subscribe($events)
	{
		$events->listen(
			'Illuminate\Auth\Events\Login',
			'App\Listeners\Users@login'
		);
/**
		$events->listen(
			'Illuminate\Auth\Events\Logout',
			'App\Listeners\UserEventSubscriber@onUserLogout'
		);
**/
	}
}
