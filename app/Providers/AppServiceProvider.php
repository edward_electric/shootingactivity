<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    	if ( config( 'database.default' ) )
    		Schema::defaultStringLength( 191 );

    	// Carbon requires _only_ a language, not a country also.
        $locale = config( 'app.locale' );
        $locale = preg_replace( '/_.*/', '', $locale );
        \Carbon\Carbon::setLocale( $locale );
    }


    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
