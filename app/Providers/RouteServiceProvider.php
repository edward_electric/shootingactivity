<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();

        Route::model( 'activity',		\App\Models\Activities\Activity::class);
        Route::model( 'category',		\App\Models\Categories\Category::class);
        Route::model( 'gun',			\App\Models\Guns\Gun::class);
        Route::model( 'gun_caliber',	\App\Models\Guns\Caliber::class);
        Route::model( 'gun_group',		\App\Models\Guns\Group::class);
        Route::model( 'notice',			\App\Models\Notices\Notice::class);
        Route::model( 'role',			\App\Models\Permissions\Role::class);
        Route::model( 'site',			\App\Models\Sites\Site::class);
        Route::model( 'user',			\App\Models\Users\User::class);
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}
