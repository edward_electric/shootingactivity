<?php

namespace App\Http\Controllers\Moderation;

use App\Models\Activities\Activity;

/**
	@brief		Accept the activity.
	@since		2018-11-23 20:57:29
**/
class Accept
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get( Activity $model )
	{
		app()->log()->info( 'Accepted %s', $model );
		$model->add_moderator();
		return redirect()->route( 'user_dashboard' );
	}
}
