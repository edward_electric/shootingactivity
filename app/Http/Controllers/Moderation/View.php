<?php

namespace App\Http\Controllers\Moderation;

use App\Models\Activities\Activity;

/**
	@brief		Show the activity.
	@since		2018-11-23 19:38:28
**/
class View
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get( Activity $model )
	{
		$data = [];
		$data[ 'model' ] = $model;

		return view( 'moderation.view', $data );
	}
}
