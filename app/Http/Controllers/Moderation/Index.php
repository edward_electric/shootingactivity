<?php

namespace App\Http\Controllers\Moderation;

use App\Models\Activities\Activity;

/**
	@brief		Index, allowing moderation of several at once.
	@since		2018-11-23 23:14:37
**/
class Index
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		Build the form.
		@since		2018-11-13 12:55:43
	**/
	public static function get_form( $models )
	{
		$form = app()->form();

		$all_ids = $models->pluck( 'id' );

		$form->all_activities = $form->hidden_input( 'all_activities' )
			->value( json_encode( $all_ids ) );

		$options = [];
		foreach( $models as $model )
			$options[ $model->id ] = sprintf( '%s %s', $model->created_at, $model->user->name );

		$form->activities_to_reject = $form->select( 'activities_to_reject' )
			->hidden()
			->label( 'Activities to reject')
			->opts( $options )
			->multiple()
			->autosize();

		// Yes, hide the label also!
		$form->activities_to_reject
			->label
			->hidden();

		$form->submit( 'mass_moderate' )
			->value( __( 'Mass moderate the above activities' ) );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();
		}
		return $form;
	}

	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get()
	{
		$data = [];
		$data[ 'models' ] = Activity::get_unmoderated_by_me();
		$data[ 'form' ] = $this->get_form( $data[ 'models' ] );

		return view( 'moderation.index', $data );
	}

	/**
		@brief		POST the page.
		@since		2018-11-13 12:56:04
	**/
	public function post()
	{
		$models = Activity::get_unmoderated_by_me();
		$form = $this->get_form( $models );

		$all_activities = $form->all_activities->get_post_value();
		$all_activities = json_decode( $all_activities );
		$all_activities = array_flip( $all_activities );
		$activities_to_reject = $form->activities_to_reject->get_post_value();
		$activities_to_reject = array_flip( $activities_to_reject );

		// From all of the activities, remove those that we are keeping.
		foreach( $activities_to_reject as $id => $ignore )
		{
			unset( $all_activities[ $id ] );
			$activity = Activity::find( $id );
			app()->log()->info( 'Mass rejected %s', $activity );
			$activity->reject();
		}

		foreach( $all_activities as $id => $ignore )
		{
			$activity = Activity::find( $id );
			app()->log()->info( 'Mass accepted %s', $activity );
			$activity->add_moderator();
		}

		app()->alerts()->success()
			->set_message( __( 'The activities have been moderated.' ) );

		return redirect()->route( 'user_dashboard' );
	}
}
