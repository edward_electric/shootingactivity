<?php

namespace App\Http\Controllers\Moderation;

use App\Models\Activities\Activity;

/**
	@brief		Reject the activity.
	@since		2018-11-23 21:07:40
**/
class Reject
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get( Activity $model )
	{
		app()->log()->info( 'Rejected %s', $model );
		// Rejecting it deletes it also.
		$model->reject();
		return redirect()->route( 'user_dashboard' );
	}
}
