<?php

namespace App\Http\Controllers\Moderation;

use App\Models\Activities\Activity;

/**
	@brief		Allow the moderator to slightly edit the activity.
	@since		2018-12-27 21:39:11
**/
class Edit
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		Build the form.
		@since		2018-12-27 21:39:11
	**/
	public function get_form( Activity $model )
	{
		$form = app()->form();

		// Does the user have more than one category?
		$categories = $model->user->site->categories()->active()->get();
		if ( count( $categories ) > 1 )
		{
			$form->category = $form->select( 'category' )
				->label( __( 'Category' ) )
				->opts( $categories->as_options() )
				->required()
				->value( $model->category_id );
		}

		$form->submit( 'save' )
			->value(  __( 'Save activity' ) );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();
		}

		return $form;
	}

	/**
		@brief		GET the page.
		@since		2018-12-27 21:39:11
	**/
	public function get( Activity $model )
	{
		return view( 'moderation.edit',
			[
				'form' => $this->get_form( $model ),
				'model' => $model,
			]
		);
	}

	/**
		@brief		POST the page.
		@since		2018-12-27 21:39:11
	**/
	public function post( Activity $model )
	{
		try
		{
			$form = $this->get_form( $model );

			// Save the category.
			$categories = app()->user()->site->categories()->active()->get();
			$category = $categories->get( $form->category->get_filtered_post_value() );
			$model->category()->associate( $category );

			$model->save();

			app()->alerts()->success()
				->set_message( __( 'The activity has been saved.' ) );

			return redirect()->route( 'moderation_view', [ $model ] );
		}
		catch( Exception $e )
		{
			$alert = app()->alerts()->danger();
			$alert->set_message( $e->getMessage() );
			return $this->get();
		}
	}
}
