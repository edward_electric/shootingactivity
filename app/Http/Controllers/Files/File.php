<?php

namespace App\Http\Controllers\Files;

/**
	@brief		Retrieve a file.
	@since		2018-11-18 12:28:10
**/
class File
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		Return the contents of the file as an attachment.
		@since		2018-11-18 12:35:35
	**/
	public function attachment( $file_id )
	{
		$file = \App\Models\Files\File::find( $file_id );
		return $file->download();
	}

	/**
		@brief		Return the contents of the file inline.
		@since		2018-11-18 12:35:35
	**/
	public function inline( $file_id, $key, $size = 'full' )
	{
		$file = \App\Models\Files\File::find( $file_id );
		return $file->display( $size );
	}
}
