<?php

namespace App\Http\Controllers\GenericModel;

use App\Models\Generic\Model;

/**
	@brief		Restore this model.
	@since		2018-11-21 00:20:02
**/
class Restore
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		Build the form.
		@since		2018-11-13 12:55:43
	**/
	public function get_form( Model $model )
	{
		$form = app()->form();

		$form->submit( 'restore_' . $model->model_key() )
			->value(  __( 'Restore :singularlabel :modelname', [ 'singularlabel' => $model->labels()->singular, 'modelname' => $model->name ] ) );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();
		}

		return $form;
	}

	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get( Model $model )
	{
		return view( $model->views()->restore,
			[
				'form' => $this->get_form( $model ),
				'model' => $model,
			]
		);
	}

	/**
		@brief		POST the page.
		@since		2018-11-13 12:56:04
	**/
	public function post( Model $model )
	{
		try
		{
			$model->restore();
			$model->save();

			app()->log()->debug( 'Restored %s %s',
				get_class( $model ),
				json_encode( $model )
			);

			app()->alerts()->success()
				// User XYZ has been restored
				->set_message( __( ':singularlabel :modelname has been restored.', [ 'singularlabel' => $model->labels()->Singular, 'modelname' => $model->name ] ) );

			return redirect()->route( $model->routes()->index );
		}
		catch( Exception $e )
		{
			$alert = app()->alerts()->danger();
			$alert->set_message( $e->getMessage() );
			return $this->get();
		}
	}
}
