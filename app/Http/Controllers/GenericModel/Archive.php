<?php

namespace App\Http\Controllers\GenericModel;

use App\Models\Generic\Model;

/**
	@brief		Archive a model.
	@since		2018-11-21 00:26:54
**/
class Archive
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		Build the form.
		@since		2018-11-13 12:55:43
	**/
	public function get_form( Model $model = null )
	{
		$form = app()->form();

		$form->submit( 'archive_' . $model->model_key() )
			->value(  __( 'Archive :singularlabel :modelname?', [ 'singularlabel' => $model->labels()->singular, 'modelname' => $model->name ] ) );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();
		}

		return $form;
	}

	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get( Model $model = null )
	{
		return view( $model->views()->archive,
			[
				'form' => $this->get_form( $model ),
				'model' => $model,
			]
		);
	}

	/**
		@brief		POST the page.
		@since		2018-11-13 12:56:04
	**/
	public function post( Model $model = null )
	{
		try
		{
			$model->archive();
			$model->save();

			app()->log()->debug( 'Archived %s %s',
				get_class( $model ),
				json_encode( $model )
			);

			app()->alerts()->success()
				->set_message( __( ':singularlabel :modelname has been archived.', [ 'singularlabel' => $model->labels()->Singular, 'modelname' => $model->name ] ) );

			return redirect()->route( $model->routes()->index );
		}
		catch( Exception $e )
		{
			$alert = app()->alerts()->danger();
			$alert->set_message( $e->getMessage() );
			return $this->get();
		}
	}
}
