<?php

namespace App\Http\Controllers\GenericModel;

use App\Models\Generic\Model;

/**
	@brief		Add a new generic model.
	@since		2018-11-21 00:26:36
**/
class Add
	extends Edit
{
	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get( Model $model = null )
	{
		return view( $model->views()->add,
			[
				'form' => static::get_form( $model ),
				'model' => $model,
			]
		);
	}

	/**
		@brief		POST the page.
		@since		2018-11-13 12:56:04
	**/
	public function post( Model $model = null )
	{
		return parent::post( $model );
	}
}
