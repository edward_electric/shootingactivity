<?php

namespace App\Http\Controllers\GenericModel;

/**
	@brief		Archived models.
	@since		2018-11-21 00:32:19
**/
class Archived
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get( $model )
	{
		$models = $model->owner_models()
			->archived()
			->orderBy( 'name' )
			->get();
		return view( $model->views()->archived,
			[
				'models' => $models,
				'model' => $model,
			]
		);
	}
}
