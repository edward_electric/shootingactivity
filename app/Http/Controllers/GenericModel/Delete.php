<?php

namespace App\Http\Controllers\GenericModel;

use App\Models\Generic\Model;

/**
	@brief		Delete a model.
	@since		2018-11-21 00:32:47
**/
class Delete
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		Build the form.
		@since		2018-11-13 12:55:43
	**/
	public function get_form( Model $model = null )
	{
		$form = app()->form();

		$form->submit( 'delete_' . $model->model_key() )
			->value(  __( 'Delete :singularlabel :modelname?', [ 'singularlabel' => $model->labels()->singular, 'modelname' => $model->name ] ) );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();
		}

		return $form;
	}

	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get( Model $model )
	{
		if ( ! $model->can_be_deleted() )
			abort( 403 );
		return view( $model->views()->delete,
			[
				'form' => $this->get_form( $model ),
				'model' => $model,
			]
		);
	}

	/**
		@brief		POST the page.
		@since		2018-11-13 12:56:04
	**/
	public function post( Model $model )
	{
		if ( ! $model->can_be_deleted() )
			abort( 403 );

		app()->log()->debug( 'Deleted %s %s',
			get_class( $model ),
			json_encode( $model )
		);

		$model->delete();

		app()->alerts()->success()
			->set_message( __( ':singularlabel :modelname has been deleted.', [ 'singularlabel' => $model->labels()->Singular, 'modelname' => $model->name ] ) );

		return redirect()->route( $model->routes()->index );
	}
}
