<?php

namespace App\Http\Controllers\GenericModel;

use App\Models\Generic\Model;

/**
	@brief		Edit a model.
	@since		2018-11-21 00:33:21
**/
class Edit
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		Build the form.
		@since		2018-11-13 12:55:43
	**/
	public static function get_form( Model $model = null )
	{
		$form = app()->form();

		$form->name = $form->text( 'name' )
			->description( $model->labels()->name_input_description )
			->label( __( 'Name' ) )
			->placeholder( $model->labels()->name_input_placeholder )
			->required()
			->trim();
		if ( $model )
			$form->name->value( $model->name );

		$model->get_edit_form( $form, $model );

		$form->submit( 'save_' . $model->model_key() )
			->value(  __( 'Save :singularlabel :modelname', [ 'singularlabel' => $model->labels()->singular, 'modelname' => $model->name ] ) );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();
		}

		return $form;
	}

	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get( Model $model = null )
	{
		return view( $model->views()->edit,
			[
				'form' => $this->get_form( $model ),
				'model' => $model,
			]
		);
	}

	/**
		@brief		POST the page.
		@since		2018-11-13 12:56:04
	**/
	public function post( Model $model = null )
	{
		try
		{
			if ( $model->id )
				$form = static::get_form( $model );
			else
			{
				$model = $model::new_model();
				$form = static::get_form( $model );
			}

			$model->name = $form->name->get_filtered_post_value();
			$model->name = htmlspecialchars_decode( $model->name );

			$model->post_edit_form( $form, $model );

			$model->save();

			$model->post_after_edit_form( $form, $model );

			app()->log()->debug( 'Saved %s %s',
				get_class( $model ),
				json_encode( $model )
			);

			app()->alerts()->success()
				->set_message( __( ':singularlabel :modelname has been saved.', [ 'singularlabel' => $model->labels()->Singular, 'modelname' => $model->name ] ) );

			return redirect()->route( $model->routes()->index );
		}
		catch( Exception $e )
		{
			$alert = app()->alerts()->danger();
			$alert->set_message( $e->getMessage() );
			return $this->get();
		}
	}
}
