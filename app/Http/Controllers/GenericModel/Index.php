<?php

namespace App\Http\Controllers\GenericModel;

/**
	@brief		Models on this site.
	@since		2018-11-21 00:42:50
**/
class Index
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get( $model )
	{
		$archived = 0;
		if ( $model->is_archivable() )
		{
			$archived = $model->owner_models()
				->archived()
				->count();
		}
		$models = $model->owner_models()
			->active()
			->orderBy( 'name' )
			->get();
		return view( $model->views()->index,
			[
				'archived' => $archived,
				'model' => $model,
				'models' => $models,
			]
		);
	}
}
