<?php

namespace App\Http\Controllers\Comments;

use App\Models\Comments\Comment;

/**
	@brief		Add a comment.
	@since		2018-12-23 18:21:55
**/
class Add
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		Build the form.
		@since		2018-12-23 18:21:55
	**/
	public static function get_form( $options = [] )
	{
		$form = app()->form();
		$options = array_merge( [
			'submit_label' => __( 'Send' ),
			'text_label' => __( 'Comment' ),
			'text_rows' => 2,
			'text_cols' => 40,
		], $options );
		$options = ( object ) $options ;

		$form->text = $form->textarea( 'text' )
			->label( $options->text_label )
			->rows( $options->text_rows, $options->text_cols )
			->required()
			->trim();

		$form->submit = $form->submit( 'add_new_comment' )
			->value( $options->submit_label );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();
		}

		return $form;
	}

	/**
		@brief		Create a comment using the _POST.
		@since		2018-12-23 18:21:55
	**/
	public static function post_comment()
	{
		$form = static::get_form();
		// First save the comment.
		$comment = new Comment();
		$comment->text = $form->text->get_filtered_post_value();
		$comment->user()->associate( app()->user() );
		$comment->save();
		return $comment;
	}
}
