<?php

namespace App\Http\Controllers\Activities\Mass_Import;

use App\Models\Activities\Mass_Import;

/**
	@brief		Mark the mass import as unready.
	@since		2019-01-20 12:55:07
**/
class Unready
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		GET the page.
		@since		2019-01-20 10:14:42
	**/
	public function get()
	{
		$model = Mass_Import::load_from_session();
		$model->unready();
		$model->save();
		return redirect()->route( 'activities_mass_import_edit' );
	}
}
