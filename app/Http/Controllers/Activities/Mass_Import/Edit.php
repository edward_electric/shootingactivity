<?php

namespace App\Http\Controllers\Activities\Mass_Import;

use App\Models\Activities\Activity;
use App\Models\Activities\Mass_Import;
use App\Models\Users\User;
use Carbon\Carbon;
use Exception;

/**
	@brief		Edit a mass import.
	@since		2019-01-20 10:14:42
**/
class Edit
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		Build the form.
		@since		2019-01-20 10:14:42
	**/
	public function get_form( Mass_import $model )
	{
		$form = app()->form();

		$form->datetime = $form->text( 'datetime' )		// I would like to use datetimelocal, but Chrome doesn't play well with Dusk.
			->description( __( 'When the activity took place.' ) )
			->label( __( 'Date' ) )
			->value( $model->get( 'datetime' )->toDateTimeString() );

		// Does the user have more than one category?
		$categories = app()->site()->categories()->active()->get();
		if ( count( $categories ) > 1 )
		{
			$form->category = $form->select( 'category' )
				->label( __( 'Category' ) )
				->opt( '', __( 'Select a category' ) )
				->opts( $categories->as_options() )
				->required()
				->value( $model->get( 'category_id' ) );
		}

		$form->ammo = $form->number( 'ammo' )
			->description( __( 'How much ammo was used in the activity.' ) )
			->label( __( 'Ammo' ) )
			->min( 1, 1000 )
			->required()
			->value( intval( $model->get( 'ammo' ) ) );

		// Strings is used for score calculations.
		$form->strings = $form->number( 'strings' )
			->description( __( 'How many strings were shot.' ) )
			->label( __( 'Strings' ) )
			->min( 0, 20 )
			->required()
			->value( intval( $model->get( 'strings' ) ) );

		$form->comment = $form->textarea( 'comment' )
			->label( __( 'Comment' ) )
			->rows( 2, 40 )
			->trim()
			->value( htmlspecialchars_decode( $model->get( 'comment' ) ) );

		$form->csv = $form->textarea( 'csv' )
			->description( __( 'Each row must contain: User name<tab>Weapon group<tab>Score<tab>String 1<tab>String 2<tab>etc' ) )
			->label( __( 'Import data' ) )
			->placeholder( 'Edward P<tab>C<tab>250<tab>[7,7,6,6,9]<tab>[4,3,7,7,8]' )
			->rows( 10, 40 )
			->trim()
			->value( htmlspecialchars_decode( $model->get( 'csv' ) ) );

		$form->preview = $form->submit( 'preview' )
			->value( __( 'Preview the import' ) );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();
		}

		return $form;
	}

	/**
		@brief		GET the page.
		@since		2019-01-20 10:14:42
	**/
	public function get()
	{
		$model = Mass_Import::load_from_session();
		if ( $model->is_ready() )
			return redirect( route( 'activities_mass_import_preview' ) );
		return view( 'activities.mass_import.edit',
			[
				'form' => $this->get_form( $model ),
				'model' => $model,
			]
		);
	}

	/**
		@brief		POST the page.
		@since		2019-01-20 10:14:42
	**/
	public function post()
	{
		$model = Mass_Import::load_from_session();
		try
		{
			$form = $this->get_form( $model );

			// Start afresh.
			$model = Mass_Import::create();

			$ammo = $form->ammo->get_filtered_post_value();
			$model->set( 'ammo', $ammo );

			$category_id = $form->category->get_filtered_post_value();
			$model->set( 'category_id', $category_id );

			$comment = $form->comment->get_filtered_post_value();
			$model->set( 'comment', $comment );

			$csv = $form->csv->get_post_value();
			$model->set( 'csv', $csv );

			$datetime = $form->datetime->get_filtered_post_value();
			$datetime = Carbon::parse( $datetime );
			$model->set( 'datetime', $datetime );

			$strings = $form->strings->get_filtered_post_value();
			$model->set( 'strings', $strings );

			$model->save();

			$category = app()->site()->categories()->find( $category_id );

			// Try and import each line.
			$lines = \plainview\sdk\base::textarea_to_array( $csv );
			foreach( $lines as $line )
			{
				$line = str_replace( ';', "\t", $line );
				$columns = explode( "\t", $line );
				// At least name and weapon group.
				if ( count( $columns ) < 2 )
					continue;
				// Does this user exist?
				$user_name = $columns[ 0 ];
				array_shift( $columns );
				$user = app()->site()->users()
					->where( 'name', $user_name )
					->get()
					->first();

				if ( ! $user )
					continue;

				$activity = new Activity();
				$activity->created_at = $datetime;
				$activity->category()->associate( $category );
				$activity->user()->associate( $user );
				$activity->ammo = $ammo;
				// Again, this is not something that is saved within the model.
				$model->collection( 'comments' )->set( $user->id, $comment );

				// Find the user's first best gun that matches this weapon group.
				$weapon_group = $columns[ 0 ];
				array_shift( $columns );
				$guns = [];
				foreach( $user->guns()->active()->get() as $gun )
					if ( $gun->group )
					{
						if ( $gun->group->name != $weapon_group )
							continue;
						$guns[] = $gun;
					}

				// If there is exactly one gun.
				if ( count( $guns ) == 1 )
					$activity->gun()->associate( $guns[ 0 ] );
				else
				{
					// Save this weapon group for later.
					$model->collection( 'weapon_groups' )
						->set( $user->id, $weapon_group );
				}

				$score = $columns[ 0 ];
				array_shift( $columns );

				if ( $score > 0 )
				{
					// The rest of the columns are the individual strings.
					$strings = $columns;

					// If not all strings were noted, then calculate how to distribute the points.
					$score_left = $score;
					foreach( $strings as $points )
					{
						$points = json_decode( $points );
						// Json likes to generate objects.
						$points = (array) $points;
						$total = array_sum( $points );
						$score_left -= $total;
						$model->collection( 'strings' )->collection( $user->id )
							->append( [ $total, $points ] );
					}

					if ( $score_left > 0 )
					{
						$strings_left = $strings - count( $scores );
						$score_per_strings = round( $score_left / $strings_left );
						for( $counter = 0; $counter < $strings_left; $counter++ )
						{
							$model->collection( 'strings' )->collection( $user->id )
								->append( [ $total ] );
						}
					}
				}

				// Save the activity in the model.
				$model->collection( 'activities' )->append( $activity );
			}

			$model->save();

			if ( count( $model->collection( 'activities' ) ) < 1 )
				throw new Exception( __( 'No users found!' ) );

			$model->ready()
				->save();

			return redirect()->route( 'activities_mass_import_preview' );
		}
		catch( Exception $e )
		{
			$alert = app()->alerts()->danger();
			$alert->set_message( $e->getMessage() );
			return $this->get();
		}
	}
}
