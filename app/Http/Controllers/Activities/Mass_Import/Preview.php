<?php

namespace App\Http\Controllers\Activities\Mass_Import;

use App\Models\Activities\Activity;
use App\Models\Activities\Mass_Import;

/**
	@brief		Preview the import.
	@since		2019-01-20 14:26:58
**/
class Preview
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		Build the form.
		@since		2019-01-20 10:14:42
	**/
	public function get_form( Mass_import $model )
	{
		$form = app()->form();

		$form->preview = $form->submit( 'import' )
			->value(  __( 'Import the above' ) );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();
		}

		return $form;
	}

	/**
		@brief		GET the page.
		@since		2019-01-20 10:14:42
	**/
	public function get()
	{
		$model = Mass_Import::load_from_session();
		return view( 'activities.mass_import.preview',
			[
				'form' => $this->get_form( $model ),
				'model' => $model,
			]
		);
	}

	/**
		@brief		POST the page.
		@since		2019-01-20 10:14:42
	**/
	public function post()
	{
		try
		{
			$model = Mass_Import::load_from_session();
			foreach( $model->collection( 'activities' ) as $activity )
			{
				app()->log()->debug( 'Mass imported activity %s', $activity );
				$activity->save();

				$comment = $model->collection( 'comments' )->get( $activity->user->id );
				$activity->set_comment( $comment );

				$scores = $activity->scores();

				// Add the scores.
				foreach( $model->collection( 'scores' )->get( $activity->user->id ) as $a_score )
				{
					if ( count( $a_score ) == 1 )
					{
						// Only 1 score is the total.
						$scores->add_score( $total );
					}
					else
					{
						// Total AND individual shots.
						$total = $a_score[ 0 ];
						$shots = $a_score[ 1 ];
						$scores->add_score( $total, $shots );
					}
				}

				$scores->save( $activity );

				$complete = true;

				// If the gun is missing, the user will have to be asked to complete it.
				if ( ! $activity->gun )
				{
					$weapon_group = $model->collection( 'weapon_groups' )->get( $activity->user->id );
					if ( $weapon_group )
						$activity->set_meta( 'incomplete_weapon_group', $weapon_group );
					$complete = false;
				}

				if ( $complete )
				{
					app()->log()->debug( 'Activity %s is complete.', $activity->id );
					app()->statistics()->add_activity_later( $activity );
				}
				else
				{
					app()->log()->debug( 'Activity %s is incomplete.', $activity->id );
					$activity->mark_as_incomplete();
				}
			}

			app()->alerts()->success()
				->set_message( __( 'The activities has been mass imported.' ) );

			Mass_Import::flush();

			return redirect()->route( 'user_dashboard' );
		}
		catch( Exception $e )
		{
			$alert = app()->alerts()->danger();
			$alert->set_message( $e->getMessage() );
			return $this->get();
		}
	}
}
