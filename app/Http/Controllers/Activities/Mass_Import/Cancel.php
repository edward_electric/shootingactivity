<?php

namespace App\Http\Controllers\Activities\Mass_Import;

use App\Models\Activities\Mass_Import;

/**
	@brief		Cancel the mass import.
	@since		2019-01-20 12:36:14
**/
class Cancel
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		GET the page.
		@since		2019-01-20 10:14:42
	**/
	public function get()
	{
		Mass_Import::flush();
		app()->alerts()->success()
			->set_message( __( 'The mass import has been canceled.' ) );
		return redirect()->route( 'user_dashboard' );
	}
}
