<?php

namespace App\Http\Controllers\Activities;

use App\Models\Activities\Activity;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
	@brief		The users activities.
	@since		2018-11-23 21:33:51
**/
class Index
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get( Request $request, Activity $model )
	{
		$data = [];
		$query = $this->get_models_query();

		$gun_id = $request->get( 'guns' );
		if ( $gun_id )
			$query->where( 'gun_id', $gun_id );

		$categories = $request->get( 'categories' );
		if ( $categories )
			$query->where( 'category_id', $categories );

		$year = $request->get( 'year' );
		$month = $request->get( 'month' );

		// If a month is selected, but not a year, assume the current year.
		if ( $month )
			if ( ! $year )
				$year = date( 'Y' );

		if ( $year )
		{
			$query->where( 'year', $year );
			if ( $month )
			{
				$date_start = sprintf( '%s-%s-01', $year, $month );
				$date_stop = sprintf( '%s-%s-31', $year, $month );
				$query->where( 'created_at', '>=', $date_start );
				$query->where( 'created_at', '<=', $date_stop );
			}
		}
		else
		{
			$year = date( 'Y' );
			$query->where( 'year', $year );
		}


		$data[ 'years' ] = Activity::years( app()->user() );
		$data[ 'models' ] = $query->get();
		return view( 'activities.index', $data );
	}

	/**
		@brief		Convenience method to return the query for activities.
		@since		2018-12-01 16:44:45
	**/
	public function get_models_query()
	{
		return app()
			->user()
			->activities()
			->orderBy( 'year', 'DESC' )
			->orderBy( 'order', 'DESC' )
			->with( 'gun', 'category' );
	}
}
