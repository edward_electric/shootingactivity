<?php

namespace App\Http\Controllers\Activities\Statistics;

use App\Models\Activities\Activity;

/**
	@brief		The user's activity statistics.
	@since		2018-11-27 13:44:25
**/
class Index
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		Download the CSV version of the stats.
		@since		2020-12-03 20:49:34
	**/
	public function csv( $year = false )
	{
		$statistics = $this->get_statistics( $year );
		return $statistics->output_csv();
	}
	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get( $year = false )
	{
		$statistics = $this->get_statistics( $year );
		$view = $this->get_view();
		return view( $view, [
			'statistics' => $statistics,
		] );
	}

	/**
		@brief		Return the statistics object for this year.
		@since		2018-12-04 00:06:31
	**/
	public function get_statistics( $year = false )
	{
		if ( ! $year )
			$year = date( 'Y' );
		$statistics = app()->statistics()->user( app()->user(), $year );
		$statistics->table_name = app()->user()->name;
		return $statistics;
	}

	/**
		@brief		Return the view to contain the stats.
		@since		2019-01-23 13:01:53
	**/
	public function get_view()
	{
		return 'activities.statistics.index';
	}

	/**
		@brief		Download the PDF version of the stats.
		@since		2018-12-04 13:24:33
	**/
	public function pdf( $year = false )
	{
		$statistics = $this->get_statistics( $year );
		$statistics->output_pdf();
	}
}
