<?php

namespace App\Http\Controllers\Activities;

use App\Models\Activities\Activity;
use App\Models\Guns\Group;
use App\Models\Guns\Gun;

/**
	@brief		Find the first best incomplete activity.
	@since		2019-01-20 16:06:54
**/
class Complete
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		Build the form.
		@since		2019-01-20 10:14:42
	**/
	public function get_form( Activity $activity )
	{
		$form = app()->form();

		if ( ! $activity->gun )
		{
			$weapon_group = $activity->get_meta( 'incomplete_weapon_group' );
			$group = app()->site()->gun_groups()
				->where( 'name', $weapon_group )
				->get()
				->first();
			// Find all of the user's gun's that match this weapon group.
			$guns = app()->user()->guns()
				->active()
				->where( 'group_id', $group->id )
				->get();

			$form->gun = $form->select( 'gun' )
				->label( __( 'Gun' ) )
				->opts( $guns->as_options() )
				->required()
				->value( $activity->gun_id );

			// If no guns are available, tell the user so.
			if ( count( $guns ) < 1 )
				$form->gun->opt( '', __( 'Please add at least one gun in the :weapongroup weapon group to complete this activity.', [ 'weapongroup' => $weapon_group ] ) );
		}

		$form->preview = $form->submit( 'save' )
			->value( __( 'Save' ) );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();
		}

		return $form;
	}

	/**
		@brief		GET the page.
		@since		2019-01-20 10:14:42
	**/
	public function get()
	{
		$activity = Activity::get_incomplete_activities( app()->user() )->first();
		if ( ! $activity )
		{
			app()->alerts()->success()
				->set_message( __( 'There are no more incomplete activities.' ) );
			return redirect( route( 'user_dashboard' ) );
		}

		return view( 'activities.complete.edit',
			[
				'form' => $this->get_form( $activity ),
				'model' => $activity,
			]
		);
	}

	/**
		@brief		POST the page.
		@since		2019-01-20 10:14:42
	**/
	public function post()
	{
		try
		{
			$activity = Activity::get_incomplete_activities( app()->user() )->first();
			$form = $this->get_form( $activity );
			if ( ! $activity->gun )
			{
				$gun_id = $form->gun->get_filtered_post_value();
				$gun = Gun::find( $gun_id );
				$activity->gun()->associate( $gun );
				$activity->delete_meta( 'incomplete_weapon_group' );
			}

			$activity->save();

			// Activity is now complete.
			$activity->unmark_as_incomplete();

			app()->statistics()->add_activity_later( $activity );

			return redirect()->route( 'activities_complete' );
		}
		catch( Exception $e )
		{
			$alert = app()->alerts()->danger();
			$alert->set_message( $e->getMessage() );
			return $this->get();
		}
	}
}
