<?php

namespace App\Http\Controllers\Activities;

use App\Models\Activities\Activity;

/**
	@brief		Delete the activity.
	@since		2019-01-23 13:29:28
**/
class Delete
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		Build the form.
		@since		2019-01-23 13:29:28
	**/
	public function get_form( Activity $activity )
	{
		$form = app()->form();

		$form->preview = $form->submit( 'delete' )
			->value( __( 'Delete this activity' ) );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();
		}

		return $form;
	}

	/**
		@brief		GET the page.
		@since		2019-01-23 13:29:28
	**/
	public function get( Activity $model )
	{
		return view( 'activities.delete',
			[
				'form' => $this->get_form( $model ),
				'model' => $model,
			]
		);
	}

	/**
		@brief		POST the page.
		@since		2019-01-23 13:29:28
	**/
	public function post( Activity $model )
	{
		app()->log()->debug( 'Deleting activity %s', $model->id );
		$model->delete();
		app()->statistics()->regenerate_later();
		app()->alerts()->success()
			->set_message( __( 'The activity has been deleted.' ) );
		return redirect()->route( 'user_dashboard' );
	}
}
