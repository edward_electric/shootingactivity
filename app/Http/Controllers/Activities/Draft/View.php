<?php

namespace App\Http\Controllers\Activities\Draft;

use App\Models\Activities\Activity;

/**
	@brief		Show the draft.
	@since		2018-11-22 14:19:29
**/
class View
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get()
	{
		return view( 'activities.draft.view',
			[
				'model' => Activity::get_draft(),
			]
		);
	}
}
