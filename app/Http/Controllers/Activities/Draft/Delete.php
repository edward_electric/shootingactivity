<?php

namespace App\Http\Controllers\Activities\Draft;

use App\Models\Activities\Activity;

/**
	@brief		Delete the user's draft.
	@since		2018-11-22 16:05:03
**/
class Delete
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get()
	{
		$model = Activity::get_draft();
		$model->delete();

		app()->alerts()->success()
			->set_message( __( 'The activity in progress has been deleted.' ) );

		return redirect()->route( 'user_dashboard' );
	}
}
