<?php

namespace App\Http\Controllers\Activities\Draft;

use App\Models\Activities\Activity;
use Exception;

/**
	@brief		Edit the string of an activity.
	@since		2018-11-25 16:26:15
**/
class Strings
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		Delete a string.
		@since		2018-11-25 17:03:41
	**/
	public function delete( $string_id )
	{
		$model = Activity::get_draft();
		$strings = $model->strings();
		$strings->reorder();	// Force conversion of keys to ints.
		$string = $strings->get( $string_id );
		$strings->forget( $string_id );
		$strings->save( $model );

		// Decrease the ammo.
		$model = Activity::get_draft();
		$model->ammo -= count( $string->shots );
		// Prevent < 0...
		$model->ammo = max( $model->ammo, 0 );
		$model->save();

		return redirect()->route( 'activities_draft' );
	}

	/**
		@brief		Build the form.
		@since		2018-11-13 12:55:43
	**/
	public function get_form( Activity $model, $string_id )
	{
		$form = app()->form();
		$form->id( 'string_editor' );

		$strings = $model->strings();
		$string_size = $this->get_string_size( $model );

		foreach( range( 1, $string_size ) as $counter )
		{
			$key = 'score_' . $counter;
			$form->$key = $form->tel( $key )
				->css_class( 'single score' )
				->label( 'Hit #' . $counter )
				->placeholder( '#' . $counter )
				->set_attribute( 'autocomplete', 'off' )
				->value( $strings->get_shot( $string_id, $counter ) );
		}

		if ( $strings->scoring_x_enabled() )
		{
			$form->score_x = $form->number( 'score_x' )
				->css_class( 'score_x' )
				->description( __( 'Use * to denote an inner 10.' ) )
				->label( 'Inner 10s' )
				->pattern( '\d*' )		// IOS bug.
				->placeholder( '#' . $counter )
				->min( 0, $string_size )
				->step( 1 )
				->value( 0 );
		}

		$form->total = $form->number( 'total' )
			->css_class( 'total' )
			->label( 'Total' )
			->placeholder( __( 'Total' ) )
			->min( 0, $string_size * 10 )
			->required()
			->step( 1 )
			->value( $strings->get_score( $string_id ) );

		$form->submit( 'save_score' )
			->value( __( 'Done' ) );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();
		}

		return $form;
	}

	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get( $string_id )
	{
		$model = Activity::get_draft();
		return view( 'activities.draft.add_score',
			[
				'form' => $this->get_form( $model, $string_id ),
				'model' => $model,
			]
		);
	}

	/**
		@brief		Return the size of the string for scoring.
		@since		2019-02-18 19:22:50
	**/
	public function get_string_size( $model )
	{
		if ( $model->gun )
			if ( $model->gun->group )
				return $model->gun->group->get_string_size();
		return 5;
	}

	/**
		@brief		POST the page.
		@since		2018-11-13 12:56:04
	**/
	public function post( $string_id )
	{
		$model = Activity::get_draft();
		try
		{
			$form = $this->get_form( $model, $string_id );

			$shots = [];

			foreach( range( 1, $this->get_string_size( $model ) ) as $counter )
			{
				$key = 'score_' . $counter;
				$value = $form->input( $key )->get_filtered_post_value();
				if ( $value == '' )
					break;
				$shots []= $value;
			}

			$total = $form->total->get_filtered_post_value();

			$data = (object)[
				'shots' => $shots,
				'score' => $total,
			];

			$strings = $model->strings();

			if ( $strings->scoring_x_enabled() )
				$data->x = $form->input( 'score_x' )->get_filtered_post_value();

			$adding = ( $string_id < 0 );

			if ( $adding )
				$strings->add_string_data( $data );
			else
				$strings->set_string_data( $string_id, $data );

			$verb = ( $adding ? "added" : "edited" );
			app()->log()->info( '%s %s a score: %s = %s',
				app()->user(),
				$verb,
				implode( ', ', $shots ),
				$total
			);

			// Automatically raise the ammo count if less than the shots fired in the strings.
			if ( $adding )
				$model->ammo += count ( $shots );

			$model->save();

			$strings->save( $model );

			app()->alerts()->success()
				->set_message( __( 'The score has been added to the activity in progress.' ) );

			return redirect()->route( 'activities_draft' );
		}
		catch( Exception $e )
		{
			$alert = app()->alerts()->danger();
			$alert->set_message( $e->getMessage() );
			return $this->get( $string_id );
		}
	}
}
