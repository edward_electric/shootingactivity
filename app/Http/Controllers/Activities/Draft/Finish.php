<?php

namespace App\Http\Controllers\Activities\Draft;

use App\Models\Activities\Activity;
use Exception;

/**
	@brief		Save the draft and send it in for moderation / to the activities.
	@since		2018-11-23 10:33:03
**/
class Finish
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get()
	{
		try
		{
			$model = Activity::get_draft();
			$model->finish_editing();

			app()->log()->info( '%s finished activity %s', app()->user(), $model );

			\App\Jobs\Activities\Reorder_For_User::dispatch( app()->user() );

			if ( app()->site()->uses_moderation() )
				$text = __( 'The activity has been sent for moderation.' );
			else
				$text = __( 'The activity has been saved.' );

			app()->alerts()->success()
				->set_message( $text );

			return redirect()->route( 'user_dashboard' );
		}
		catch( Exception $e )
		{
			app()->alerts()->danger()
				->set_message( $e->getMessage() );

			return redirect()->route( 'activities_draft' );
		}
	}
}
