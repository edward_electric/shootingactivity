<?php

namespace App\Http\Controllers\Activities\Draft;

use App\Models\Activities\Activity;
use Carbon\Carbon;
use Exception;

/**
	@brief		Edit the user's draft.
	@since		2018-11-22 15:00:14
**/
class Edit
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		Return an array with customized file options.
		@details	Add to this method to customize the options.
		@since		2018-11-22 17:16:42
	**/
	public function get_file_options()
	{
		$options = $this->get_default_file_options();
		$options->file_is_image = true;
		$options->upload_file_input_description		= __( 'Take a photo of your visit to the range.' );
		return $options;
	}

	/**
		@brief		Build the form.
		@since		2018-11-13 12:55:43
	**/
	public function get_form( Activity $model )
	{
		$form = app()->form();

		$form->description = $form->text( 'description' )
			->description( __( 'A short description of this activity that is visible in the activity overview.' ) )
			->label( __( 'Description' ) )
			->size( 40 )
			->trim()
			->value( htmlspecialchars_decode( $model->description ) );

		// Does the user have more than one category?
		$categories = app()->user()->site->categories()->active()->get();
		if ( count( $categories ) > 1 )
		{
			$form->category = $form->select( 'category' )
				->label( __( 'Category' ) )
				->opts( $categories->as_options() )
				->required()
				->value( $model->category_id );
		}

		// Does the user have more than one gun?
		$guns = app()->user()->guns()->active()->get();
		if ( count( $guns ) > 1 )
		{
			$gun_opts = $guns->as_options();
			asort( $gun_opts );
			$form->gun = $form->select( 'gun' )
				->label( __( 'Gun' ) )
				->opts( $gun_opts )
				->required()
				->value( $model->gun_id );
		}

		$form->ammo = $form->number( 'ammo' )
			->description( __( 'How much ammo have you used today?' ) )
			->label( __( 'Ammo' ) )
			->min( 0, 1000 )
			->pattern( '\d*' )		// IOS bug.
			->required()
			->value( intval( $model->ammo ) );

		$form->comment = $form->textarea( 'comment' )
			->label( __( 'Comment' ) )
			->rows( 2, 40 )
			->trim()
			->value( htmlspecialchars_decode( $model->get_comment() ) );

		$model->get_file_form( $form );

		$activity_date_set = App()->user()->site->get_meta( 'activity_date_set', 0 );
		if ( $activity_date_set > 0 )
			// Allow the user to set the date manually.
			$form->activity_date_set = $form->checkbox( 'activity_date_set' )
			->description( __( 'Modify the date for this activity.' ) )
				->label( __( 'Set another date' ) );

		$form->submit( 'save_draft' )
			->icon( 'fa-solid fa-floppy-disk' )
			->value( __( 'Save activity in progress' ) );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();
		}

		return $form;
	}

	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get()
	{
		$model = Activity::get_draft();
		return view( 'activities.draft.edit',
			[
				'form' => $this->get_form( $model ),
				'model' => $model,
			]
		);
	}

	/**
		@brief		POST the page.
		@since		2018-11-13 12:56:04
	**/
	public function post()
	{
		$model = Activity::get_draft();
		try
		{
			$form = $this->get_form( $model );

			$model->ammo = $form->ammo->get_filtered_post_value();

			// Save the category.
			$categories = app()->user()->site->categories()->active()->get();
			if( count( $categories ) > 1 )
			{
				$category = $categories->get( $form->category->get_filtered_post_value() );
			}
			else
				$category = $categories->first();
			app()->user()->set_meta( 'last_activity_category', $category->id );
			$model->category()->associate( $category );

			$comment = $form->comment->get_filtered_post_value();
			$model->set_comment( $comment );

			$description = $form->description->get_filtered_post_value();
			if ( $description == '' )
				$description = null;
			$model->description = $description;

			// Save the gun.
			$guns = app()->user()->guns()->active()->get();
			if( count( $guns ) > 1 )
			{
				$gun = $guns->get( $form->gun->get_filtered_post_value() );
			}
			else
				$gun = $guns->first();
			app()->user()->set_meta( 'last_activity_gun', $gun->id );
			$model->gun()->associate( $gun );

			$model->post_file_form( $form );

			app()->log()->debug( 'Saving activity draft: %s', json_encode( $model ) );

			$model->save();

			// To reload the file.
			$model->refresh();

			if ( function_exists( 'exif_read_data' ) )
				// Check the image exif.
				if ( $model->file )
				{
					$path = storage_path( 'app/' . $model->file->get_path() );
					// @ Due to Illegal IFD bug in exif reader.
					$exif = @ exif_read_data( $path );
					app()->log()->debug( 'Exif for activity %s: %s', $model->id, json_encode( $exif ) );
					if ( is_array( $exif ) && isset( $exif[ 'DateTime' ] ) )
					{
						$exif_date = Carbon::parse( $exif[ 'DateTime' ] );
						// Allow a person to create the activity at home and upload the image later.
						$earliest = $model->created_at->timestamp - ( 60 * 60 * 6 );
						if ( $exif_date->timestamp < $earliest )
						{
							app()->log()->debug( 'Deleting old image for activity %s', $model->id );
							$this->delete_model_file( $model );
							throw new Exception( __( 'The photo you have uploaded is not from today!' ) );
						}
					}
				}

			$redirect_route = 'activities_draft';
			if ( isset( $form->activity_date_set ) )
			{
				if ( $form->activity_date_set->is_checked() )
				{
					$redirect_route = 'activities_draft_edit_date';
				}
			}
			else
			{
				app()->alerts()->success()
					->set_message( __( 'The activity in progress has been saved.' ) );

			}

			return redirect()->route( $redirect_route );
		}
		catch( Exception $e )
		{
			$alert = app()->alerts()->danger();
			$alert->set_message( $e->getMessage() );
			return $this->get();
		}
	}
}
