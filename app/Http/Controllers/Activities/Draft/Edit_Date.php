<?php

namespace App\Http\Controllers\Activities\Draft;

use App\Models\Activities\Activity;
use Carbon\Carbon;
use Exception;

/**
	@brief		Edit the date of a draft.
	@since		2020-08-20 21:30:14
**/
class Edit_Date
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		Build the form.
		@since		2018-11-13 12:55:43
	**/
	public function get_form( Activity $model )
	{
		$form = app()->form();

		$activity_date_set = App()->user()->site->get_meta( 'activity_date_set', 0 );
		if ( $activity_date_set < 1 )
			throw new Exception( 'How did you get here?' );

		$earliest = strtotime( sprintf( '%s days ago', $activity_date_set ) );
		$time = strtotime( $model->created_at );

		$form->year = $form->number( 'year' )
			->label( __( 'Year' ) )
			->min( date( 'Y', $earliest ) )
			->max( date( 'Y' ) )
			->value( date( 'Y', $time ) );

		$form->month = $form->number( 'month' )
			->label( __( 'Month' ) )
			->min( 1, 12 )
			->value( date( 'm', $time ) );

		$form->day = $form->number( 'day' )
			->label( __( 'Day' ) )
			->min( 1 )
			->max( 31 )
			->value( date( 'd', $time ) );

		$form->hour = $form->number( 'hour' )
			->label( __( 'Hour' ) )
			->min( 0 )
			->max( 23 )
			->value( date( 'H', $time ) );

		$form->minute = $form->number( 'minute' )
			->label( __( 'Minute' ) )
			->min( 0 )
			->max( 59 )
			->value( date( 'i', $time ) );

		$form->submit( 'save_draft' )
			->value( __( 'Save activity in progress' ) );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();
		}

		return $form;
	}

	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get()
	{
		$model = Activity::get_draft();
		return view( 'activities.draft.edit_date',
			[
				'form' => $this->get_form( $model ),
				'model' => $model,
			]
		);
	}

	/**
		@brief		POST the page.
		@since		2018-11-13 12:56:04
	**/
	public function post()
	{
		$model = Activity::get_draft();
		try
		{
			$form = $this->get_form( $model );

			// Assemble a complete string.
			$string = sprintf( '%s-%s-%s %s:%s',
				$form->input( 'year' )->get_filtered_post_value(),
				$form->input( 'month' )->get_filtered_post_value(),
				$form->input( 'day' )->get_filtered_post_value(),
				$form->input( 'hour' )->get_filtered_post_value(),
				$form->input( 'minute' )->get_filtered_post_value()
			);

			$input_time = strtotime( $string );

			$activity_date_set = App()->user()->site->get_meta( 'activity_date_set', 0 );
			$earliest = strtotime( sprintf( '%s days ago', $activity_date_set ) );
			$earliest_string = date( 'Y-m-d H:i', $earliest );
			if ( $input_time < $earliest )
				throw new Exception( __( 'You may not enter a date that is so far back. The earliest date is :earliestdate', [ 'earliestdate' => $earliest_string ] ) );

			if ( $input_time > time() )
				throw new Exception(__( 'You may not enter a future date.' ) );

			app()->log()->debug( 'Saving activity draft date: %s', json_encode( $model ) );
			$model->created_at = Carbon::parse( $string );
			$model->save();

			app()->alerts()->success()
				->set_message( __( 'The activity in progress has been saved.' ) );

			$redirect_route = 'activities_draft';
			return redirect()->route( $redirect_route );
		}
		catch( Exception $e )
		{
			$alert = app()->alerts()->danger();
			$alert->set_message( $e->getMessage() );
			return $this->get();
		}
	}
}
