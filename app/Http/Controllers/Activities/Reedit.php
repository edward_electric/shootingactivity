<?php

namespace App\Http\Controllers\Activities;

use App\Models\Activities\Activity;

/**
	@brief		Reedit the activity (= put it back in draft mode).
	@since		2019-01-23 13:29:28
**/
class Reedit
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		Check if the user already has a draft.
		@since		2019-01-23 14:20:48
	**/
	public function draft_check()
	{
		if ( ! Activity::has_draft() )
			return;
		app()->alerts()->danger()
			->set_message( __( 'You are currently editing an activity. Only one activity can be edited at once.' ) );

		return redirect()->route( 'user_dashboard' );
	}
	/**
		@brief		Build the form.
		@since		2019-01-23 13:29:28
	**/
	public function get_form( Activity $activity )
	{
		$form = app()->form();

		$form->preview = $form->submit( 'reedit' )
			->value( __( 'Re-edit this activity' ) );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();
		}

		return $form;
	}

	/**
		@brief		GET the page.
		@since		2019-01-23 13:29:28
	**/
	public function get( Activity $model )
	{
		$this->draft_check();
		return view( 'activities.reedit',
			[
				'form' => $this->get_form( $model ),
				'model' => $model,
			]
		);
	}

	/**
		@brief		POST the page.
		@since		2019-01-23 13:29:28
	**/
	public function post( Activity $model )
	{
		app()->log()->debug( 'Re-editing activity %s', $model->id );
		$this->draft_check();
		$model->mark_as_draft();
		$model->moderators()->delete();
		app()->statistics()->regenerate_later();
		return redirect()->route( 'activities_draft_edit' );
	}
}
