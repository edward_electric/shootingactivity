<?php

namespace App\Http\Controllers\Admin\Statistics;

/**
	@brief		Regenerate the stats.
	@since		2019-01-23 12:52:20
**/
class Regenerate
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		Get.
		@since		2019-01-23 12:53:21
	**/
	public function get()
	{
		app()->statistics()->regenerate_later();
		app()->alerts()->success()
			->set_message( __( 'The statistics of the site have been regenerated.' ) );
		return redirect( route( 'user_dashboard' ) );
	}
}
