<?php

namespace App\Http\Controllers\Admin\Statistics;

/**
	@brief		Statistics index for the site.
	@since		2018-11-27 21:23:52
**/
class Index
	extends \App\Http\Controllers\Activities\Statistics\Index
{
	/**
		@brief		Return the statistics object for this year.
		@since		2018-12-04 00:06:31
	**/
	public function get_statistics( $year = false )
	{
		if ( ! $year )
			$year = date( 'Y' );
		$statistics = app()->statistics()->site( app()->site(), $year );
		$statistics->table_name = app()->site()->name;
		return $statistics;
	}

	/**
		@brief		Return the view to contain the stats.
		@since		2019-01-23 13:01:53
	**/
	public function get_view()
	{
		return 'admin.statistics.index';
	}
}
