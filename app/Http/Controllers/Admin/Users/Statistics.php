<?php

namespace App\Http\Controllers\Admin\Users;

use App\Models\Users\User;

/**
	@brief		Show the statistics for the specified user.
	@since		2018-11-30 21:39:46
**/
class Statistics
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		Download the CSV version of the stats.
		@since		2018-12-04 13:24:33
	**/
	public function csv( User $user, $year = false )
	{
		$statistics = $this->get_statistics( $user, $year );
		return $statistics->output_csv();
	}

	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get( User $user, $year = false )
	{
		$statistics = $this->get_statistics( $user, $year );
		return view( 'activities.statistics.index', [
			'statistics' => $statistics,
		] );
	}

	/**
		@brief		Return the statistics object for this year.
		@since		2018-12-04 00:06:31
	**/
	public function get_statistics( User $user, $year = false )
	{
		if ( ! $year )
			$year = date( 'Y' );
		$statistics = app()->statistics()->user( $user, $year );
		$statistics->table_name = $user->name;
		return $statistics;
	}

	/**
		@brief		Download the PDF version of the stats.
		@since		2018-12-04 13:24:33
	**/
	public function pdf( User $user, $year = false )
	{
		$statistics = $this->get_statistics( $user, $year );
		$statistics->output_pdf();
	}
}
