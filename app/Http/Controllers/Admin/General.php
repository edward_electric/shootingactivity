<?php

namespace App\Http\Controllers\Admin;

use Exception;

/**
	@brief		General site settings.
	@since		2018-11-16 18:42:59
**/
class General
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		Build the form.
		@since		2018-11-13 12:55:43
	**/
	public function get_form()
	{
		$form = app()->form();
		$form->normal_inputs = [];

		$user = app()->user();

		$fs = $form->fieldset( 'fs_site' );
		$fs->legend()->label( __( 'Site settings' ) );

		$form->site_name = $fs->text( 'site_name' )
			->description( __( 'This is the name of the installation that is visible to users and in documents. At least 3 characters.' ) )
			->label( __( 'Site name' ) )
			->pattern( '.{3,}' )		// At least 3 chars.
			->required()
			->trim()
			->value( $user->site->name );

		$form->organisation_url = $fs->url( 'organisation_url' )
			->description( __( 'This is the optional address where the users in your organisation can read more about Shooting Activity.' ) )
			->label( __( 'Organisation address' ) )
			->trim()
			->value( $user->site->get_meta( 'organisation_url' ) );
		$form->normal_inputs []= 'organisation_url';

		$form->organisation_email = $fs->email( 'organisation_email' )
			->description( __( 'This is the optional e-mail address the users can contact for help.' ) )
			->label( __( 'Organisation e-mail' ) )
			->trim()
			->value( $user->site->get_meta( 'organisation_email' ) );
		$form->normal_inputs []= 'organisation_email';

		$fs = $form->fieldset( 'fs_activities' );
		$fs->legend()->label( __( 'Activity settings' ) );

		$form->activity_date_set = $fs->number( 'activity_date_set' )
			->description( __( 'How many days in the past a user is allowed to date an activity. Set to 0 to disable and always use the current date.' ) )
			->label( __( 'Activity backdating days' ) )
			->min( 0 )
			->value( $user->site->get_meta( 'activity_date_set', 0 ) );
		$form->normal_inputs []= 'activity_date_set';

		$form->scoring = $fs->checkbox( 'scoring' )
			->checked( $user->site->has_enabled( 'scoring' ) )
			->description( __( 'Allow users to add their precision scores in activities.' ) )
			->label( __( 'Enable scoring' ) );
		$form->normal_inputs []= 'scoring';

		$form->scoring_x = $fs->checkbox( 'scoring_x' )
			->checked( $user->site->has_enabled( 'scoring_x' ) )
			->description( __( 'Allow users to track inner 10 hits.' ) )
			->label( __( 'Enable X scoring' ) );
		$form->normal_inputs []= 'scoring_x';

		$fs = $form->fieldset( 'fs_images' );
		$fs->legend()->label( __( 'Image settings' ) );

		$form->images = $fs->checkbox( 'images' )
			->checked( $user->site->get_meta( 'images' ) )
			->description( __( 'Allow users to upload images to their activities and of their guns.' ) )
			->label( __( 'Enable images' ) );
		$form->normal_inputs []= 'images';

		$fs = $form->fieldset( 'fs_moderation' );
		$fs->legend()->label( __( 'Moderation settings' ) );

		$form->allow_mass_import = $fs->checkbox( 'allow_mass_import' )
			->checked( $user->site->get_meta( 'allow_mass_import' ) )
			->description( __( 'Allow moderators to mass import activities.' ) )
			->label( __( 'Allow mass import' ) );
		$form->normal_inputs []= 'allow_mass_import';

		$form->moderators = $fs->number( 'moderators' )
			->description( __( 'How many moderators are required to check the user activities.' ) )
			->label( __( 'Moderators' ) )
			->min( 0, 10 )
			->value( $user->site->get_meta( 'moderators' ) );
		$form->normal_inputs []= 'moderators';

		$fs = $form->fieldset( 'fs_socialization' );
		$fs->legend()->label( __( 'Socialization' ) );

		$form->socialization = $fs->checkbox( 'socialization' )
			->checked( $user->site->get_meta( 'socialization' ) )
			->description( __( 'Allow users socialize with each other using profiles and messages.' ) )
			->label( __( 'Enable socializing' ) );
		$form->normal_inputs []= 'socialization';

		$form->public_guns = $fs->checkbox( 'public_guns' )
			->checked( $user->site->get_meta( 'public_guns' ) )
			->description( __( "Show the user's guns in the user profile." ) )
			->label( __( 'Enable public guns' ) );
		$form->normal_inputs []= 'public_guns';

		$form->notice_board = $fs->checkbox( 'notice_board' )
			->checked( $user->site->get_meta( 'notice_board' ) )
			->description( __( 'The notice board allows users to post messages for other users to read and comment on.' ) )
			->label( __( 'Enable notice board' ) );
		$form->normal_inputs []= 'notice_board';

		$form->notice_board_days = $fs->number( 'notice_board_days' )
			->description( __( 'How many days to keep messages on the notice board before automatically deleting them. 0 keeps messages forever, but that is not recommended in these GDPR days.' ) )
			->label( __( 'Notice board age' ) )
			->min( 0, 365 )
			->value( $user->site->get_meta( 'notice_board_days' ) );
		$form->normal_inputs []= 'notice_board_days';

		$form->submit( 'save' )
			->value( __( 'Save settings' ) );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();
		}

		return $form;
	}

	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get()
	{
		$form = $this->get_form();

		return view( 'admin.general',
			[
				'form' => $form,
			]
		);
	}

	/**
		@brief		POST the page.
		@since		2018-11-13 12:56:04
	**/
	public function post()
	{
		$form = $this->get_form();

		try
		{
			$user = app()->user();

			$site_name = $form->site_name->get_filtered_post_value();
			$user->site->name = $site_name;

			foreach( $form->normal_inputs as $key )
				$user->site->set_meta( $key, $form->$key->get_filtered_post_value() );

			$user->site->save();

			return redirect()->route( 'user_dashboard' );
		}
		catch( Exception $e )
		{
			$alert = app()->alerts()->danger();
			$alert->set_message( $e->getMessage() );
			return $this->get();
		}
	}
}
