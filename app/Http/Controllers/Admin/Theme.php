<?php

namespace App\Http\Controllers\Admin;

use Exception;

/**
	@brief		Site theme settings.
	@since		2023-02-25 12:20:35
**/
class Theme
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		Build the form.
		@since		2018-11-13 12:55:43
	**/
	public function get_form()
	{
		$form = app()->form();
		$form->colors = [];
		$theme = app()->site()->theme();
		$theme_defaults = $theme->get_defaults();

		$key = 'reset_colors';
		$form->$key = $form->checkbox( $key )
			->description( __( 'Reset all of the colors to the default.' ) )
			->label( __( 'Reset colors' ) );

		$fs = $form->fieldset( 'fs_images' );
		$fs->legend()->label( __( 'Images' ) );

		// Load the header image from the site.
		$header_image = $theme->header_image();
		$header_image->get_file_form( $fs );

		$fs = $form->fieldset( 'fs_colors' );
		$fs->legend()->label( __( 'Colors' ) );

		$key = 'theme_color_primary';
		$form->$key = $fs->color( $key )
			->description( __( 'This is the primary color associated to your organisation. The default value is :color', [ 'color' => $theme_defaults->$key ] ) )
			->label( __( 'Primary color' ) )
			->value( $theme->get( $key, $theme_defaults->$key ) );
		$form->colors []= $key;

		$key = 'theme_color_text';
		$form->$key = $fs->color( $key )
			->description( __( 'This is the text color seen mostly everywhere. The default value is :color', [ 'color' => $theme_defaults->$key ] ) )
			->label( __( 'Text color' ) )
			->value( $theme->get( $key, $theme_defaults->$key ) );
		$form->colors []= $key;

		$key = 'theme_color_background';
		$form->$key = $fs->color( $key )
			->description( __( 'This is the background color associated to your organisation. The default value is :color', [ 'color' => $theme_defaults->$key ] ) )
			->label( __( 'Background color' ) )
			->value( $theme->get( $key, $theme_defaults->$key ) );
		$form->colors []= $key;

		$key = 'theme_css';
		$form->$key = $fs->textarea( $key )
			->cols( 40, 10 )
			->description( __( 'Use this text box to paste your custom CSS that is used on all pages.' ) )
			->label( __( 'Custom CSS' ) )
			->value( $theme->get( $key ) );

		$form->submit( 'save' )
			->value( __( 'Save settings' ) );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();
		}

		return $form;
	}

	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get()
	{
		$form = $this->get_form();

		return view( 'admin.theme',
			[
				'form' => $form,
			]
		);
	}

	/**
		@brief		POST the page.
		@since		2018-11-13 12:56:04
	**/
	public function post()
	{
		$form = $this->get_form();
		$theme = app()->site()->theme();

		try
		{
			$user = app()->user();

			if ( $form->reset_colors->is_checked() )
			{
				foreach( $form->colors as $key )
					$theme->delete( $key );
			}
			else
			{
				// Save all of the colors.
				foreach( $form->colors as $key )
					$theme->set( $key, $form->$key->get_filtered_post_value() );
			}

			$header_image = $theme->header_image();
			$header_image->post_file_form( $form );

			$theme->set( 'theme_css', $form->theme_css->get_filtered_post_value() );

//			return redirect()->route( 'user_dashboard' );
			return redirect()->back();
		}
		catch( Exception $e )
		{
			$alert = app()->alerts()->danger();
			$alert->set_message( $e->getMessage() );
			return $this->get();
		}
	}
}
