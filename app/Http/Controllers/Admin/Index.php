<?php

namespace App\Http\Controllers\Admin;

/**
	@brief		Site admin index.
	@since		2018-11-16 14:23:04
**/
class Index
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get()
	{
		return view( 'admin.index',
			[
			]
		);
	}
}
