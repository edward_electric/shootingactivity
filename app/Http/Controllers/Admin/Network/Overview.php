<?php

namespace App\Http\Controllers\Admin\Network;

use App\Models\Sites\Site;

/**
	@brief		Network overview of all sites and users.
	@since		2020-02-23 18:00:57
**/
class Overview
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get()
	{
		return view( 'admin.network.overview',
			[
				'sites' => Site::orderBy( 'name' )->get(),
			]
		);
	}
}
