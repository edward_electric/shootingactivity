<?php

namespace App\Http\Controllers\Admin\Network;

/**
	@brief		Network / sites admin.
	@since		2018-11-16 00:31:35
**/
class Index
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get()
	{
		return view( 'admin.network.index',
			[
			]
		);
	}
}
