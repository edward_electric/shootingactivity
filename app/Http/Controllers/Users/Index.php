<?php

namespace App\Http\Controllers\Users;

/**
	@brief		Users on this site.
	@since		2018-12-22 23:18:17
**/
class Index
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get()
	{
		$models = app()->site()->users()->orderBy( 'name' )->get();
		return view( 'users.index',
			[
				'models' => $models,
			]
		);
	}
}
