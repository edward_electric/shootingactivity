<?php

namespace App\Http\Controllers\Users;

use App\Models\Users\User;

/**
	@brief		Show this user.
	@since		2018-12-22 23:27:35
**/
class View
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get( User $user )
	{
		return view( 'users.view',
			[
				'model' => $user,
			]
		);
	}
}
