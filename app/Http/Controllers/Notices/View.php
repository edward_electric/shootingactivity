<?php

namespace App\Http\Controllers\Notices;

use App\Models\Notices\Notice;

/**
	@brief		View a notice.
	@since		2018-12-23 18:01:16
**/
class View
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		View the notice.
		@since		2018-12-23 18:49:27
	**/
	public function get( Notice $model )
	{
		return view( 'notices.view', [
			'form' => Add::get_form(),
			'model' => $model,
		] );
	}

	/**
		@brief		POST the page.
		@since		2018-12-23 17:07:49
	**/
	public function post( Notice $notice )
	{
		try
		{
			// Save the new comment.
			$comment = Add::post_comment();

			// Make it a reply to the first comment in the notice.
			$comment->parent()->associate( $notice->comment );
			$comment->save();

			$notice->touch();

			app()->log()->info( 'Replied to notice %s, comment %s',
				json_encode( $notice ),
				json_encode( $comment )
			);

			app()->alerts()->success()
				->set_message( __( 'Reply added!' ) );

			$notice->notify_users();

			return redirect()->route( 'notices_view', [ $notice ] );
		}
		catch( Exception $e )
		{
			$alert = app()->alerts()->danger();
			$alert->set_message( $e->getMessage() );
			return $this->get();
		}
	}
}
