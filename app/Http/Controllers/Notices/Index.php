<?php

namespace App\Http\Controllers\Notices;

use App\Models\Comments\Comment;
use App\Models\Notices\Notice;

/**
	@brief		The sitewide noticeboard.
	@since		2018-12-23 14:57:36
**/
class Index
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		Get all notices
		@since		2018-12-23 16:42:07
	**/
	public function get()
	{
		return view( 'notices.index', [
			'models' => Notice::on_this_site()->get(),
		] );
	}
}
