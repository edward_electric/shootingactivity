<?php

namespace App\Http\Controllers\Notices;

use App\Models\Comments\Comment;
use App\Models\Notices\Notice;

/**
	@brief		Add a notice.
	@since		2018-12-23 17:07:49
**/
class Add
	extends \App\Http\Controllers\Comments\Add
{
	/**
		@brief		GET the page.
		@since		2018-12-23 17:07:49
	**/
	public function get()
	{
		return view( 'notices.add',
			[
				'form' => static::get_form( [
					'text_label' => __( 'Notice text' ),
				] ),
			]
		);
	}

	/**
		@brief		POST the page.
		@since		2018-12-23 17:07:49
	**/
	public function post()
	{
		try
		{
			// First save the comment.
			$comment = parent::post_comment();

			// Now we can save the notice.
			$notice = new Notice();
			$notice->comment()->associate( $comment );
			$notice->site()->associate( app()->site() );
			$notice->save();

			app()->log()->info( 'Added notice %s, comment %s',
				json_encode( $notice ),
				json_encode( $comment )
			);

			app()->alerts()->success()
				->set_message( __( 'Notice added!' ) );

			$notice->notify_users();

			return redirect()->route( 'notices_index' );
		}
		catch( Exception $e )
		{
			$alert = app()->alerts()->danger();
			$alert->set_message( $e->getMessage() );
			return $this->get();
		}
	}
}
