<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

/**
	@brief		The start page.
	@since		2018-11-13 12:53:50
**/
class Start
	extends Controller
{
	/**
		@brief		GET the page.
		@since		2018-11-13 12:54:53
	**/
	public function get()
	{
		// If the user is not logged in, go to the login window.
		if ( ! Auth::check() )
			return redirect()->route( 'user_login' );
		else
			return redirect()->route( 'user_dashboard' );
	}
}
