<?php

namespace App\Http\Controllers\Install;

/**
	@brief		Prepare for a new install.
	@since		2018-11-16 17:20:57
**/
class Step_0 extends
	\App\Http\Controllers\Controller
{
	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get()
	{
		\Artisan::call( 'key:generate', [ '--force' => true ] );		// Force because we're on "production"

		return redirect()->route( 'install_step_1' );
	}
}
