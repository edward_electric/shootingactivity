<?php

namespace App\Http\Controllers\Install;

use App\Models\Users\User;
use App\Models\Sites\Site;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
	@brief		The first, and hopefully only, step necessary.
	@since		2018-11-13 23:39:13
**/
class Step_1 extends
	\App\Http\Controllers\Controller
{
	/**
		@brief		Build the install form.
		@since		2018-11-13 12:55:43
	**/
	public function get_form()
	{
		$form = app()->form();

		$fs = $form->fieldset( 'fs_database' );
		$fs->legend->label( __( 'Database connection' ) );

		$fs->text( 'db_host' )
			->description( __( 'The host name of the database server. The default is usually localhost.' ) )
			->label( __( 'Database address' ) )
			->placeholder( 'localhost' )
			->required()
			->trim()
			->value( env( 'DB_HOST', 'localhost' ) );

		$fs->number( 'db_port' )
			->description( __( 'The default is usually 3306.' ) )
			->label( __( 'Database port' ) )
			->placeholder( '3306' )
			->required()
			->size( 5 )
			->trim()
			->value( env( 'DB_PORT', '3306' ) );

		$fs->text( 'db_database' )
			->description( __( 'The name of the database to install to.' ) )
			->label( __( 'Database name' ) )
			->placeholder( 'shootingactivity' )
			->required()
			->trim()
			->value( env( 'DB_DATABASE', '' ) );

		$fs->text( 'db_username' )
			->description( __( 'The database account user name.' ) )
			->label( __( 'User name' ) )
			->required()
			->trim()
			->value( env( 'DB_USERNAME', '' ) );

		$fs->text( 'db_password' )
			->description( __( 'The database account password.' ) )
			->label( __( 'Password' ) )
			->required()
			->trim()
			->value( env( 'DB_PASSWORD', '' ) );

		$fs = $form->fieldset( 'fs_admin' );
		$fs->legend->label( __( 'Account details' ) );

		$fs->text( 'site_name' )
			->description( __( 'The name of your club or organisation.' ) )
			->label( __( 'Club name' ) )
			->required()
			->trim()
			->value( env( 'APP_NAME', '' ) );

		$fs->email( 'admin_email' )
			->description( __( 'Your e-mail address that will be used to create the administrator account.' ) )
			->label( __( 'Admin e-mail' ) )
			->required()
			->trim()
			->value( env( 'ADMIN_EMAIL', '' ) );

		$form->submit( 'install' )
			->value( __( 'Install to this database' ) );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();
		}

		return $form;
	}

	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get()
	{
		$form = $this->get_form();

		return view( 'install.step_1',
			[
				'form' => $form,
			]
		);
	}

	/**
		@brief		POST the page.
		@since		2018-11-13 12:56:04
	**/
	public function post()
	{
		$form = $this->get_form();

		$db_host = $form->input( 'db_host' )->get_filtered_post_value();
		$db_port = $form->input( 'db_port' )->get_filtered_post_value();
		$db_database = $form->input( 'db_database' )->get_post_value();
		$db_username = $form->input( 'db_username' )->get_filtered_post_value();
		$db_password = $form->input( 'db_password' )->get_filtered_post_value();

		$admin_email = $form->input( 'admin_email' )->get_filtered_post_value();

		// Try to connect to the database.
		try
		{
			// Create an install connection.
			$install = config( 'database.connections.mysql' );
			$install[ 'host' ] = $db_host;
			$install[ 'port' ] = $db_port;
			$install[ 'database' ] = $db_database;
			$install[ 'username' ] = $db_username;
			$install[ 'password' ] = $db_password;
			// Change the namespace.
			foreach( $install as $key => $value )
			{
				unset( $install[ $key ] );
				$install[ 'database.connections.install.' . $key ] = $value;
			}
			config( $install );

			DB::setDefaultConnection( 'install' );
			DB::connection( 'install' )->statement( 'SHOW TABLES' );

			// That worked! Install.

			app()->set_env( [
				'APP_INSTALLED' => 'yes',
				'APP_URL' => route( 'start' ),
				'DB_HOST' => $db_host,
				'DB_PORT' => $db_port,
				'DB_DATABASE' => $db_database,
				'DB_USERNAME' => $db_username,
				'DB_PASSWORD' => $db_password,
			] );

			// In order to save the session with this new key.
			request()->session()->regenerate();

			Schema::defaultStringLength( 191 );			// Workaround for older versions of Mysql. "Specified key was too long; max key length is 767 bytes"

			// Clean the database.
			foreach( DB::select('SHOW TABLES') as $table)
			{
				$table_array = get_object_vars($table);
				Schema::drop($table_array[key($table_array)]);
			}

			\Artisan::call( 'migrate:install' );
			\Artisan::call( 'migrate', [ '--force' => true ] );		// Force because we're on "production"

			// Create the site
			$site = new Site();
			$site->name = $form->input( 'site_name' )->get_filtered_post_value();
			$site->save();

			// Generate a password for admin.
			$admin_password = md5( microtime() );
			$admin_password = substr( $admin_password, 8 );
			session( [ 'install_admin_password' => $admin_password ] );

			// And now create the admin user.
			$user = new User();
			$user->name = 'admin';
			$user->email = $admin_email;
			$user->site()->associate( $site );
			$user->set_password( $admin_password );
			$user->save();

			// This is for dusk testing that seems to remember that we're logged in between tests.
			Auth::logout();

			// Assign the user the various caps.
			$user->assign_roles( [ 'Network Admin' ] );

			return redirect()->route( 'install_finished' );
		}
		catch( Exception $e )
		{
			$alert = app()->alerts()->danger();
			$alert->set_message( __( 'Unable to connect to the database:' ) . $e->getMessage() );
			return $this->get();
		}
	}
}
