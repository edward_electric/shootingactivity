<?php

namespace App\Http\Controllers\Install;

use App\Models\Users\User;

/**
	@brief		Install is finished.
	@since		2018-11-14 10:28:26
**/
class Finished extends
	\App\Http\Controllers\Controller
{
	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get()
	{
		if ( ! session( 'install_admin_password' ) )
			return redirect()->route( 'start' );

		$user = User::firstOrFail();

		return view( 'install.finished',
			[
				'user' => $user,
				'install_admin_password' => session( 'install_admin_password' ),
			]
		);
	}
}
