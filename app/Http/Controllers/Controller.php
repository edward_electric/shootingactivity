<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	public function __construct()
	{
		// Provide a collection that the templates can use to store data between includes.
		$vars = new \App\Classes\Collections\Collection();
		$vars->set( 'controller_started', microtime( true ) );
		View::share ( 'vars', $vars );
	}
}
