<?php

namespace App\Http\Controllers\User;

use Illuminate\Support\Facades\Auth;

/**
	@brief		Log the user out.
	@since		2018-11-13 23:34:54
**/
class Logout
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get()
	{
		if ( Auth::check() )
		{
			app()->log()->info( '%s logged out.', app()->user() );

			Auth::logout();
	        app()->alerts()->success()->set_message( __( 'You have been logged out!' ) );
        }
		return redirect()->route( 'start' );
	}
}
