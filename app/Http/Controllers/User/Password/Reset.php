<?php

namespace App\Http\Controllers\User\Password;

use App\Models\Users\User;
use Exception;

/**
	@brief		Reset the user's password.
	@since		2018-11-13 20:14:42
**/
class Reset
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		Build the install form.
		@since		2018-11-13 12:55:43
	**/
	public function get_form()
	{
		$form = app()->form();

		$new_password = User::generate_password();

		$form->password = $form->text( 'password' )
			->label( __( 'Type your chosen password here, or use this auto-generated password.' ) )
			->label( __( 'Your new password' ) )
			->required()
			->trim()
			->value( $new_password );

		$form->submit( 'set_password' )
			->value( __( 'Change my password' ) );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();
		}

		return $form;
	}

	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get( $user_id, $token )
	{
		try
		{
			$user = User::findOrFail( $user_id );

			// Validate the token.
			if( $token != $user->get_reminder_token() )
				throw new Exception();	// There's no reason to be more specific than this.

			$form = $this->get_form();

			return view( 'user.password.reset',
				[
					'form' => $form,
				]
			);
		}
		catch ( Exception $e )
		{
			sleep( rand( 2, 5 ) );		// Just because.
			$alert = app()->alerts()->danger();
			$alert->set_message( __( 'The password reset link is invalid! Please try again.' ) );
			return redirect()->route( 'user_password_send' );
		}
	}

	/**
		@brief		POST the page.
		@since		2018-12-29 19:04:12
	**/
	public function post( $user_id, $token )
	{
		try
		{
			$user = User::findOrFail( $user_id );

			// Validate the token.
			if( $token != $user->get_reminder_token() )
				throw new Exception();	// There's no reason to be more specific than this.

			$form = $this->get_form();

			$new_password = $form->password->get_filtered_post_value();

			$user->set_password( $new_password );
			$user->save();

			$alert = app()->alerts()->success();
			$alert->set_message( __( 'Your new password has been set to: :newpassword', [ 'newpassword' => '<strong>' . $new_password . '</strong>' ] ) );
			$alert = app()->alerts()->success();
			$alert->set_message( __( 'The new password is already in the password field, so login to make your browser remember the new password.' ) );
			session( [ 'new_password_data' => [
				'user_id' => $user_id,
				'new_password' => $new_password,
			]  ] );
			return redirect()->route( 'user_login' );
		}
		catch( Exception $e )
		{
			$alert = app()->alerts()->danger();
			$alert->set_message( $e->getMessage() );
			return $this->get();
		}
	}
}
