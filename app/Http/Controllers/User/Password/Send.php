<?php

namespace App\Http\Controllers\User\Password;

use App\Models\Users\User;
use Exception;

/**
	@brief		Send a passord reset link to a user.
	@since		2018-11-13 23:40:29
**/
class Send
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		Build the install form.
		@since		2018-11-13 12:55:43
	**/
	public function get_form()
	{
		$form = app()->form();

		$form->email( 'email' )
			->label( __( 'Your e-mail address' ) )
			->required()
			->trim();

		$form->submit( 'send_reset' )
			->value( __( 'Send me a password reset link' ) );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();
		}

		return $form;
	}

	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get()
	{
		$form = $this->get_form();

		return view( 'user.password.send',
			[
				'form' => $form,
			]
		);
	}

	/**
		@brief		POST the page.
		@since		2018-11-13 12:56:04
	**/
	public function post()
	{
		$form = $this->get_form();

		$email = $form->input( 'email' )->get_filtered_post_value();

		try
		{
			// Try to find a user with this e-mail address.
			$user = User::where( 'email', $email )->get();
			if ( count( $user ) < 1 )
				throw new Exception( __( 'Invalid e-mail address!' ) );
			$user = $user->first();

			$user->send_password();

			$alert = app()->alerts()->success();
			$alert->set_message( __( 'An e-mail with a password reset link is on its way!' ) );
			return redirect()->route( 'user_login' );
		}
		catch( Exception $e )
		{
			$alert = app()->alerts()->danger();
			$alert->set_message( $e->getMessage() );
			return $this->get();
		}
	}
}
