<?php

namespace App\Http\Controllers\User\Guns;

/**
	@brief		Show the user's guns.
	@since		2018-11-15 18:25:00
**/
class Index
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		GET the page.
		@since		2018-11-15 15:15:29
	**/
	public function get()
	{
		$guns = [];
		return view( 'user.guns.index',
			[
				'guns' => $guns,
			]
		);
	}
}
