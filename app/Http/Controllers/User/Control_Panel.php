<?php

namespace App\Http\Controllers\User;

/**
	@brief		User's control panel where he can configure his settings.
	@since		2018-11-15 15:11:26
**/
class Control_Panel
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get()
	{
		return view( 'user.control_panel',
			[
			]
		);
	}
}
