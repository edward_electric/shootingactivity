<?php

namespace App\Http\Controllers\User;

use App\Models\Users\User;
use Exception;

/**
	@brief		The user is changing his own settings.
	@since		2018-12-20 17:38:03
**/
class Settings
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		Build the form.
		@since		2018-12-20 17:38:03
	**/
	public function get_form()
	{
		$form = app()->form();
		$user = app()->user();

		$form->email = $form->email( 'email' )
			->description( __( 'Enter your e-mailaddress. It is used to send password reminders and notifications.' ) )
			->label( __( 'E-mail' ) )
			->placeholder( 'info@shootingactivity.com' )
			->required()
			->trim()
			->value( $user->email );

		User::get_email_inputs( $form, $user );

		if ( $user->site->has_enabled( 'scoring' ) )
		{
			$fs = $form->fieldset( 'fs_scoring' )
				->label( __( 'Scoring' ) );


			$key = 'string_order';
			$form->string_order = $fs->select( $key )
				->description( __( 'How should the points in a score be sorted?' ) )
				->label( __( 'Point order' ) )
				->opt( '', __( 'As I input them' ) )
				->opt( 'ascending', __( 'Ascending, smallest points first' ) )
				->opt( 'descending', __( 'Descending, largest points first' ) )
				->value( $user->get_meta( $key ) );

			$key = 'score_analysis';
			$form->score_analysis = $fs->select( $key )
				->description( __( 'What kind of score analysis to display.' ) )
				->label( __( 'Score analysis' ) )
				->opt( 'none', __( 'None' ) )
				->opt( '', __( 'Summary' ) )		// Default
				->opt( 'detailed', __( 'Detailed' ) )
				->value( $user->get_meta( $key ) );
		}

		$form->submit( 'save' )
			->value( __( 'Save settings' ) );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();
		}

		return $form;
	}

	/**
		@brief		GET the page.
		@since		2018-12-20 17:38:03
	**/
	public function get()
	{
		$form = $this->get_form();

		return view( 'user.settings',
			[
				'form' => $form,
			]
		);
	}

	/**
		@brief		POST the page.
		@since		2018-12-20 17:38:03
	**/
	public function post()
	{
		$form = $this->get_form();
		$user = app()->user();

		try
		{
			try
			{
				$user->email = $form->email->get_filtered_post_value();
				$user->save();

			}
			catch( Exception $e )
			{
				throw new Exception( __( 'That e-mail address is already in use. Please choose another.' ) );
			}

			// These are saved in the meta, so the user has to exist.
			User::post_email_inputs( $form, $user );

			if ( $user->site->has_enabled( 'scoring' ) )
			{
				$key = 'string_order';
				$value = $form->string_order->get_filtered_post_value();
				if ( $value == '' )
					$user->delete_meta( $key );
				else
					$user->set_meta( $key, $value );

				$key = 'score_analysis';
				$value = $form->score_analysis->get_filtered_post_value();
				if ( $value == '' )
					$user->delete_meta( $key );
				else
					$user->set_meta( $key, $value );
			}

			app()->alerts()->success()
				->set_message( __( 'Settings saved!' ) );

			return redirect()->route( 'user_dashboard' );
		}
		catch( Exception $e )
		{
			app()->alerts()->danger()
				->set_message( $e->getMessage() );
			return $this->get();
		}
	}
}
