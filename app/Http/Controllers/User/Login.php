<?php

namespace App\Http\Controllers\User;

use App\Models\Users\User;
use Exception;
use Illuminate\Support\Facades\Auth;

/**
	@brief		Handle logins.
	@since		2018-11-13 17:17:48
**/
class Login
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		Build the form.
		@since		2018-11-13 12:55:43
	**/
	public function get_form()
	{
		$form = app()->form();

		$form->email = $form->email( 'email' )
			->label( __( 'E-mail' ) )
			->required()
			->trim();

		$form->password = $form->password( 'password' )
			->label( __( 'Password' ) )
			->required()
			->trim();

		$form->login = $form->submit( 'login' )
			->value( __( 'Log in' ) );

		$new_password_data = session( 'new_password_data' );
		if ( $new_password_data )
		{
			$form->email->value( User::find( $new_password_data[ 'user_id' ] )->email );
			$form->password->value( $new_password_data[ 'new_password' ] );
		}

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();
		}

		return $form;
	}

	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get()
	{
		if ( app()->user() )
			return redirect()->route( 'user_dashboard' );

		$form = $this->get_form();

		return view( 'user.login',
			[
				'form' => $form,
			]
		);
	}

	/**
		@brief		POST the page.
		@since		2018-11-13 12:56:04
	**/
	public function post()
	{
		$form = $this->get_form();

		$email = $form->input( 'email' )->get_filtered_post_value();
		$password = $form->input( 'password' )->get_filtered_post_value();

		// Try to connect to the database.
		try
		{
			Auth::attempt(
			[
				'email' => $email,
				'password' => $password,
			], true );		// For remember me

			if ( ! Auth::check() )
				throw new Exception( __( 'The password you entered is invalid.' ) );

			$url = session()->get( '403_url' );
			if ( ! $url )
				$url = route( 'user_dashboard' );
			session()->forget( '403_url' );
			session()->forget( 'new_password_data' );

			// This is not the best place for it, since it should be run on a schedule anyways.
			\App\Jobs\Notices\Prune::dispatch();

			app()->log()->info( '%s logged in.', app()->user() );

			return redirect()->to( $url );
		}
		catch( Exception $e )
		{
			$alert = app()->alerts()->danger();
			$alert->set_message( $e->getMessage() );
			return $this->get();
		}
	}
}
