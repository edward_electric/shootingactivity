<?php

namespace App\Http\Controllers\User;

use App\Models\Activities\Activity;

/**
	@brief		User dashboard.
	@since		2018-11-13 17:17:48
**/
class Dashboard
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		GET the page.
		@since		2018-11-13 12:55:52
	**/
	public function get()
	{
		$data = [];
		$data[ 'has_draft' ] = Activity::has_draft();

		if ( app()->user()->may( 'moderate_activities' ) )
			$data[ 'unmoderated' ] = \App\Models\Activities\Activity::get_unmoderated_by_me();

		return view( 'user.dashboard', $data );
	}
}
