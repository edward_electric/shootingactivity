<?php

namespace App\Http\Controllers\User;

use Exception;

/**
	@brief		Modify the password.
	@since		2018-11-15 15:15:29
**/
class Password
	extends \App\Http\Controllers\Controller
{
	/**
		@brief		Build the form.
		@since		2018-11-15 15:15:29
	**/
	public function get_form()
	{
		$form = app()->form();

		$form->password( 'old_password' )
			->label( __( 'Your current password' ) )
			->required()
			->trim();

		$form->password( 'new_password_1' )
			->description( __( 'At least 8 characters.' ) )
			->label( __( 'Your new password' ) )
			->pattern( '.{8,}' )
			->required()
			->trim();

		$form->password( 'new_password_2' )
			->label( __( 'Retype your new password' ) )
			->pattern( '.{8,}' )
			->required()
			->trim();

		$form->submit( 'change_password' )
			->value( __( 'Change my password' ) );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();
		}

		return $form;
	}

	/**
		@brief		GET the page.
		@since		2018-11-15 15:15:29
	**/
	public function get()
	{
		$form = $this->get_form();

		return view( 'user.password',
			[
				'form' => $form,
			]
		);
	}

	/**
		@brief		POST the page.
		@since		2018-11-15 15:15:29
	**/
	public function post()
	{
		$form = $this->get_form();

		try
		{
			$old_password = $form->input( 'old_password' )->get_filtered_post_value();
			$new_password_1 = $form->input( 'new_password_1' )->get_filtered_post_value();
			$new_password_2 = $form->input( 'new_password_2' )->get_filtered_post_value();

			$user = app()->user();
			$user->refresh();	// Model caching + dusk testing requires this.
			if ( ! $user->check_password( $old_password ) )
				throw new Exception( __( 'Your current password is incorrect.' ) );

			if ( strlen( $new_password_1 ) < 8 )
				throw new Exception( __( 'Your new password is too short.' ) );

			if ( $new_password_1 != $new_password_2 )
				throw new Exception( __( 'Your new passwords do not match.' ) );

			$user->set_password( $new_password_1 );
			$user->save();

			app()->alerts()->success()
				->set_message( __( 'Your password has been changed!' ) );

			return redirect()->route( 'user_dashboard' );
		}
		catch( Exception $e )
		{
			app()->alerts()->danger()
				->set_message( $e->getMessage() );
			return $this->get();
		}
	}
}
