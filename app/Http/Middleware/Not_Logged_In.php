<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

/**
	@brief		If you're logged in, you should go to the dashboard.
	@since		2018-12-28 17:01:38
**/
class Not_Logged_In
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ( Auth::guard( $guard )->check() )
			return redirect()->route( 'user_dashboard' );
        return $next( $request );
    }
}
