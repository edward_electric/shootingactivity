<?php

namespace App\Http\Middleware;

use Closure;

/**
	@brief		Generic check for competition r/w passwords.
	@since		2018-07-26 20:27:52
**/
class Installed
{
	/**
		@brief		Check whether the system is installed.
		@since		2018-11-13 12:50:27
	**/
	public static function is_installed()
	{
		return env( 'APP_INSTALLED' ) != '';
	}

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle( $request, Closure $next )
    {
    	if ( static::is_installed() )
    		return $next( $request );
		return redirect()->route( 'install' );
    }
}
