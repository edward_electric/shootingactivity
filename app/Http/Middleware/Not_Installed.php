<?php

namespace App\Http\Middleware;

use Closure;

/**
	@brief		Check whether the app is not installed.
	@since		2018-07-26 20:27:52
**/
class Not_Installed
	extends Installed
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle( $request, Closure $next )
    {
    	if ( ! static::is_installed() )
    		return $next( $request );
		return redirect()->route( 'start' );
    }
}
