<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Files\File;

/**
	@brief		Check whether a user has access to this file.
	@since		2018-11-18 12:18:04
**/
class File_Access
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle( $request, Closure $next )
    {
    	$file_id = $request->segment( 3 );
    	$key = $request->segment( 4 );
    	$file = File::findOrFail( $file_id );

    	if ( $file->get_key() != $key )
    		throw new Exception( 'Image not found.' );

		return $next( $request );
    }
}
