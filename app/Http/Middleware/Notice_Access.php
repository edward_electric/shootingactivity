<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Notices\Notice;

/**
	@brief		May the user access this notice?
	@since		2018-12-23 17:37:32
**/
class Notice_Access
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle( $request, Closure $next, $index )
    {
    	$notice_id = $request->segment( $index );
    	$notice = Notice::findOrFail( $notice_id );

		if ( $notice->site_id != app()->site()->id )
			abort( 403 );

		return $next( $request );
    }
}
