<?php

namespace App\Http\Middleware;

use Closure;

/**
	@brief		Check if the user may do this capability.
	@since		2018-11-16 00:04:41
**/
class User_May
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle( $request, Closure $next, $capability )
    {
    	// Network admins get to do what they want.
    	if ( App()->user()->may( 'manage_network' ) )
    		return $next( $request );

    	// User must be capable of...
    	if ( ! App()->user()->may( $capability ) )
			return redirect()->route( 'user_dashboard' );

        return $next( $request );
    }
}
