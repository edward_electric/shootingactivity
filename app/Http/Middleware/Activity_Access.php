<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Activities\Activity;

/**
	@brief		May the user access this activity?
	@since		2018-11-23 19:40:33
**/
class Activity_Access
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle( $request, Closure $next, $index )
    {
    	$activity_id = $request->segment( $index );
    	$activity = Activity::findOrFail( $activity_id );

		// Is the user allowed to edit this activity?
		if ( $activity->user_id != app()->user()->id )
			abort( 403 );

		return $next( $request );
    }
}
