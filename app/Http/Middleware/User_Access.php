<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Users\User;

/**
	@brief		May this user be accessed?
	@since		2018-12-22 23:16:42
**/
class User_Access
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle( $request, Closure $next, $index )
    {
    	$user_id = $request->segment( $index );
    	$user = User::findOrFail( $user_id );

		// Is the user allowed to edit this user?
		if ( $user->site_id != app()->user()->site_id )
			abort( 403 );

		return $next( $request );
    }
}
