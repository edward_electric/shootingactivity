<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

/**
	@brief		Check that the user is logged in.
	@since		2018-11-13 20:52:34
**/
class Logged_In
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ( ! Auth::guard( $guard )->check() )
        {
        	session( [ '403_url' => request()->url() ] );
			return redirect()->route( 'user_login' );
		}
        return $next( $request );
    }
}
