<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Guns\Gun;

/**
	@brief		Check whether the user may access this gun.
	@since		2018-11-18 00:49:53
**/
class Gun_Access
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle( $request, Closure $next, $index )
    {
    	$gun_id = $request->segment( $index );

    	$gun = Gun::findOrFail( $gun_id );

		// Is the user allowed to edit this gun?
		if ( $gun->user_id != app()->user()->id )
		{
			// TODO: Who else is allowed to edit?
			return redirect()->route( 'user_dashboard' );
		}

		return $next( $request );
    }
}
