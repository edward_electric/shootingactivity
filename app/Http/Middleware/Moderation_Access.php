<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Activities\Activity;

/**
	@brief		May the user moderate this activity?
	@since		2018-11-24 21:33:21
**/
class Moderation_Access
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle( $request, Closure $next, $index )
    {
    	$activity_id = $request->segment( $index );
    	$activity = Activity::findOrFail( $activity_id );

		if ( $activity->user->site->id != app()->site()->id )
			abort( 403 );
			//return redirect()->route( 'user_dashboard' );

		return $next( $request );
    }
}
