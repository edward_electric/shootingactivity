<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Activities\Activity;
use App\Models\Categories\Category;
use App\Models\Users\User;

class Activities_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	foreach( range( 1, 1000 ) as $counter )
    	{
			$user = User::where( 'name', 'LIKE', 'user%' )->inRandomOrder()->first();
			$gun = $user->guns()->inRandomOrder()->first();
			if ( ! $gun )
				continue;
			$category = Category::inRandomOrder()->first();
			$activity = new Activity();
			$activity->category()->associate( $category );
			$activity->user()->associate( $user );
			$activity->gun()->associate( $gun );
			$activity->ammo = rand( 1, 100 );

			// Add some scores.
			$scores = $activity->scores();
			foreach( range( 1, rand( 0, 15 ) ) as $counter )
			{
				$scores->append( rand( 0, 50 ) );
			}

			$activity->save();

			$scores->reorder();
			$scores->save( $activity );

			$time = strtotime( "-" . rand( 0, 1000 ) . " days" );
			$activity->created_at = date( 'Y-m-d H:i:s', $time );
			$activity->save();
			app()->statistics()->add_activity_later( $activity );
			app()->log()->notice( 'Seeded activity %s for user %s', $activity->id, $user->name );
    	}
    }
}
