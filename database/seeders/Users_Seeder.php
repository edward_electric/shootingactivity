<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Sites\Site;
use App\Models\Users\User;

class Users_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// Find the first available site.
        $site = Site::firstOrFail();

        $domain = '@example.org';

        $random = rand( 1, 1000000000 );

        $users = [
        	[ 'name' => 'administrator_' . $random,			'roles' => [ 'User', 'Moderator', 'Administrator' ] ],
        	[ 'name' => 'administrator_only_' . $random,	'roles' => [ 'Administrator' ] ],
        	[ 'name' => 'moderator_' . $random,				'roles' => [ 'User', 'Moderator' ] ],
        	[ 'name' => 'moderator_only_' . $random,		'roles' => [ 'Moderator' ] ],
        ];

        // Add a bunch of users.
        foreach( range( 1, rand( 1000, 1500 ) ) as $counter )
        	$users []= [ 'name' => 'user' . ( $random + $counter ),			'roles' => [ 'User' ] ];

        foreach( $users as $data )
        {
        	$data = (object) $data;
			$user = new User();
			$user->name = $data->name;
			$user->email = $data->name . $domain;
			$user->set_password( $user->email );
			$user->site()->associate( $site );
			$user->save();
			$user->assign_roles( $data->roles );
			app()->log()->notice( sprintf( "Seeded user %s", $user->name ) );
        }
    }
}
