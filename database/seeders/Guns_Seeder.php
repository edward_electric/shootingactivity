<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Users\User;
use App\Models\Guns\Gun;
use App\Models\Guns\Caliber;
use App\Models\Guns\Group;

class Guns_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	foreach( range( 1, 1000 ) as $counter )
    	{
			// Only seed for users, not mods or admins.
			$user = User::where( 'name', 'LIKE', 'user%' )->inRandomOrder()->first();
			$caliber = Caliber::inRandomOrder()->first();
			$group = Group::inRandomOrder()->first();
			$gun = new Gun();
			$gun->name = sprintf( "%s %s %s", $caliber->name, $group->name, microtime( true ) );
			$gun->caliber()->associate( $caliber );
			$gun->group()->associate( $group );
			$gun->user()->associate( $user );
			$gun->save();
			app()->log()->notice( 'Seeded gun %s for %s', $gun->name, $user->name );
    	}
    }
}
