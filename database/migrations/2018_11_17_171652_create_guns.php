<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guns', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean( 'archived' )	->nullable();
            $table->string( 'name' );
            $table->integer( 'caliber_id' )	->unsigned()->index();		// Gun caliber from gun_calibers table.
            $table->integer( 'file_id' )	->unsigned()->nullable();	// Image for this gun.
            $table->integer( 'group_id' )	->unsigned()->nullable();	// Gun caliber from gun_calibers table.
            $table->integer( 'user_id' )	->unsigned()->index();		// Who owns this gun
            $table->timestamps();
        });

        Schema::create('gun_calibers', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean( 'archived' )	->nullable();
            $table->integer( 'site_id' )	->unsigned()->index();
            $table->string( 'name' );
            $table->timestamps();
        });

        Schema::create('gun_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean( 'archived' )	->nullable();
            $table->string( 'name' );
            $table->integer( 'site_id' )	->unsigned()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guns');
        Schema::dropIfExists('gun_calibers');
        Schema::dropIfExists('gun_groups');
    }
}
