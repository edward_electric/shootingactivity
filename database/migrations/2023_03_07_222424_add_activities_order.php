<?php

use App\Models\Activities\Activity;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('activities', function (Blueprint $table) {
            $table->integer( 'year' )			// Which year this activity is from.
            	->after( 'user_id' )
            	->default( 0 )
            	->unsigned()
            	->index();

            $table->integer( 'order' )			// The order of the activity in the year.
            	->after( 'category_id' )
            	->default( 0 )
            	->unsigned()
            	->index();
        });

       	Activity::reorder_for_all_users();
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('activities', function (Blueprint $table) {
        		$table->dropColumn( ['year', 'order', ] );
        });
    }
};
