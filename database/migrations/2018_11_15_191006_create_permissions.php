<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer( 'site_id' )->unsigned()->index();
            $table->string( 'name' );
            $table->timestamps();
        });

        Schema::create('capabilities', function (Blueprint $table) {
            $table->increments('id');
            $table->string( 'name' );
            $table->timestamps();
        });

        Schema::create('role_capabilities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer( 'role_id' );
            $table->integer( 'capability_id' );
        });

        Schema::create('user_roles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer( 'user_id' )->index();
            $table->integer( 'role_id' );
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('capabilities');
        Schema::dropIfExists('roles');
        Schema::dropIfExists('role_capabilities');
        Schema::dropIfExists('user_roles');
    }
}
