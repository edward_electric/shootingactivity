<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer( 'ammo' )		->default( 0 );
            $table->integer( 'file_id' )	->nullable();
            $table->integer( 'gun_id' )		->nullable();
            $table->integer( 'category_id' )	->nullable();
            $table->integer( 'user_id' )					->index();
            $table->timestamps();
        });

        // Index for drafts.
        Schema::create('activities_drafts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer( 'activity_id' )	->index();
            $table->integer( 'user_id' )	->index();
            $table->timestamps();
        });

        // Who has moderated the activity.
        Schema::create('activities_moderators', function (Blueprint $table) {
            $table->increments('id');
            $table->integer( 'activity_id' )->index();
            $table->integer( 'user_id' )->index();
            $table->timestamps();
        });

        // Index for unmoderated activities.
        Schema::create('activities_unmoderated', function (Blueprint $table) {
            $table->increments('id');
            $table->integer( 'activity_id' )	->index();
            $table->integer( 'site_id' )	->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activities');
        Schema::dropIfExists('activities_drafts');
        Schema::dropIfExists('activities_moderators');
        Schema::dropIfExists('activities_unmoderated');
    }
}
