<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meta', function (Blueprint $table) {
            $table->increments('id');
            $table->string( 'meta_key' )->index();
            $table->longtext( 'meta_value' );
            $table->timestamps();
        });

        Schema::create('metas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('meta_id')->index();
            $table->integer('object_id')->index();
            $table->string('object_type')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meta');
        Schema::dropIfExists('metas');
    }
}
