<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
	@brief		Check that the app IS NOT installed.
	@since		2018-11-13 12:35:31
**/
Route::middleware( [ '\\App\\Http\\Middleware\\Not_Installed' ] )->group( function()
{
	Route::get( '/install/0', 'Install\\Step_0@get' )->name( 'install' );
	Route::get( '/install/', 'Install\\Step_1@get' )->name( 'install_step_1' );
	Route::post( '/install/', 'Install\\Step_1@post' );
} );

/**
	@brief		Check that the app IS installed.
	@since		2018-11-13 12:35:31
**/
Route::middleware( [
	'\\App\\Http\\Middleware\\Installed',
	] )->group( function()
{
	Route::get( '/', 'Start@get' )->name( 'start' );
	Route::get( '/user/logout/',	'User\\Logout@get' )										->name( 'user_logout' );

	Route::middleware( [
		'\\App\\Http\\Middleware\\Installed',
		'\\App\\Http\\Middleware\\Not_Logged_In',
	] )->group( function()
	{

		Route::get( '/install/finished/', 'Install\\Finished@get' )->name( 'install_finished' );

		Route::get( '/user/login/',								'User\\Login@get' )					->name( 'user_login' );
		Route::post( '/user/login/', 'User\\Login@post' );

		Route::get( '/user/password/send/',						'User\\Password\\Send@get' )		->name( 'user_password_send' );
		Route::post( '/user/password/send/',					'User\\Password\\Send@post' );
		Route::get( '/user/password/reset/{user_id}/{token}',	'User\\Password\\Reset@get' )		->name( 'user_password_reset' );
		Route::post( '/user/password/reset/{user_id}/{token}',	'User\\Password\\Reset@post' );
	} );
} );

// Installed and logged in.
/**
	@brief		Must be installed and the user logged in.
	@since		2018-11-13 23:38:43
**/
Route::middleware( [
	'\\App\\Http\\Middleware\\BeforeMiddleware',		// DB logger for dev.
	'\\App\\Http\\Middleware\\Installed',
	'\\App\\Http\\Middleware\\Logged_In',
] )->group( function()
{
	Route::get( '/dashboard/',		'User\\Dashboard@get' )	->name( 'user_dashboard' );		// should be /user/dashboard, but this is cleaner.

	Route::prefix( 'activities' )->group( function()
	{
		Route::get( '/',							'Activities\\Index@get' )		->name( 'activities_index' );

		Route::get( '/complete/',					'Activities\\Complete@get' )	->name( 'activities_complete' );
		Route::post( '/complete/',					'Activities\\Complete@post' );

		Route::prefix( 'draft' )->group( function()
		{
			Route::get( '/',						'Activities\\Draft\\View@get' )			->name( 'activities_draft' );
			Route::get( '/edit/',					'Activities\\Draft\\Edit@get' )			->name( 'activities_draft_edit' );
			Route::post( '/edit/',					'Activities\\Draft\\Edit@post' );
			Route::get( '/edit_date/',				'Activities\\Draft\\Edit_Date@get' )	->name( 'activities_draft_edit_date' );
			Route::post( '/edit_date/',				'Activities\\Draft\\Edit_Date@post' );
			Route::get( '/delete/',					'Activities\\Draft\\Delete@get' )		->name( 'activities_draft_delete' );
			Route::get( '/finish/',					'Activities\\Draft\\Finish@get' )		->name( 'activities_draft_finish' );
			Route::get( '/strings/{string_id}',		'Activities\\Draft\\Strings@get' )		->name( 'activities_edit_string' );
			Route::post( '/strings/{string_id}',		'Activities\\Draft\\Strings@post' );
			Route::get( '/strings/{string_id}/delete',	'Activities\\Draft\\Strings@delete' )		->name( 'activities_delete_string' );
		} );

		Route::prefix( 'statistics' )->group( function()
		{
			Route::get( '/csv/{year?}',				'Activities\\Statistics\\Index@csv' )		->name( 'activities_statistics_csv' );
			Route::get( '/html/{year?}',			'Activities\\Statistics\\Index@get' )		->name( 'activities_statistics' );
			Route::get( '/pdf/{year?}',				'Activities\\Statistics\\Index@pdf' )		->name( 'activities_statistics_pdf' );
		} );

		Route::middleware( [ '\\App\\Http\\Middleware\\User_May:moderate_activities' ] )
			->prefix( 'mass_import' )
			->group( function()
		{
			Route::get( '/',						'Activities\\Mass_Import\\Edit@get' )			->name( 'activities_mass_import_edit' );
			Route::post( '/',						'Activities\\Mass_Import\\Edit@post' );
			Route::get( '/cancel/',					'Activities\\Mass_Import\\Cancel@get' )			->name( 'activities_mass_import_cancel' );
			Route::get( '/preview/',				'Activities\\Mass_Import\\Preview@get' )		->name( 'activities_mass_import_preview' );
			Route::post( '/preview/',				'Activities\\Mass_Import\\Preview@post' );
			Route::get( '/unready/',				'Activities\\Mass_Import\\Unready@get' )		->name( 'activities_mass_import_unready' );
		} );

		// activities / view / x
		Route::middleware( [ '\\App\\Http\\Middleware\\Activity_Access:3' ] )->group( function()
		{
			Route::get( '/delete/{activity}',				'Activities\\Delete@get' )					->name( 'activities_delete' );
			Route::post( '/delete/{activity}',				'Activities\\Delete@post' );
			Route::get( '/reedit/{activity}',				'Activities\\Reedit@get' )					->name( 'activities_reedit' );
			Route::post( '/reedit/{activity}',				'Activities\\Reedit@post' );
			Route::get( '/view/{activity}',					'Activities\\View@get' )					->name( 'activities_view' );
			Route::post( '/view/{activity}',				'Activities\\View@post' );
		} );
	} );

	Route::middleware( [ '\\App\\Http\\Middleware\\User_May:access_admin' ] )->prefix( 'admin' )->group( function()
	{
		Route::middleware( [ '\\App\\Http\\Middleware\\User_May:manage_site' ] )->prefix( 'general' )->group( function()
		{
			Route::get( '/',				'Admin\\General@get' )					->name( 'admin_general' );
			Route::post( '/',				'Admin\\General@post' );
		} );

		Route::middleware( [ '\\App\\Http\\Middleware\\User_May:manage_site' ] )->prefix( 'statistics' )->group( function()
		{
			Route::get( '/html/{year?}/',			'Admin\\Statistics\\Index@get' )		->name( 'admin_statistics' );
			Route::get( '/pdf/{year?}/',			'Admin\\Statistics\\Index@pdf' )		->name( 'admin_statistics_pdf' );
			Route::get( '/regenerate/',				'Admin\\Statistics\\Regenerate@get' )	->name( 'admin_statistics_regenerate' );
		} );

		Route::middleware( [ '\\App\\Http\\Middleware\\User_May:manage_site' ] )->prefix( 'categories' )->group( function()
		{
			Route::get( '/',					function(){	return App\Models\Categories\Category::generic_controller( 'Index', 'get' );							} )->name( 'categories_index' );
			Route::get( '/add/',				function(){	return App\Models\Categories\Category::generic_controller( 'Add', 'get' );								} )->name( 'categories_add' );
			Route::post( '/add/',				function(){	return App\Models\Categories\Category::generic_controller( 'Add', 'post' );							} );
			Route::get( '/archived/',			function(){	return App\Models\Categories\Category::generic_controller( 'Archived', 'get' );						} )->name( 'categories_archived' );
			Route::get( '/archive/{category}',	function( App\Models\Categories\Category $model ){	return $model->generic_controller( 'Archive', 'get', $model );	} )->name( 'categories_archive' );
			Route::post( '/archive/{category}',	function( App\Models\Categories\Category $model ){	return $model->generic_controller( 'Archive', 'post', $model );	} );
			Route::get( '/delete/{category}',	function( App\Models\Categories\Category $model ){	return $model->generic_controller( 'Delete', 'get', $model );	} )->name( 'categories_delete' );
			Route::post( '/delete/{category}',	function( App\Models\Categories\Category $model ){	return $model->generic_controller( 'Delete', 'post', $model );	} );
			Route::get( '/edit/{category}',		function( App\Models\Categories\Category $model ){	return $model->generic_controller( 'Edit', 'get', $model );		} )->name( 'categories_edit' );
			Route::post( '/edit/{category}',	function( App\Models\Categories\Category $model ){	return $model->generic_controller( 'Edit', 'post', $model );	} );
			Route::get( '/restore/{category}',	function( App\Models\Categories\Category $model ){	return $model->generic_controller( 'Restore', 'get', $model );	} )->name( 'categories_restore' );
			Route::post( '/restore/{category}',	function( App\Models\Categories\Category $model ){	return $model->generic_controller( 'Restore', 'post', $model );	} );
		} );

		Route::middleware( [ '\\App\\Http\\Middleware\\User_May:manage_site' ] )->prefix( 'roles' )->group( function()
		{
			Route::get( '/',					function(){	return App\Models\Permissions\Role::generic_controller( 'Index', 'get' );							} )->name( 'roles_index' );
			Route::get( '/add/',				function(){	return App\Models\Permissions\Role::generic_controller( 'Add', 'get' );								} )->name( 'roles_add' );
			Route::post( '/add/',				function(){	return App\Models\Permissions\Role::generic_controller( 'Add', 'post' );							} );
			Route::get( '/delete/{role}',		function( App\Models\Permissions\Role $model ){	return $model->generic_controller( 'Delete', 'get', $model );	} )->name( 'roles_delete' );
			Route::post( '/delete/{role}',		function( App\Models\Permissions\Role $model ){	return $model->generic_controller( 'Delete', 'post', $model );	} );
			Route::get( '/edit/{role}',			function( App\Models\Permissions\Role $model ){	return $model->generic_controller( 'Edit', 'get', $model );		} )->name( 'roles_edit' );
			Route::post( '/edit/{role}',		function( App\Models\Permissions\Role $model ){	return $model->generic_controller( 'Edit', 'post', $model );	} );
		} );

		Route::middleware( [ '\\App\\Http\\Middleware\\User_May:manage_site' ] )->prefix( 'theme' )->group( function()
		{
			Route::get( '/',						[ \App\Http\Controllers\Admin\Theme::class, 'get' ] )					->name( 'admin_theme' );
			Route::post( '/',						[ \App\Http\Controllers\Admin\Theme::class, 'post' ] );
		} );

		Route::middleware( ['\\App\\Http\\Middleware\\User_May:manage_users' ] )->prefix( 'users' )->group( function()
		{
			Route::get( '/',					function(){	return App\Models\Users\User::generic_controller( 'Index', 'get' );							} )->name( 'users_index' );
			Route::get( '/add/',				function(){	return App\Models\Users\User::generic_controller( 'Add', 'get' );							} )->name( 'users_add' );
			Route::post( '/add/',				function(){	return App\Models\Users\User::generic_controller( 'Add', 'post' );							} );
			Route::get( '/archived/',			function(){	return App\Models\Users\User::generic_controller( 'Archived', 'get' );						} )->name( 'users_archived' );

			Route::get( '/archive/{user}',					function( App\Models\Users\User $model ){	return $model->generic_controller( 'Archive', 'get', $model );	} )->name( 'users_archive' );
			Route::post( '/archive/{user}',					function( App\Models\Users\User $model ){	return $model->generic_controller( 'Archive', 'post', $model );	} );
			Route::get( '/delete/{user}',					function( App\Models\Users\User $model ){	return $model->generic_controller( 'Delete', 'get', $model );	} )->name( 'users_delete' );
			Route::post( '/delete/{user}',					function( App\Models\Users\User $model ){	return $model->generic_controller( 'Delete', 'post', $model );	} );
			Route::get( '/edit/{user}',						function( App\Models\Users\User $model ){	return $model->generic_controller( 'Edit', 'get', $model );		} )->name( 'users_edit' );
			Route::post( '/edit/{user}',					function( App\Models\Users\User $model ){	return $model->generic_controller( 'Edit', 'post', $model );	} );
			Route::get( '/restore/{user}',					function( App\Models\Users\User $model ){	return $model->generic_controller( 'Restore', 'get', $model );	} )->name( 'users_restore' );
			Route::post( '/restore/{user}',					function( App\Models\Users\User $model ){	return $model->generic_controller( 'Restore', 'post', $model );	} );
			Route::get( '/statistics/{user}/csv/{year?}',	'Admin\\Users\\Statistics@csv' )			->name( 'admin_user_statistics_csv' );
			Route::get( '/statistics/{user}/html/{year?}',	'Admin\\Users\\Statistics@get' )			->name( 'admin_user_statistics' );
			Route::get( '/statistics/{user}/pdf/{year?}',	'Admin\\Users\\Statistics@pdf' )			->name( 'admin_user_statistics_pdf' );

			// This super special route allows the network admin to log in as a user. Purely a convenience method.
			Route::middleware( [ '\\App\\Http\\Middleware\\User_May:manage_network' ] )->group( function()
			{
				Route::get( '/login_as/{user}',		function( App\Models\Users\User $model ){
					Auth::login( $model );
					return redirect()->route( 'user_dashboard' );
				} )->name( 'users_login_as' );
			} );
		} );
	} );

	Route::middleware( [ '\\App\\Http\\Middleware\\File_Access' ] )->group( function()
	{
		Route::get( '/file/attachment/{id}/{key}',			'Files\\File@attachment' )	->name( 'file_attachment' );
		Route::get( '/file/inline/{id}/{key}/{size?}',		'Files\\File@inline' )		->name( 'file_inline' );
	} );

	Route::middleware( [ '\\App\\Http\\Middleware\\User_May:change_password' ] )->prefix( 'password' )->group( function()
	{
		Route::get( '/',	'User\\Password@get' )			->name( 'user_password' );
		Route::post( '/',	'User\\Password@post' );
	} );

	Route::prefix( 'guns' )->group( function()
	{
		Route::get( '/',					function(){	return App\Models\Guns\Gun::generic_controller( 'Index', 'get' );							} )->name( 'guns_index' );
		Route::get( '/add/',				function(){	return App\Models\Guns\Gun::generic_controller( 'Add', 'get' );								} )->name( 'guns_add' );
		Route::post( '/add/',				function(){	return App\Models\Guns\Gun::generic_controller( 'Add', 'post' );							} );
		Route::get( '/archived/',			function(){	return App\Models\Guns\Gun::generic_controller( 'Archived', 'get' );						} )->name( 'guns_archived' );
		Route::middleware( [ '\\App\\Http\\Middleware\\Gun_Access:3' ] )->group( function()
		{
			Route::get( '/archive/{gun}',		function( App\Models\Guns\Gun $model ){	return $model->generic_controller( 'Archive', 'get', $model );	} )->name( 'guns_archive' );
			Route::post( '/archive/{gun}',		function( App\Models\Guns\Gun $model ){	return $model->generic_controller( 'Archive', 'post', $model );	} );
			Route::get( '/delete/{gun}',		function( App\Models\Guns\Gun $model ){	return $model->generic_controller( 'Delete', 'get', $model );	} )->name( 'guns_delete' );
			Route::post( '/delete/{gun}',		function( App\Models\Guns\Gun $model ){	return $model->generic_controller( 'Delete', 'post', $model );	} );
			Route::get( '/edit/{gun}',			function( App\Models\Guns\Gun $model ){	return $model->generic_controller( 'Edit', 'get', $model );		} )->name( 'guns_edit' );
			Route::post( '/edit/{gun}',			function( App\Models\Guns\Gun $model ){	return $model->generic_controller( 'Edit', 'post', $model );	} );
			Route::get( '/restore/{gun}',		function( App\Models\Guns\Gun $model ){	return $model->generic_controller( 'Restore', 'get', $model );	} )->name( 'guns_restore' );
			Route::post( '/restore/{gun}',		function( App\Models\Guns\Gun $model ){	return $model->generic_controller( 'Restore', 'post', $model );	} );
		} );

		Route::middleware( [ '\\App\\Http\\Middleware\\User_May:manage_site' ] )->prefix( 'calibers' )->group( function()
		{
			Route::get( '/',						function(){	return App\Models\Guns\Caliber::generic_controller( 'Index', 'get' );		} )->name( 'gun_calibers_index' );
			Route::get( '/add/',					function(){	return App\Models\Guns\Caliber::generic_controller( 'Add', 'get' );			} )->name( 'gun_calibers_add' );
			Route::post( '/add/',					function(){	return App\Models\Guns\Caliber::generic_controller( 'Add', 'post' );		} );
			Route::get( '/archived/',				function(){	return App\Models\Guns\Caliber::generic_controller( 'Archived', 'get' );	} )->name( 'gun_calibers_archived' );
			Route::get( '/archive/{gun_caliber}',	function( App\Models\Guns\Caliber $model ){	return $model->generic_controller( 'Archive', 'get', $model );	} )->name( 'gun_calibers_archive' );
			Route::post( '/archive/{gun_caliber}',	function( App\Models\Guns\Caliber $model ){	return $model->generic_controller( 'Archive', 'post', $model );	} );
			Route::get( '/delete/{gun_caliber}',	function( App\Models\Guns\Caliber $model ){	return $model->generic_controller( 'Delete', 'get', $model );	} )->name( 'gun_calibers_delete' );
			Route::post( '/delete/{gun_caliber}',	function( App\Models\Guns\Caliber $model ){	return $model->generic_controller( 'Delete', 'post', $model );	} );
			Route::get( '/edit/{gun_caliber}',		function( App\Models\Guns\Caliber $model ){	return $model->generic_controller( 'Edit', 'get', $model );		} )->name( 'gun_calibers_edit' );
			Route::post( '/edit/{gun_caliber}',		function( App\Models\Guns\Caliber $model ){	return $model->generic_controller( 'Edit', 'post', $model );	} );
			Route::get( '/restore/{gun_caliber}',	function( App\Models\Guns\Caliber $model ){	return $model->generic_controller( 'Restore', 'get', $model );	} )->name( 'gun_calibers_restore' );
			Route::post( '/restore/{gun_caliber}',	function( App\Models\Guns\Caliber $model ){	return $model->generic_controller( 'Restore', 'post', $model );	} );
		} );

		Route::middleware( [ '\\App\\Http\\Middleware\\User_May:manage_site' ] )->prefix( 'groups' )->group( function()
		{
			Route::get( '/',						function(){	return App\Models\Guns\Group::generic_controller( 'Index', 'get' );		} )->name( 'gun_groups_index' );
			Route::get( '/add/',					function(){	return App\Models\Guns\Group::generic_controller( 'Add', 'get' );		} )->name( 'gun_groups_add' );
			Route::post( '/add/',					function(){	return App\Models\Guns\Group::generic_controller( 'Add', 'post' );		} );
			Route::get( '/archived/',				function(){	return App\Models\Guns\Group::generic_controller( 'Archived', 'get' );	} )->name( 'gun_groups_archived' );
			Route::get( '/archive/{gun_group}',		function( App\Models\Guns\Group $model ){	return $model->generic_controller( 'Archive', 'get', $model );	} )->name( 'gun_groups_archive' );
			Route::post( '/archive/{gun_group}',	function( App\Models\Guns\Group $model ){	return $model->generic_controller( 'Archive', 'post', $model );	} );
			Route::get( '/delete/{gun_group}',		function( App\Models\Guns\Group $model ){	return $model->generic_controller( 'Delete', 'get', $model );	} )->name( 'gun_groups_delete' );
			Route::post( '/delete/{gun_group}',		function( App\Models\Guns\Group $model ){	return $model->generic_controller( 'Delete', 'post', $model );	} );
			Route::get( '/edit/{gun_group}',		function( App\Models\Guns\Group $model ){	return $model->generic_controller( 'Edit', 'get', $model );		} )->name( 'gun_groups_edit' );
			Route::post( '/edit/{gun_group}',		function( App\Models\Guns\Group $model ){	return $model->generic_controller( 'Edit', 'post', $model );	} );
			Route::get( '/restore/{gun_group}',		function( App\Models\Guns\Group $model ){	return $model->generic_controller( 'Restore', 'get', $model );	} )->name( 'gun_groups_restore' );
			Route::post( '/restore/{gun_group}',	function( App\Models\Guns\Group $model ){	return $model->generic_controller( 'Restore', 'post', $model );	} );
		} );
	} );

	Route::middleware( [
		'\\App\\Http\\Middleware\\User_May:moderate_activities',
	] )->prefix( 'moderate' )->group( function()
	{
		Route::get( '/',							'Moderation\\Index@get' )		->name( 'moderation_index' );
		Route::post( '/',							'Moderation\\Index@post' );
		Route::middleware( [
			'\\App\\Http\\Middleware\\Moderation_Access:2',
		] )->group( function()
		{
			Route::get( '/{activity}/',				'Moderation\\View@get' )		->name( 'moderation_view' );
			Route::get( '/{activity}/accept/',		'Moderation\\Accept@get' )		->name( 'moderation_accept' );
			Route::get( '/{activity}/edit/',		'Moderation\\Edit@get' )		->name( 'moderation_edit' );
			Route::post( '/{activity}/edit/',		'Moderation\\Edit@post' );
			Route::get( '/{activity}/reject/',		'Moderation\\Reject@get' )		->name( 'moderation_reject' );
		} );
	} );

	Route::middleware( [ '\\App\\Http\\Middleware\\User_May:manage_network' ] )->prefix( 'network' )->group( function()
	{
		Route::prefix( 'sites' )->group( function()
		{
			Route::get( '/',					function(){	return App\Models\Sites\Site::generic_controller( 'Index', 'get' );		} )->name( 'sites_index' );
			Route::get( '/add/',				function(){	return App\Models\Sites\Site::generic_controller( 'Add', 'get' );		} )->name( 'sites_add' );
			Route::post( '/add/',				function(){	return App\Models\Sites\Site::generic_controller( 'Add', 'post' );		} );
			Route::get( '/archived/',			function(){	return App\Models\Sites\Site::generic_controller( 'Archived', 'get' );	} )->name( 'sites_archived' );
			Route::get( '/archive/{site}',		function( App\Models\Sites\Site $model ){	return $model->generic_controller( 'Archive', 'get', $model );	} )->name( 'sites_archive' );
			Route::post( '/archive/{site}',		function( App\Models\Sites\Site $model ){	return $model->generic_controller( 'Archive', 'post', $model );	} );
			Route::get( '/delete/{site}',		function( App\Models\Sites\Site $model ){	return $model->generic_controller( 'Delete', 'get', $model );	} )->name( 'sites_delete' );
			Route::post( '/delete/{site}',		function( App\Models\Sites\Site $model ){	return $model->generic_controller( 'Delete', 'post', $model );	} );
			Route::get( '/edit/{site}',			function( App\Models\Sites\Site $model ){	return $model->generic_controller( 'Edit', 'get', $model );		} )->name( 'sites_edit' );
			Route::post( '/edit/{site}',		function( App\Models\Sites\Site $model ){	return $model->generic_controller( 'Edit', 'post', $model );	} );
			Route::get( '/restore/{site}',		function( App\Models\Sites\Site $model ){	return $model->generic_controller( 'Restore', 'get', $model );	} )->name( 'sites_restore' );
			Route::post( '/restore/{site}',		function( App\Models\Sites\Site $model ){	return $model->generic_controller( 'Restore', 'post', $model );	} );

			// Special route just for switching to a site.
			Route::get( '/switch_to/{site}',	function( App\Models\Sites\Site $site )
			{
				$user = app()->user();
				$site->migrate_user( $user );
				app()->alerts()->success()->set_message( __( 'Welcome to :sitename!', [ 'sitename' => $site->name ] ) );
				return redirect()->route( 'user_dashboard' );
			} )->name( 'sites_switch_to' );
		} );
		Route::get( '/overview/',				'Admin\\Network\\Overview@get' )		->name( 'network_overview' );
	} );

	Route::middleware( [ '\\App\\Http\\Middleware\\User_May:socialize' ] )->prefix( 'users' )->group( function()
	{
		Route::get( '/',				'Users\\Index@get' )	->name( 'user_profiles_index' );
		Route::middleware( [ '\\App\\Http\\Middleware\\User_Access:2' ] )->group( function()
		{
			Route::get( '/{user}/',			'Users\\View@get' )		->name( 'user_profiles_view' );
		} );
	} );

	Route::middleware( [ '\\App\\Http\\Middleware\\User_May:notice_board' ] )->prefix( 'notices' )->group( function()
	{
		Route::get( '/',				'Notices\\Index@get' )		->name( 'notices_index' );
		Route::get( '/add/',			'Notices\\Add@get' )		->name( 'notices_add' );
		Route::post( '/add/',			'Notices\\Add@post' );

		// notices / view / 123
		Route::middleware( [ '\\App\\Http\\Middleware\\Notice_Access:3' ] )->group( function()
		{
			Route::get( '/view/{notice}/',	'Notices\\View@get' )	->name( 'notices_view' );
			Route::post( '/view/{notice}/',	'Notices\\View@post' );
		} );
	} );

	Route::get( '/settings/',	'User\\Settings@get' )			->name( 'user_settings' );
	Route::post( '/settings/',	'User\\Settings@post' );
} );
