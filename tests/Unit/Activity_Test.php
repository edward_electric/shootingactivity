<?php

namespace Tests\Unit;

use App\Models\Activities\Activity;

/**
	@brief		Test the activities.
	@since		2018-12-06 22:57:45
**/
class Activity_Test
	extends TestCase
{
	/**
		@brief		Test scores.
		@since		2018-12-06 22:57:57
	**/
	public function test_scores()
	{
		$user = \App\Models\Users\User::firstOrFail();
		$activity = new Activity();
		$activity->user()->associate( $user );
		$activity->save();

		$scores = $activity->strings();

		$shots1 = [ 9, 9, 9, 9, 10 ];
		$total1 = array_sum( $shots1 );
		$scores->add_string( $total1, $shots1 );

		$this->assertEquals( $total1, $scores->get_score( 0 ) );
		$this->assertEquals( $shots1, $scores->get_shots( 0 ) );
		$this->assertEquals( $total1, $scores->get_sum() );

		$shots2 = [ 4, 3, 2, 1, 2, 3, 4, 5 ];
		$total2 = array_sum( $shots2 );
		$scores->add_string( $total2, $shots2 );
		$this->assertEquals( $total2, $scores->get_score( 1 ) );
		$this->assertEquals( $shots2, $scores->get_shots( 1 ) );
		$this->assertEquals( $total1 + $total2, $scores->get_sum() );

		// Forget the first shots.
		$scores->forget( "0" );
		$this->assertEquals( $total2, $scores->get_score( 0 ) );
		$this->assertEquals( $shots2, $scores->get_shots( 0 ) );
		$this->assertEquals( $total2, $scores->get_sum() );
	}
}
