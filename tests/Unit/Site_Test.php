<?php

namespace Tests\Unit;

use App\Models\Permissions\Role;
use App\Models\Permissions\Capability;
use App\Models\Sites\Site;
use App\Models\Users\User;

/**
	@brief		Set the sites model.
	@since		2018-11-15 23:15:19
**/
class Site_Test
	extends TestCase
{
	/**
		@brief		Clear all permissions.
		@since		2018-11-19 23:18:48
	**/
	public function clear_permissions()
	{
		Capability::truncate();
		Role::truncate();
		Site::truncate();
	}

	/**
		@brief		Test creating the permissions.
		@since		2018-11-15 23:15:33
	**/
	public function test_create_permissions()
	{
		$this->clear_permissions();

		$this->assertEquals( 0, Capability::count() );
		$this->assertEquals( 0, Role::count() );
		$this->assertEquals( 0, Site::count() );

		// Create a random site.
		$site = new Site();
		$site->name = md5( microtime() );
		$site->save();
		$this->assertEquals( 1, Site::count() );

		// Create the intial roles
		$site->install();

		$this->assertTrue( Capability::count() > 0 );
		$this->assertTrue( Role::count() > 0 );
	}

	/**
		@brief		Test user migration.
		@since		2018-11-19 22:44:32
	**/
	public function test_migration()
	{
		$this->clear_permissions();

		// Create site 1
		$site1 = new Site();
		$site1->name = md5( microtime() );
		$site1->save();
		$this->assertEquals( 1, Site::count() );

		// And site 2
		$site2 = new Site();
		$site2->name = md5( microtime() );
		$site2->save();
		$this->assertEquals( 2, Site::count() );

		// Create a user for site 1.
		$user = new User();
		$user->site()->associate( $site1 );
		$user->password = microtime();
		$user->email = microtime();
		$user->name = microtime();
		$user->save();
		$role = 'Moderator';
		$user->assign_role( $role );
		$site1_role = $user->roles->first();

		$site2->migrate_user( $user );
		$user->refresh();
		$site2_role = $user->roles->first();

		$this->assertNotEquals( $site1_role->id, $site2_role->id );
		$this->assertEquals( $site1_role->name, $site2_role->name );
		$this->assertEquals( $role, $site2_role->name );
	}
}
