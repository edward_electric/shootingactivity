<?php

namespace Tests\Unit;

use App\Models\Activities\Activity;
use App\Models\Activities\Draft_Index;
use App\Models\Comments\Comment;
use App\Models\Guns\Caliber;
use App\Models\Guns\Gun;
use App\Models\Notices\Notice;
use App\Models\Sites\Site;
use App\Models\Users\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;

/**
	@brief		Test deleting users.
	@since		2018-12-27 11:34:50
**/
class User_Delete_Test
	extends TestCase
{
//	use RefreshDatabase;

	/**
		@brief		Delete a user
		@group		now
		@since		2018-12-27 11:35:00
	**/
	public function test_delete_user()
	{
		$site = new Site();
		$site->name = 'Random site';
		$site->save();

		// Create a user.
		$user = new User();
		$user->email = microtime();
		$user->name = microtime();
		$user->password = microtime();
		$user->site()->associate( $site );
		$user->save();

		$caliber = new Caliber();
		$caliber->name = microtime();
		$caliber->site()->associate( $site );
		$caliber->save();

		// Create a gun for the user.
		$gun = new Gun();
		$gun->caliber()->associate( $caliber );
		$gun->name = microtime();
		$gun->user()->associate( $user );
		$gun->save();
		$gun->name = microtime();

		// And an activity.
		$activity = new Activity();
		$activity->user()->associate( $user );
		$activity->save();

		// Add the indexes.
		$activity->mark_as_draft();
		$activity->mark_as_unmoderated();
		$activity->moderators()->syncWithoutDetaching( $user );

		// A comment.
		$comment = new Comment();
		$comment->user()->associate( $user );
		$comment->text = microtime();
		$comment->save();

		// A notice.
		$notice = new Notice();
		$notice->site()->associate( $site );
		$notice->comment()->associate( $comment );
		$notice->save();

		// And now a reply, but from someone else.
		$reply = new Comment();
		$reply->parent()->associate( $comment );
		$reply->text = microtime();
		$reply->user_id = rand( 100, 1000 );
		$reply->save();

		// Some preliminary tests.
		$di_query = DB::table( Draft_Index::getTableName() )->where( 'activity_id', $activity->id );

		$this->assertNotNull( Activity::find( $activity->id ) );
		$this->assertEquals( 1, $activity->moderators()->count() );
		$this->assertEquals( 1, $di_query->count() );
		$this->assertNotNull( Comment::find( $comment->id ) );
		$this->assertNotNull( Gun::find( $gun->id ) );

		// Off we go!
		$user->delete();

		// The gun should be gone.
		$this->assertNull( Gun::find( $gun->id ) );

		// And the activity and everything associated to it.
		$this->assertNull( Activity::find( $activity->id ) );
		$this->assertEquals( 0, $activity->moderators()->count() );
		$this->assertEquals( 0, $di_query->count() );

		// The notice itself gone.
		$this->assertNull( Notice::find( $notice->id ) );
		// And the comments.
		$this->assertNull( Comment::find( $comment->id ) );
		$this->assertNull( Comment::find( $reply->id ) );
	}
}
