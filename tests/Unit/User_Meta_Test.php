<?php

namespace Tests\Unit;

use App\Models\Meta;
use App\Models\Users\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

class User_Meta_Test
	extends TestCase
{
//	use RefreshDatabase;

	/**
		@brief		Test creating meta.
		@since		2018-11-14 21:52:29
	**/
	public function test_create_meta()
	{
		$user = new User();
		$user->email = time();
		$user->name = time();
		$user->site_id = 1;
		$user->password = time();
		$user->save();

		$meta = new Meta;
		$meta->meta_key = '123';
		$meta->meta_value = '234';
		$meta->save();
		$meta->user()->attach( $user );

		$this->assertEquals( '234', $user->get_meta( '123' ) );
	}

	/**
		@brief		Test get and sets.
		@since		2018-11-14 22:12:09
	**/
	public function test_get_and_set()
	{
		Meta::truncate();

		$user = User::firstOrFail();
		$key = md5( microtime() );
		$value = md5( microtime() );
		$user->set_meta( $key, $value );
		$this->assertEquals( $value, $user->get_meta( $key ) );

		$this->assertEquals( 1, Meta::count() );

		// Set the same meta, but with a new key.
		$new_value = md5( microtime() );
		$user->set_meta( $key, $new_value );

		// Should still be just one meta.
		$this->assertEquals( 1, Meta::count() );

		$this->assertEquals( $new_value, $user->get_meta( $key ) );

		// Save some object meta.
		$object = (object)[ 'key1' => 'value1' ];
		$user->set_meta( $key, $object );
		$meta_object = $user->get_meta( $key );
		$this->assertTrue( is_object( $meta_object ) );

		// Try a true and false.
		$key = md5( microtime() );
		$user->set_meta( $key, true );
		$this->assertTrue( $user->get_meta( $key ) );
	}
}
