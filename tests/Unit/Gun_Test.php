<?php

namespace Tests\Unit;

use App\Models\Files\File;
use App\Models\Guns\Caliber;
use App\Models\Guns\Gun;
use App\Models\Sites\Site;
use App\Models\Users\User;

/**
	@brief		Test the gun model.
	@group		guns
	@since		2018-11-18 15:50:12
**/
class Gun_Test
	extends TestCase
{
	/**
		@brief		Test the model.
		@since		2018-11-18 15:54:17
	**/
	public function test_functions()
	{
		$site = new Site();
		$site->name = 'Random site';
		$site->save();

		// Create a user.
		$user = new User();
		$user->email = microtime();
		$user->name = microtime();
		$user->password = microtime();
		$user->site()->associate( $site );
		$user->save();

		$gun_count = Gun::count();

		$gun = new Gun();
		$gun->name = microtime();
		$gun->set_caliber( Caliber::first()->id );
		$gun->user()->associate( $user );
		$gun->save();

		$this->assertEquals( $gun_count + 1, Gun::count() );

		$this->assertNull( $gun->file );

		// Try uploading a file.
		$test_image = __DIR__ . '/../files/rugermk4.jpg';
		$gun->store_file( $test_image, basename( $test_image ) );

		// Apparently our friend gun doesn't know that we now have an image.
		$gun->refresh();

		$file = $gun->file;
		$this->assertTrue( $file->id > 0 );

		// Check the file meta.
		$this->assertEquals( 'image/jpeg', $file->get_meta( 'mimetype' ) );
	}
}
