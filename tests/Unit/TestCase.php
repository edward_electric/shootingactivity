<?php

namespace Tests\Unit;

/**
	@brief		Convenience class to not have to refer to different namespaces.
	@since		2018-11-15 23:14:42
**/
class TestCase extends
	\Tests\TestCase
{
}
