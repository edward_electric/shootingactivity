<?php

namespace Tests\Browser;

use App\Models\Activities\Activity;
use App\Models\Guns\Caliber;
use App\Models\Guns\Group;
use App\Models\Guns\Gun;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/**
	@brief		Test the mass import.
	@group		mass_import
	@since		2019-01-21 14:01:57
**/
class Mass_Import_Test extends DuskTestCase
{
	/**
		@brief		Test a competition.
		@since		2019-01-21 14:02:08
	**/
	public function test_competition()
	{
		// We need a user.
		$this->user();
		$this->normal_user = $this->user;

		// Add a gun for this user.
		$this->goto_route( 'guns_add' );
		$this->browser->value( 'input.input_name', 'The C gun' );
		$group_id = Group::where( 'name', 'C' )->first()->id;
		$this->browser->select( 'group', $group_id );
		$this->browser->click( '.input_save_guns' );

		// We should not see the incomplete activity icon.
		$this->browser->assertMissing( '.action.question' );

		$this->logout();

		// Switch to a moderator, who has access to mass import.
		$this->create_user( 'Moderator' );
		$this->login();

		$this->goto_route( 'activities_mass_import_edit' );

		$seconds = rand( 10000, 50000 );
		$datetime = Carbon::parse( $seconds . ' seconds ago' );
		$this->browser->type( '.input_datetime', $datetime->toDateTimeString() );

		$ammo = rand( 40, 60  );
		$this->browser->value( '.input_ammo', $ammo );

		$comment = microtime();
		$this->browser->value( '.input_comment', $comment );

		$series = 6;
		$this->browser->value( '.input_series', $series );

		$total_score = 94 + rand( 50, 100 );
		$line = [
			$this->normal_user->name,
			'C',
			$total_score,
			json_encode( [ 8, 8, 9, 9, 10 ] ),		// 44
			json_encode( [ 1, 5, 5, 5, 6 ] ),		// 22
			json_encode( [ 7, 7, 7, 4, 3 ] ),		// 28
		];
		// 94 points total, means the rest of the series (3) should have $total_score - 94 points divided amongst them.
		$rest_of_series = $total_score - 94;

		// Add C gun
		$csv = '';
		$csv .= implode( ";", $line ) . "\n";

		// Add an A gun
		$line[ 1 ] = 'A';
		$csv .= implode( ";", $line ) . "\n";

		$this->browser->keys( 'textarea.input_csv', $csv );

		$this->browser->click( '.input_preview' );

		// Fail because category is not chosen.
		$this->browser->assertRouteIs( 'activities_mass_import_edit' );

		$this->browser->select( '.form_item_category select' );		// Any cat will do.
		$this->browser->click( '.input_preview' );

		$this->browser->pause( 3000 );
		$this->browser->assertRouteIs( 'activities_mass_import_preview' );

		$this->browser->click( '.unready_link a' );

		$this->browser->click( '.input_preview' );
		$this->browser->assertRouteIs( 'activities_mass_import_preview' );

		$activities_count = DB::table( Activity::getTableName() )->count();

		// Finally accept.
		$this->browser->click( '.input_import' );

		$this->browser->assertRouteIs( 'user_dashboard' );

		$this->assertEquals( $activities_count + 2, DB::table( Activity::getTableName() )->count() );

		$this->logout();

		// Log in as the user with the incomplete activities.
		$this->user = $this->normal_user;
		$this->login();
		$this->browser->click( '.action.question' );

		// Select anything.
		$this->browser->select( '.form_item_gun select' );
		$this->browser->click( '.input_save' );

		// Should not work, since the user is still missing an A gun.
		$this->browser->assertRouteIs( 'activities_complete' );

		// Add an A gun.
		$this->goto_route( 'guns_add' );
		$this->browser->value( 'input.input_name', 'The A gun' );
		$group_id = Group::where( 'name', 'A' )->first()->id;
		$this->browser->select( 'group', $group_id );
		$this->browser->click( '.input_save_guns' );

		$this->goto_route( 'user_dashboard' );
		$this->browser->click( '.action.question' );
		$this->browser->select( '.form_item_gun select' );
		$this->browser->click( '.input_save' );

		$this->browser->assertRouteIs( 'user_dashboard' );
		$this->browser->assertMissing( '.action.question' );
	}
}
