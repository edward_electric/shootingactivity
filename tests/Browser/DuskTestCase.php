<?php

namespace Tests\Browser;

use App\Models\Sites\Site;
use App\Models\Users\User;
use Laravel\Dusk\TestCase as BaseTestCase;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\CreatesApplication;

/**
	@brief		Base dusk test case, with common functions.
	@since		2018-11-15 15:46:02
**/
class DuskTestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
    	@brief		The browser instance.
    	@since		2018-10-06 13:34:59
    **/
    public static $instance;

	/**
		@brief		The logged in user.
		@since		2018-11-16 19:08:36
	**/
	public $user;

	/**
		@brief		Login as an administrator.
		@since		2018-11-16 18:53:02
	**/
	public function administrator()
	{
		$this->install();
		$this->create_user( 'Administrator' );
		$this->login();
	}

	/**
		@brief		Clear all caches.
		@since		2018-12-09 22:33:57
	**/
	public function clear_cache()
	{
		\Cache::flush();
	}

	/**
		@brief		Clear the installation by copying over the dusk env.
		@since		2018-11-16 18:20:36
	**/
	public function clear_install()
	{
		$env = app_path( '../.env' );
		$env_dusk = app_path( '../.env.dusk' );
		copy( $env_dusk, $env );
	}

	/**
		@brief		Create a user.
		@since		2018-11-16 22:00:09
	**/
	public function create_user( $roles = [ 'User' ] )
	{
		// Create a new user.
		$this->user = new User();
		$this->user->email = md5( microtime() ) . '@shootingactivity.com';
		$this->user->name = md5( microtime() );
		$this->user->site()->associate( Site::firstOrFail() );
		$this->user->set_password( md5( microtime() ) );
		$this->user->save();

		if ( ! is_array( $roles ) )
			$roles = [ $roles ];

		$this->user->assign_roles( $roles );
	}

    /**
     * Create the RemoteWebDriver instance.
     *
     * @return \Facebook\WebDriver\Remote\RemoteWebDriver
     */
    protected function driver()
    {
        $options = (new ChromeOptions)->addArguments([
            '--disable-gpu',
            //'--headless',		// Tests fail if this is headless. :(
        ]);

        return RemoteWebDriver::create(
            'http://localhost:9515', DesiredCapabilities::chrome()->setCapability(
                ChromeOptions::CAPABILITY, $options
            )
        );
    }

    /**
    	@brief		Go to a route and check that we are there.
    	@since		2019-01-21 14:09:38
    **/
    public function goto_route( $route )
    {
		$this->browser->visit( route( $route ) );
		$this->browser->assertRouteIs( $route );
    }

	/**
		@brief		Install.
		@since		2018-11-15 16:05:05
	**/
	public function install()
	{
		$this->clear_install();

		$caller = $this;
		$this->browse( function( $browser ) use ( $caller )
		{
			$caller->browser = $browser;
		} );

		// Visiting start should redirect to install.
		$this->browser->visit( route( 'start' ) );
		$this->browser->assertRouteIs( 'install_step_1' );

		$this->browser->value( '.input_db_host', env( 'DB_HOST' ) );
		$this->browser->value( '.input_db_port', env( 'DB_PORT' ) );
		$this->browser->value( '.input_db_database', env( 'DB_DATABASE' ) );
		$this->browser->value( '.input_db_username', env( 'DB_USERNAME' ) );
		$this->browser->value( '.input_db_password', env( 'DB_PASSWORD' ) );

		$site_name = md5( microtime() . 'Smörgåsbord \slashed\\' );
		$this->browser->value( '.input_site_name', $site_name );

		$admin_email = 'testing@shootingactivity.com';
		$this->browser->value( '.input_admin_email', $admin_email );

		// Install!
		$this->browser->click( '.submit.input_install' );

		$this->browser->assertRouteIs( 'install_finished' );

		// This is for the model cache.
		$this->clear_cache();

		// Was the site created?
		$site = Site::firstOrFail();
		$caller->assertEquals( $site_name, $site->name );

		// Was the user created?
		$this->user = User::firstOrFail();
		$this->assertEquals( 'admin', $caller->user->name );
		$this->assertEquals( $admin_email, $caller->user->email );
	}

	/**
		@brief		Log in the current user.
		@since		2018-11-16 22:03:48
	**/
	public function login()
	{
		\Cache::flush();

		// Reset the password to something we know.
		$password = md5( microtime() );
		$this->user->set_password( $password );
		$this->user->save();

		$this->browser->visit( route( 'user_login' ) );
		$this->browser->assertRouteIs( 'user_login' );

		$this->browser->value( '.input_email', $this->user->email );
		$this->browser->value( '.input_password', $password );

		$this->browser->click( '.input_login' );

		$this->browser->assertRouteIs( 'user_dashboard' );
	}

	/**
		@brief		Log the user out.
		@since		2018-11-16 22:17:37
	**/
	public function logout()
	{
		$this->browser->visit( route( 'user_logout' ) );
		$this->browser->assertRouteIs( 'user_login' );
	}

	/**
		@brief		Login as an moderator.
		@since		2018-11-16 18:53:02
	**/
	public function moderator()
	{
		$this->install();
		$this->create_user( 'Moderator' );
		$this->login();
	}

	/**
		@brief		Log in as the network admin.
		@since		2018-11-16 22:15:53
	**/
	public function network_admin()
	{
		$this->install();
		$this->login();
	}

    /**
     * Prepare for Dusk test execution.
     *
     * @beforeClass
     * @return void
     */
    public static function prepare()
    {
        static::startChromeDriver();
    }

	/**
		@brief		Browse to SA while not logged in.
		@since		2018-11-15 16:37:14
	**/
	public function unauthenticated()
	{
		$this->install();
	}

	/**
		@brief		Login as a normal user.
		@since		2018-11-15 15:56:35
	**/
	public function user()
	{
		$this->install();
		$this->create_user();
		$this->login();
	}
}
