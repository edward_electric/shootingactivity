<?php

namespace Tests\Browser;

use App\Models\Activities\Activity;
use App\Models\Meta;
use Illuminate\Support\Facades\DB;

/**
	@brief		Test activities.
	@group		activities
	@details	The generic model is already tested. We only test the extra stuff like images.
	@since		2018-11-18 16:07:56
**/
class Activities_Test extends DuskTestCase
{
	/**
		@brief		Test the activity functionality.
		@group		now
		@since		2018-12-07 00:27:51
	**/
	public function test_activity()
	{
		// We need a moderator first
		$this->moderator();

		// Log in as a user.
		$this->logout();
		$this->create_user( 'User' );
		$this->login();

		$this->assertEquals( 0, Activity::count() );

		// Enable moderation for the site.
		$this->user->site->set_meta( 'moderators', 1 );

		// Are we on the dashboard?
		$this->browser->assertRouteIs( 'user_dashboard' );

		$this->browser->assertVisible( '.guns.add_a_model' );

		// We need to add a gun.
		$route = 'guns_index';
		$this->browser->visit( route( $route ) );
		$this->browser->assertRouteIs( $route );

		// Add a gun.
		$route = 'guns_add';
		$this->browser->visit( route( $route ) );
		$this->browser->assertRouteIs( $route );

		$this->browser->value( 'input.input_name', 'Testing a Ruger' );
		$this->browser->select( 'caliber', '.22 Long Rifle' );
		$this->browser->select( 'group', 'B' );

		$this->browser->click( '.input_save_guns' );

		// Is the message gone?
		$this->browser->visit( route( 'user_dashboard' ) );
		$this->browser->assertMissing( '.guns.add_a_model' );

		// Now start a new activity.
		$this->browser->visit( route( 'activities_draft_edit' ) );

		$comment = 'Random comment' . rand( 1, 50 );

		$this->browser->value( 'input.input_ammo', '1' );
		$this->browser->value( 'textarea.input_comment', $comment );

		// Upload an old image.
		$filename = __DIR__ . '/../files/old.jpg';
		/**
		touch( $filename, time() - ( 8 * 60 * 60 ) );
		// Set the exif date to the file date.
		exec( 'jhead -dsft ' . $filename );
		$this->browser->attach( 'file', $filename );

		$this->browser->click( '.sa_nice_submit' );

		// We should still be on the edit due to the old image.
		$this->browser->assertRouteIs( 'activities_draft_edit' );
		**/
		// Try again
		touch( $filename, time() - 5 * 60 * 60 );
		// Set the exif date to the file date.
		exec( 'jhead -dsft ' . $filename );
		$this->browser->attach( 'file', $filename );

		$this->browser->click( 'button.submit' );

		// Add a score.
		$this->browser->click( '.add_a_score' );
		$this->browser->pause( 500 );
		$this->browser->assertVisible( '.cancel.btn' );

		// Check the save button.
		$save_button = '.input_save_score.btn';
		$this->browser->assertVisible( $save_button );
		$save_button_text = $this->browser->text( $save_button );
		$this->assertTrue( strlen( $save_button_text ) > 0 );

		$this->browser->value( '.input_score_1', 9 );
		$this->browser->value( '.input_score_2', 1 );
		$this->browser->value( '.input_score_3', 9 );
		$this->browser->value( '.input_score_4', 9 );
		$this->browser->value( '.input_score_5', 9 );
		// Total 37
		$this->browser->click( '.input_save_score' );

		// Can we see the total?
		$this->browser->assertSee( '37' );

		// Scores table visible?
		$this->browser->assertVisible( 'tr.scores' );

		// Add another.
		$this->browser->assertVisible( '.add_a_score' );
		$this->browser->click( '.add_a_score' );
		$this->browser->pause( 500 );

		$this->browser->value( '.input_score_1', 5 );
		$this->browser->value( '.input_score_2', 4 );
		$this->browser->value( '.input_score_3', 3 );
		$this->browser->value( '.input_score_4', 2 );
		$this->browser->value( '.input_score_5', 1 );
		// Total 15
		$this->browser->click( '.input_save_score' );

		// Can we see the total?
		$this->browser->pause( 10000 );
		$this->browser->assertSee( '52' );

		// Delete the first score.
		$this->browser->click( '.scores .delete.action a' );

		// Now the average should be 15.
		$this->browser->assertSee( '15' );

		// We have to query the DB directly due to weird model caching.
		$email_new_activities_notified_query = \DB::table( 'meta' )->where( 'meta_key', Activity::get_user_notification_data()->user_notification_key );
		$this->assertEquals( 0, $email_new_activities_notified_query->get()->count() );

		// Send for moderation.
		$this->browser->pause( 500 );
		$this->browser->click( '.action.check' );

		$this->browser->waitFor( '.activities' );

		$this->assertEquals( 1, $email_new_activities_notified_query->get()->count() );

		// The activity should be unmoderated.
		$activity = Activity::firstOrfail();
		$this->assertTrue( $activity->is_unmoderated() );

		$this->assertEquals( $comment, $activity->get_meta( 'comment' ) );

		// Log in as a moderator.
		$this->logout();
		$this->create_user( 'Moderator' );
		$this->login();

		// Check single moderation page.
		$this->browser->click( '.unmoderated.activities .activity a' );
		$this->browser->assertVisible( '.action.times' );
		$this->browser->assertVisible( '.action.check' );

		// Edit the activity for small fixes.
		$this->browser->click( '.edit_link a' );

		$this->browser->visit( route( 'user_dashboard' ) );

		// Check mass moderation page.
		$this->browser->click( '.unmoderated .action.eye' );

		// Switch between the two.
		$this->browser->click( '.choice.accept' );
		$this->browser->click( '.choice.reject' );

		$this->browser->click( '.input_mass_moderate' );
		$activity->refresh();
		$this->assertFalse( $activity->is_unmoderated() );
	}
}
