<?php

namespace Tests\Browser;

/**
	@brief		Test the installation procedure... Or "not" installing it, rather.
	@since		2018-11-16 21:23:40
**/
class Install_Test
	extends DuskTestCase
{
    /**
    	@brief		Check that / goes to install, since we are not installed.
    	@since		2018-11-14 00:58:53
    **/
    public function test_install_redirect()
    {
    	$this->clear_install();
		$this->browse( function ( $browser )
		{
			// Visiting start should redirect to install.
			$browser->visit( route( 'start' ) );
			$browser->assertRouteIs( 'install_step_1' );
		} );
    }
}
