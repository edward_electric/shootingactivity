<?php

namespace Tests\Browser;

use App\Models\Users\User;
use Illuminate\Support\Facades\Mail;

/**
	@brief		Test password reset function.
	@group		passwords
	@since		2018-11-14 15:59:53
**/
class User_Password_Reset_Test extends DuskTestCase
{
    /**
    	@brief		Test a valid e-mail address.
    	@since		2018-11-14 17:15:55
    **/
    public function test_bad_send_password()
    {
    	$this->unauthenticated();

		// Don't really send any e-mails.
		Mail::fake();

		// Should go to login.
		$this->browser->visit( route( 'user_password_send' ) );
		$this->browser->assertRouteIs( 'user_password_send' );

		// Use random address.
		$this->browser->value( '.input_email', time() . '@shootingactivity.com' );

		$this->browser->click( '.submit.input_send_reset' );

		$this->browser->assertVisible( '.alert-danger' );

		$this->browser->assertRouteIs( 'user_password_send' );
	}

    /**
    	@brief		Test a valid e-mail address.
    	@since		2018-11-14 17:15:55
    **/
    public function test_good_send_password()
    {
    	$this->unauthenticated();

		// Don't really send any e-mails.
		Mail::fake();

		// Should go to login.
		$this->browser->visit( route( 'user_password_send' ) );
		$this->browser->assertRouteIs( 'user_password_send' );

		$this->browser->value( '.input_email', $this->user->email );

		$this->browser->click( '.submit.input_send_reset' );

		$this->browser->assertVisible( '.alert.alert-success' );

		$this->browser->assertRouteIs( 'user_login' );

		$token = $this->user->get_reminder_token();

		$this->browser->visit( route( 'user_password_reset', [ 'user_id' => $this->user->id, 'token' => $token ] ) );

		$new_password = microtime();
		$this->browser->value( '.input_password', $new_password );
		$this->browser->click( '.submit.input_set_password' );

		$this->browser->assertVisible( '.alert.alert-success' );

		$this->browser->assertRouteIs( 'user_login' );
    }
}
