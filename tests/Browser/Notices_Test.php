<?php

namespace Tests\Browser;

use App\Models\Notices\Notice;
use App\Models\Users\User;
use Illuminate\Support\Facades\DB;

/**
	@brief		Test notice board.
	@group		notices
	@since		2018-12-24 12:55:57
**/
class Notices_Test extends DuskTestCase
{
	/**
		@brief		Test the activity functionality.
		@since		2018-12-07 00:27:51
	**/
	public function test_switch_on_and_off()
	{
		$this->administrator();

		$this->browser->assertRouteIs( 'user_dashboard' );

		// Notice board should be visible.
		$this->browser->assertVisible( '.comments.action' );
		// And no subtitle
		$this->browser->assertMissing( '.comments.action .subtitle' );

		// Go to the settings and disable the notice board.
		$this->browser->visit( route( 'admin_general' ) );
		$this->browser->uncheck( '.input_notice_board' );
		$this->browser->click( '.input_save' );

		$this->browser->assertRouteIs( 'user_dashboard' );

		$this->browser->assertMissing( '.comments.action' );

		// Switch it on again.
		$this->browser->visit( route( 'admin_general' ) );
		$this->browser->check( '.input_notice_board' );
		$this->browser->click( '.input_save' );

		$this->browser->assertVisible( '.comments.action' );

		// Log in as a user.
		$this->logout();
		$this->create_user( 'User' );
		$this->login();

		$email_notified_query = \DB::table( 'meta' )->where( 'meta_key', Notice::get_user_notification_data()->user_notification_key );
		$this->assertEquals( 0, $email_notified_query->get()->count() );

		// And now go add a notice.
		$this->browser->visit( route( 'notices_index' ) );
		$this->browser->assertMissing( '.thread_summary' );
		$this->browser->visit( route( 'notices_add' ) );

		$comment = md5( microtime() );
		$this->browser->value( '.input_text', '<h1>' . $comment );
		$this->browser->click( '.input_add_new_comment' );

		$user_count = User::count();
		$this->assertEquals( $user_count-1, $email_notified_query->get()->count() );

		// You should be back at the index.
		$this->browser->assertRouteIs( 'notices_index' );

		// And your comment should be there.
		$this->browser->assertVisible( '.thread_summary' );
		// With a nice message.
		$this->browser->assertVisible( '.alert.alert-success' );

		// And the _escaped_ text should also be there.
		$this->browser->assertSee( '&lt;h1&gt;' . $comment );

		$this->browser->visit( route( 'user_dashboard' ) );
		$this->browser->assertVisible( '.comments.action .text-muted' );

		$this->browser->visit( route( 'notices_index' ) );
		// Click the first available replies.
		$this->browser->click( '.replies a' );

		$comment = md5( microtime() );
		$this->browser->value( '.input_text', '<pre>' . $comment );
		$this->browser->click( '.input_add_new_comment' );

		$this->browser->assertSee( '&lt;pre&gt;' . $comment );

		$this->browser->visit( route( 'notices_index' ) );
		$this->browser->assertSee( '&lt;pre&gt;' . $comment );
	}
}
