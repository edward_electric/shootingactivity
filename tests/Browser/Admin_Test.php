<?php

namespace Tests\Browser;

use App\Models\Sites\Site;

/**
	@brief		Test admin stuff.
	@since		2018-11-16 21:22:23
**/
class Admin_Test extends DuskTestCase
{
	/**
		@brief		Test the access.
		@since		2018-11-16 22:16:31
	**/
	public function test_403()
	{
		$this->user();
		$this->browser->visit( route( 'admin_general' ) );
		$this->browser->assertRouteIs( 'user_dashboard' );

		$this->logout();

		$this->moderator();
		$this->browser->visit( route( 'admin_general' ) );
		$this->browser->assertRouteIs( 'user_dashboard' );
	}

	/**
		@brief		Test the general admin settings.
		@since		2018-11-16 22:05:42
	**/
	public function test_general_settings()
	{
		$this->administrator();
		$this->browser->visit( route( 'admin_general' ) );
		$this->browser->assertRouteIs( 'admin_general' );

		// Test the name.
		$name = md5( microtime() );
		$this->browser->value( '.input_site_name', $name );

		// And moderators.
		$moderators = rand( 0, 10 );
		$this->browser->value( '.input_moderators', $moderators );

		$this->browser->click( '.submit.input_save' );

		// Fetch the site.
		$site = Site::firstOrFail();

		$this->assertEquals( $name, $site->name );
		$this->assertEquals( $moderators, $site->get_meta( 'moderators' ) );
	}
}
