<?php

namespace Tests\Browser;

use App\Models\Guns\Gun;

/**
	@brief		Test guns.
	@group		guns
	@details	The generic model is already tested. We only test the extra stuff like images.
	@since		2018-11-18 16:07:56
**/
class Gun_Test extends DuskTestCase
{
	/**
		@brief		Test the adding and removing of guns.
		@since		2018-11-18 16:08:17
	**/
	public function test_gun()
	{
		$this->user();

		$route = 'guns_index';
		$this->browser->visit( route( $route ) );
		$this->browser->assertRouteIs( $route );

		// Add a gun.
		$route = 'guns_add';
		$this->browser->visit( route( $route ) );
		$this->browser->assertRouteIs( $route );

		$this->browser->value( 'input.input_name', 'Testing a Ruger' );
		$this->browser->select( 'caliber', '.22 Long Rifle' );
		$this->browser->select( 'group', 'B' );

		$this->browser->click( '.input_save_guns' );

		$this->browser->assertRouteIs( 'guns_index' );

		// Edit the gun.
		$this->browser->click( '.guns.active .name a' );

		$this->browser->value( 'input.input_name', 'Testing a Ruger 2' );
		$this->browser->select( 'caliber' );
		$this->browser->select( 'group' );
		// Upload an image.
		$this->browser->attach( 'file', __DIR__ . '/../files/rugermk4.jpg' );
		$this->browser->click( '.input_save_guns' );

		$this->browser->assertRouteIs( 'guns_index' );

		// Did the image work?
		$this->browser->assertVisible( '.guns.active .image img' );

		// Edit the gun.
		$this->browser->click( '.guns.active .name a' );

		$this->browser->value( 'input.input_name', 'Testing a Ruger 2' );
		$this->browser->select( 'caliber' );
		$this->browser->select( 'group' );
		// Delete the image
		$this->browser->click( '.form_item_delete_file label' );		// For some reason the checkbox itself cannot be check()ed.
		$this->browser->click( '.input_save_guns' );
		// Should now be gone.
		$this->browser->assertMissing( '.guns.active .image img' );
	}
}
