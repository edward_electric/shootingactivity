<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;

class Login extends Page
{
	/**
		@brief		Create a user.
		@since		2018-11-15 15:56:10
	**/
	public function create_user()
	{
	}

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return route( 'user_login' );
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@element' => '#selector',
        ];
    }
}
