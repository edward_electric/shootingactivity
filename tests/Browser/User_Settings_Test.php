<?php

namespace Tests\Browser;

use App\Models\Users\User;

/**
	@brief		Test user settings.
	@group		user_settings
	@since		2018-12-20 22:02:44
**/
class User_Settings_Test extends DuskTestCase
{
	/**
		@brief		Test that the settings page is visible.
		@since		2018-12-20 22:03:13
	**/
	public function test_for_user()
	{
		$this->user();

		$this->browser->visit( route( 'user_settings' ) );
		$this->browser->assertMissing( '.form_item_email_new_activities select' );
		$this->browser->click( '.input_save' );
		$this->browser->assertRouteIs( 'user_dashboard' );

	}

	/**
		@brief		Moderators have other settings.
		@since		2018-12-20 22:03:35
	**/
	public function test_for_moderator()
	{
		$this->moderator();

		$this->browser->visit( route( 'user_settings' ) );
		$selector = '.form_item_email_new_activities select';
		$this->browser->assertValue( $selector, 'once_per_login' );
		$this->browser->select( $selector, 'never' );
		$this->browser->select( $selector, 'always' );
		$this->browser->click( '.input_save' );
		$this->browser->assertRouteIs( 'user_dashboard' );

		$this->browser->visit( route( 'user_settings' ) );
		$this->browser->assertValue( $selector, 'always' );
	}
}
