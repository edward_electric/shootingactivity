<?php

namespace Tests\Browser;

use App\Models\Users\User;

/**
	@brief		Test logging in.
	@group		login
	@since		2018-11-14 13:29:23
**/
class Login_Test
	extends DuskTestCase
{
	/**
		@brief		Test a failed login.
		@since		2018-11-14 13:34:39
	**/
	public function test_login()
	{
    	$this->unauthenticated();

    	// 1. Failure.

		// Should go to login.
		$this->browser->visit( route( 'start' ) );
		$this->browser->assertRouteIs( 'user_login' );

		$this->browser->value( '.input_email', time() . '@example.org' );
		$this->browser->value( '.input_password', microtime() );

		$this->browser->click( '.submit.input_login' );

		$this->browser->assertRouteIs( 'user_login' );
		$this->browser->assertVisible( '.alert-danger' );

    	// 2. Success

		$password = microtime();

		// We need to set the password for admin to something we know.
		$this->user->set_password( $password );
		$this->user->save();

		$this->browser->value( '.input_email', $this->user->email );
		$this->browser->value( '.input_password', $password );

		$this->browser->click( '.submit.input_login' );

		$this->browser->assertRouteIs( 'user_dashboard' );
	}

}
