<?php

namespace Tests\Browser;

use App\Models\Categories\Category;

/**
	@brief		Test categories.
	@details	Testing categories tests all generic models also.
	@group		categories
	@since		2018-11-18 16:07:56
**/
class Categories_Test extends DuskTestCase
{
	/**
		@brief		Test the adding and removing of categories.
		@since		2018-11-18 16:08:17
	**/
	public function test_category()
	{
		$this->administrator();

		$index_route = 'categories_index';

		$this->browser->visit( route( $index_route ) );
		$this->browser->assertRouteIs( $index_route );

		// Add a category.
		$this->browser->visit( route( 'categories_add' ) );
		$this->browser->assertRouteIs( 'categories_add' );

		$category_name = microtime();
		$this->browser->value( 'input.input_name', $category_name );
		$this->browser->click( '.input_save_categories' );
		$this->browser->assertRouteIs( $index_route );
		$this->browser->assertSee( $category_name );

		// Refresh and it should still exist in the index.
		$this->browser->visit( route( $index_route ) );
		$this->browser->assertSee( $category_name );

		// Edit the category
		// --------------
		$this->browser->click( '.categories .name a:first-of-type' );

		$category_name = microtime();
		$this->browser->value( 'input.input_name', $category_name );
		$this->browser->click( '.input_save_categories' );

		$this->browser->assertRouteIs( $index_route );

		$this->browser->assertSee( $category_name );

		// Archive the category
		// -----------------
		$this->browser->assertMissing( '.archived.container' );

		$this->browser->click( '.categories.active .action.archive a' );
		$this->browser->click( '.input_archive_categories' );
		$this->browser->assertRouteIs( $index_route );

		// We should see that there is a category archived.
		$this->browser->assertVisible( '.archived.container' );

		// Restore the category
		// -----------------
		$this->browser->click( '.archived.container a' );

		$this->browser->click( '.categories.archived .action.restore a' );
		$this->browser->click( '.input_restore_categories' );

		$this->browser->assertRouteIs( $index_route );
		$this->browser->assertMissing( '.archived.container' );

		// Delete the category
		// ----------------
		$this->browser->click( '.categories.active .action.delete' );
		$this->browser->click( '.input_delete_categories' );

		$this->browser->assertRouteIs( $index_route );

		// We should see this in an alert.
		$this->browser->assertSee( $category_name );

		// Visit the page again to get rid of the notice.
		$this->browser->visit( route( $index_route ) );
		$this->browser->assertDontSee( $category_name );
	}
}
