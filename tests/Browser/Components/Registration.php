<?php

namespace Tests\Browser\Components;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Component as BaseComponent;

class Registration extends BaseComponent
{
    /**
     * Get the root selector for the component.
     *
     * @return string
     */
    public function selector()
    {
        return '#selector';
    }

    /**
     * Assert that the browser page contains the component.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert( Browser $browser )
    {
        $browser->assertVisible($this->selector());
    }

    /**
     * Get the element shortcuts for the component.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@element' => '#selector',
        ];
    }

    /**
    	@brief		Register someone.
    	@since		2018-10-06 13:29:15
    **/
    public function register( Browser $browser, $name )
    {
		$browser->click( 'button.registration' );
		$browser->value( '.form_item_participant_name input', $name );

		$save_button = 'form#registration .save_button button';
		$browser->select( 'lane', '1' );
		$browser->click( $save_button );
		$browser->pause( 250 );
    }
}
