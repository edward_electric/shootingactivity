<?php

namespace Tests\Browser;

use App\Models\Users\User;

/**
	@brief		Test modifying the user password.
	@group		password
	@since		2018-11-15 15:48:23
**/
class User_Password_Test extends DuskTestCase
{
	/**
		@brief		Test a new password that is too short.
		@since		2018-11-15 15:48:39
	**/
	public function test_password_length()
	{
		$this->user();

		// Set the user's password.
		$password = md5( microtime() );
		$this->user->set_password( $password );

		// Visit the user password change page.
		$this->browser->visit( route( 'user_password' ) );
		$this->browser->assertRouteIs( 'user_password' );
		$this->browser->value( '.input_old_password', $password );

		$new_password = '1234';
		$this->browser->value( '.input_new_password_1', $new_password );
		$this->browser->value( '.input_new_password_2', $new_password );

		$this->browser->click( '.input_change_password' );

		// The HTML5 pattern will stop us from clicking.
		$this->browser->assertRouteIs( 'user_password' );
	}

	/**
		@brief		Test that the old password matches.
		@since		2018-11-15 16:23:12
	**/
	public function test_incorrect_old_password()
	{
		$this->user();

		// Set the user's password.
		$password = md5( microtime() );
		$this->user->set_password( $password );
		$this->user->save();

		// Visit the user password change page.
		$this->browser->visit( route( 'user_password' ) );
		$this->browser->assertRouteIs( 'user_password' );
		$this->browser->value( '.input_old_password', microtime() );

		$new_password = md5( microtime() );
		$this->browser->value( '.input_new_password_1', $new_password );
		$this->browser->value( '.input_new_password_2', $new_password );

		$this->browser->click( '.input_change_password' );
		$this->browser->assertRouteIs( 'user_password' );
		$this->browser->assertVisible( '.alert-danger' );
	}

	/**
		@brief		Test mismatched passwords.
		@since		2018-11-15 16:23:12
	**/
	public function test_mismatched_new_passwords()
	{
		$this->user();

		// Set the user's password.
		$password = md5( microtime() );
		$this->user->set_password( $password );
		$this->user->save();

		// Visit the user password change page.
		$this->browser->visit( route( 'user_password' ) );
		$this->browser->assertRouteIs( 'user_password' );
		$this->browser->value( '.input_old_password', $password );

		$new_password = md5( microtime() );
		$this->browser->value( '.input_new_password_1', $new_password );
		$this->browser->value( '.input_new_password_2', $new_password . 'x' );

		$this->browser->click( '.input_change_password' );
		$this->browser->assertRouteIs( 'user_password' );
		$this->browser->assertVisible( '.alert-danger' );
	}

	/**
		@brief		Test a new password that works.
		@since		2018-11-15 16:23:12
	**/
	public function test_successful_password_change()
	{
		$this->user();

		// Set the user's password.
		$password = md5( microtime() );
		$this->user->set_password( $password );
		$this->user->save();

		// Visit the user password change page.
		$this->browser->visit( route( 'user_password' ) );
		$this->browser->assertRouteIs( 'user_password' );
		$this->browser->value( '.input_old_password', $password );

		$new_password = md5( microtime() );
		$this->browser->value( '.input_new_password_1', $new_password );
		$this->browser->value( '.input_new_password_2', $new_password );

		$this->browser->click( '.input_change_password' );

		$this->browser->assertVisible( '.alert.alert-success' );
		$this->browser->assertRouteIs( 'user_dashboard' );
	}
}
