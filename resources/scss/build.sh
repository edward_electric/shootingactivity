#!/bin/bash

if [[ "$1" == "" ]]; then
	echo Please specify a config file to use.
	exit
fi

if [ -f "$1.conf" ]; then
	source "$1.conf"
fi

if [ "$OUTPUT" == "" ]; then
	OUTPUT="$1.css"
fi

if [ -f "$OUTPUT" ]; then
    rm "$OUTPUT"
fi

sassc "$1.scss" > $OUTPUT

