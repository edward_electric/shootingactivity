/**
	@brief		Make adding scores easier.
	@since		2018-11-25 17:14:05
**/
;(function( $ )
{
    $.fn.extend(
    {
        sa_string_editor : function()
        {
            return this.each( function()
            {
                var $this = $(this);

                var $inputs = $( 'input.single.score', $this );	// The 5 single scores.
                var $total = $( 'input.input_total', $this );	// The total input.

                var $score_x = false;	// Are we keeping track of Xs (inner 10s?)

                /**
                	@brief		Make the form look nicer by moving things around.
                	@since		2018-11-25 17:40:47
                **/
                $this.beautify_form = function()
                {
                	var counter = 1;
                	var $submit_button = $( '.form_item_save_score button' );
                	var $submit = $( '<div class="save_score_button col-4 mb-2">' );
                	$submit_button.appendTo( $submit );
                	$submit_button.css( 'width', '100%' );

                	while( true )
                	{
                		// Decide whether there are more inputs to add.
                		var selector = '.input_score_' + counter;
                		var $input = $( selector, $this );
                		if ( $input.length < 1 )
                			break;

                		// Every 5 inputs = new row.
                		var $row = $( '<div class="scores row">' );
                		$row.appendTo( $this );

                		// Add the 5 inputs.
                		for ( five = 0; five < 5 ; five++ )
                		{
                			selector = '.input_score_' + ( counter + five );
                			$input = $( selector, $this ).parent();
                			$input.addClass( 'col-4 mb-2' );
                			$input.appendTo( $row );
                		}

                		// A submit after every 5.
                		$submit.clone().insertAfter( $input );
                		counter += 5;
                	}

                	// Total gets its own row.
               		var $total_row = $( '<div class="scores row mb-4">' );
               		$total_row.appendTo( $this );
                	$( '.form_item_total .input_container', $this )
                		.addClass( 'col' )
                		.appendTo( $total_row );

                	// Remove the old total row.
                	$( '.form_item_total .label_container' ).parent().remove();

                	// Not needed anymore.
                	$submit.remove();

                	// Do we have a score_x?
                	$score_x = $( '.form_item_score_x', $this );
                	$( '.label_container', $score_x ).hide();
                	$( '.input_container', $score_x ).hide();

                	// Get the actual input.
                	$score_x = $( 'input', $score_x );

                	// Remove the now-empty score rows.
                	$( '.form_item.single.score', $this ).remove();
                }
                $this.beautify_form();

                /**
                	@brief		Sum the single scores into the total.
                	@since		2018-11-25 17:34:04
                **/
                $this.sum_scores = function()
                {
                	var scores = [];
                	$.each( $inputs, function( index, item )
                	{
                		var val = $( item ).val();
                		val = parseInt( val ) || 0 ;
               			scores.push( val );
                	} );

                	scores = scores.sort( (a,b)=>a-b )	// Numerical sort, instead of alphabetical.
                		.reverse()	// Highest values first
                		.slice( 0, scoring_settings.string_score_count );	// We only want the string count.

                	var total = 0;
                	for ( var counter = 0; counter < scores.length ; counter++ )
                		total += scores[ counter ];

                	if ( total < 1 )
                		total = '';
                	$total.val( total );
                }

				/**
					@brief		Automove the focus automatically when entering single scores.
					@since		2018-11-25 17:34:14
				**/
				$inputs.on( 'input', function()
				{
					var $shot = $( this );
					var shot = $shot.val();

					if ( shot.trim() == '' )
						return;

					// X?
					if ( $score_x )
					{
						if ( shot == '*' )
						{
							$shot.addClass( 'inner_10' );
							$shot.val( 10 );
							shot = 10;
						}
						else
						{
							$shot.removeClass( 'inner_10' );
						}

						// Set the score_x counter.
						var new_score_x = $( '.inner_10' ).length;
						$score_x.val( new_score_x );
					}

					// Convert to int.
					shot = parseInt( shot );
					if ( isNaN( shot ) )
					{
						shot = 0;
						$shot.val( 0 );
					}

					if ( shot == 1 )	// Because 1 could be 1 or 10
						return;

					var carry = 0;
					if ( shot > 10 )
					{
						$shot.val( 1 );
						carry = shot - 10;
					}

					// Find the next input to focus.
					var focus_found = false;
					$.each( $inputs, function( index, options_shot )
					{
						var $options_shot = $( options_shot );
						if ( focus_found )
						{
							// Having inputted a > 10, the "remainder" should be inputted here, if empty.
							$options_shot.focus().select();
							if ( carry > 0 )
							{
								$options_shot.val( carry );
								carry = 0;
								// Leave the focus_found alone to go to the next input.
							}
							else
								focus_found = false;
							return;
						}

						if ( $options_shot.is( ':focus' ) )
						{
							focus_found = true;
						}
					} );

					$this.sum_scores();

					// Was the last input focused?
					if ( focus_found )
						$total.focus();
				} );

				$inputs.blur( function()
				{
					$this.sum_scores();
				} );

				// Focus the first single.
				$focused_input = $inputs.first().focus();
				$focused_input[ 0 ].select();

            } ); // return this.each( function()
        } // plugin: function()
    } ); // $.fn.extend({
} )( jQuery );
