/**
	@brief		Allow for mass moderation.
	@since		2018-11-23 23:36:01
**/
;(function( $ )
{
    $.fn.extend(
    {
        sa_moderation_index : function()
        {
            return this.each( function()
            {
                var $this = $(this);

                var $activities_to_reject = $( '.form_item_activities_to_reject select' );

                // Clear the selection.
                $activities_to_reject.val( '' );

                // Unhide the controls.
                $( 'table .choice' ).removeClass( 'hidden' );

                // And make them clickable.
                $( 'table tbody td.choice' ).click( function()
                {
                	var $choice = $( this );
                	var $tr = $choice.closest( 'tr' );
                	var model_id = $tr.data( 'model_id' );

                	if ( $choice.hasClass( 'accept' ) )
                	{
                		$choice.addClass( 'reject' );
                		$choice.removeClass( 'accept' );
                		$( 'option[value="' + model_id + '"]', $activities_to_reject ).attr( 'selected','selected' );
                	}
                	else
                	{
                		$choice.addClass( 'accept' );
                		$choice.removeClass( 'reject' );
                		$( 'option[value="' + model_id + '"]', $activities_to_reject ).removeAttr( 'selected','selected' );
                	}
                } ).click();

                $( '.do_moderation' ).removeClass( 'hidden' )
                	.click( function()
                	{
                		// Assemble the commands and send them in.
                	} );

                // When clicking on the gun inset, move it around.
                $( '.image.gun_inset' ).click( function( e )
                {
                	e.preventDefault();
                	var position = {
                		top: 'auto',
                		bottom: 'auto',
                		left: 'auto',
                		right: 'auto',
                	};
                	var $gun_inset = $( this );
                	var position = $gun_inset.position();
                	// Reset the css.
              		var new_position = { 'left' : 'auto', 'right' : 'auto', 'bottom' : 'auto', 'top' : 'auto' };
                	if ( position.top == 0 )
                	{
                		// At top
                		if ( position.left == 0 )
                		{
                			new_position[ 'top' ] = 0;
                			new_position[ 'right' ] = 0;
                		}
                		else
                		{
                			new_position[ 'right' ] = 0;
                			new_position[ 'bottom' ] = 0;
                		}
                	}
                	else
                	{
                		// At bottom
                		if ( position.left == 0 )
                		{
                			new_position[ 'top' ] = 0;
                			new_position[ 'left' ] = 0;
                		}
                		else
                		{
                			new_position[ 'bottom' ] = 0;
                			new_position[ 'left' ] = 0;
                		}
                	}
                	$gun_inset.css( new_position );
                } );

            } ); // return this.each( function()
        } // plugin: function()
    } ); // $.fn.extend({
} )( jQuery );
