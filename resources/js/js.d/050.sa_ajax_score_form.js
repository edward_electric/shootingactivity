/**
	@brief		Load the score editor via ajax on the current page.
	@since		2018-12-01 14:48:56
**/
;(function( $ )
{
    $.fn.extend(
    {
        sa_ajax_score_form : function()
        {
            return this.each( function()
            {
                var $this = $(this);

                // Where the button exists and where we put the form.
                var $container = $this.closest( '.col' );

                // Automatically load the form.
                var $form;	// The form from the score editor.
                var link = $this.attr( 'href' );

                // When the user clicks the link...
                $this.click( function( e )
                {
                	e.preventDefault();

					// Hide the existing stuff.
					$container.children().hide();

					$.ajax( {
						'type' : 'GET',
						'url' : link,
					} ).done( function( data )
					{
						var $data = $( data );
						$form = $( '.content', $data );

						// Make the form submittable.
						$form.appendTo( $container )
							.sa_csrf_tokenize();

						// And make it nice to edit.
						$( 'form#string_editor' ).sa_string_editor();

						// Take over the cancel button.
						$( '.cancel.button', $form ).click( function( e )
						{
							e.preventDefault();
							$form.remove();
							$container.children().show();
						} );
					} );
                } );

            } ); // return this.each( function()
        } // plugin: function()
    } ); // $.fn.extend({
} )( jQuery );
