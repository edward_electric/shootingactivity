/**
	@brief		Add the csrf token to the form.
	@since		2018-12-01 15:11:56
**/
;(function( $ )
{
    $.fn.extend(
    {
        sa_csrf_tokenize : function()
        {
            return this.each( function()
            {
                var $this = $(this);

                // Add the csrf token to all forms.
				var $csrf_token = $('meta[name="csrf-token"]');
				var csrf_token = $csrf_token.attr( 'content' );
				// This is for manually submitted forms.
				$( 'form' ).append( '<input type="hidden" name="_token" value="' + csrf_token + '" />' );
				// And this is for ajax forms.
				$.ajaxSetup(
				{
					headers:
					{
						'X-CSRF-TOKEN': csrf_token,
					}
				} );

            } ); // return this.each( function()
        } // plugin: function()
    } ); // $.fn.extend({
} )( jQuery );
