/**
	@brief		Ajaxify the image uploads, adding a preview and browser resize.
	@since		2018-11-26 10:50:45
**/
;(function( $ )
{
    $.fn.extend(
    {
        sa_ajaxify_file_upload : function()
        {
            return this.each( function()
            {
                var $this = $(this);

                var $form = $this.closest( 'form' );
                var $rotater = {};

                // Add a preview above the input.
                var $preview = $( '<img class="ajax image preview">' );
                $preview[ 0 ].crossOrigin = "anonymous";	// To prevent security warnings, as per https://stackoverflow.com/questions/25753754/canvas-todataurl-security-error-the-operation-is-insecure
                $preview.prependTo( $this.parent() );

                /**
                	@brief		Show the image that is about to be uploaded.
                	@since		2018-11-26 23:09:39
                **/
                $preview.load = function()
                {
					var reader = new FileReader();

					reader.onload = function( e )
					{
						// get loaded data and render thumbnail.
						$preview.attr( 'src', e.target.result );
					};

					// read the image file as a data URL.
					reader.readAsDataURL( $this[ 0 ].files[ 0 ] );

					$rotater.$div.show();
               }

                /**
                	@brief		Todo: This is for when we resize the image before sending it in.
                	@since		2018-11-27 08:13:17
                **/
                $preview.resize = function()
                {
                	var $canvas = $( '<canvas>' );
					var ctx = $canvas[ 0 ].getContext("2d");
					ctx.drawImage( $preview[ 0 ], 0, 0);

					var width = $preview[ 0 ].naturalWidth;
					var height = $preview[ 0 ].naturalHeight;

					var max = 1600;
					var longest = Math.max( height, width );

					height = height / longest * max;
					height = Math.round( height );
					width = width / longest * max;
					width = Math.round( width );

					$canvas[ 0 ].width = width;
					$canvas[ 0 ].height = height;
					var ctx = $canvas[ 0 ].getContext("2d");
					ctx.drawImage( $preview[ 0 ], 0, 0, width, height);

					// Convert to jpg.
					$preview.dataurl = $canvas[ 0 ].toDataURL( "image/jpeg" );
                }

                /**
                	@brief		Rotate the preview image 90 degrees clockwise.
                	@since		2020-07-03 21:56:53
                **/
                $this.rotate = function()
                {
                	var $canvas = $( '<canvas>' );
					var context = $canvas[ 0 ].getContext("2d");
					var image = $preview[ 0 ];

					// natural* is the real size, while width is just the visible, resized size.
					$canvas[ 0 ].height = image.naturalWidth;
					$canvas[ 0 ].width = image.naturalHeight;

					context.translate( image.naturalHeight, 0 );
					context.rotate( 90 * Math.PI / 180 );	// Radians.
					context.drawImage( image, 0, 0 );

					var dataURL = $canvas[ 0 ].toDataURL( "image/jpeg", 0.8 );
					$preview.attr( 'src', dataURL );
                }

                // When the upload changes, load the preview.
                $this.change( function()
				{
					var the_file = $this[ 0 ].files[ 0 ];

					// Only jpegs can be rotated.
					if ( the_file.type !== 'image/jpeg' )
					{
						$rotater.$div.hide();
						$preview.hide();
						return;
					}

					$preview.show();
					$preview.load();
				} );

				$form.submit( function( event )
				{
					if ( $form.submitting !== undefined )
						return true;

					// If there is no image, just upload.
					if ( $this[ 0 ].files.length < 1 )
						return true;

					// Only resize jpg.
					var the_file = $this[ 0 ].files[ 0 ];
					if ( the_file.type !== 'image/jpeg' )
						return true;

					$preview.resize();

					var $file_data = $( '<input type="hidden" name="file_data">' );
					$file_data.val( $preview.dataurl.replace( /^data:image\/(png|jpeg);base64,/, "") );
					$file_data.appendTo( $form );

					var $file_name = $( '<input type="hidden" name="file_name">' );
					$file_name.val( $this[ 0 ].files[ 0 ].name );
					$file_name.appendTo( $form );

					// The original image is no longer necessary.
					$( '.input_file' ).remove();

					$form.submitting = true;
					$form.submit();
				} );

				$this.rotater = function( $parent )
				{
					// The div contains the whole thing.
					$rotater.$div = $( '<span>' )
						.addClass( 'rotater small secondary button' )
						.click( function()
						{
							$this.rotate();
						} )
						.insertAfter( $preview );

					$( '<br>' ).insertAfter( $preview );

					$rotater.$icon = $( '<i>' )
					.addClass( 'fas fa-redo-alt' )
					.css( 'cursor', 'pointer' )
					.attr( 'title', 'Rotate' )
					.appendTo( $rotater.$div );

					$rotater.$div.append( ' Rotate' );

					// Hide the whole div.
					$rotater.$div.hide();
				}

				// Add rotate button.
				$this.rotater( $this.parent() );

				// Prevent the form from submitting until the image has been reloaded.
            } ); // return this.each( function()
        } // plugin: function()
    } ); // $.fn.extend({
} )( jQuery );
