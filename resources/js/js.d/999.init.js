jQuery( document ).ready( function( $ )
{
	$( 'form' ).sa_csrf_tokenize();
	$( '#moderation_index' ).sa_moderation_index();
	$( 'form#string_editor' ).sa_string_editor();
	$( '.add_a_score a' ).sa_ajax_score_form();
	$( 'input.sa_ajax_upload' ).sa_ajaxify_file_upload();
} );
