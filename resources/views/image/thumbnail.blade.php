<div class="file image thumbnail">
	<a href="{{ $file->get_inline_url() }}" title={{ __( 'View full image' ) }}>
		<img src="{{ $file->get_inline_url( 'thumbnail' ) }} " />
	</a>
</div>
