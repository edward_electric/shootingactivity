<div class="file image">
	<a href="{{ $file->get_inline_url() }}" title="{{ __( 'View full image' ) }}">
		<img src="{{ $file->get_inline_url() }} " />
	</a>
</div>
