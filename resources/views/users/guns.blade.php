<h2>{{ __( 'Guns' ) }}</h2>

@if ( ! app()->site()->get_meta( 'public_guns' ) && app()->user()->may( 'access_admin' ) )
	<p>{{ __( "You are able to see this user's guns because you are an administrator." ) }}</p>
@endif

<table class="table public_guns">
	<thead>
		<tr>
			<th>{{ __( 'Name' ) }}</th>
			<th>{{ __( 'Caliber' ) }}</th>
			<th>{{ __( 'Group' ) }}</th>
		</tr>
	</thead>
	<tbody>
		@foreach( $model->guns as $gun )
		<tr>
			<td>{{ $gun->name }}</td>
			<td>{{ $gun->caliber->name }}</td>
			<td>
				@if ( $gun->group )
					{{ $gun->group->name }}
				@endif
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
