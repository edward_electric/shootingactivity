@extends( 'layouts.default' )

@section( 'h1', __( 'Users' ) )
@section( 'h1_icon', 'fa-solid fa-users' )

@section( 'content' )

<table class="table table-striped">
	<thead>
		<tr>
			<th class="name">{{ __( "Name" ) }}</th>
			<th class="email">{{ __( "E-mail" ) }}</th>
		</tr>
	</thead>
	<tbody>
		@foreach( $models as $model )
			<tr>
				<td class="name">
					<a href="{{ route( 'user_profiles_view', [ 'user' => $model ] ) }}">
						{{ $model->name }}
					</a>
				</td>
				<td class="email">
					<a href="mailto:{{ $model->email }}">
						{{ $model->email }}
					</a>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

@append
