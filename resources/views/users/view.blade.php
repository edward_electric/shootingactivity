@extends( 'layouts.default' )

@section( 'h1', $model->name )
@section( 'h1_icon', 'fa-solid fa-user' )

@section( 'content' )

<div class="name">
	<i class="fa fa-fw fa-user"></i>
	{{ $model->name }}
</div>

<div class="email">
	<i class="fa fa-fw fa-envelope"></i>
	<a href="mailto:{{ $model->email }}">
		{{ $model->email }}
	</a>
</div>

<div class="last_seen">
	<i class="fa fa-fw fa-eye"></i>
	{{ __( 'Last seen: :ago', [ 'ago' => $model->get_last_seen_for_humans() ] ) }}
</div>

@if ( app()->site()->get_meta( 'public_guns' ) || app()->user()->may( 'access_admin' ) )
	@includeWhen( $model->guns->count() > 0, 'users.guns' )
@endif

@append
