<div class="comments thread_summary row">
	<div class="col-sm-12 meta">
		<div class="row">
			<div class="col-sm-12 col-md-2">
				<div class="row">
					<div class="col-sm-6 col-md-12 author">
						{{ $model->user->name }}
					</div>
					<div class="col-sm-6 col-md-12 ago">
						{{ $model->updated_at->diffForHumans() }}
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-10 text">
				{!! $model->html_text() !!}
			</div>
		</div>
	</div>
	@if ( $model->replies()->count() > 0 )
	<div class="col-sm-12 last_reply">
		<div class="row">
			<div class="col-sm-11 offset-sm-1">
				<div class="row">
					<div class="col-sm-12 col-md-2 meta">
						<div class="row">
							<div class="col-sm-6 col-md-12 author">
								{!! $model->replies->last()->user->name !!}
							</div>
							<div class="col-sm-6 col-md-12 ago">
								{{ $model->replies->last()->created_at->diffForHumans() }}
							</div>
						</div>
					</div>
					<div class="col-sm-12 col-md-10 text">
						{!! $model->replies->last()->html_text() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
	@endif
	<div class="col-sm-12 replies">
		<a href="{{ $url }}">
			{{ __( ':commentreplies comments.', [ 'commentreplies' => $model->replies()->count() ] ) }}
		</a>
	</div>
</div>
