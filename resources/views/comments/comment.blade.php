<div class="comments comment row">
	<div class="col-sm-12 col-md-3 meta">
		<div class="row">
			<div class="col-sm-6 col-md-12 author">
				<a href="{{ route( 'user_profiles_view', [ $model->user ] ) }}" title="{{ __( 'View user profile' ) }}">
					{{ $model->user->name }}
				</a>
			</div>
			<div class="col-sm-6 col-md-12 ago">
				{{ $model->updated_at->diffForHumans() }}
			</div>
		</div>
	</div>
	<div class="col-sm-12 col-md-9 text">
		{!! $model->html_text() !!}
	</div>
</div>
