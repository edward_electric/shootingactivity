@extends( 'layouts.default' )

@section( 'h1', __( 'Notice board' ) )
@section( 'h1_icon', 'fa-solid fa-comments' )

@section( 'content' )

<p>
	{{ __( 'Notices are deleted automatically after :days days.', [ 'days' => app()->site()->get_meta( 'notice_board_days' ) ] ) }}
</p>

@include( 'components.add_a_model', [
	'css_class' => 'add_a_notice',
	'label' => __( 'Add a new notice' ),
	'url' => route( 'notices_add' ),
] )

@if ( count( $models ) > 0 )
	@foreach( $models as $index => $model )
		@include( 'comments.thread_summary', [ 'model' => $model->comment, 'url' => route( 'notices_view', [ $model ] ) ] )
	@endforeach
@endif

@append
