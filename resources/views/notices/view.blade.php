@extends( 'layouts.default' )

@section( 'h1', $model->comment->first_words() . '...' )
@section( 'h1_icon', 'fa-solid fa-comments' )

@section( 'content' )

<div class="back_to_notices">
	<a href="{{ route( 'notices_index' ) }}"><i class="fa fas fa-arrow-left"></i> {{ __( 'Back to notices' ) }}</a>
</div>

@include( 'comments.comment', [ 'model' => $model->comment ] )

@foreach( $model->comment->replies as $reply )
	@include( 'comments.comment', [ 'model' => $reply ] )
@endforeach

{!! $form !!}

@append
