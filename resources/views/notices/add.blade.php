@extends( 'layouts.default' )

@section( 'h1', __( 'Write a new notice' ) )
@section( 'h1_icon', 'fa-solid fa-comment' )

@section( 'content' )

<div class="add_a_comment add_a_notice">
	{!! $form !!}
</div>

@endsection
