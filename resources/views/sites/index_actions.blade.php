@if ( Auth::user()->may( 'manage_network' ) )
	@if ( Auth::user()->site->id != $model->id )
		@php
		$tri->add( [
			'action' => 'switch_to',
			'icon' => 'fa-solid fa-sign-in-alt',
			'url' => route( 'sites_switch_to', [ 'site' => $model->id ] ),
			'text' => __( 'Switch to this site' ),
		] );
		@endphp
	@endif
@endif
