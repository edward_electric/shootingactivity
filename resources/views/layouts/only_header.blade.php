@extends( 'layouts.base' )

@section( 'header' )
	<header>
		<h1>
			@hasSection ( 'h1_icon' )
				<i class="fa fa-@yield( 'h1_icon' )"></i>
			@endif
			<a href="">
				@yield( 'h1' )
			</a>
		</h1>
	</header>
@endsection
