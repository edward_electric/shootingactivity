@extends( 'layouts.base' )

@section( 'header' )
	@include( 'layouts.header' )
@endsection

@section( 'footer' )
	@include( 'layouts.footer' )
@endsection
