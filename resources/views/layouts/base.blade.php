@section( 'page_id', Route::currentRouteName() )
<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="/css/app.css?{{ @filemtime(public_path( '/css/app.css' )) }}" rel="stylesheet" type="text/css" />
		@include( 'parts.local_css' )
        <link rel="icon" href="/svg/sa_logo.svg?{{ @filemtime(public_path( '/svg/sa_logo.svg' )) }}">
        <title>@include( 'parts.title' )</title>
    </head>
    <body id="@yield('page_id')">
    	<div class="container">

			@yield( 'header' )

			@if ( app()->alerts()->count() > 0 )
				@include( 'components.alerts.section' )
			@endif

			<div class="row content">
				<div class="col">
					@yield( 'content' )
				</div>
			</div>

			@yield( 'footer' )

			@includeWhen( env( 'APP_ENV' ) == 'dev', 'query_log' )

  		</div>

		<script type='text/javascript' src='/js/app.js?{{ @filemtime(public_path( '/js/app.js' )) }}'></script>
    </body>
</html>
