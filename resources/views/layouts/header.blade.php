@php
	$is_home = ( Route::currentRouteName() == 'user_dashboard' );
	$auth_check = Auth::check();
	$show_home = ( $auth_check ) && ( ! $is_home );

	if ( $auth_check )
		$theme_header_image_file = app()->site()->theme()->header_image()->get_file_model();
@endphp
<header class="row">
	@if ( $is_home )
	<div class="col mt-2 text-center">
		@if ( $auth_check && app()->site()->theme()->header_image()->get_file_model() )
			<div class="custom_header_image">
				@include ( 'image.full', [ 'file' => $theme_header_image_file ] )
				<div class="visually-hidden">{{ app()->site()->name }}</div>
			</div>
		@else
			<h1>
				<a href="{{ url()->current() }}">
				@hasSection ( 'h1_icon' )
					<i class="@yield( 'h1_icon' )"></i>
				@endif
				@yield( 'h1' )
				</a>
			</h1>
		@endif
	</div><!-- col -->
	@else
	<div class="col">
		<div class="nav mt-4">
			<nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
			  <ol class="breadcrumb">
				@if ( $show_home )
					<li class="breadcrumb-item">
						<a href="{{ route( 'user_dashboard' ) }}" title="{{ __( 'Link to the start page' ) }}">
						<i class="fa fa-home"></i>
						</a>
					</li>
				@endif
				<li class="breadcrumb-item active" aria-current="page">
						@hasSection ( 'h1_icon' )
							<i class="@yield( 'h1_icon' )"></i>
						@endif
						<a href="{{ url()->current() }}">
						@yield( 'h1' )
						</a>
				</li>
			  </ol>
			</nav>
		</div>
	</div>
	@endif
</header>

