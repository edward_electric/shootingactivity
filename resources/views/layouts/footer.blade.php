<footer class="footer row">
	@if ( Auth::check() )
	<div class="col-sm-12 col-md-12">
	@else
	<div class="col-sm-12 col-md-6">
	@endif
		<div class="menu row">
			@if ( Auth::check() )
			<div class="col-sm-12 col-md-6">
				<ul>
					<li class="user_dashboard"><a href="{{ route( 'user_dashboard' ) }}"><i class="fa fa-fw fa-home"></i></i> {{ __( "Dashboard" ) }}</a></li>
					<li class="logout"><a href="{{ route( 'user_logout' ) }}"><i class="fa fa-fw fa-sign-out-alt"></i></i> {{ __( "Log out" ) }}</a> {{ app()->user()->name }}</li>
				</ul>
			</div>
			<div class="col-sm-12 col-md-6">
				<ul>
					@if ( app()->site()->get_meta( 'organisation_email' ) )
					<li class="organisation_email"><a href="mailto:{{ app()->site()->get_meta( 'organisation_email' ) }}"><i class="fa fa-fw fa-envelope"></i> {{ app()->site()->get_meta( 'organisation_email' ) }}</a></li>
					@endif
					@if ( app()->site()->get_meta( 'organisation_url' ) )
					<li class="organisation_url"><a href="{{ app()->site()->get_meta( 'organisation_url' ) }}"><i class="fa fa-fw fa-globe"></i> {{ app()->site()->name }}</a></li>
					@endif
				</ul>
			</div>
			@else
			<div class="col-sm-12">
				<ul>
					@if( request()->route()->getName() != 'user_login' )
						<li class="user_login"><a href="{{ route( 'user_login' ) }}"><i class="fa fa-fw fa-sign-in-alt"></i></i> {{ __( "Log in" ) }}</a></li>
					@endif
					@if( request()->route()->getName() != 'user_password_send' )
						<li class="user_password_send"><a href="{{ route( 'user_password_send' ) }}"><i class="fa fa-fw fa-question"></i></i> {{ __( "Password reminder" ) }}</a></li>
					@endif
					@if ( env( 'CREATE_ACCOUNT_URL' ) != '' )
						<li class="create_account"><a href="{{ env( 'CREATE_ACCOUNT_URL' ) }}"><i class="fa fa-fw fa-star"></i></i> {{ __( "Create account" ) }}</a></li>
					@endif
				</ul>
			</div>
			@endif
		</div>
	</div>
	<div class="col shootingactivity text-center">
		<a href="{{ config( 'app.url' ) }}"><i class="fa fa-fw fa-globe"></i>{{ __( 'Shooting Activity' ) }}</a>
	</div>
</footer>
@php
$files = env( 'HTML_FOOTER_FILES', '' );
$files = explode( ';', $files );
$files = array_filter( $files );
foreach( $files as $file )
	readfile( app_path() . '/../' . $file );
@endphp
