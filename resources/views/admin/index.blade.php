<h2>
	Admin
</h2>
@php
	$ai = app()->action_icons( 'entry_view' );
	if ( Auth::user()->may( 'manage_site' ) )
	{
		$ai->add( [
			'icon' => 'fa-solid fa-cog',
			'route' => 'admin_general',
			'sort_order' => 25,
			'text' =>  __( "General settings" ),
		] )
		->add( [ 'generic_model' => App\Models\Categories\Category::class, ] )
		->add( [ 'generic_model' => App\Models\Guns\Caliber::class, ] )
		->add( [ 'generic_model' => App\Models\Guns\Group::class, ] )
		->add( [ 'generic_model' => App\Models\Permissions\Role::class, ] )
		->add( [
			'icon' => 'fa-solid fa-chart-line',
			'url' => route( 'admin_statistics' ),
			'text' =>  __( "Site statistics" ),
		] )

		->add( [
			'icon' => 'fa-solid fa-paintbrush',
			'route' => 'admin_theme',
			'text' =>  __( "Theme" ),
		] )

		;
	}

	if ( Auth::user()->may( 'manage_users' ) )
		$ai->add( [ 'generic_model' => App\Models\Users\User::class, ] );

	$ai->output();
@endphp
