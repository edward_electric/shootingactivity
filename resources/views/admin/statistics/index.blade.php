@extends( 'layouts.default' )

@section( 'page_id', 'site_statistics_index' )

@section( 'h1', __( 'Site statistics :year', [ 'year' => $statistics->year ] ) )
@section( 'h1_icon', 'fa-solid fa-chart-line' )

@section( 'content' )

@include( 'statistics.statistics' )

<p>
	<a href="{!! route( 'admin_statistics_regenerate' ) !!}">
		<i class="fa fa-recycle"></i>
		{{ __( 'Regenerate statistics' ) }}
	</a>
</p>

@append
