@extends( 'layouts.default' )

@section( 'page_id', 'admin_theme' )

@section( 'h1', __( 'Theme settings' ) )
@section( 'h1_icon', 'fa-solid fa-paintbrushog' )

@section( 'content' )
	<p>{{ __( 'These settings allow you to customize the look of the webapp to suit your organisation.' ) }}</p>

	{!! $form !!}
@append
