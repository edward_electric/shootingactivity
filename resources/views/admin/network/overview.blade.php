@extends( 'layouts.default' )

@section( 'page_id', 'network_overview' )

@section( 'h1', __( 'Network overview' ) )
@section( 'h1_icon', 'fa-solid fa-eye' )

@section( 'content' )
	@foreach( $sites as $site )
		<div class="site">
			<h2>{{ $site->name }}</h2>

			@include( 'admin.network.colors' )

			@foreach( $site->users->order_by( 'name' ) as $user )
				<article class="user">
					<span class="name">
						{{ $user->name }}
					</span>
					<span class="last_seen">
						{{ $user->get_last_seen_for_humans() }}
					</span>
					<ul class="guns">
						@foreach( $user->guns->order_by( 'name' ) as $gun )
							<li class="gun">
								<span class="name">
								@if ( $gun->is_archived() )
									<strike>{{ $gun->name }}</strike>
								@else
									{{ $gun->name }}
								@endif
								</span>
								<span class="caliber">
									{{ $gun->caliber->name }}
								</span>
							</li>
						@endforeach
					</ul>
				</article>
			@endforeach
		</div>
	@endforeach
@append
