<h2>
	{{ __( 'Network admin' ) }}
</h2>
@php
	$ai = app()->action_icons( 'network_admin' );

	$ai->add( [
		'icon' => 'fa-solid fa-eye',
		'route' => 'network_overview',
		'text' => __( 'Network overview' ),
	] );

	$ai->add( [ 'generic_model' => App\Models\Sites\Site::class, ] );

	$ai->output();
@endphp
