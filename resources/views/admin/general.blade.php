@extends( 'layouts.default' )

@section( 'page_id', 'admin_general' )

@section( 'h1', __( 'General settings' ) )
@section( 'h1_icon', 'fa-solid fa-cog' )

@section( 'content' )
	{!! $form !!}
@append
