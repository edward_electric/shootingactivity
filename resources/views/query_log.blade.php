@php
	$time_end = microtime( true );
	$difference = $time_end - $vars->get( 'controller_started' );
@endphp
<div class="row query_log">
	<div class="col">
		<p>
			Time: {{ round( $difference, 2 ) }}
		</p>
		<p>
			Queries: {{ count( DB::getQueryLog() ) }}
		</p>
		<p>
		<ol>
			@foreach( DB::getQueryLog() as $item )
				<li>{{ json_encode( $item ) }}</li>
			@endforeach
		</ol>
		</p>
	</div>
</div>
