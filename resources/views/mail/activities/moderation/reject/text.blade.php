@php
	$link = sprintf( '<a href="%s">%s</a>',
		route( 'activities_view', $activity ),
		$activity->created_at->diffForHumans()
	);
@endphp
@extends( 'mail.base' )

@section( 'mail.body' )

<p>{!! __( 'Your activity from :ago has been rejected.', [ 'ago' => $link ]  ) !!}</p>

@append
