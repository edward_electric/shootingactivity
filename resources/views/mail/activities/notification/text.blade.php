@extends( 'mail.base' )

@section( 'mail.body' )

<p>{!! sprintf( __( 'There are %snew activities%s to moderate on :sitename.', [ 'sitename' => $user->site->name ] ), '<a href="' . route( 'moderation_index' ) . '">', '</a>' ) !!}</p>

@append
