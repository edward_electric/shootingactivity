@extends( 'mail.base' )

@section( 'mail.body' )

<p>{{ __( 'This is your password reset link for :sitename. If you did not request a new password, ignore this e-mail.', [ 'sitename' => $user->site->name ] ) }}</p>

<p>{{ __( 'Click the link to reset your password.' ) }}</p>

<p><a href="{{ $user->get_password_reset_url() }}">{{ $user->get_password_reset_url() }}</a></p>

@append
