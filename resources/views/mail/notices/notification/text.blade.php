@extends( 'mail.base' )

@section( 'mail.body' )

<p>{!! sprintf( __( 'There is a %snew notice%s on :sitename.', [ 'sitename' => $user->site->name ] ), '<a href="' . route( 'notices_view', [ $model ] ) . '">', '</a>',  ) !!}</p>

<p>{{ __( ':username wrote:', [ 'username' => $model->comment->user->name ] ) }}</p>

<blockquote>
{!! $model->comment->html_text() !!}
</blockquote>

@foreach( $model->comment->replies as $reply )
	<p>{{ __( ':username replied:', [ 'username' => $model->comment->user->name ] ) }}</p>
	<blockquote>
	{!! $reply->html_text() !!}
	</blockquote>
@endforeach

@append
