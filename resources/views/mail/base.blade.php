<html>
	<style>
		body
		{
		}

		a
		{
		}

		.signature
		{
			color: #444;
		}

		.signature a
		{
			color: #111;
		}
	</style>
	<body>

	@yield( 'mail.body' )

	@section( 'mail.signature' )

	<div class="signature">
		--<br/>
		<a href="{{ route( 'start' ) }}">{{ config( 'app.name' ) }}</a>
	</div>

	@endsection

	@yield( 'mail.signature' )

	</body>
</html>
