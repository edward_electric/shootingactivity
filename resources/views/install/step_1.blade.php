@extends( 'layouts.only_header' )

@section( 'h1', __( 'Shooting Activity installation step 1' ) )
@section( 'h1_icon', 'fa-solid fa-hammer' )

@section( 'content' )
	<p>{{ __( 'To install Shooting Activity, you will need a connection to a database server.' ) }} </p>

	{!! $form !!}
@append
