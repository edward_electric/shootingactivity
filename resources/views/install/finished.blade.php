@extends( 'layouts.only_header' )

@section( 'h1', __( 'Shooting Activity installation finished' ) )
@section( 'h1_icon', 'fa-solid fa-hammer' )

@section( 'content' )

	<p>{!! __( 'Your e-mail address is: :email', [ 'email' => '<strong>' . $user->email . '</strong>' ] ) !!} </p>

	<p>{!! __( 'Your password is: :password', [ 'password' => '<strong>' . session( 'install_admin_password' ) . '</strong>' ] ) !!} </p>

	<p>{!! sprintf( __( 'Go ahead and %slog in%s!' ), '<a href="' . route( 'user_login' ) . '">', '</a>' ) !!}</a></p>

@append
