@extends( 'layouts.default' )

@section( 'h1', __( 'Mass import' ) )
@section( 'h1_icon', 'fa-solid fa-file-upload' )

@section( 'content' )
	{!! $form !!}

	<p class="cancel_link">
		<a href="{{ route( 'activities_mass_import_cancel' ) }}">
			<i class="fa fas fa-times"></i>
			{{ __( 'Cancel this mass import' ) }}
		</a>
	</p>
@append
