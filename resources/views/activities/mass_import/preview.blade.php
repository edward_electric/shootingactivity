@extends( 'layouts.default' )

@section( 'h1', __( 'Mass import preview' ) )
@section( 'h1_icon', 'fa-solid fa-file-upload' )

@section( 'content' )

	<table class="info table">
		<caption>{{ __( 'Activity information' ) }}</caption>
		<thead>
			<tr>
				<th>{{ __( 'Date' ) }}</th>
				<th>{{ __( 'Category' ) }}</th>
				<th>{{ __( 'Ammo' ) }}</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="date">
					{{ $model->get( 'datetime' )->toDateTimeString() }}<br/>
					{{ $model->get( 'datetime' )->diffForHumans() }}
				</td>
				<td class="category">{{ app()->site()->categories()->find( $model->get( 'category_id' ) )->name }}</td>
				<td class="ammo">{{ $model->get( 'ammo' ) }}</td>
		</tbody>
	</table>

	<table class="activities table">
		<caption>{{ __( 'Users' ) }}</caption>
		<thead>
			<tr>
				<th>{{ __( 'User' ) }}</th>
				<th>{{ __( 'Gun' ) }}</th>
			</tr>
		</thead>
		<tbody>
			@foreach( $model->collection( 'activities' ) as $activity )
			<tr>
				<td>{{ $activity->user->name }}</td>
				<td class="gun">
				@if( $activity->gun )
					{{ $activity->gun->name }}
				@else
					{{ __( 'User will be asked' ) }}
				@endif
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>

	{!! $form !!}

	<p class="unready_link">
		<a href="{{ route( 'activities_mass_import_unready' ) }}">
			<i class="fa fas fa-edit"></i>
			{{ __( 'Edit the import' ) }}
		</a>
	</p>
@append
