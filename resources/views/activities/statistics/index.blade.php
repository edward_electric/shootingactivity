@extends( 'layouts.default' )

@section( 'h1', __( 'Statistics' ) )
@section( 'h1_icon', 'fa-solid fa-chart-line' )

@section( 'content' )

@include( 'statistics.statistics' )

@include( 'statistics.link', [ 'file_type' => 'pdf', 'file_icon' => 'fa-file-pdf' ] )
@include( 'statistics.link', [ 'file_type' => 'csv', 'file_icon' => 'fa-file' ] )

@append
