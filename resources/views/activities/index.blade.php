@extends( 'layouts.default' )

@if ( isset( $year ) )
	@section( 'h1', __( 'Your activities during :month, :year', [ 'month' => \App\Classes\Statistics\Statistics::months()[ $month ], 'year' => $year ] ) )
@else
	@section( 'h1', __( 'Your activities' ) )
@endif
@section( 'h1_icon', 'fa-solid fa-history' )

@section( 'content' )

@php
	$columns = 2;
	if ( app()->site()->has_categories() )
		$columns++;
@endphp

<div class="row year_selector">
	@foreach( $years as $year )
		<div class="col text-center">
			<a href="{{ route( 'activities_index', [ 'year' => $year ] ) }}">
				{{ $year }}
			</a>
		</div>
	@endforeach
</div>

<table class="centered table table-striped">
	<thead>
		<tr>
			<th>{{ __( 'Date' ) }}</th>
			<th>{{ __( 'Description' ) }}</th>
			<th>{{ __( 'Gun' ) }}</th>
		</tr>
	</thead>
	<tbody>
		@foreach( $models as $model )
			<tr>
				<td class="date">
					<a href="{{ route( 'activities_view', [ 'activity' => $model ] ) }}">
						@include( 'components.table_date_diff_row' )
					</a>
				</td>

				<td class="description">
					@if ( app()->site()->has_categories() )
						{{ $model->category->name }}
					@endif
					@if ( $model->description )
					- {{ $model->get_description() }}
					@endif
				</td>
				<td>
				@if ( app()->user()->has_guns() )
					@if ( $model->ammo > 0 )
						@if ( $model->gun )
							{{ $model->gun->name }} ({{ __( ':ammo rounds', [ 'ammo' => $model->ammo ] ) }})
						@endif
					@endif
				@else
					{{ __( ':ammo rounds', [ 'ammo' => $model->ammo ] ) }}
				@endif
				@if ( app()->site()->has_enabled( 'scoring' ) )
					@if ( $model->strings()->get_sum() > 0 )
						<div class="strings">
							{{ $model->strings()->get_sum() }}
							<span class="divider">/</span>
							{{ $model->strings()->get_average() }}
							@if ( \App\Models\Activities\Strings::scoring_x_enabled() )
								@if ( $model->strings()->get_point_occurrence( 'X' ) > 0 )
									<span class="divider">/</span>
									{{ $model->strings()->get_point_occurrence( 'X' ) }}%
								@endif
							@endif
						</div>
					@endif
				@endif
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

@append
