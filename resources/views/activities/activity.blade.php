@php
	if ( ! isset( $navigation ) )
		$navigation = false;
@endphp
<div class="activity">
	<div class="activity_meta">
		<div class="activity_meta_1 row">
			@if ( $navigation )
			<div class="col">
				<div class="navigation_container row">
				@if ( $model->previous() )
					<div class="navigation previous col order-first">
						<a href="{{ route( 'activities_view', [ 'activity' => $model->previous() ] ) }}" title="{{ __( 'View previous activity' ) }}">
							<button class="btn btn-secondary">
								<i class="fas fa-arrow-left"></i>
							</button>
						</a>
					</div>
				@endif
				@if ( $model->next() )
					<div class="navigation next col order-2 order-md-12">
						<a href="{{ route( 'activities_view', [ 'activity' => $model->next() ] ) }}" title="{{ __( 'View next activity' ) }}">
							<button class="btn btn-secondary">
								<i class="fas fa-arrow-right"></i>
							</button>
						</a>
					</div>
				@endif
				</div>
			</div>
			@endif
		@if ( $model->description != '' )
		<div class="activity_meta_title row">
			<div class="col title text-center">
				<h1>{{ htmlspecialchars_decode( $model->description ) }}</h1>
			</div>
		</div>
		@endif
		<div class="activity_meta_2 row">
			<div class="time col-12 col-md-4 order-3">
				<div class="meta_key time d-none d-md-block">
					<i class="fa fa-fw fa-clock"></i>
				</div>
				<div class="meta_value" title="{{ $model->created_at->timezone( env( 'TIMEZONE', 'UTC' ) ) }}">
					{{ $model->created_at->diffForHumans() }}
				</div>
			</div>
			<div class="gun col-12 col-md-4 order-4">
				@if ( $model->gun )
				<div class="meta_key gun">
					<div class="gun">
					{{ $model->gun->name }} <span class="caliber">{{ $model->gun->caliber->name }}</span>
					</div>
				</div>
				@endif
				<div class="meta_value">
					{{ $model->ammo }} {{ __( 'rounds' ) }}
				</div>
			</div>
			<div class="category col-12 col-md-4 order-5">
				<div class="meta_key category d-none d-md-block">
					<i class="fa fa-fw fa-object-group"></i>
				</div>
				<div class="meta_value">
					{{ $model->category->name }}
				</div>
			</div>
		</div>
	</div>

	@includeWhen( count( $model->strings() ) > 0, 'activities.strings' )

	@includeWhen( $model->is_unmoderated(), 'activities.unmoderated' )

	@includeWhen( $comment = $model->get_meta( 'comment' ), 'activities.comment' )

	@yield( 'activities.activity.before_image' )

	@includeWhen( $model->file, 'image.full', [ 'file' => $model->file ] )

	@includeWhen( count( $model->moderators ) > 0, 'activities.moderators' )
</div>
