@php
	$activity_moderators = \App\Models\Activities\Moderated_Index::where( 'activity_id', $model->id )
		->with( 'user' )
		->orderBy( 'id', 'DESC' )
		->get();
@endphp
<div class="moderators radius callout">
	<p>
		<i class="fa fa-eye"></i>
		@foreach( $activity_moderators as $moderator )
			{{ __( 'Moderated by:' ) }} {{ $moderator->user->name }}, {{ $moderator->created_at->format( 'Y-m-d' ) }}
		@endforeach
	</p>
</div>
