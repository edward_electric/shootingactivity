@extends( 'layouts.default' )

@section( 'h1', __( 'Re-edit activity' ) )
@section( 'h1_icon', 'fa-solid fa-edit' )

@section( 'content' )

<p>
	{{ __( 'Do you wish to re-edit the activity from :ago?', [ 'ago' => $model->created_at->diffForHumans() ] ) }}
</p>

{!! $form !!}

@append
