@php
	$strings = $model->strings();
	$score_analysis = $model->user->get_meta( 'score_analysis' );
	if ( $score_analysis == 'detailed' )
		$medium_size = 6;
	else $medium_size = 12;
@endphp
<div class="row scores">
	<div class="col-12 col-md-{{ $medium_size }}" title="{{ __( 'Your scores' ) }}">
		<table class="text-center table table-striped">
			<caption>{{ __( 'Scores' ) }}</caption>
			<thead class="screen-reader-text">
				<tr>
					<th>{{ __( 'Points' ) }}</th>
					<th>{{ __( 'Sum' ) }}</th>
				</tr>
			</thead>
			<tbody>
		@foreach( $strings as $index => $ignore )
			<tr>
				<td class="shots">
					<span class="each">
						<span class="score">{!! implode( '</span><span class="plus">+</span><span class="score">', $strings->get_sorted_shots( $index, $model->user ) ) !!}</span>
					</span>
				</td>
				<td class="score">
					<span class="sum">{{ $strings->get_score( $index ) }}</span>
				</td>
			</td>
		@endforeach
			</tbody>
		</table>
		@if ( $score_analysis == '' )
			<div class="summary analysis">
				<span class="series" title="{{ __( 'Series fired' ) }}">{{ count( $strings ) }}</span>
				<span class="divider">/</span>
				<span class="sum" title="{{ __( 'Total points' ) }}">{{ $strings->get_sum() }}</span>
				<span class="divider">/</span>
				@if ( $strings->count_shots() > 0 )
					<span class="average_per_shot" title="{{ __( 'Average points per shot' ) }}">{{ round( $strings->get_sum() / $strings->count_shots(), 2 ) }}</span>
					<span class="divider">/</span>
				@endif
				<span class="average" title="{{ __( 'Average score' ) }}">{{ $strings->get_average() }}</span>
			</div>
		@endif
	</div>
		@if ( $score_analysis == 'detailed' )
			<div class="col-12 col-md-6 detailed analysis" title="{{ __( 'Your scores' ) }}">
				<div class="analysis">
					<table class="table table-striped">
						<caption>{{ __( 'Details' ) }}</caption>
						<tbody>
							<tr>
								<th scope="row">{{ __( 'Series fired' ) }}</th>
								<td>{{ count( $strings ) }}</td>
							</tr>
							<tr>
								<th scope="row">{{ __( 'Total points' ) }}</th>
								<td>{{ $strings->get_sum() }}</td>
							</tr>
							@if ( $strings->count_shots() > 0 )
								<tr>
									<th scope="row">{{ __( 'Average points per shot' ) }}</th>
									<td>{{ round( $strings->get_shot_sum() / $strings->count_shots(), 2 ) }}</td>
								</tr>
							@endif
							<tr>
								<th scope="row">{{ __( 'Average score' ) }}</th>
								<td>{{ $strings->get_average() }}</td>
							</tr>
						</tbody>
					</table>
					<table class="text-center table table-striped">
						<caption>{{ __( 'Point distribution' ) }}</caption>
						<thead class="screen-reader-text">
							<tr>
								<th>{{ __( 'Points' ) }}</th>
								<th>{{ __( 'Occurrence' ) }}</th>
							</tr>
						</thead>
						<tbody>
						@foreach( $strings->get_distribution_array() as $points => $occurrence )
							<tr>
								<td>{{ $points }}</td>
								<td>{{ $occurrence[ 'count' ] }} <span class="divider">/</span> {{ $occurrence[ 'percent' ] }}%</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		@endif
</div>
