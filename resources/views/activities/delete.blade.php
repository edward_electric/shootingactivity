@extends( 'layouts.default' )

@section( 'h1', __( 'Delete activity' ) )
@section( 'h1_icon', 'fa-solid fa-times' )

@section( 'content' )

<p>
	{{ __( 'Do you wish to delete the activity from :ago?', [ 'ago' => $model->created_at->diffForHumans() ] ) }}
</p>

{!! $form !!}

@append
