@extends( 'layouts.default' )

@section( 'h1', __( 'Activity from :ago', [ 'ago' => $model->created_at->diffForHumans() ] ) )
@section( 'h1_icon', 'fa-solid fa-file-alt' )

@section( 'content' )

@include( 'activities.activity', [
	'navigation' => true,
	] )

@if ( ! \App\Models\Activities\Activity::has_draft() )
<p class="reedit_link">
	<a href="{{ route( 'activities_reedit', $model ) }}">
		<i class="fas fa-edit"></i>
		{{ __( 'Re-edit the activity' ) }}
	</a>
</p>
@endif

<p class="delete_link">
	<a href="{{ route( 'activities_delete', $model ) }}">
		<i class="fas fa-times"></i>
		{{ __( 'Delete the activity' ) }}
	</a>
</p>

@append
