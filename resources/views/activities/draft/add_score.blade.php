@php
	// Assume a string size of 5 in case the group doesn't have a string size.
	$string_size = ( $model->gun->group ? $model->gun->group->get_meta( 'string_size', 5 ) : 5 );
	$string_score_count = ( $model->gun->group ? $model->gun->group->get_meta( 'string_score_count', 5 ) : 5 );
@endphp

@extends( 'layouts.default' )

@section( 'h1', __( 'Add a new score' ) )
@section( 'h1_icon', 'fa-solid fa-edit' )

@section( 'content' )

	<script type="text/javascript">
		var scoring_settings = {
			'string_size' : {{ $string_size }},
			'string_score_count' : {{ $string_score_count }},
		};
	</script>

	{!! $form !!}

	<div class="row">
		<div class="col">
			<a href="{{ route( 'activities_draft' ) }}">
				<button class="cancel btn btn-secondary">
					<i class="fas fa-arrow-left"></i>
					{{ __( 'Cancel' ) }}
				</button>
			</a>
		</div>
	</div>
@append
