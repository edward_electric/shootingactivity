@extends( 'layouts.default' )

@section( 'h1', __( 'Edit the activity date' ) )
@section( 'h1_icon', 'fa-solid fa-edit' )

@section( 'content' )
	{!! $form !!}
@append
