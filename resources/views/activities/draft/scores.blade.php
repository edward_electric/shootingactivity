<div class="add_a_score">
	<div class="row mb-4">
		<div class="col">
			<div class="text-center">
				<a class="add_a_score btn btn-primary" href="{{ route( 'activities_edit_string', [ 'string_id' => -1 ] ) }}">
					<i class="fa fa-fw fa-crosshairs"></i>
					{{ __( 'Add a score' ) }}
				</a>
			</div>
		</div>
	</div>
</div>

@if ( $model->strings()->count() > 0 )
@php
	$strings = $model->strings();
@endphp

<table class="table table-striped activity centered table_row_icons scores">
	<thead>
		<tr>
			<th>{{ __( 'Score' ) }}</th>
			<th class="actions">{{ __( 'Actions' ) }}</th>
		</tr>
	</thead>
	<tbody>
		@foreach( $strings as $index => $ignore )
		<tr class="scores">
			<td>{!! implode( '<span class="plus">+</span>', $strings->get_shots_and_x( $index ) ) !!} <span class="divider">=</span> {{ $strings->get_score( $index ) }}</td>
			<td class="actions">
				@php
					$tri = app()->table_row_icons( 'score' . $index);
					$tri->add( [
						'action' => 'edit',
						'icon' => 'fa-solid fa-pencil',
						'url' => route( 'activities_edit_string', [ 'string_id' => $index ] ),
						'text' => __( 'Edit this score' ),
					] );
					$tri->add( [
						'action' => 'delete',
						'icon' => 'fa-solid fa-times',
						'url' => route( 'activities_delete_string', [ 'string_id' => $index ] ),
						'text' => __( 'Delete this score' ),
					] );
					$tri->output();
				@endphp
			</td>
		</tr>
		@endforeach
	</tbody>
</table>

@endif

