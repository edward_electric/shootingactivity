@extends( 'layouts.default' )

@section( 'h1', __( 'Activity in progress' ) )
@section( 'h1_icon', 'fa-solid fa-edit' )

@section( 'content' )

@section( 'activities.activity.before_image' )
	@includeWhen ( app()->site()->has_enabled( 'scoring' ), 'activities.draft.scores' )
@append

@include( 'activities.activity' )

@php
	$ai = app()->action_icons( 'activity_view' );
	$ai->small_items = 3;

	$ai->add( [
		'icon' => 'fa-solid fa-edit',
		'text' => __( 'Edit' ),
		'route' => 'activities_draft_edit',
		'sort_order' => 50,
	] )
		->add( [
			'icon' => 'fa-solid fa-times',
			'text' => __( 'Delete' ),
			'route' => 'activities_draft_delete',
			'sort_order' => 25,
		] );

	if ( app()->site()->uses_moderation() )
		$text = __( 'Send to moderators' );
	else
		$text = __( 'Finish editing' );

	$ai->add( [
		'icon' => 'fa-solid fa-check',
		'text' => $text,
		'route' => 'activities_draft_finish',
		'sort_order' => 75,
	] )
		->output();
@endphp

@append
