<div class="is_unmoderated warning callout">
	<p>
		<i class="fa fa-eye fa-fw"></i>
		{{ __( 'This activity is in the moderation queue.' ) }}
	</p>
</div>
