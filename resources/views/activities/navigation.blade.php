@php
	$previous = $model->previous();
	$next = $model->next();
@endphp
<div class="activity navigation grid-x">
	<div class="small-6 previous cell">
		@if ( $previous )
			<a href="{{ route( 'activities_view', [ 'activity' => $previous ] ) }}" title="{{ __( 'View previous activity' ) }}">
				<button class="button secondary">
					<i class="fas fa-arrow-left"></i>
					Previous
				</button>
			</a>
		@endif
	</div>
	<div class="small-6 next cell">
		@if ( $next )
			<a href="{{ route( 'activities_view', [ 'activity' => $next ] ) }}" title="{{ __( 'View next activity' ) }}">
				<button class="button secondary">
					Next
					<i class="fas fa-arrow-right"></i>
				</button>
			</a>
		@endif
	</div>
</div>
