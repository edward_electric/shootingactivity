@extends( 'layouts.default' )

@section( 'h1', __( 'Complete the activity' ) )
@section( 'h1_icon', 'fa-solid fa-question' )

@section( 'content' )

	<p>
		{{ __( 'During the activity from :ago...', [ 'ago' => $model->created_at->diffForHumans() ] ) }}
	</p>

	@if ( $model->get_comment() != '' )
	<blockquote>
		{{ $model->get_comment() }}
	</blockquote>
	@endif

	@if ( ! $model->gun )
	<p>
		{{ __( 'Please select the gun you used during this activity.' ) }}
	</p>
	@endif

	{!! $form !!}

@append
