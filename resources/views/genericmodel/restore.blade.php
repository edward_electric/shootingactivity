@extends( 'layouts.default' )

@section( 'page_id', $model->model_key() . '_restore' )

@section( 'h1', __( 'Restore :singularlabel :modelname?', [ 'singularlabel' => $model->labels()->singular, 'modelname' => $model->name ] ) )

@section( 'content' )
	{!! $form !!}
@append
