@include( 'components.add_a_model', [
	'css_class' => $model->model_key(),
	'label' => __( 'Add a new :singularlabel', [ 'singularlabel' => $model->labels()->singular ] ),
	'url' => route( $model->routes()->add ),
] )
