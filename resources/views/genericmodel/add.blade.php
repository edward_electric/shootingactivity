@extends( 'layouts.default' )

@section( 'page_id', $model->model_key() . '_add' )

@section( 'h1', __( 'Add a new :singularlabel', [ 'singularlabel' => $model->labels()->singular ] ) )

@section( 'content' )
	{!! $form !!}
@append
