@extends( 'layouts.default' )

@section( 'page_id', $model->model_key() . '_archived_index' )

@section( 'h1', __( 'Archived :plurallabel', [ 'plurallabel' => $model->labels()->plural ] ) )

@section( 'content' )

<table class="{{ $model->model_key() }} archived table table-striped table_row_icons">
	<thead>
		<tr>
			<th>{{ __( "Name" ) }}</th>
			@includeIf( $model->view( 'index_table_th' ), [ 'model' => $model ] )
			<th class="actions">{{ __( "Actions" ) }}</th>
		</tr>
	</thead>
	<tbody>
		@foreach( $models as $model )
			<tr>
				<td class="name">
					<a href="{{ $model->route( 'edit' ) }}" title="{{ __( 'Edit this :singularlabel', [ 'singularlabel' => $model->labels()->singular ] ) }}">
						{{ $model->name }}
					</a>
				</td>
				@includeIf( $model->view( 'index_table_td' ), [ 'model' => $model ] )
				<td class="actions">
					@php
						$tri = app()->table_row_icons( $model->id );
						if ( $model->can_be_restored() )
							$tri->add( [
								'action' => 'restore',
								'url' => $model->route( 'restore' ),
								'text' => __( 'Restore this item' ),
							] );
						if ( $model->can_be_deleted() )
							$tri->add( [
								'action' => 'delete',
								'url' => $model->route( 'delete' ),
								'text' => __( 'Delete this item' ),
							] );
						$tri->output();
					@endphp
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

@append
