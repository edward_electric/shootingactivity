@extends( 'layouts.default' )

@section( 'page_id', $model->model_key() . '_edit' )

@section( 'h1', __( 'Editing :singularlabel :modelname', [ 'singularlabel' => $model->labels()->singular, 'modelname' => $model->name ] ) )

@section( 'content' )
	{!! $form !!}
@append
