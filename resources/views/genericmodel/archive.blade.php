@extends( 'layouts.default' )

@section( 'page_id', $model->model_key() . '_archive' )

@section( 'h1', __( 'Archive :singularlabel :modelname?', [ 'singularlabel' => $model->labels()->singular, 'modelname' => $model->name ] ) )

@section( 'content' )
	{!! $form !!}
@append
