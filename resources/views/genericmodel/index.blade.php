@extends( 'layouts.default' )

@section( 'page_id', $model->model_key() . '_index' )

@section( 'h1', $model->labels()->Plural )
@section( 'h1_icon', $model->icon() )

@section( 'content' )

@if ( count( $models ) > 0 )
	<table class="{{ $model->model_key() }} active table table-striped table_row_icons">
		<thead>
			<tr>
				<th class="name">{{ __( "Name" ) }}</th>
				@includeIf( $model->view( 'index_table_th' ), [ 'model' => $model ] )
				<th class="actions">{{ __( "Actions" ) }}</th>
			</tr>
		</thead>
		<tbody>
			@foreach( $models as $model )
				<tr>
					<td class="name">
						<a href="{{ $model->route( 'edit' ) }}" title="{{ __( 'Edit this :singularlabel', [ 'singularlabel' => $model->labels()->singular ] ) }}">
							{{ $model->name }}
						</a>
						@includeIf( $model->view( 'index_table_after_name' ), [ 'model' => $model ] )
					</td>
					@includeIf( $model->view( 'index_table_td' ), [ 'model' => $model ] )
					<td class="actions">
						@php
							$tri = app()->table_row_icons( $model->id );
						@endphp
							@includeIf( $model->view( 'index_actions' ), [ 'model' => $model ] )
						@php
							if ( $model->can_be_edited() )
								$tri->add( [
									'action' => 'edit',
									'url' => $model->route( 'edit' ),
								] );
							if ( $model->can_be_archived() )
								$tri->add( [
									'action' => 'archive',
									'icon' => 'fa-solid fa-trash',
									'url' => $model->route( 'archive' ),
									'text' => __( 'Trash this item' ),
								] );
							if ( $model->can_be_deleted() )
								$tri->add( [
									'action' => 'delete',
									'icon' => 'fa-solid fa-times',
									'url' => $model->route( 'delete' ),
									'text' => __( 'Delete this item' ),
								] );
							$tri->output();
						@endphp
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	<p class="text-center">{{ __( "There are no :plurallabel.", [ 'plurallabel' => $model->labels()->plural ] ) }}</p>
@endif

@include( $model->views()->add_a_model )

@if ( $archived > 0 )
<div class="archived container">
	<p>
	{!! sprintf( __( 'You have %sarchived :plurallabel%s.', [ 'plurallabel' => $model->labels()->plural ] ), '<a href="' . route( $model->routes()->archived ) . '">', '</a>' ) !!}
	</p>
</div>
@endif

@append
