@php
	$v = $vars->collection( 'table_date_diff_row' );

	$newdate = [];

	if ( $model->created_at->year != $v->get( 'year', '' ) )
	{
		if ( $model->created_at->year != date( 'Y' ) )
			$newdate []= $model->created_at->year;
		$v->set( 'year', $model->created_at->year );
	}
	if ( $model->created_at->day != $v->get( 'day', '' ) )
	{
		$newdate []= $model->created_at->localeMonth;
		$newdate []= $model->created_at->day;
		$v->set( 'day', $model->created_at->day );
		$diff = $model->created_at->timezone( env( 'TIMEZONE', 'UTC' ) )->diffForHumans();
	}
	else
		$diff = '';
	$newdate []= $model->created_at->timezone( env( 'TIMEZONE', 'UTC' ) )->format( 'H:i' );
@endphp
@if ( count( $newdate ) > 0 )
	<div class="date">
		{{ implode( ' ', $newdate ) }}
	</div>
	@if ( $diff )
		<div class="diff">
			{{ $model->created_at->diffForHumans() }}
		</div>
	@endif
@endif
