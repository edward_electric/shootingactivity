<div class="alert alert-{{ $type }}">
	<div class="message">
		@if ( $type == 'alert' )
			<i class="fa fa-2x fa-fw fa-thumbs-down"></i>
		@endif
		@if ( $type == 'success' )
			<i class="fa fa-2x fa-fw fa-thumbs-up"></i>
		@endif
		{!! $message !!}
	</div>
</div>
