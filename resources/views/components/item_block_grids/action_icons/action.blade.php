<div class="action {{ preg_replace( '/fa[-\s]/', '', $icon ) }} text-center col-6 col-md-4 mb-4 text-center">
	@if ( $url != '' )
		<a href="{{ $url }}">
	@endif
	@if ( $icon != '' )
		<i class="fa-4x {{ $icon }}"></i>
	@endif
	@if ( $text != '' )
		<div class="text">
			{{ $text }}
		</div>
	@endif
	@if ( $html != '' )
		<div class="text">
			{!! $html !!}
		</div>
	@endif
	@if ( $url != '' )
		</a>
	@endif
	@if ( isset( $subtitle ) )
		<div class="text-muted">
			{{ $subtitle }}
		</div>
	@endif
</div>
