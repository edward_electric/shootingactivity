<div class="action {{ $action }} col text-center">
	<a href="{{ $url }}">
		<i class="fa-2x {{ $icon }}" title="{{ $text }}"></i>
		<p class="screen-reader-text">{{ $text }}</p>
	</a>
</div>
