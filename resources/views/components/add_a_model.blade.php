<div class="row">
	<div class="col text-center">
		<a href="{{ $url }}">
			<div class="add_a_model {{ $css_class }} btn btn-primary">
				<i class="fa fa-fw fa-plus"></i>
				{{ $label }}
			</div>
		</a>
	</div>
</div>
