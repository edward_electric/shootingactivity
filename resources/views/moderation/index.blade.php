@extends( 'layouts.default' )

@section( 'h1', __( 'Moderation overview' ) )
@section( 'h1_icon', 'fa-solid fa-eye' )

@section( 'content' )

<table class="table table-striped centered">
	<thead>
		<tr>
			<th class="date">{{ __( 'Date' ) }}</th>
			@if ( app()->site()->uses_images() )
				<th class="image">{{ __( 'Image' ) }}</th>
			@endif
			<th class="info">{{ __( 'Info' ) }}</th>
			<th class="hidden choice">{{ __( 'Decision' ) }}</th>
		</tr>
	</thead>
	<tbody>
		@foreach( $models as $model )
			<tr data-model_id="{{ $model->id }}">
				<td class="date">
					<a href="{{ route( 'moderation_view', [ 'activity' => $model ] ) }}" title="{{ __( 'Moderate only this activity' ) }}">
						@include( 'components.table_date_diff_row' )
					</a>
				</td>
				@if ( app()->site()->uses_images() )
					<td class="image">
						@includeWhen( $model->file, 'image.thumbnail', [ 'file' => $model->file ] )
						@includeWhen( $model->gun->file, 'image.gun_inset', [ 'file' => $model->gun->file ] )
					</td>
				@endif
				<td class="info">
					<ul class="info">
						<li>{{ $model->user->name }}</li>
						@if ( app()->site()->has_categories() )
							<li>{{ $model->category->name }}</li>
						@endif
						<li>
							{{ $model->gun->name }}
						</li>
						<li>
							{{ __( ':ammo rounds', [ 'ammo' => $model->ammo ] ) }}
						</li>
					</ul>
				</td>
				<td class="hhidden choice text-center">
					<div class="accept">
						<i class="fa fa-3x fa-check"></i>
					</div>
					<div class="reject">
						<i class="fa fa-3x fa-times"></i>
					</div>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $form !!}

@append
