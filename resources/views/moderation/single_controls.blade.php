@php
	$ai = app()->action_icons( 'moderation' );
	$ai->small_items = 3;
	$ai->add( [
		'icon' => 'fa-solid fa-times',
		'sort_order' => 25,
		'text' => __( 'Reject' ),
		'url' => route( 'moderation_reject', [ 'activity' => $model ] ),
	] );
	if ( $model->gun->file )
		$ai->add( [
			'html' => view( 'image.thumbnail', [ 'file' => $model->gun->file ] ),
		] );
	$ai->add( [
		'icon' => 'fa-solid fa-check',
		'sort_order' => 75,
		'text' => __( 'Accept' ),
		'url' => route( 'moderation_accept', [ 'activity' => $model ] ),
	] );
	$ai->output();
@endphp
