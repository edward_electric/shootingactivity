@extends( 'layouts.default' )

@section( 'h1', $model->user->name )
@section( 'h1_icon', 'fa-solid fa-eye' )

@section( 'content' )

@include( 'activities.activity' )

@include( 'moderation.single_controls' )

<p class="edit_link text-center">
	<a href="{{ route( 'moderation_edit', [ $model ] ) }}">
		<i class="fas fa-edit"></i>
		{{ __( 'Edit this activity' ) }}
	</a>
</p>
@append
