@extends( 'layouts.default' )

@section( 'h1', $model->user->name )
@section( 'h1_icon', 'fa-solid fa-edit' )

@section( 'content' )

<p>{{ __( "Use this form to slightly modify the user's activity if you see an obvious mistake." ) }}</p>

{!! $form !!}

@append
