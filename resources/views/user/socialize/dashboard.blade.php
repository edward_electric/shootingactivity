<div class="socialize">

<h2>
	{{ __( 'Socialize' ) }}
</h2>
@php
	$ai = app()->action_icons( 'socialize' );

	if ( app()->site()->has_enabled( 'notice_board' ) )
	{
		$data =[
			'icon' => 'fa-solid fa-comments',
			'route' => 'notices_index',
			'text' => __( 'Notice board' ),
		];
		if ( app()->site()->notices->last() )
			$data[ 'subtitle' ] = __( ':lastcommenttime by :username', [ 'lastcommenttime' => app()->site()->notices->last()->comment->last_comment()->updated_at->diffForHumans(), 'username' => app()->site()->notices->last()->comment->last_comment()->user->name ] );
		$ai->add( $data );
	}

	$ai->add( [
		'icon' => 'fa-solid fa-users',
		'text' => __( 'Users' ),
		'route' => 'user_profiles_index',
	] );

	$ai->output();
@endphp

</div>
