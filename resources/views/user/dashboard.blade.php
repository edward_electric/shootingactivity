@extends( 'layouts.default' )

@section( 'h1', config( 'app.name' ) )
@section( 'h1_icon', 'fa-solid fa-home' )

@section( 'content' )

	@if ( app()->user()->may( 'activities' ) )
		@if ( count( Auth::user()->guns ) < 1 )
			@include( 'user.no_guns' )
		@else
			@include( 'user.activities' )
		@endif
	@endif

	@if ( app()->site()->has_enabled( 'socialization' ) )
		@includeWhen ( app()->user()->may( 'socialize' ), 'user.socialize.dashboard' )
	@endif

	@includeWhen ( app()->user()->may( 'moderate_activities' ), 'user.moderation.dashboard' )

	@include( 'user.control_panel' )

	@includeWhen ( app()->user()->may( 'access_admin' ), 'admin.index' )

	@includeWhen ( app()->user()->may( 'manage_sites' ), 'admin.network.index' )

@append
