@if ( Auth::user()->may( 'manage_network' ) )
	@php
	$tri->add( [
		'action' => 'login',
		'url' => route( 'users_login_as', [ 'user' => $model->id ] ),
		'text' => __( 'Sign in as this user' ),
	] );
	@endphp
@endif
@php
$tri->add( [
	'action' => 'statistics',
	'icon' => 'fa-solid fa-chart-line',
	'url' => route( 'admin_user_statistics', [ 'user' => $model->id ] ),
	'text' => __( "View user's statistics" ),
] );
@endphp
