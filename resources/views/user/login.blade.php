@extends( 'layouts.default' )

@section( 'h1', __( 'Log in to :sitename', [ 'sitename' => config( 'app.name' ) ] ) )

@section( 'content' )
	@include( 'parts.logo' )
	{!! $form !!}
@append
