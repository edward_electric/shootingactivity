<div class="activities">

<h2>
	{{ __( 'Activities' ) }}
</h2>
@php
	$ai = app()->action_icons( 'activities' );
	if ( $has_draft )
		$ai->add( [
			'icon' => 'fa-solid fa-edit',
			'text' => __( 'Continue editing activity' ),
			'route' => 'activities_draft',
			'sort_order' => 25,
		] );
	else
		$ai->add( [
				'icon' => 'fa-solid fa-plus',
				'text' => __( 'Create a new activity' ),
				'route' => 'activities_draft_edit',
				'sort_order' => 25,
		] );

	if ( app()->user()->may( 'moderate_activities' ) )
		if ( app()->site()->get_meta( 'allow_mass_import' ) )
			$ai->add( [
				'icon' => 'fa-solid fa-file-upload',
				'text' => __( 'Mass import' ),
				'route' => 'activities_mass_import_edit',
			] );

	if ( \App\Models\Activities\Activity::get_incomplete_activities( app()->user() )->count() > 0 )
		$ai->add( [
			'icon' => 'fa-solid fa-question',
			'text' => __( 'Incomplete activities' ),
			'route' => 'activities_complete',
		] );
	$ai->add( [
		'icon' => 'fa-solid fa-history',
		'text' => __( 'Previous activities' ),
		'route' => 'activities_index',
	] );
	$ai->add( [
		'icon' => 'fa-solid fa-chart-line',
		'text' => __( 'Statistics' ),
		'route' => 'activities_statistics',
	] );
	$ai->output();
@endphp

</div>