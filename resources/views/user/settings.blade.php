@extends( 'layouts.default' )

@section( 'h1', __( 'Your settings' ) )
@section( 'page_icon', 'cog' )

@section( 'content' )
	{!! $form !!}
@append
