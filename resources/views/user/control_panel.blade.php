<h2>
	{{ __( 'Account settings' ) }}
</h2>

@php
	$ai = app()->action_icons( 'control_panel' );

	$ai->add( [
		'icon' => 'fa-solid fa-cog',
		'text' =>  __( "Settings" ),
		'route' => 'user_settings',
	] );

	if ( app()->user()->may( 'activities' ) )
		$ai->add( [
			'icon' => 'fa-solid fa-gun',
			'text' =>  __( "Manage my guns" ),
			'route' => 'guns_index',
		] );

	if ( app()->user()->may( 'change_password' ) )
		$ai->add( [
			'icon' => 'fa-solid fa-key',
			'text' =>  __( "Password" ),
			'route' => 'user_password',
		] );

	$ai->output();
@endphp
