@extends( 'layouts.default' )

@section( 'h1', __( 'Password reset link' ) )
@section( 'h1_icon', 'fa-solid fa-key' )

@section( 'content' )
	@parent

	<p>{{ __( 'Use the form below if you have forgotten your password. A password reset link will be sent to your e-mail address.' ) }}</p>

	{!! $form !!}

@endsection
