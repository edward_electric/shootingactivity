@extends( 'layouts.default' )

@section( 'h1', __( 'Reset your password' ) )
@section( 'h1_icon', 'fa-solid fa-key' )

@section( 'content' )
	@parent

	{!! $form !!}

@endsection
