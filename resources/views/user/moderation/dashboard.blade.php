@if ( $unmoderated && count( $unmoderated ) > 0 )
	<h2>
		{{ __( 'Moderation' ) }}
	</h2>
	<div class="grid-x unmoderated">
		<div class="small-12 medium-4 cell">
		@php
			app()->action_icons( 'moderation' )->add( [
				'icon' => 'fa-solid fa-eye',
				'text' => __( 'Unmoderated:' ) . ' ' . count( $unmoderated ),
				'route' => 'moderation_index',
			] )
			->output();
		@endphp
		</div>
		<div class="small-12 medium-8 cell">
			<ul class="unmoderated activities">
				@foreach( $unmoderated->take( 5 ) as $activity )
					<li class="activity"><a href="{{ route( 'moderation_view', [ 'activity' => $activity->id ] ) }}">{{ $activity->user->name }}</a> <span class="datetime">{{ $activity->created_at->diffForHumans() }}</spanm></li>
				@endforeach
			</ul>
		</div>
	</div>
@endif
