<p class="text-center">
	{{ __( 'To being using Shooting Activity, you must first add a gun.' ) }}
</p>

@include( \App\Models\Guns\Gun::view( 'add_a_model' ), [ 'model' => new \App\Models\Guns\Gun() ] )
