<style>
	:root
	{
		@foreach ( app()->site()->theme()->get_defaults() as $key => $value )
			--{{ $key }} : {{ app()->site()->theme()->get( $key ) }};
		@endforeach
	}

	{{ app()->site()->theme()->get( 'theme_css' ) }}
</style>
