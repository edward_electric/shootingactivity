@php
	// This only works if the user is logged in.
	if ( Auth::user() )
		$app_name = app()->site()->name;
	else
		$app_name = config( 'app.name' );
@endphp
@hasSection( 'h1' )
	@yield( 'h1' ) | {{ $app_name }}
@else
	{{ $app_name }}
@endif
