@php
	$route = Route::currentRouteName() . '_' . $file_type;
	$route_options = [ 'year' => $statistics->year ];
	if ( Route::getCurrentRoute()->hasParameter( 'user' ) )
		$route_options[ 'user' ] = Route::getCurrentRoute()->parameters[ 'user' ]->id;
@endphp
<div class="{{ $file_type }}_link">
	<a href="{{ route( $route, $route_options ) }}">
		<i class="far {{ $file_icon }}"></i>
		{{ strtoupper( $file_type) }}
	</a>
</div>
