<div class="statistics">
<h2>
	{{ __( 'Totals' ) }}
</h2>

@php
	$urlopts = [];
	[
		'year' => $statistics->year,
	];
@endphp

<table class="totals centered table table-striped">

	<caption>
		{{ $statistics->table_name }}
	</caption>
	<thead>
		<tr>
			<th>{{ __( 'Year' ) }}</th>
			<th>{{ __( 'Activities' ) }}</th>
			<th>{{ __( 'Rounds' ) }}</th>
			@if ( $statistics->scoring )
				<th>{{ __( 'Series' ) }}</th>
				<th>{{ __( 'Score average' ) }}</th>
			@endif
			@if ( $statistics->new_users )
				<th>{{ __( 'New users' ) }}</th>
				<th>{{ __( 'Total users' ) }}</th>
			@endif
		</tr>
	</thead>
	<tbody>
		<tr>
			<th scope="row">
				{{ __( 'Sum' ) }}
			</th>
			<td>{{ $statistics->get( 'activities' ) }}</td>
			<td>{{ $statistics->get( 'ammo' ) }}</td>
			@if ( $statistics->scoring )
				<td>{{ $statistics->get( 'scores_count' ) }}</td>
				<td>{{ $statistics->get( 'scores_average' ) }}</td>
			@endif
			@if ( $statistics->new_users )
				<td>{{ $statistics->get( 'new_users' ) }}</td>
				<td>{{ $statistics->get( 'total_users' ) }}</td>
			@endif
		</tr>
		@foreach( $statistics->collection( 'years' ) as $year => $year_c )
			@php
				$urlopts[ 'year' ] = $year;
			@endphp
			<tr>
				<th scope="row">
					<a href="{{ route( 'activities_statistics', [ 'year' => $year ] ) }}" title="{{ __( 'Show only activities from :year', [ 'year' => $year ] ) }}">
						{{ $year }}
					</a>
				</th>
				<td>{{ $year_c->get( 'activities' ) }}</td>
				<td>{{ $year_c->get( 'ammo' ) }}</td>
				@if ( $statistics->scoring )
					<td>{{ $year_c->get( 'scores_count' ) }}</td>
					<td>{{ $year_c->get( 'scores_average' ) }}</td>
				@endif
				@if ( $statistics->new_users )
					<td>{{ $year_c->get( 'new_users' ) }}</td>
					<td>{{ $year_c->get( 'total_users' ) }}</td>
				@endif
			</tr>
		@endforeach
	</tbody>
</table>

<h2 class="year">
	{{ $statistics->year }}
</h2>

@php
	$year_c = $statistics->collection( 'years' )->collection( $statistics->year );
	$urlopts[ 'year' ] = $statistics->year;
@endphp

<table class="per_year centered table table-striped table-hover">
	<caption>
		{{ __( 'Monthly details' ) }}
	</caption>
	<thead>
		<tr>
			<th>{{ __( 'Month' ) }}</th>
			<th>{{ __( 'Activities' ) }}</th>
			<th>{{ __( 'Rounds' ) }}</th>
			@if ( $statistics->scoring )
				<th>{{ __( 'Series' ) }}</th>
				<th>{{ __( 'Score average' ) }}</th>
			@endif
			@if ( $statistics->new_users )
				<th>{{ __( 'New users' ) }}</th>
			@endif
		</tr>
	</thead>
	<tbody>
		@foreach( $statistics::months() as $month_index => $label )
		@php
			$temp_opts = $urlopts;
			$temp_opts[ 'month' ] = $month_index;
			$activity_count = $year_c->collection( 'months' )->collection( $month_index )->get( 'activities' );
			$has_activities = $activity_count > 0;
		@endphp
		<tr>
			<th scope="row">
				@if ( $has_activities )
					<a href="{{ route_params( 'activities_index', $temp_opts ) }}" title="{{ __( 'Show only activities from :label, :year', [ 'label' => $label, 'year' => $statistics->year ] ) }}">
				@endif
				{{ $label }}
				@if ( $has_activities )
					</a>
				@endif
			</th>
			<td>
				{{ $activity_count }}
			</td>
			<td>{{ $year_c->collection( 'months' )->collection( $month_index )->get( 'ammo' ) }}</td>
			@if ( $statistics->scoring )
				<td>{{ $year_c->collection( 'months' )->collection( $month_index )->get( 'scores_count' ) }}</td>
				<td>{{ $year_c->collection( 'months' )->collection( $month_index )->get( 'scores_average' ) }}</td>
			@endif
			@if ( $statistics->new_users )
				<td>{{ $year_c->collection( 'months' )->collection( $month_index )->get( 'new_users' ) }}</td>
			@endif
		</tr>
		@endforeach
		<tr>
			<th scope="row">
				<a href="{{ route_params( 'activities_index', $urlopts ) }}" title="{{ __( 'Show only activities from :label, :year', [ 'label' => $label, 'year' => $statistics->year ] ) }}">
				{{ __( 'Sum' ) }}
				</a>
			</th>
			<td>{{ $year_c->get( 'activities' ) }}</td>
			<td>{{ $year_c->get( 'ammo' ) }}</td>
			@if ( $statistics->scoring )
				<td>{{ $year_c->get( 'scores_count' ) }}</td>
				<td>{{ $year_c->get( 'scores_average' ) }}</td>
			@endif
			@if ( $statistics->new_users )
				<td>{{ $year_c->get( 'new_users' ) }}</td>
			@endif
		</tr>
	</tbody>
</table>

@foreach( $year_c->collection( 'related_items' ) as $related_item_slug => $related_items_c )
	<h3 class="related_items">
		{{ $statistics->collection( 'related_item_labels' )->get( $related_item_slug ) }}
	</h3>

	<div class="row">
	@foreach( $related_items_c as $item_id => $item )
		<div class="col col-md-6">
			<table class="totals centered table table-striped">
				<caption>
					{{ $item->get( 'name' ) }}
				</caption>
				<thead>
					<tr>
						<th>{{ __( 'Month' ) }}</th>
						<th>{{ __( 'Activities' ) }}</th>
						<th>{{ __( 'Rounds' ) }}</th>
						@if ( $statistics->scoring )
							<th>{{ __( 'Series' ) }}</th>
							<th>{{ __( 'Score average' ) }}</th>
						@endif
					</tr>
				</thead>
				<tbody>
					@foreach( $statistics::months() as $month_index => $month_label )
					@php
						$item_c = $year_c->collection( 'months' )->collection( $month_index )->collection( 'related_items' )->collection( $related_item_slug )->collection( $item_id );
						$temp_opts = $urlopts;
						$temp_opts[ 'month' ] = $month_index;
						$temp_opts[ $related_item_slug ] = $item_id;
						$has_activities = $item_c->get( 'activities' ) > 0;
					@endphp
					<tr>
						<th>
							@if ( $has_activities )
								<a href="{{ route_params( 'activities_index', $temp_opts ) }}">
							@endif
							{{ $month_label }}
							@if ( $has_activities )
								</a>
							@endif
						</th>
						<td>
							{{ $item_c->get( 'activities' ) }}
						</td>
						<td>{{ $item_c->get( 'ammo' ) }}</td>
						@if ( $statistics->scoring )
							<td>{{ $item_c->get( 'scores_count' ) }}</td>
							<td>{{ $item_c->get( 'scores_average' ) }}</td>
						@endif
					</tr>
					@endforeach
					<tr>
						<th>
							@php
								unset( $temp_opts[ 'month' ] );
							@endphp
							<a href="{{ route_params( 'activities_index', $temp_opts ) }}">
							{{ __( 'Sum' ) }}
							</a>
						</th>
						<td>{{ $year_c->collection( 'related_items' )->collection( $related_item_slug )->collection( $item_id )->get( 'activities' ) }}</td>
						<td>{{ $year_c->collection( 'related_items' )->collection( $related_item_slug )->collection( $item_id )->get( 'ammo' ) }}</td>
						@if ( $statistics->scoring )
							<td>{{ $year_c->collection( 'related_items' )->collection( $related_item_slug )->collection( $item_id )->get( 'scores_count' ) }}</td>
							<td>{{ $year_c->collection( 'related_items' )->collection( $related_item_slug )->collection( $item_id )->get( 'scores_average' ) }}</td>
						@endif
					</tr>
				</tbody>
			</table>
		</div>
	@endforeach
	</div>
@endforeach

</div>
