@if ( app()->site()->uses_images() )
	<td class="image">
	@if ( $model->file )
		@include( 'image.thumbnail', [ 'file' => $model->file ] )
	@endif
	</td>
@endif
