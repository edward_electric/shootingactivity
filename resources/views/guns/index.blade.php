@extends( 'genericmodel.index' )

@section( 'content' )

@if ( count( $models ) > 0 )
	<p class="text-center">
		{{ __( 'You now have a gun and may start creating new activities.' ) }} &rarr;
		<a href="{{ route( 'user_dashboard' ) }}"><i class="fa fa-home"></i> {{ __( 'Dashboard' ) }}</a>
	</p>
@endif

@append
