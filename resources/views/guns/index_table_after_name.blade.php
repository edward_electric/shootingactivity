<div class="caliber_and_group">
@if ( $model->caliber )
<span class="caliber">
	{{ $model->caliber->name }}
</span>
@endif
@if ( $model->group )
<span class="separator">,</span>
<span class="group">
	{{ $model->group->name }}
</span>
@endif
</div>
