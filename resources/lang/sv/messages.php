<?php

return
[
	'gun.group.Black_powder' => 'Svartkrut',
	'gun.group.string_score_count.description' => 'Hur många av de högsta poängen att räkna i en serie. Svartkrut bör ange 10.',
	'gun.group.string_score_count.label' => 'Poängantal',
	'gun.group.string_size.description' => 'Hur många skott att visa när man anger poäng i serien. Svartkrut bör ange 13.',
	'gun.group.string_size.label' => 'Antal skott per serie',
	'user.last_seen' => 'Senast inloggad',
];
