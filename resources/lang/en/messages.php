<?php

return
[
	'gun.group.Black_powder' => 'Black powder',
	'gun.group.string_score_count.description' => 'How many of the highest scores in a string to summarize. Black powder should use 10.',
	'gun.group.string_score_count.label' => 'String score count',
	'gun.group.string_size.description' => 'For scoring, how many rounds to show in a string. Black powder should use 13.',
	'gun.group.string_size.label' => 'String size',
	'user.last_seen' => 'Last seen',
];
