cp .env .env.temp
cp .env.dusk .env
sudo chmod 666 .env*

sudo find storage/app -type d -exec chmod 777 {} \;
sudo find storage/app -type f -exec chmod 666 {} \;

# The old file is used to simulate an old image being uploaded to a new activity.
cp tests/files/rugermk4.jpg tests/files/old.jpg

php artisan dusk $@

rm tests/files/old.jpg

mv .env.temp .env
sudo chmod 666 .env*
