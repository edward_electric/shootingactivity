<?php

/**
	@brief		Return a route with optional query parameters.
	@since		2020-02-04 15:05:31
**/
function route_params( $route, $route_options, $params = false )
{
	if ( ! $params )
	{
		$params = $route_options;
		$route_options = [];
	}
	return route( $route, $route_options ) . '?' . http_build_query( $params );
}